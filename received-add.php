<?php

include_once 'include/config.php';
include_once 'include/admin-functions.php';
include_once 'include/classes/CSRF.class.php';
$admin = new AdminFunctions();

if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: index.php");
	exit();
}
$pageName = "Received(Income)";
$pageURL = 'received-add.php';
$parentPageURL = 'received.php';
$senPageURL='compose-mail.php';
$deleteURL = 'received-add.php';


$item_details = $admin->getActiveItemDetails(); 
$getIncomeHeadDetails = $admin->getHeadDetails(0);
$getActiveCustomerDetails = $admin->getActiveCustomerDetails();

// $item_id=$_POST['item_id'];
// $item_details1111 = $admin->getUniqueItemMasterById($item_id);
// $itemRate=$admin->getActiveItemRateDetails($item_details1111['rate']);


 
$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);


if(isset($_POST['register'])){
	if($csrf->check_valid('post')) {
		$result = $admin->addRecevied($_POST,$loggedInUserDetailsArr['id']);
		header("location:".$parentPageURL."?registersuccess&id=".$result);
	  exit();
	}
}
if(isset($_POST['register_send'])){
	if($csrf->check_valid('post')) {
		$result = $admin->addRecevied($_POST,$loggedInUserDetailsArr['id']);
		header("location:".$senPageURL."?registersuccess&id=".$result);
	exit();
	}
}
if(isset($_GET['edit'])){
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
	$data = $admin->getUniqueReceviedNoById($id);
}

if(isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['update'])) {
	if($csrf->check_valid('post')) {
		$id = trim($admin->escape_string($admin->strip_all($_POST['id'])));
		$result = $admin->updateRecevied($_POST, $loggedInUserDetailsArr['id']);
		header("location:".$parentPageURL."?updatesuccess&id=".$result);
		exit();
  }
}

if(isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['update_send'])) {
  if($csrf->check_valid('post')) {
    $id = trim($admin->escape_string($admin->strip_all($_POST['id'])));
    $result = $admin->updateRecevied($_POST, $loggedInUserDetailsArr['id']);
    header("location:".$senPageURL."?updatesuccess&id=".$id);
    exit();
  }
}
if(isset($_GET['edit'])){
  $getUniqueReceivedDetailsById = $admin->getUniqueReceivedDetailsById($_GET['id']);
 // $getUniqueTaxInvoiceNoByCustomerID=$admin->getUniqueTaxInvoiceNoByCustomerID($_GET['id']);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title> Unique Invoice | <?php echo $pageName; ?></title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Select2 -->

  <link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  
  <script src="../../plugins/notiflix/notiflix-aio-1.5.0.min.js"></script>
</head>
<body class="hold-transition sidebar-mini layout-footer-fixed sidebar-collapse" >
<!-- Site wrapper -->
<div class="wrapper">
 
  <?php 
  include('include/header.php');
  include('include/sidebar.php');
  ?>
<style>
    label{
        font-size:11px;
        padding:0px;
        margin:0px;
    }
    .form-control{
       border:1px solid #48544b;

    }
    .dataTables_wrapper {
    font-size: 14px;
}
em{
  color:red;
}
.mytable{
    font-size:14px;
    text-align:center;
}

.table td, .table th{
  padding: .13rem;
  border:1px solid #000;
}
.mytable td input{
    /* padding:1px; */
    text-align:center;
}

.mytable thead tr td{
    background:#ddd;
}
.select2-container .select2-selection--single {
            height: 25px;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            top: 31%;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 25px;
        }
        
    </style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
<br>
    <!-- Main content -->

    <section class="content" style="zoom: 90%;">
    
      <form action="" id="form" method="post" enctype="multipart/form-data" autocomplete="off">
      <!-- Default box -->
   <div id="cardeffect" style="dispaly:none">
      <div class="card" >
        <div class="card-header">
          <h3 class="card-title"> <?php echo $pageName; ?></h3>
          <a href="<?php echo $parentPageURL; ?>" align="right" style="font-size:14px;color:red;float:right;"> <i class="fas fa-hand-point-left"></i> Back</a>
        </div>
        <div class="card-body" style="padding-top:5px;padding-bottom:5px;">
          <div class="form-group row">
            <div class="col-md-2 fromerrorcheck">
                <label>Received No<em>*</em> </label>
                <input type="text" name="recevied_no" value="<?php if(isset($_GET['edit'])) { echo $data['recevied_no']; }else{ echo $admin-> getUniuqeReceviedNo(); } ?>" class="form-control form-control-sm rounded-0" readonly>
            </div> 
            <div class="col-md-2 fromerrorcheck">
              <label>Received Date<em>*</em> </label>
              <input type="date" name="received_date" id="received_date" value="<?php if(isset($_GET['edit'])) { echo $data['received_date']; }else{ echo date("Y-m-d"); } ?>" class="form-control form-control-sm rounded-0">
            </div>
            <div class="col-md-3 fromerrorcheck">
              <label>Account Name<em>*</em> </label>
              <select class="form-control form-control-sm rounded-0" name="account_head_id" id="account_head_id" required>
              <option value="">Select Account Name</option>
                  <?php while($row = $admin->fetch($getIncomeHeadDetails)){ ?>
                  <option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) and $data['account_head_id']==$row['id']) { echo 'selected'; } ?>><?php echo $row['description']; ?>
                  <?php } ?>
              </select>
            </div>
            <div class="col-md-2 fromerrorcheck">
              <label>Customer Name<em>*</em> </label>
              <select class="form-control form-control-sm rounded-0 "  name="customer_id" id="customer_id">
              <option value="">Select Customer Name</option>
                <?php while($row = $admin->fetch($getActiveCustomerDetails)){ ?>
                  <option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) and $data['customer_id']==$row['id']) { echo 'selected'; } ?>><?php echo $row['customer_name']; ?>
                  <?php } ?>
              </select>
            </div> 
            
            <div class="col-md-2 fromerrorcheck">
              <label>Cutomer Ledgers<em>*</em> </label>
              <input type="text" name="ledger_id" id="ledger_id" value="<?php if(isset($_GET['edit'])) { echo $data['ledger_id']; }?>" class="form-control form-control-sm rounded-0">
            </div>  
           
            <div class="col-md-2 fromerrorcheck" style="display:none">
              <label>Bank Name<em>*</em> </label>
              <input type="text" name="bank_name" id="bank_name" value="<?php if(isset($_GET['edit'])) { echo $data['bank_name']; }?>" class="form-control form-control-sm rounded-0">
            </div>
          </div>
          <div class="form-group row">
          <div class="col-md-2 fromerrorcheck" style="display:none">
              <label>Transaction No<em>*</em> </label>
              <input type="text" name="transaction_no" id="transaction_no" value="<?php if(isset($_GET['edit'])) { echo $data['transaction_no']; }?>" class="form-control form-control-sm rounded-0">
            </div>
            <div class="col-md-2 fromerrorcheck">
              <label>Received Mode<em>*</em> </label>
              <select class="form-control form-control-sm rounded-0" name="payment_mode" onchange="calc()" id="payment_mode">
                <option value="Cash" <?php if(isset($_GET['edit']) and $data['payment_mode']=='Cash') { echo 'selected'; } ?>>Cash</option>
                <option value="NEFT" <?php if(isset($_GET['edit']) and $data['payment_mode']=='Cheque') { echo 'selected'; } ?>>NEFT</option>
                <option value="RTGS" <?php if(isset($_GET['edit']) and $data['payment_mode']=='Bank Transfer') { echo 'selected'; } ?>>RTGS</option>
                <option value="Cheque" <?php if(isset($_GET['edit']) and $data['payment_mode']=='"Credit Card') { echo 'selected'; } ?>>Cheque</option>
              </select> 
            </div>
            <div class="col-md-2 fromerrorcheck">
              <label>Instrument No<em>*</em> </label>
              <input type="text" name="instrument_no" id="instrument_no" value="<?php if(isset($_GET['edit'])) { echo $data['instrument_no']; }?>" class="form-control form-control-sm rounded-0">
            </div> 
            <div class="col-md-3 fromerrorcheck">
              <label>Particulars<em>*</em> </label>
              <input type="text" name="reference" id="reference" value="<?php if(isset($_GET['edit'])) { echo $data['reference']; }?>" class="form-control form-control-sm rounded-0">
            </div>
            <div class="col" >
              <input type="radio" id="against_ref"  name="radio_name" value="against_ref" <?php if(isset($_GET['edit']) && $data['radio_name']=='against_ref'){ echo "checked";}  ?> onclick="fun_customer_details(this)">
              <label for="against_ref"> Against Bill</label>
            </div>
           
            <div class="col">
              <input type="radio" id="advance" class="rmclass" name="radio_name" value="advance" <?php if(isset($_GET['edit']) && $data['radio_name']=='advance'){ echo "checked";}  ?>>
              <label for="advance">Advance</label>
            </div>
            <div class="col">
              <input type="radio" id="on_account" class="rmclass" name="radio_name" value="on_account" <?php if(isset($_GET['edit']) && $data['radio_name']=='on_account'){ echo "checked";}  ?>>
              <label for="on_account">On Account</label>
           </div>
          </div>
          <div class="form-group row">
          <div class="col-md-4 fromerrorcheck">
              <label>Remark<em>*</em> </label>
              <input type="text" name="remark" id="remark" value="<?php if(isset($_GET['edit'])) { echo $data['remark']; } ?>" class="form-control form-control-sm rounded-0">
            </div>
          <div class="col-md-2 fromerrorcheck">
              <label>Received Now<em>*</em> </label>
              <input type="text" name="received_now" id="received_now" style="text-align:right" onkeyup="fun_customer_details(this)" onchange="calc()" value="<?php if(isset($_GET['edit'])) { echo $data['received_now']; } ?>" class="form-control form-control-sm rounded-0" required>
            </div>
        </div>
      
      <div class=" row">
               <table border="1" class="table mytable">
                <thead>
                <tr>
                <td >Bill No</td>
                <td >Date</td>
                <td >Disc Amt</td>
                <td >After Disc Amt</td>
                <td >Inv Amt</td>
                <td>Recd Amt</td>
                <td >Bal Amt</td>
                <td style="display:none">Cal Amt</td>
                <td >Amount</td>
                </tr>
                <thead>
                <tbody>
                <?php
                     if(isset($_GET['edit'])){
                      $inv_amt=0;
                      $recd_amt=0;
                      $balance_amt=0;
                       $counter=0;
                       while($rows = $admin->fetch($getUniqueReceivedDetailsById)){
                        $invoiceDetails = $admin ->getUniqueTaxInvoiceDetailsById($rows['invoice_id']);
                     
                        ?>
                  <tr>
                  <td style="display:none"><input type="text"  name="invoice_id[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['id']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 invoice_id" readonly></td>
                  <td><input type="text"  name="invoice_id_no[<?php echo $counter; ?>]"  value="<?php if(isset($_GET['edit'])) { echo $invoiceDetails['invoice_no']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 invoice_no" readonly></td>
                  <td><input type="text"  name="invoice_date[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo date("d-m-Y", strtotime($invoiceDetails['invoice_date'])); }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 invoice_date" readonly></td>
                  <td><input type="text"  name="inv_amt[<?php echo $counter; ?>]"  value="<?php if(isset($_GET['edit'])) { echo $rows['inv_amt']; }else{ echo '0.00'; } ?>" class="form-control form-control-sm rounded-0 inv_amt" readonly></td>
                  <td><input type="text"  name="recd_amt[<?php echo $counter; ?>]"  value="<?php if(isset($_GET['edit'])) { echo $rows['recd_amt'];} else{ echo '0.00'; } ?>" class="form-control form-control-sm rounded-0 recd_amt" style="text-align:right"  readonly></td>
                  <td><input type="text" onkeyup="calc()" name="balance_amt[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['balance_amt']; }else{ echo '0.00'; } ?>" class="form-control form-control-sm rounded-0 balance_amt"  readonly></td>
                  <td style="display:none"><input type="text" onkeyup="calc()" name="cal_amt[<?php echo $counter; ?>]" value="" class="form-control form-control-sm rounded-0 cal_amt" style="text-align:right"  ></td>
                  <td><input type="text" onkeyup="calc()" name="net_amt[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['net_amt']; }else{ echo '0.00'; } ?>" class="form-control form-control-sm rounded-0 net_amt" style="text-align:right"  ></td>

                    <?php $counter++; }  }?>
                        
                <tr>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"style="display:none"></td>
                <td style="border:1px solid #fff;" style="text-align:right" ><b>Total</b></td>
                <td style="border:1px solid #fff;"><input type="text" style="text-align:right"  id="total_balance_amt" name="total_balance_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_balance_amt']; }?>" class="form-control form-control-sm rounded-0" readonly></td>
                <td style="border:1px solid #fff;"><input type="text" style="text-align:right"  id="total_amt" name="total_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_amt']; }?>" class="form-control form-control-sm rounded-0" readonly></td>
                </tr>
                </tbody>
               </table>
               </div>
            </div>
        <!-- /.card-body -->
        <div class="card-footer">
        <input type="hidden" name="<?php echo $token_id; ?>" value="<?php echo $token_value; ?>" />
        
        <div class="row">
            <?php if(isset($_GET['edit'])){ ?>
                <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id ?>"/>
                <div class="col-sm-6"><button type="submit" name="update" value="update" id="update" class="btn btn-warning btn-block"><i class="fas fa-save"></i> Update <?php echo $pageName; ?></button></div>
                <!-- <div class="col-sm-4"><button type="submit" name="update_send" value="update_send" id="update_send" class="btn btn-warning btn-block"><i class="fas fa-save"></i> Update <?php echo $pageName; ?> And Send Email</button></div> -->

                <?php } else { ?>
                <div class="col-sm-6"><button type="submit" name="register" id="register" class="btn btn-success btn-block"><i class="fas fa-save"></i> Add <?php echo $pageName; ?></button></div>
                <!-- <div class="col-sm-6"><button type="submit" name="register_send" id="register_send" class="btn btn-success btn-block" value="<?php echo $id ?>"><i class="fas fa-save"></i> Add <?php echo $pageName; ?> And Send Email</button></div> -->
            <?php } ?>
            <div class="col-sm-6"><a class="btn btn-danger btn-block" href="javascript:void(0);" onclick="clearall()"><i class="fas fa-broom"></i>Clear All</a></div>
        </div>

        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->
      </form> 
    </section>
   

    <!-- /.content -->
    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- jquery-validation -->
<script src="../../plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="../../plugins/jquery-validation/additional-methods.min.js"></script>
<script src="../../dist/js/demo.js"></script>
<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });

  <?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>
      $("#cardeffect").slideDown("slow");
      <?php } ?>
  
$(function () {
  $('#form').validate({
    rules: {
      ignore: [],
		  debug: false,
      invoice_no : {
         required: true,
      },
      invoice_date : {
         required: true,
      },
      customer_id : {
         required: true,
      },
      due_date : {
         required: true,
      },
      gst_applicable : {
         required: true,
      },
      service_charges : {
         required: true,
      },
      service_charges_cgst_amt : {
         required: true,
      },
      service_charges_sgst_amt : {
         required: true,
      },
      service_charges_igst_amt : {
         required: true,
      },final_amt : {
         required: true,
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
    },
    errorElement: 'span',
     errorPlacement: function (error, element) {
       error.addClass('invalid-feedback');
       element.closest('.fromerrorcheck').append(error);
     },
     highlight: function (element, errorClass, validClass) {
       $(element).addClass('is-invalid');
     },
     unhighlight: function (element, errorClass, validClass) {
       $(element).removeClass('is-invalid');
     }
  });
});
    
$(document)
.on('click', 'form button[type=submit]', function() {
  var received_nw=parseFloat($('#received_now').val());
  var total_amt=parseFloat($('#total_amt').val());
  if(total_amt!=received_nw){
    alert('Total Amount Should Not Be Greater Than Received Amount..');
      e.preventDefault(); //prevent the default action
    }
    
});
function sameasbillingaddress(){
    $('#shipping_address').val($('#billing_address').val());
}

function arrayamountcac(){
  var input = parseFloat($('#received_now').val());
  var counter=0;
  var old_cal_amt=0;
$('.balance_amt').each(function(){
    var bill_amount=parseFloat($(this).val());
      if(counter==0){
        if(input<=bill_amount){
          var cal_amt=bill_amount-input;
          old_cal_amt=cal_amt;
          $(this).parent().parent().find('.cal_amt').val(0);
        }else{
          var cal_amt=input-bill_amount;
          old_cal_amt=cal_amt;
          $(this).parent().parent().find('.cal_amt').val(cal_amt);
        } 
     
        counter++;
      }else{
        old_cal_amt=old_cal_amt-bill_amount;
        $(this).parent().parent().find('.cal_amt').val(old_cal_amt);
      }
});

  var old_pre_cal_amt=0;
  var counter1=0;
$('.cal_amt').each(function(){
    var cal_amt=parseFloat($(this).val());
    var bill_amount = parseFloat($(this).parent().parent().find('.balance_amt').val());
    if(cal_amt>=0){
      if(counter1==0){
        if(bill_amount<=input){
          $(this).parent().parent().find('.net_amt').val(bill_amount);
        }else{
          $(this).parent().parent().find('.net_amt').val(input);
        }
       
        counter1++;
      }else{
        $(this).parent().parent().find('.net_amt').val(bill_amount);
      }
    
    }else{
        if(old_pre_cal_amt>=0){
          $(this).parent().parent().find('.net_amt').val(old_pre_cal_amt);
        }else{
          $(this).parent().parent().find('.net_amt').val(0);
        }
    }
    old_pre_cal_amt=cal_amt;
});
}

$('.rmclass').on('click',function(){
  $('.mytable > tbody > tr').not(':last').each(function(){
    $(this).find('td').each(function(){
    $(this).remove();
  });
  });
});

  function fun_customer_details(e){
  
    var against_ref=$('#against_ref').val();
    var customer_id=$('#customer_id').val();
    var received_nw=$('#received_now').val();

    var balance=$('#balance_amt').val();

    $('.mytable > tbody > tr').not(':last').each(function(){
    $(this).find('td').each(function(){
    $(this).remove();
  });
  });
  
  if(customer_id!='' && $('#against_ref').prop('checked')==true){
      $.ajax({
                type: 'POST',
                data: 'customer_id='+customer_id,
                url: 'getAjaxCustomerBillDetail.php',
                success: function (respone) {
                $(".table>tbody>tr:last").before(respone);
                arrayamountcac();
              calc();
                }
            });
    }
  }



function calc(){
  var total_amt=0;
  var total_balance_amt=0;
  $('.mytable > tbody > tr').not(':last').each(function (){
    if($(this).find('.net_amt').val()!=undefined){
      var net_amt = parseFloat($(this).find('.net_amt').val());
      var balance_amt = parseFloat($(this).find('.balance_amt').val());
      total_balance_amt = total_balance_amt + balance_amt;
      total_amt = total_amt + net_amt;

    }
})
  $('#total_amt').val(total_amt.toFixed(2));
  $('#total_balance_amt').val(total_balance_amt.toFixed(2));
}


function clearall(){
  $('input').val('');
  $('textarea').val('');
}

$('.loadstarter').on('click',function(){
      Notiflix.Loading.Init({});
      Notiflix.Loading.Hourglass();
    });
   
    $("#cardeffect").slideDown("slow");



  $(document).ready(function(){
    $(function () {
      $('.select2').select2()
    });

      //Initialize Select2 Elements
      $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

});
</script>
</body>
</html>