<?php 
include_once 'include/admin-functions.php';
$admin = new AdminFunctions();
$customer_id=$_POST['customer_id'];
$item_id=$_POST['item_id'];

$item_details = $admin->getUniqueItemDetailsByCustomerID($customer_id,$item_id); 



while($row = $admin->fetch($item_details)){
 

    $item_name = $admin -> getUniqueItemMasterById($row['item_id'])['item_name'];
?>
<tr>
    <td><input type="text" name="item_id" value="<?php echo $item_name; ?>" class="form-control form-control-sm rounded-0 item_id"></td>
    <td><input type="text"  name="rate" value="<?php echo $row['rate'] ?>" class="form-control form-control-sm rounded-0 rate"></td>
    <td><input type="text" name="qty" value="<?php echo $row['qty']; ?>" class="form-control form-control-sm rounded-0 qty"></td>
    <td><input type="text" name="unit" value="<?php echo $row['unit']; ?>" class="form-control form-control-sm rounded-0 unit"></td>
    <td><input type="text" name="amt" value="<?php echo $row['amt'];  ?>" class="form-control form-control-sm rounded-0 amt"></td>
    <td><input type="text" name="disc_per" value="<?php echo $row['disc_per'];  ?>" class="form-control form-control-sm rounded-0 disc_per"></td>
    <td><input type="text" name="disc_amt" value="<?php echo $row['disc_amt'];  ?>" class="form-control form-control-sm rounded-0 disc_amt"></td>
    <td><input type="text" name="after_disc_amt" value="<?php echo $row['after_disc_amt']; ?>"  class="form-control form-control-sm rounded-0 after_disc_amt"></td>
</tr>

<?php } ?>