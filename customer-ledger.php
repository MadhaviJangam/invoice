


<?php

include_once 'include/config.php';
include_once 'include/admin-functions.php';
include_once 'include/classes/CSRF.class.php';
$admin = new AdminFunctions();

if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: index.php");
	exit();
}
$pageName = "Customer Ledger";
// $pageURL = 'tax-invoice-add.php';
// $pageURL = 'credit-note-add.php';
// $pageURL = 'customer-master.php';
// $pageURL = 'delivery-chanllan-add.php';


$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);

$results = $admin->getActiveItemDetails();
$getActiveCustomerDetails = $admin->getActiveCustomerDetails();


//$getPrivousClosing = $admin->PreviousClosing($_GET['customer_id'],$_GET['from_date'],$_GET['to_date']);

//$prevousbalance=(($getPrivousClosing['opening']+$getPrivousClosing['plus'])-$getPrivousClosing['minus']);


//$getAllCreditDataByID =$admin->getAllCreditDataByID($_GET['customer_id'],$_GET['from_date'],$_GET['to_date']);


if(isset($_GET['customer_id'])){

  $getAllCreditDataByID =$admin->getAllCreditDataByID($_GET['customer_id'],$_GET['from_date'],$_GET['to_date']);
  $getPrivousClosing = $admin->PreviousClosing($_GET['customer_id'],$_GET['from_date'],$_GET['to_date']);
  $prevousbalance=(($getPrivousClosing['opening']+$getPrivousClosing['plus'])-$getPrivousClosing['minus']);

}else{
  $getAllCreditDataByID =$admin->getAllCreditDataByID(0,'','');
  $getPrivousClosing = $admin->PreviousClosing(0,'','');
  $prevousbalance=(($getPrivousClosing['opening']+$getPrivousClosing['plus'])-$getPrivousClosing['minus']);

}

 ?>
 
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title> Unique Invoice | <?php echo $pageName; ?></title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <script src="../../plugins/notiflix/notiflix-aio-1.5.0.min.js"></script>

    <!-- Select2 -->
    <link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">

</head>
<body class="hold-transition sidebar-mini layout-footer-fixed">
<!-- Site wrapper -->
<div class="wrapper">
 
  <?php 
  include('include/header.php');
  include('include/sidebar.php');
  ?>
<style>
    label{
        font-size:12px;
    }
    .dataTables_wrapper {
    font-size: 14px;
}
em{
  color:red;
}

.select2-container--default .select2-selection--single {
  border-radius:0px;
}

.dataTables_wrapper {
    font-size: 12px; 
}

    </style>
  <!-- Content Wrapper. Contains page content -->
  <?php if(isset($_GET['registersuccess'])){ ?>
  <script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Success('<?php echo $pageName; ?> successfully Added');
       </script>
<?php } ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1> <?php echo $pageName; ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Transaction</a></li>
              <li class="breadcrumb-item active"> <?php echo $pageName; ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->

    <section class="content" >
    
    <!-- /.content -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><b><?php echo $pageName; ?> Details</b></h3><br>
              </div>
              <div class="card-body">
              <form action = "" method = "GET" name="form" id="form">
                <div class="form-group row">
                    <div class="col-md-3 fromerrorcheck">
                        <label>From Date</label>
                        <input type="date" class="form-control form-control-sm rounded-0 " name="from_date" id="from_date">
                    </div>
                    <div class="col-md-3 fromerrorcheck">
                        <label>To Date</label><input type="date" class="form-control form-control-sm rounded-0 " name="to_date" id="to_date">
                    </div>
                    <div class="col-md-3 fromerrorcheck">
                        <label>Customer Name</label>
                        <select class="form-control form-control-sm rounded-0 " name="customer_id" id="customer_id">
                        <option value="">Select Customer Name</option>
                        <?php while($row = $admin->fetch($getActiveCustomerDetails)){ ?>
                            <option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['customer_id']) and $_GET['customer_id']==$row['id']) { echo 'selected'; } ?>><?php echo $row['customer_name']; ?>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-3 fromerrorcheck"> 
                      <label>Search <em>*</em> </label>
                    <button class="btn btn-sm btn-primary btn-block">Search</button>
                </div> 
                </form>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Sr. No.</th>
                    <th>Date</th>
                    <th>Document No</th>
                    <th>Transaction Type</th>
                    <th style="text-align:right">Debit</th>
                    <th style="text-align:right">Credit</th>
                    <th style="text-align:right">Closing Balance</th>
                                     
                  </tr>
                  </thead>
                  <tbody>
                   <?php
                   $x=1;
                   $opening_balance=0;
                   $debitTotal=0;
                   $cradit_total=0;
                    $balance = 0;  
                      
                    while($rows = $admin->fetch($getAllCreditDataByID)){
                      

                      if($rows['description']=='Opening Balanace'){
                        $opening_balance=$rows['opening'];
                        $balance=$prevousbalance;
                      }

                      if($rows['debit']!=''){
                        $debitTotal=$debitTotal+$rows['debit'];
                      }
                      if($rows['credit']!=''){
                        $cradit_total=$cradit_total+$rows['credit'];
                      }
                      
                      $type='';
                      switch ($rows['priority']) {
                        case '0': $type=''; break;
                        case '1': $type='Tax Invoice'; break;
                        case '2': $type='Delivery Challan'; break;
                        case '3': $type='Credit Note'; break;
                      }
                      $url='';
                      switch ($rows['priority']) {
                        case '0': $url=''; break;
                        case '1': $url='tax-invoice-add.php'; break;
                        case '2': $url='delivery-challan-add.php'; break;
                        case '3': $url='credit-note-add.php'; break;
                      }
                     
                   ?>   
                  <tr>
                        <td><?php echo $x; ?></td>
                        <td><?php echo $rows['tr_date'];?></td>
                        <td><a href="<?php echo $url;?>?edit&id=<?php echo $rows['id']; ?>"><?php echo $rows['description'];?></a></td>
                        <td><?php echo $type; ?></td>
                        <td style="text-align:right"><?php echo $rows['debit'];?></td>
                        <td style="text-align:right"><?php echo $rows['credit'];?></td>
                        <td style="text-align:right"><?php 
                      if($rows['description']=='Opening Balanace'){
                      echo $balance.' Dr';
                      }
  
                      if($rows['description']!='Opening Balanace'){                     
                        if($rows['debit']!=''){
                        $balance=$balance+$rows['debit'];
                      if($balance>=0){
                        echo $balance.' Dr';
                      }else{
                        echo abs($balance).' Cr';
                      }
                       
                      
                      }else{
                          $balance=$balance-$rows['credit'];
                         
                          if($balance>=0){
                            echo $balance.' Dr';
                          }else{
                            echo abs($balance).' Cr';
                          }
                        }
                      }
                      ?></td>
                    
                    </tr>
                    
                    <?php $x++; }
                   
                    $closing = ($opening_balance+$debitTotal)-$cradit_total;
                    ?>
                    <tr>
                        <td><?php echo $x++; ?></td>
                        <td></td>
                        <td>Total</td>
                        <td></td>
                       
                        <td></td>  <td></td>
                        <td style="text-align:right"><?php if($balance>=0){
                            echo $balance.' Dr';
                          }else{
                            echo abs($balance).' Cr';
                          }?></td>
                          
                    </tr>
                    
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <!-- <div class="float-left d-none d-sm-block">
      <a href="<?php echo $subpageURL; ?>" class="btn btn-primary"> <i class="fas fa-plus-circle"></i> Create New <?php echo $pageName; ?></a>
    </div> -->
    <strong class="float-right">Copyright &copy; 2020-2021 <a href="https://usssoft.com">Unique Software System</a>. All rights reserved.</strong> 
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- jquery-validation -->
<script src="../../plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="../../plugins/jquery-validation/additional-methods.min.js"></script>
<script src="../../dist/js/demo.js"></script>

<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>

<!-- Page specific script -->
<script>
  // $(function () {
  //   $("#example1").DataTable({
  //     "paging": false,
  //     "responsive": true, "lengthChange": false, "autoWidth": false,
  //     "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
  //   }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    
  //   $('#example2').DataTable({
  //     "paging": true,
  //     "lengthChange": false,
  //     "searching": false,
  //     "ordering": true,
  //     "info": true,
  //     "autoWidth": false,
  //     "responsive": true,
  //   });
  // });

  <?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>
      $("#cardeffect").slideDown("slow");
      <?php } ?>
   
$(function () {
 

  


  $('#form').validate({
    rules: {
    ignore: [],
		debug: false,
    from_date:{
        required: true,
      },
      to_date:{
        required: true,
      },
      customer_id:{
        required: true,
        number:true,
      },
     
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
    },
    errorElement: 'span',
     errorPlacement: function (error, element) {
       error.addClass('invalid-feedback');
       element.closest('.fromerrorcheck').append(error);
     },
     highlight: function (element, errorClass, validClass) {
       $(element).addClass('is-invalid');
     },
     unhighlight: function (element, errorClass, validClass) {
       $(element).removeClass('is-invalid');
     }
  });
});

// function CustomerLedger(){

//   var from_date=$('#from_date').val();
//   var to_date=$('#to_date').val();
//   var customer_id=$('#customer_id').val();
 
// if(from_date!='' && to_date!='' && customer_id!=''){
//       alert('Select From And To Date And Customer Name');
//     }
  
// }
function clearall(){
  $('input').val('');
  $('textarea').val('');
}

$('.loadstarter').on('click',function(){
      Notiflix.Loading.Init({});
      Notiflix.Loading.Hourglass();
    });
    
    $(function () {
      $('.select2').select2()
    });

</script>

<?php if(isset($_GET['id'])){ ?>
  
  <script>  
//   setTimeout(function(){  
//   window.open('html-pdf/invoice-view.php?id=<?php echo $_GET['id']; ?>');
//   }, 300);
  </script>

  <?php } ?>

</body>
</html>