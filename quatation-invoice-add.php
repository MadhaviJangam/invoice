<?php
include_once 'include/config.php';
include_once 'include/admin-functions.php';
include_once 'include/classes/CSRF.class.php';
$admin = new AdminFunctions();

if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: index.php");
	exit();
}
$pageName = "Quatation Invoice";
$pageURL = 'quatation-invoice.php';
$subpageURL = 'quatation-invoice-add.php';
$deleteURL = 'quatation-invoice.php';
$tableName = 'tax_invoice_transaction';
$parentPageURL = 'tax-invoice.php';
$senPageURL='compose-mail.php';



 $item_details = $admin->getActiveItemTaxMaster(); 
 $getActiveCustomerDetails = $admin->getActiveCustomerDetails();
 

$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);


if(isset($_POST['register'])){
	if($csrf->check_valid('post')) {
		$result = $admin->addTaxInvoice($_POST,$loggedInUserDetailsArr['id']);
		header("location:".$parentPageURL."?registersuccess");
	exit();
	}
}
if(isset($_POST['register_send'])){
	if($csrf->check_valid('post')) {
		$result = $admin->addTaxInvoice($_POST,$loggedInUserDetailsArr['id']);
		header("location:".$senPageURL."?registersuccess&id=".$result);
	exit();
	}
}
if(isset($_GET['edit'])){
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
	$data = $admin->getUniqueTaxInvoiceDetailsById($id);
}

if(isset($_POST['id']) && !empty($_POST['id'])) {
	if($csrf->check_valid('post')) {
		$id = trim($admin->escape_string($admin->strip_all($_POST['id'])));
		$result = $admin->updateTaxInvoice($_POST, $loggedInUserDetailsArr['id']);
		header("location:".$parentPageURL."?updatesuccess");
		exit();
	}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title> Unique Invoice | <?php echo $pageName; ?></title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Select2 -->

  <link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  
  <script src="../../plugins/notiflix/notiflix-aio-1.5.0.min.js"></script>
</head>
<body class="hold-transition sidebar-mini layout-footer-fixed sidebar-collapse" >
<!-- Site wrapper -->
<div class="wrapper">
 
  <?php 
  include('include/header.php');
  include('include/sidebar.php');
  ?>
<style>
    label{
        font-size:11px;
        padding:0px;
        margin:0px;
    }
    .form-control{
       border:1px solid #48544b;

    }
    .dataTables_wrapper {
    font-size: 14px;
}
em{
  color:red;
}
.mytable{
    font-size:14px;
    text-align:center;
}

.table td, .table th{
  padding: .13rem;
  border:1px solid #000;
}
.mytable td input{
    /* padding:1px; */
    text-align:center;
}

.mytable thead tr td{
    background:#ddd;
}
.select2-container .select2-selection--single {
            height: 25px;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            top: 31%;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 25px;
        }
        
    </style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
<br>
    <!-- Main content -->

    <section class="content" style="zoom: 90%;">
    
      <form action="" id="form" method="post" enctype="multipart/form-data" autocomplete="off">
      <!-- Default box -->
   <div id="cardeffect" style="display:none;">
      <div class="card" >
        <div class="card-header">
          <h3 class="card-title"> <?php echo $pageName; ?></h3>
          <a href="<?php echo $parentPageURL; ?>" align="right" style="font-size:14px;color:red;float:right;"> <i class="fas fa-hand-point-left"></i> Back</a>
        </div>
       
        <div class="card-body" style="padding-top:5px;padding-bottom:5px;">
          <div class="form-group row">
           
            <div class="col-md-2 fromerrorcheck">
              <label>Invoice Date<em>*</em> </label>
              <input type="date" name="invoice_date" value="<?php if(isset($_GET['edit'])) { echo $data['invoice_date']; }else{ echo date("Y-m-d"); } ?>" class="form-control form-control-sm rounded-0">
            </div> 

            <div class="col-md-2 fromerrorcheck">
              <label>Customer Name<em>*</em> </label>
              <select class="form-control form-control-sm rounded-0 " name="customer_id">
              <option value="">Select Customer Name</option>
                <?php while($row = $admin->fetch($getActiveCustomerDetails)){ ?>
                  <option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) and $data['customer_id']==$row['id']) { echo 'selected'; } ?>><?php echo $row['customer_name']; ?>
                  <?php } ?>
              </select>
            </div> 
            <div class="col-md-2 fromerrorcheck">
              <label>Item Name<em>*</em> </label>
                <select class="form-control form-control-sm rounded-0 item_id" onchange="fun_item_details(this)" name="item_id[0]">
                <option value="">Select Item</option>
                <?php while($row = $admin->fetch($item_details)){
                ?>
                <option value="<?php echo $row['item_name']; ?>" <?php if(isset($_GET['edit']) and $data['item_name']==$row['item_name']) { echo 'selected'; } ?>><?php echo $row['item_name']; ?>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-2 fromerrorcheck">
              <label>GST % <em>*</em> </label>
                <select class="form-control form-control-sm rounded-0" name="gst_per" id="gst_per">
                <option value="0">None</option>
                    <option value="3">3 %</option>
                    <option value="5">5 %</option>
                    <option value="12">12 %</option>
                    <option value="18">18 %</option>
                    <option value="28">28 %</option>
                </select> 
            </div>  
            <div class="col-md-12 " align="center">
            <b>Item Details</b>
            </div>
            
          </div>
          
          <div class=" row">
               <table border="1" class="table mytable">
                <thead>
                <tr>
                <td rowspan="2" width="15%">Item</td>
                <td rowspan="2">Rate</td>
                <td rowspan="2">Qty</td>
                <td rowspan="2">Unit</td>
                <td rowspan="2">Amt</td>
                <td rowspan="2">Disc %</td>
                <td rowspan="2">Disc Amt</td>
                <td rowspan="2">After Disc. Amt</td>
                <td colspan="2">CGST</td>
                <td colspan="2">SGST</td>
                <td colspan="2">IGST</td>
                <td rowspan="2">Net Amt</td>
                <td rowspan="2">Acn</td>
                </tr>
                <tr>
                <td>Rate</td>
                <td>Amt</td>
                <td>Rate</td>
                <td>Amt</td>
                <td>Rate</td>
                <td>Amt</td>
                </tr>
                <thead>
                <tbody>
                <?php
               


                
                  ?>
                <tr>
                <td>
              <select class="form-control form-control-sm rounded-0 item_id" onchange="fun_item_details(this)" name="item_id[0]">
                  <option value="">Select Item</option>
                  <?php while($row = $admin->fetch($item_details)){
                   ?>
                  <option value="<?php echo $row['item_name']; ?>" <?php if(isset($_GET['edit']) and $data['item_name']==$row['item_name']) { echo 'selected'; } ?>><?php echo $row['item_name']; ?>
                  <?php } ?>
              </select>
               
                </td>
                <td><input type="text" onkeyup="calc()" name="rate[0]" value="<?php if(isset($_GET['edit'])) { echo $data['rate']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 rate"></td>
                <td><input type="text" onkeyup="calc()" name="qty[0]" value="<?php if(isset($_GET['edit'])) { echo $data['qty']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 qty"></td>
                <td><input type="text"  onkeyup="calc()" name="unit[0]" value="<?php if(isset($_GET['edit'])) { echo $data['unit']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 unit"></td>
                <td><input type="text"  onkeyup="calc()" name="amt[0]" value="<?php if(isset($_GET['edit'])) { echo $data['amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 amt" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="dicountcalc(this)" name="disc_per[0]" value="<?php if(isset($_GET['edit'])) { echo $data['disc_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 disc_per"  ></td>
                <td><input type="text"  onkeyup="dicountcalc(this)" name="disc_amt[0]" value="<?php if(isset($_GET['edit'])) { echo $data['disc_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 disc_amt" ></td>
                <td><input type="text"  onkeyup="calc()" name="after_disc_amt[0]" value="<?php if(isset($_GET['edit'])) { echo $data['after_disc_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 after_disc_amt" style="text-align:right"  readonly ></td>
                <td><input type="text"  onkeyup="calc()" name="cgst_per[0]" value="<?php if(isset($_GET['edit'])) { echo $data['cgst_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 cgst_per" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="cgst_amt[0]" value="<?php if(isset($_GET['edit'])) { echo $data['cgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 cgst_amt" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="sgst_per[0]" value="<?php if(isset($_GET['edit'])) { echo $data['sgst_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 sgst_per" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="sgst_amt[0]" value="<?php if(isset($_GET['edit'])) { echo $data['sgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 sgst_amt" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="igst_per[0]" value="<?php if(isset($_GET['edit'])) { echo $data['igst_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 igst_per" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="igst_amt[0]" value="<?php if(isset($_GET['edit'])) { echo $data['igst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 igst_amt"  style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="net_amt[0]" value="<?php if(isset($_GET['edit'])) { echo $data['net_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 net_amt" style="text-align:right"  readonly></td>
                <td><a href="javascript:void(0);" onclick="addnewitem()" class="btn btn-sm btn-success">+</a></td>
                </tr>
                  
                <tr>
                <td style="border:1px solid #fff;">Total</td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"><input type="text" onchange="total_amt_calc(this)"  style="text-align:right"  id="total_amt" name="total_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"><input type="text" style="text-align:right"  id="total_disc_amt" name="total_disc_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_disc_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly></td>
                <td style="border:1px solid #fff;"><input type="text" style="text-align:right"  id="total_after_disc_amt" name="total_after_disc_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_after_disc_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly ></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"><input type="text" style="text-align:right"  id="total_cgst_amt" name="total_cgst_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_cgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"><input type="text" style="text-align:right"  id="total_sgst_amt" name="total_sgst_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_sgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"><input type="text"  style="text-align:right"  id="total_igst_amt" name="total_igst_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_igst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly></td>
                <td style="border:1px solid #fff;"><input type="text"  style="text-align:right" id="total_net_amt" name="total_net_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_net_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly></td>

                </tr>
                </tbody>
               </table>
               </div>
               <div class="form-group row">
               

            <div class="col-md-5 fromerrorcheck">
              <label>Tearm And Conditions<em>*</em> </label>
              <textarea type="text" name="terms_of_payment" value="<?php if(isset($_GET['edit'])) { echo $data['terms_of_payment']; }else{ echo '-'; } ?>" class="form-control form-control-sm rounded-0" >
                  </textarea>
            </div> 

         
            <div class="col-md-1 fromerrorcheck">
              <label>Transport Chg.<em>*</em> </label>
              <input type="text" name="service_charges"  style="text-align:center"  value="<?php if(isset($_GET['edit'])) { echo $data['service_charges']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" >
            </div> 
        
            <div class="col-md-1 fromerrorcheck">
              <label>Transport CGST Amt<em>*</em> </label>
              <input type="text" name="service_charges_cgst_amt" style="text-align:right"  value="<?php if(isset($_GET['edit'])) { echo $data['service_charges']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly>
            </div> 

            <div class="col-md-1 fromerrorcheck">
              <label>Transport SGST Amt<em>*</em> </label>
              <input type="text" name="service_charges_sgst_amt"  style="text-align:right"  value="<?php if(isset($_GET['edit'])) { echo $data['service_charges']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly >
            </div> 

            <div class="col-md-1 fromerrorcheck">
              <label>Transport IGST Amt<em>*</em> </label>
              <input type="text" name="service_charges_igst_amt" style="text-align:right"  value="<?php if(isset($_GET['edit'])) { echo $data['service_charges']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly >
            </div> 
           
            <div class="col-md-3 fromerrorcheck">
              <label>Final Paid Amt<em>*</em> </label>
              <input type="text" name="final_amt" style="text-align:right"  value="<?php if(isset($_GET['edit'])) { echo $data['final_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly>
            </div> 
          </div>

        </div>
        <!-- /.card-body -->
        <div class="card-footer">
        <input type="hidden" name="<?php echo $token_id; ?>" value="<?php echo $token_value; ?>" />
        
        <div class="row">
            <?php if(isset($_GET['edit'])){ ?>
                <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id ?>"/>
                <div class="col-sm-4"><button type="submit" name="update" value="update" id="update" class="btn btn-warning btn-block"><i class="fas fa-save"></i> Update <?php echo $pageName; ?></button></div>
            <?php } else { ?>
                <div class="col-sm-4"><button type="submit" name="register" id="register" class="btn btn-success btn-block"><i class="fas fa-save"></i> Add <?php echo $pageName; ?></button></div>
                <div class="col-sm-4"><button type="submit" name="register_send" id="register_send" class="btn btn-success btn-block" value="<?php echo $id ?>"><i class="fas fa-save"></i> Add <?php echo $pageName; ?> And Send</button></div>
            <?php } ?>
            <div class="col-sm-4"><a class="btn btn-danger btn-block" href="javascript:void(0);" onclick="clearall()"><i class="fas fa-broom"></i>Clear All</a></div>
        </div>

        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->
      </form> 
    </section>
   

    <!-- /.content -->
    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- jquery-validation -->
<script src="../../plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="../../plugins/jquery-validation/additional-methods.min.js"></script>
<script src="../../dist/js/demo.js"></script>
<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });

  <?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>
      $("#cardeffect").slideDown("slow");
      <?php } ?>
   
$(function () {
 

  


  $('#form').validate({
    rules: {
      ignore: [],
		  debug: false,
      customer_name : {
         required: true,
      },
      company_phone : {
         required: true,
      },
      company_pan : {
         required: true,
      },
      company_email : {
         required: true,
      },
      company_gst : {
         required: true,
      },
      billing_address : {
         required: true,
      },
      shipping_address : {
         required: true,
      },
      contact_person_name : {
         required: true,
      },
      contact_person_phone : {
         required: true,
      },
      contact_person_whatsapp : {
         required: true,
      },
      contact_person_email : {
         required: true,
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
    },
    errorElement: 'span',
     errorPlacement: function (error, element) {
       error.addClass('invalid-feedback');
       element.closest('.fromerrorcheck').append(error);
     },
     highlight: function (element, errorClass, validClass) {
       $(element).addClass('is-invalid');
     },
     unhighlight: function (element, errorClass, validClass) {
       $(element).removeClass('is-invalid');
     }
  });
});

function sameasbillingaddress(){
    $('#shipping_address').val($('#billing_address').val());
}

function calc(){

}

function removeitem(e){
  $(e).parent().parent().remove();
}

function clearall(){
  $('input').val('');
  $('textarea').val('');
}

$('.loadstarter').on('click',function(){
      Notiflix.Loading.Init({});
      Notiflix.Loading.Hourglass();
    });
   
    $("#cardeffect").slideDown("slow");


  function addnewitem(){
    $('.mytable').length;
    var count=($('table > tbody > tr').length)-1;
    $.ajax({
                    type: 'POST',
                    data: 'count='+count,
                    url: 'getAjaxItemDetails.php',
                    success: function (services_clone) {
                        $(".table > tbody tr:last").before(services_clone);

                    }
                });
  }

  $(document).ready(function(){
    $(function () {
      $('.select2').select2()
    });

      //Initialize Select2 Elements
      $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

});

function fun_item_details(e){
  var item_id=$(e).val();
  if(item_id!=''){

  }
}

function calc(){
  $('.mytable > tbody > tr').each(function (){
    var rate = parseFloat($(this).find('.rate').val());
    var qty = parseFloat($(this).find('.qty').val());
    var unit = $(this).find('.unit').val();
    var amt = rate*qty;
    $(this).find('.amt').val(amt);
    var disc_per = parseFloat($(this).find('.disc_per').val());
    var disc_amt = amt * disc_per/100;
    $(this).find('.disc_amt').val(disc_amt);
    var after_disc_amt = amt-disc_amt;
      $(this).find('.after_disc_amt').val(after_disc_amt);
      var gst_applicable = parseFloat($('#gst_applicable').val());
      
      var cgst_per = 0;
      var sgst_per = 0;
      var igst_per = 0;
      var gst_per = parseFloat($('#gst_per').val());

      if(gst_applicable==2){
      cgst_per=gst_per/2;
      sgst_per=gst_per/2;
      igst_per = 0;
      }else if(gst_applicable==3){
      cgst_per = 0;
      sgst_per = 0;
      igst_per = gst_per;
      }
      
    $(this).find('.cgst_per').val(cgst_per);
    $(this).find('.sgst_per').val(sgst_per);
    $(this).find('.igst_per').val(igst_per);
    var cgst_amt = after_disc_amt * cgst_per/100;
    var sgst_amt = after_disc_amt * sgst_per/100;
    var igst_amt = after_disc_amt * igst_per/100;
      
    $(this).find('.cgst_amt').val(cgst_amt);
    $(this).find('.sgst_amt').val(sgst_amt);
    $(this).find('.igst_amt').val(igst_amt);

    var net_amt = after_disc_amt + cgst_amt + sgst_amt +igst_amt;
    $(this).find('.net_amt').val(net_amt);

  })

}

function dicountcalc(e){
  var value = $(e).val();
  var getinputname = $(e).hasClass('disc_per');
  if(getinputname==true){
    var amt = $(e).parent().parent().find('.amt').val();
    var disc_amt = amt*value/100;
    $(e).parent().parent().find('.disc_amt').val(disc_amt);
    calc();
  }else{
      var disc_per=value*100/amt;
      $(e).parent().parent().find('.disc_per').val(disc_per);
    calc();
    
  }

}


</script>
</body>
</html>