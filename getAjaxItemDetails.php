
<?php 
include_once 'include/admin-functions.php';
$admin = new AdminFunctions();
$item_details = $admin->getActiveItemDetails(); 
$count=$_POST['count'];
?>
<tr>
                <td>
              <select class="form-control form-control-sm rounded-0 item_id chosen"  onchange="fun_item_details(this)" name="item_id[<?php echo $count; ?>]"required>
                  <option value="">Select Item</option>
                  <?php while($row = $admin->fetch($item_details)){
                    $stockDetails = $admin -> getLiveStockDetails($row['id']);
                   ?>
                  <option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) and $data['item_name']==$row['item_name']) { echo 'selected'; } ?>><?php echo $row['item_name'];echo' '; echo' '; echo $stockDetails['current_stock']; ?>
                  <?php } ?>
              </select>
              <table style="width:100%">
                    <td>Sheet Type</td>
                    <td>Lenght</td>
                    <td>Width</td>
                    <td>Thickness</td>
                    <td>Pcs</td>
                    <tbody>
                      <td> 
                        <select class="form-control form-control-sm rounded-0 sheet_type" name="sheet_type" onkeyup="itemQtyFun(this)" >
                        <option value="">Select Sheet Type</option>
                          <option value="0" <?php if(isset($_GET['edit']) and $data['sheet_type']=='0') { echo 'selected'; } ?>>No</option>
                          <option value="1" <?php if(isset($_GET['edit']) and $data['sheet_type']=='1') { echo 'selected'; } ?>>SS</option>
                          <option value="2" <?php if(isset($_GET['edit']) and $data['sheet_type']=='2') { echo 'selected'; } ?>>MS</option>
                          <option value="3" <?php if(isset($_GET['edit']) and $data['sheet_type']=='3') { echo 'selected'; } ?>>GI</option>
                          <option value="4" <?php if(isset($_GET['edit']) and $data['sheet_type']=='4') { echo 'selected'; } ?>>AL</option>
                          <option value="5" <?php if(isset($_GET['edit']) and $data['sheet_type']=='5') { echo 'selected'; } ?>>SF</option>
                        </select> 
                      </td> 
                      <td><input type="text" name="length[0]" onkeyup="itemQtyFun(this)" value="<?php if(isset($_GET['edit'])) { echo $data['length']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 length"></td> 
                      <td><input type="text" name="width[0]" onkeyup="itemQtyFun(this)" value="<?php if(isset($_GET['edit'])) { echo $data['width']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 width"></td> 
                      <td><input type="text" name="thickness[0]" onkeyup="itemQtyFun(this)" value="<?php if(isset($_GET['edit'])) { echo $data['thickness']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 thickness"></td> 
                      <td><input type="text" name="pcs[0]" onkeyup="itemQtyFun(this)" value="<?php if(isset($_GET['edit'])) { echo $data['pcs']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 pcs"></td> 
                    </tbody>
              </table>
                </td>
                <td><input type="text" style="height:100px;"  name="description[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['description']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 description"></td> 
                <td><input type="text" onkeyup="calc()" name="rate[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['rate']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 rate" required></td>
                <td><input type="text" onkeyup="calc()" name="qty[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['qty']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 qty" required></td>
                <td><input type="text"  onkeyup="calc()" name="unit[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['unit']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 unit" readonly></td>
                <td><input type="text"  onkeyup="calc()" name="amt[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 amt" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="dicountcalc(this)" name="disc_per[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['disc_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 disc_per"  ></td>
                <td><input type="text"  onkeyup="dicountcalc1(this)" name="disc_amt[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['disc_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 disc_amt" ></td>
                <td><input type="text"  onkeyup="calc()" name="after_disc_amt[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['after_disc_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 after_disc_amt" style="text-align:right"  readonly ></td>
                <td style="display:none" class="rmclass"><input type="text"  onkeyup="calc()" name="cgst_per[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['cgst_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 cgst_per" style="text-align:right"  readonly></td>
                <td style="display:none" class="rmclass"><input type="text"  onkeyup="calc()" name="cgst_amt[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['cgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 cgst_amt" style="text-align:right"  readonly></td>
                <td style="display:none" class="rmclass"><input type="text"  onkeyup="calc()" name="sgst_per[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['sgst_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 sgst_per" style="text-align:right"  readonly></td>
                <td style="display:none" class="rmclass"><input type="text"  onkeyup="calc()" name="sgst_amt[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['sgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 sgst_amt" style="text-align:right"  readonly></td>
                <td style="display:none" class="rmclass"><input type="text"  onkeyup="calc()" name="igst_per[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['igst_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 igst_per" style="text-align:right"  readonly></td>
                <td style="display:none" class="rmclass"><input type="text"  onkeyup="calc()" name="igst_amt[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['igst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 igst_amt"  style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="net_amt[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['net_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 net_amt" style="text-align:right"  readonly></td>
                          <td><a href="javascript:void(0);" onclick="removeitem(this)" class="btn btn-sm btn-danger">-</a></td>
                </tr>
<script>

jQuery(document).ready(function(){
  
	jQuery(".chosen").chosen();
  
});
</script>

<!-- </html> -->