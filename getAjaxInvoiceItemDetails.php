<?php 
include_once 'include/admin-functions.php';
$admin = new AdminFunctions();
$invoice_id=$_POST['invoice_id'];
$invoice_item_details = $admin->getUniqueInvoiceItemDetailsByInvoiceId($invoice_id); 
//echo json_encode($item_details);
$counter=0;
while($row = $admin->fetch($invoice_item_details)){
    $item_details = $admin->getActiveItemDetails(); 
?>
<tr>
    <td>
        <select class="form-control form-control-sm rounded-0 item_id" disabled="disabled" required onchange="fun_item_details(this)" name="item_id[<?php echo $counter ?>]">
            <option value="">Select Item</option>
            <?php while($rows = $admin->fetch($item_details)){
            ?>
            <option value="<?php echo $rows['id']; ?>" <?php if($row['item_id']==$rows['id']) { echo 'selected'; } ?>><?php echo $rows['item_name']; ?>
            <?php } ?>
        </select>
    </td>
    <td><input type="text"  name="rate[<?php echo $counter; ?>]" value="<?php echo $row['rate'] ?>" class="form-control form-control-sm rounded-0 rate" readonly></td>
    <td><input type="text"  name="qty[<?php echo $counter; ?>]" value="<?php echo $row['qty']; ?>" class="form-control form-control-sm rounded-0 qty" readonly></td>
    <td><input type="text"  name="unit[<?php echo $counter; ?>]" value="<?php echo $row['unit'];  ?>" class="form-control form-control-sm rounded-0 unit" readonly></td>
    <td><input type="text"  name="amt[<?php echo $counter; ?>]"  value="<?php echo $row['amt'];  ?>" class="form-control form-control-sm rounded-0 amt" style="text-align:right"  readonly></td>
    <td><input type="text"  name="disc_per[<?php echo $counter; ?>]" value="<?php echo $row['disc_per'];  ?>" class="form-control form-control-sm rounded-0 disc_per"  readonly></td>
    <td><input type="text"  name="disc_amt[<?php echo $counter; ?>]" value="<?php echo $row['disc_amt'];  ?>" class="form-control form-control-sm rounded-0 disc_amt" readonly></td>
    <td><input type="text"  name="after_disc_amt[<?php echo $counter; ?>]" value="<?php echo $row['after_disc_amt']; ?>" class="form-control form-control-sm rounded-0 after_disc_amt" style="text-align:right"  readonly ></td>
    <td><input type="text"  name="cgst_per[<?php echo $counter; ?>]" value="<?php echo $row['cgst_per'];  ?>" class="form-control form-control-sm rounded-0 cgst_per" style="text-align:right"  readonly></td>
    <td><input type="text"  name="cgst_amt[<?php echo $counter; ?>]" value="<?php echo $row['cgst_amt'];  ?>" class="form-control form-control-sm rounded-0 cgst_amt" style="text-align:right"  readonly></td>
    <td><input type="text"  name="sgst_per[<?php echo $counter; ?>]" value="<?php echo $row['sgst_per'];  ?>" class="form-control form-control-sm rounded-0 sgst_per" style="text-align:right"  readonly></td>
    <td><input type="text"  name="sgst_amt[<?php echo $counter; ?>]" value="<?php echo $row['sgst_amt'];  ?>" class="form-control form-control-sm rounded-0 sgst_amt" style="text-align:right"  readonly></td>
    <td><input type="text"  name="igst_per[<?php echo $counter; ?>]" value="<?php echo $row['igst_per'];  ?>" class="form-control form-control-sm rounded-0 igst_per" style="text-align:right"  readonly></td>
    <td><input type="text"  name="igst_amt[<?php echo $counter; ?>]" value="<?php echo $row['igst_amt'];  ?>" class="form-control form-control-sm rounded-0 igst_amt"  style="text-align:right"  readonly></td>
    <td><input type="text"  name="net_amt[<?php echo $counter; ?>]" value="<?php echo $row['net_amt'];  ?>" class="form-control form-control-sm rounded-0 net_amt" style="text-align:right"  readonly></td>
    <td style="display:none"><a href="javascript:void(0);" onclick="addnewitem()" class="btn btn-sm btn-success">+</a></td>
</tr>

<?php $counter++;  }?>