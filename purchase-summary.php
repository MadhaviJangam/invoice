


<?php

include_once 'include/config.php';
include_once 'include/admin-functions.php';
include_once 'include/classes/CSRF.class.php';
$admin = new AdminFunctions();

if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: index.php");
	exit();
}
$pageName = "Puechase Summary";
$pageURL = 'purchase-summary.php';
$senPageURL  = 'html-pdf/purchase-summary-view.php';

$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);

$results = $admin->getActiveItemDetails();
$getActiveSupplierDetails = $admin->getActiveSupplierDetails();
$item_details = $admin->getActiveItemDetails(); 


if(isset($_POST['print_report'])) {
  $supplier_id = $_POST['supplier_id'];
  $from_date = $_POST['from_date'];
  $to_date = $_POST['to_date'];
  $item_id = $_POST['item_id'];
    // header("location:".$senPageURL."?&supplier_id=".$supplier_id."&from_date=".$from_date."&to_date=".$to_date."&item_id=".$item_id);
   }
 
if(isset($_POST['print_summary'])) {
 $supplier_id = $_POST['supplier_id'];
 $from_date = $_POST['from_date'];
 $to_date = $_POST['to_date'];
    //header("location:".$senPageURL."?&customer_id=".$customer_id."&from_date=".$from_date."&to_date=".$to_date);
  }

 ?>
 
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title> Unique Invoice | <?php echo $pageName; ?></title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <script src="../../plugins/notiflix/notiflix-aio-1.5.0.min.js"></script>

    <!-- Select2 -->
    <link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">

</head>
<body class="hold-transition sidebar-mini layout-footer-fixed">
<!-- Site wrapper -->
<div class="wrapper">
 
  <?php 
  include('include/header.php');
  include('include/sidebar.php');
  ?>
<style>
    label{
        font-size:12px;
    }
    .dataTables_wrapper {
    font-size: 14px;
}
em{
  color:red;
}

.select2-container--default .select2-selection--single {
  border-radius:0px;
}

.dataTables_wrapper {
    font-size: 12px; 
}

    </style>
  <!-- Content Wrapper. Contains page content -->
  <?php if(isset($_GET['registersuccess'])){ ?>
  <script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Success('<?php echo $pageName; ?> successfully Added');
       </script>
<?php } ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1> <?php echo $pageName; ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Transaction</a></li>
              <li class="breadcrumb-item active"> <?php echo $pageName; ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->

    <section class="content" >
    
    <!-- /.content -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><b><?php echo $pageName; ?> Details</b></h3><br>
              </div>
              <div class="card-body">
              <form action = "" method = "POST" name="form" id="form">
                <div class="form-group row">
                    <div class="col-md-3 fromerrorcheck">
                        <label>Purchase From Date</label>
                        <input type="date" class="form-control form-control-sm rounded-0 " name="from_date" id="from_date" value="<?php if(isset($_GET['edit'])) { echo $_GET['from_date']; }else{ echo date("Y-m-d"); } ?>">
                    </div>
                    <div class="col-md-3 fromerrorcheck">
                        <label>Purchase To Date</label><input type="date" class="form-control form-control-sm rounded-0 " name="to_date" id="to_date" value="<?php if(isset($_GET['edit'])) { echo $_GET['to_date']; }else{ echo date("Y-m-d"); } ?>">
                    </div>
                    <div class="col-md-3 fromerrorcheck">
                        <label>Supplier Name</label>
                        <select class="form-control form-control-sm rounded-0 " name="supplier_id" id="supplier_id">
                        <option value="">Select Supplier Name</option>
                        <?php while($row = $admin->fetch($getActiveSupplierDetails)){ ?>
                            <option value="<?php echo $row['id']; ?>" <?php if(isset($_POST['supplier_id']) and $_POST['supplier_id']==$row['id']) { echo 'selected'; } ?>><?php echo $row['supplier_name']; ?>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-3 fromerrorcheck">
                    <label>Item Name</label>
                        <select class="form-control form-control-sm rounded-0 item_id" name="item_id">
                        <option value="">Select Item</option>
                        <?php while($row = $admin->fetch($item_details)){
                        ?>
                        <option value="<?php echo $row['id']; ?>" <?php if(isset($_POST['item_id']) and $_POST['item_id']==$row['id']) { echo 'selected'; } ?>><?php echo $row['item_name']; ?>
                        <?php } ?>
                    </select>
                    </div>
                    <div class="col-md-3 fromerrorcheck"> 
                      <label>Print<em>*</em> </label>
                    <button class="btn btn-sm btn-primary btn-block" name="print_report"  id="print">Print Report</button>
                </div> 
                    <div class="col-md-3 fromerrorcheck"> 
                      <label>Print<em>*</em> </label>
                    <button class="btn btn-sm btn-primary btn-block" name="print_summary"  id="print">Print Summary</button>
                </div> 
                </div> 
                </form>
                </div>
              </div>
              <!-- /.card-header -->
             
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <!-- <div class="float-left d-none d-sm-block">
      <a href="<?php echo $subpageURL; ?>" class="btn btn-primary"> <i class="fas fa-plus-circle"></i> Create New <?php echo $pageName; ?></a>
    </div> -->
    <strong class="float-right">Copyright &copy; 2020-2021 <a href="https://usssoft.com">Unique Software System</a>. All rights reserved.</strong> 
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- jquery-validation -->
<script src="../../plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="../../plugins/jquery-validation/additional-methods.min.js"></script>
<script src="../../dist/js/demo.js"></script>

<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>

<!-- Page specific script -->
<script>
  // $(function () {
  //   $("#example1").DataTable({
  //     "paging": false,
  //     "responsive": true, "lengthChange": false, "autoWidth": false,
  //     "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
  //   }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    
  //   $('#example2').DataTable({
  //     "paging": true,
  //     "lengthChange": false,
  //     "searching": false,
  //     "ordering": true,
  //     "info": true,
  //     "autoWidth": false,
  //     "responsive": true,
  //   });
  // });

  <?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>
      $("#cardeffect").slideDown("slow");
      <?php } ?>
   
$(function () {
 

  


  $('#form').validate({
    rules: {
    ignore: [],
		debug: false,
    
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
    },
    errorElement: 'span',
     errorPlacement: function (error, element) {
       error.addClass('invalid-feedback');
       element.closest('.fromerrorcheck').append(error);
     },
     highlight: function (element, errorClass, validClass) {
       $(element).addClass('is-invalid');
     },
     unhighlight: function (element, errorClass, validClass) {
       $(element).removeClass('is-invalid');
     }
  });
});

// function CustomerLedger(){

//   var from_date=$('#from_date').val();
//   var to_date=$('#to_date').val();
//   var customer_id=$('#customer_id').val();
 
// if(from_date!='' && to_date!='' && customer_id!=''){
//       alert('Select From And To Date And Customer Name');
//     }
  
// }
function clearall(){
  $('input').val('');
  $('textarea').val('');
}

$('.loadstarter').on('click',function(){
      Notiflix.Loading.Init({});
      Notiflix.Loading.Hourglass();
    });
    
    $(function () {
      $('.select2').select2()
    });

</script>
<?php if(isset($_POST['print_report'])){ ?>
  <script>  
  setTimeout(function(){  
  window.open("html-pdf/details-of-purchase-view.php?supplier_id=<?php echo $supplier_id;?>&from_date=<?php echo $from_date;?>&to_date=<?php echo $to_date;?>&item_id=<?php echo $item_id;?>");
  }, 300);
  </script>

  <?php } ?>

<?php if(isset($_POST['print_summary'])){ ?>
  <script>  
  setTimeout(function(){  
  window.open("html-pdf/purchase-summary-view.php?supplier_id=<?php echo $supplier_id;?>&from_date=<?php echo $from_date;?>&to_date=<?php echo $to_date;?>");
  }, 300);
  </script>

  <?php } ?>

</body>
</html>