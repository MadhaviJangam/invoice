<?php

include_once 'include/config.php';
include_once 'include/admin-functions.php';
include_once 'include/classes/CSRF.class.php';
$admin = new AdminFunctions();

if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: index.php");
	exit();
}

$pageName = "Debit Note (Supplier)";
$parentPageURL = 'debit-note.php';
$senPageURL='compose-debit-note.php';
$pageURL = 'debit-note-add.php';
$deleteURL = 'debit-note-add.php';


$item_details = $admin->getActiveItemDetails(); 
$getActiveSupplierDetails = $admin->getActiveSupplierDetails();

 
$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);


if(isset($_POST['register'])){
	if($csrf->check_valid('post')) {
		$result = $admin->addDebitNote($_POST,$loggedInUserDetailsArr['id']);
		header("location:".$parentPageURL."?registersuccess&id=".$result);
     exit();
	}
}
if(isset($_POST['register_send'])){
	if($csrf->check_valid('post')) {
		$result = $admin->addDebitNote($_POST,$loggedInUserDetailsArr['id']);
		header("location:".$senPageURL."?registersuccess&id=".$result);
	exit();
	}
}
if(isset($_GET['edit'])){
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
	$data = $admin->getUniqueDebitNoteDetailsById($id);
}

if(isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['update'])) {
	if($csrf->check_valid('post')) {
		$id = trim($admin->escape_string($admin->strip_all($_POST['id'])));
		$result = $admin->updateDebitNote($_POST, $loggedInUserDetailsArr['id']);
		header("location:".$parentPageURL."?updatesuccess&id=".$result);
		exit();
  }
}

if(isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['update_send'])) {
  if($csrf->check_valid('post')) {
    $id = trim($admin->escape_string($admin->strip_all($_POST['id'])));
    $result = $admin->updateDebitNote($_POST, $loggedInUserDetailsArr['id']);
    header("location:".$senPageURL."?updatesuccess&id=".$id);
    exit();
  }
}

if(isset($_GET['edit'])){
  $getUniqueDebitNoteDetailsByTaxInvoiceId = $admin->getUniqueDebitNoteDetailsByTaxInvoiceId($_GET['id']);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title> Unique Invoice | <?php echo $pageName; ?></title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Select2 -->

  <link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  
  <script src="../../plugins/notiflix/notiflix-aio-1.5.0.min.js"></script>
</head>
<body class="hold-transition sidebar-mini layout-footer-fixed sidebar-collapse" >
<!-- Site wrapper -->
<div class="wrapper">
 
  <?php 
  include('include/header.php');
  include('include/sidebar.php');
  ?>
<style>
    label{
        font-size:11px;
        padding:0px;
        margin:0px;
    }
    .form-control{
       border:1px solid #48544b;

    }
    .dataTables_wrapper {
    font-size: 14px;
}
em{
  color:red;
}
.mytable{
    font-size:14px;
    text-align:center;
}

.table td, .table th{
  padding: .13rem;
  border:1px solid #000;
}
.mytable td input{
    /* padding:1px; */
    text-align:center;
}

.mytable thead tr td{
    background:#ddd;
}
.select2-container .select2-selection--single {
            height: 25px;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            top: 31%;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 25px;
        }
        
    </style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
<br>
    <!-- Main content -->

    <section class="content" style="zoom: 90%;">
    
      <form action="" id="form" method="post" enctype="multipart/form-data" autocomplete="off">
      <!-- Default box -->
   <div id="cardeffect" style="display:none;">
      <div class="card" >
        <div class="card-header">
          <h3 class="card-title"> <?php echo $pageName; ?></h3>
          <a href="<?php echo $parentPageURL; ?>" align="right" style="font-size:14px;color:red;float:right;"> <i class="fas fa-hand-point-left"></i> Back</a>
        </div>
       
        <div class="card-body" style="padding-top:5px;padding-bottom:5px;">
          <div class="form-group row">
            <div class="col-md-2 fromerrorcheck">
              <label>Credit Note No<em>*</em> </label>
              <input type="text" name="debit_note_no" value="<?php if(isset($_GET['edit'])) { echo $data['debit_note_no']; }else{ echo $admin-> getUniuqeDebitNoteNo(); } ?>" class="form-control form-control-sm rounded-0" readonly>
            </div> 
           
            <div class="col-md-2 fromerrorcheck">
              <label>Date<em>*</em> </label>
              <input type="date" name="debit_date" id="debit_date" value="<?php if(isset($_GET['edit'])) { echo $data['debit_date']; }else{ echo date("Y-m-d"); } ?>" class="form-control form-control-sm rounded-0">
            </div> 

            <div class="col-md-2 fromerrorcheck">
              <label>Supplier Name<em>*</em> </label>
              <select class="form-control form-control-sm rounded-0 " onchange="fun_supplier_details(this)" name="supplier_id" id="supplier_id" required>
              <option value="">Select Supplier Name</option>
                <?php while($row = $admin->fetch($getActiveSupplierDetails)){ ?>
                  <option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) and $data['supplier_id']==$row['id']) { echo 'selected'; } ?>><?php echo $row['supplier_name']; ?>
                  <?php } ?>
              </select>
            </div> 
             
            <div class="col-md-2 fromerrorcheck">
            
              <label>Purchase Bill No<em>*</em> </label>
              <select class="form-control form-control-sm rounded-0 " onchange="fun_purchase_item_details(this)"  name="supplier_bill_id" id="supplier_bill_id">
              <option value="">Select Bill No</option>
              <?php if(isset($_GET['edit'])){ ?>
              <?php $supplier_bill_details = $admin->getUniquePurchaseBillDetailsBySupplierId($data['supplier_id']); ?>
                <?php while($row = $admin->fetch($supplier_bill_details)){?>
              <option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) and $data['supplier_bill_id']==$row['id']) { echo 'selected'; } ?>><?php echo $row['supplier_bill_no']; ?>
              <?php } ?>
              <?php } ?>
              </select>
            </div>

            <div class="col-md-2 fromerrorcheck">
              <label>Company GST No<em>*</em> </label>
              <input type="text" name="company_gst_no"
                value="<?php if(isset($_GET['edit'])) { echo $data['company_gst_no']; } ?>"
                class="form-control form-control-sm rounded-0">
            </div>
            <div class="col-md-2 fromerrorcheck">
              <label>Payment Mode<em>*</em> </label>
              <select class="form-control form-control-sm rounded-0" name="transaction_mode" onchange="calc()" id="transaction_mode">
                <option value="Cash" <?php if(isset($_GET['edit']) and $data['transaction_mode']=='Cash') { echo 'selected'; } ?>>Cash</option>
                <option value="Cheque" <?php if(isset($_GET['edit']) and $data['transaction_mode']=='Cheque') { echo 'selected'; } ?>>Cheque</option>
                <option value="Bank Transfer" <?php if(isset($_GET['edit']) and $data['transaction_mode']=='Bank Transfer') { echo 'selected'; } ?>>Bank Transfer</option>
                <option value="Credit Card" <?php if(isset($_GET['edit']) and $data['transaction_mode']=='"Credit Card') { echo 'selected'; } ?>>Credit Card</option>
              </select> 
            </div>
            
            <div class="col-md-2 fromerrorcheck">
              <label>Transaction No<em>*</em> </label>
              <input type="text" name="transaction_no" id="transaction_no" value="<?php if(isset($_GET['edit'])) { echo $data['transaction_no']; }?>" class="form-control form-control-sm rounded-0">
            </div> 
            <div class="col-md-2 fromerrorcheck">
              <label>Deposite To<em>*</em> </label>
              <select class="form-control form-control-sm rounded-0" name="deposite_to" onchange="calc()" id="deposite_to">
                <option value="Petty Cash" <?php if(isset($_GET['edit']) and $data['deposite_to']=='Petty Cash') { echo 'selected'; } ?>>Petty Cash</option>
                <option value="Office Bank"<?php if(isset($_GET['edit']) and $data['deposite_to']=='Office Bank') { echo 'selected'; } ?>>Office Bank</option>
                
              </select> 
            </div> 
            
            <div class="col-md-2 fromerrorcheck">
              <label>#Reference<em>*</em> </label>
              <input type="text" name="reference" id="reference" value="<?php if(isset($_GET['edit'])) { echo $data['reference']; }?>" class="form-control form-control-sm rounded-0">
            </div>
            <div class="col-md-2 fromerrorcheck">
              <label>Bill Amount<em>*</em> </label>
              <input type="text" name="bill_amt" id="bill_amt" value="<?php if(isset($_GET['edit'])) { echo $data['bill_amt']; }?>" class="form-control form-control-sm rounded-0" readonly>
            </div>
            <div class="col-md-2 fromerrorcheck">
              <label>Paid Amount<em>*</em> </label>
              <input type="text" name="paid_amt" id="paid_amt" value="<?php if(isset($_GET['edit'])) { echo $data['paid_amt']; } ?>" class="form-control form-control-sm rounded-0" readonly>
            </div> 
            <div class="col-md-2 fromerrorcheck">
              <label>Pending Amount<em>*</em> </label>
              <input type="text" name="pending_amt" id="pending_amt" value="<?php if(isset($_GET['edit'])) { echo $data['pending_amt']; }?>" class="form-control form-control-sm rounded-0" readonly>
            </div> 
            <div class="col-md-2 fromerrorcheck">
              <label>Payment Now<em>*</em> </label>
              <input type="text" name="payment_now" id="payment_now" value="<?php if(isset($_GET['edit'])) { echo $data['payment_now']; } ?>" class="form-control form-control-sm rounded-0">
            </div> 
            <div class="col-md-5 fromerrorcheck">
              <label>Debit Note & Description<em>*</em> </label>
              <textarea type="text" name="description" class="form-control form-control-sm rounded-0" >
              <?php if(isset($_GET['edit'])) { echo $data['description']; }else{ echo '-'; } ?>     
            </textarea>
            </div> 
          
         
           
            
    
            <div class="col-md-12 " align="center">
            <b>Item Details</b>
            </div>
            
          </div>
          
          <div class=" row">
               <table border="1" class="table mytable">
                <thead>
                <tr>
                <td rowspan="2" width="15%">Item</td>
                <td rowspan="2">Rate</td>
                <td rowspan="2">Qty</td>
                <td rowspan="2">Unit</td>
                <td rowspan="2">Amt</td>
                <td rowspan="2">Disc %</td>
                <td rowspan="2">Disc Amt</td>
                <td rowspan="2">After Disc. Amt</td>
                <td colspan="2">CGST</td>
                <td colspan="2">SGST</td>
                <td colspan="2">IGST</td>
                <td rowspan="2">Net Amt</td>
                <td rowspan="2" style="display:none">Acn</td>
                </tr>
                <tr>
                <td>Rate</td>
                <td>Amt</td>
                <td>Rate</td>
                <td>Amt</td>
                <td>Rate</td>
                <td>Amt</td>
                </tr>
                <thead>
                <tbody>
                <?php
                     if(isset($_GET['edit'])){
                       $counter=0;
                      while($rows = $admin->fetch($getUniqueDebitNoteDetailsByTaxInvoiceId)){ 
                        $item_details1 = $admin->getActiveItemDetails(); 
                        ?>
              <tr>
                <td>
              <select class="form-control form-control-sm rounded-0 item_id" disabled="disabled" onchange="fun_item_details(this)" name="item_id[<?php echo $counter; ?>]">
                  <option value="">Select Item</option>
                  <?php while($row = $admin->fetch($item_details1)){
                   ?>
                  <option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) and $rows['item_id']==$row['id']) { echo 'selected'; } ?>><?php echo $row['item_name']; ?>
                  <?php } ?>
              </select>
               
                </td>
                <td><input type="text" onkeyup="calc()" name="rate[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['rate']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 rate" readonly></td>
                <td><input type="text" onkeyup="calc()" name="qty[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['qty']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 qty" readonly></td>
                <td><input type="text"  onkeyup="calc()" name="unit[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['unit']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 unit" readonly></td>
                <td><input type="text"  onkeyup="calc()" name="amt[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 amt" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="dicountcalc(this)" name="disc_per[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['disc_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 disc_per"  readonly></td>
                <td><input type="text"  onkeyup="dicountcalc1(this)" name="disc_amt[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['disc_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 disc_amt" readonly></td>
                <td><input type="text"  onkeyup="calc()" name="after_disc_amt[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['after_disc_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 after_disc_amt" style="text-align:right"  readonly ></td>
                <td><input type="text"  onkeyup="calc()" name="cgst_per[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['cgst_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 cgst_per" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="cgst_amt[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['cgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 cgst_amt" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="sgst_per[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['sgst_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 sgst_per" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="sgst_amt[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['sgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 sgst_amt" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="igst_per[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['igst_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 igst_per" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="igst_amt[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['igst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 igst_amt"  style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="net_amt[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['net_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 net_amt" style="text-align:right"  readonly></td>
                </tr>

                    <?php $counter++; } }
                    
                    ?>
                <tr>
                <td style="border:1px solid #fff;">Total</td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"><input type="text" style="text-align:right"  id="total_amt" name="total_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_amt'];}?>"  class="form-control form-control-sm rounded-0" readonly></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"><input type="text" style="text-align:right"  id="total_disc_amt" name="total_disc_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_disc_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly></td>
                <td style="border:1px solid #fff;"><input type="text" style="text-align:right"  id="total_after_disc_amt" name="total_after_disc_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_after_disc_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly ></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"><input type="text" style="text-align:right"  id="total_cgst_amt" name="total_cgst_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_cgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"><input type="text" style="text-align:right"  id="total_sgst_amt" name="total_sgst_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_sgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"><input type="text"  style="text-align:right"  id="total_igst_amt" name="total_igst_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_igst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly></td>
                <td style="border:1px solid #fff;"><input type="text"  style="text-align:right" id="total_net_amt" name="total_net_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_net_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly></td>

                </tr>
                </tbody>
               </table>
               </div>
               <div class="form-group row">
               
            <div class="col-md-1 fromerrorcheck">
              <label>Transport Chg.<em>*</em> </label>
              <input type="text" name="service_charges" id="service_charges"  onkeyup="calc1()" style="text-align:center"  value="<?php if(isset($_GET['edit'])) { echo $data['service_charges']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly>
            </div> 
        
            <div class="col-md-1 fromerrorcheck">
              <label>Transport CGST Amt<em>*</em> </label>
              <input type="text" name="service_charges_cgst_amt" id="service_charges_cgst_amt" onkeyup="calc1()" style="text-align:right"  value="<?php if(isset($_GET['edit'])) { echo $data['service_charges_cgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly>
            </div> 

            <div class="col-md-1 fromerrorcheck">
              <label>Transport SGST Amt<em>*</em> </label>
              <input type="text" name="service_charges_sgst_amt" id="service_charges_sgst_amt" onkeyup="calc1()" style="text-align:right"  value="<?php if(isset($_GET['edit'])) { echo $data['service_charges_sgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly >
            </div> 

            <div class="col-md-1 fromerrorcheck">
              <label>Transport IGST Amt<em>*</em> </label>
              <input type="text" name="service_charges_igst_amt" id="service_charges_igst_amt" onkeyup="calc1()" style="text-align:right"  value="<?php if(isset($_GET['edit'])) { echo $data['service_charges_igst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly >
            </div> 
           
            <div class="col-md-3 fromerrorcheck">
              <label>Final Paid Amt<em>*</em> </label>
              <input type="text" name="final_amt" id="final_amt" style="text-align:right" onkeyup="calc1()" value="<?php if(isset($_GET['edit'])) { echo $data['final_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly>
            </div> 
          </div>

        </div>
        <!-- /.card-body -->
        <div class="card-footer">
        <input type="hidden" name="<?php echo $token_id; ?>" value="<?php echo $token_value; ?>" />
        
        <div class="row">
            <?php if(isset($_GET['edit'])){ ?>
                <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id ?>"/>
                <div class="col-sm-4"><button type="submit" name="update" value="update" id="update" class="btn btn-warning btn-block"><i class="fas fa-save"></i> Update <?php echo $pageName; ?></button></div>
                <div class="col-sm-4"><button type="submit" name="update_send" value="update_send" id="update_send" class="btn btn-warning btn-block"><i class="fas fa-save"></i> Update <?php echo $pageName; ?> And Send Email</button></div>

                <?php } else { ?>
                <div class="col-sm-4"><button type="submit" name="register" id="register" class="btn btn-success btn-block"><i class="fas fa-save"></i> Add <?php echo $pageName; ?></button></div>
                <div class="col-sm-4"><button type="submit" name="register_send" id="register_send" class="btn btn-success btn-block" value="<?php echo $id ?>"><i class="fas fa-save"></i> Add <?php echo $pageName; ?> And Send Email</button></div>
            <?php } ?>
            <div class="col-sm-4"><a class="btn btn-danger btn-block" href="javascript:void(0);" onclick="clearall()"><i class="fas fa-broom"></i>Clear All</a></div>
        </div>

        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->
      </form> 
    </section>
   

    <!-- /.content -->
    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- jquery-validation -->
<script src="../../plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="../../plugins/jquery-validation/additional-methods.min.js"></script>
<script src="../../dist/js/demo.js"></script>
<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });

  <?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>
      $("#cardeffect").slideDown("slow");
      <?php } ?>
   
$(function () {
 

  


  $('#form').validate({
    rules: {
      ignore: [],
		  debug: false,
      invoice_id : {
         required: true,
      },
      invoice_date : {
         required: true,
      },
      customer_id : {
         required: true,
      },
      due_date : {
         required: true,
      },
      gst_applicable : {
         required: true,
      },
      service_charges : {
         required: true,
      },
      service_charges_cgst_amt : {
         required: true,
      },
      service_charges_sgst_amt : {
         required: true,
      },
      service_charges_igst_amt : {
         required: true,
      },final_amt : {
         required: true,
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
    },
    errorElement: 'span',
     errorPlacement: function (error, element) {
       error.addClass('invalid-feedback');
       element.closest('.fromerrorcheck').append(error);
     },
     highlight: function (element, errorClass, validClass) {
       $(element).addClass('is-invalid');
     },
     unhighlight: function (element, errorClass, validClass) {
       $(element).removeClass('is-invalid');
     }
  });
});

function sameasbillingaddress(){
    $('#shipping_address').val($('#billing_address').val());
}
function removeitem(e){
 
}


function clearall(){
  $('input').val('');
  $('textarea').val('');
}

$('.loadstarter').on('click',function(){
      Notiflix.Loading.Init({});
      Notiflix.Loading.Hourglass();
    });
   
    $("#cardeffect").slideDown("slow");


 

  $(document).ready(function(){
    $(function () {
      $('.select2').select2()
    });

      //Initialize Select2 Elements
      $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

});
$('#form').on('submit', function() {
    $('input, select').prop('disabled', false);
});
function fun_purchase_item_details(e){
  var supplier_id=$('#supplier_id').val();
  var supplier_bill_id=$('#supplier_bill_id').val();
//   $('.mytable > tbody > tr').not(':last').each(function(){
//   $(this).find('td').each(function(){
//     $(this).remove();
// });

// });
  if(supplier_bill_id!=''){
    $.ajax({
                    type: 'POST',
                    data: 'supplier_bill_id='+supplier_bill_id,
                    url: 'getAjaxPurchaseBillItem.php',
                    success: function (respone) {
                   $(".table>tbody>tr:last").before(respone);
                }
  });
  $.ajax({
                    type: 'POST',
                    data: 'supplier_bill_id='+supplier_bill_id,
                    url: 'getAjaxPurchaseBillTotal.php',
                    success: function (respone) {
                      //$(".table>tbody>tr:last").after(respone);
                      console.log(res);
                      var res = JSON.parse(respone);
                      console.log(res);
                      $('#total_amt').val(res['total_amt']);
                      $('#total_disc_amt').val(res['total_disc_amt']);
                      $('#total_after_disc_amt').val(res['total_after_disc_amt']);
                      $('#total_cgst_amt').val(res['total_cgst_amt']);
                      $('#total_sgst_amt').val(res['total_sgst_amt']);
                      $('#total_igst_amt').val(res['total_igst_amt']);
                      $('#total_net_amt').val(res['total_net_amt']);
                      $('#service_charges_cgst_amt').val(res['service_charges_cgst_amt']);
                      $('#service_charges_sgst_amt').val(res['service_charges_sgst_amt']);
                      $('#service_charges_igst_amt').val(res['service_charges_igst_amt']);
                      $('#final_amt').val(res['final_amt']);
                      $('#service_charges').val(res['service_charges']);
                    

                    
                  }
          });
}


}

function fun_invoice_details(e){
 
}
function fun_supplier_details(e){
  var supplier_id=$('#supplier_id').val();
  var supplier_bill_id=$('#supplier_bill_id').val();
//   $('.mytable > tbody > tr').not(':last').each(function(){
//   $(this).find('td').each(function(){
//     $(this).remove();
// });

// });
  if(supplier_id!=''){
    $.ajax({
              type: 'POST',
              data: 'supplier_bill_id='+supplier_bill_id+'&supplier_id='+supplier_id,
              url: 'getAjaxSupplierBillNo.php',
              success: function (respone) {
              //var res = JSON.parse(respone);
              $('#supplier_bill_id').html(respone);
              }
          });
  }
}
</script>
</body>
</html>