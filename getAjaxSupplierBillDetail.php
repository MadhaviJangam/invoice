<?php 
include_once 'include/admin-functions.php';
$admin = new AdminFunctions();
$supplier_id=$_POST['supplier_id'];
$supplier_invoice_details = $admin->getUniquePurchaseBillNoBySupplierId($supplier_id); 
$counter=0;
$inv_amt=0;
$paid_amt=0;
$balance_amt=0;

while($row = $admin->fetch($supplier_invoice_details)){
    $receivedDetails = $admin ->getUniqueInvoicePaymentSumById($row['id']);
    if($admin->getUniqueDebitNoteDetailsById1($row['supplier_bill_no'])){

        $debitDetails = $admin->getUniqueDebitNoteDetailsById1($row['supplier_bill_no']);
        $inv_amt = $row['final_amt'] - $debitDetails['total_after_disc_amt'];
 
    }else{
        $debitDetails = '';
        
    $inv_amt = $row['final_amt'];
 
    }
    $balance_amt = $inv_amt-$receivedDetails['paid_amt'];

   echo $receivedDetails['paid_amt'];
    if($balance_amt!=0){
?>
<tr>
    <td style="display:none"><input type="text"  name="supplier_bill_id[<?php echo $counter; ?>]" value="<?php echo $row['id'] ?>" class="form-control form-control-sm rounded-0 supplier_bill_id" readonly></td>
    <td><input type="text"  name="supplier_bill_no[<?php echo $counter; ?>]" value="<?php echo $row['supplier_bill_no'] ?>" class="form-control form-control-sm rounded-0 supplier_bill_no" readonly></td>
    <td><input type="text"  name="supplier_date[<?php echo $counter; ?>]" value="<?php echo date("d-m-Y", strtotime($row['supplier_bill_date'])); ?>" class="form-control form-control-sm rounded-0 supplier_date" readonly></td>
    <td><input type="text" style="text-align:right"  name="disc_amt[<?php echo $counter; ?>]" value="<?php if( $debitDetails) { echo $debitDetails['total_disc_amt']; } else { echo '0.00';} ?>" class="form-control form-control-sm rounded-0 disc_amt" readonly></td>
    <td><input type="text" style="text-align:right"  name="after_disc_amt[<?php echo $counter; ?>]" value="<?php if( $debitDetails) {echo $debitDetails['total_after_disc_amt'];  } else { echo '0.00';} ?>" class="form-control form-control-sm rounded-0 after_disc_amt" readonly></td>
    <td><input type="text" style="text-align:right"  name="inv_amt[<?php echo $counter; ?>]" value="<?php echo $inv_amt; ?>" class="form-control form-control-sm rounded-0 inv_amt" readonly></td>
    <td><input type="text" style="text-align:right"  name="paid_amt[<?php echo $counter; ?>]"  value="<?php if($receivedDetails['paid_amt']!='') { echo $receivedDetails['paid_amt']; } else  { echo '0.00';}?>" class="form-control form-control-sm rounded-0 paid_amt" style="text-align:right"  readonly></td>
    <td><input type="text"style="text-align:right"  onkeyup="calc()" name="balance_amt[<?php echo $counter; ?>]" value="<?php echo $balance_amt; ?>" class="form-control form-control-sm rounded-0 balance_amt"  readonly></td>
    <td style="display:none"><input type="text" onkeyup="calc()" name="cal_amt[<?php echo $counter; ?>]" value="" class="form-control form-control-sm rounded-0 cal_amt" style="text-align:right"  ></td>
    <td><input type="text" onkeyup="calc()" name="net_amt[<?php echo $counter; ?>]" value="" class="form-control form-control-sm rounded-0 net_amt" style="text-align:right"  ></td>
    <td><input type="text" onkeyup="calc()" name="tds[<?php echo $counter; ?>]" value="" class="form-control form-control-sm rounded-0 tds" style="text-align:right"  ></td>
    <td style="display:none"><a href="javascript:void(0);" onclick="addnewitem()" class="btn btn-sm btn-success">+</a></td>
</tr>
<?php $counter++; } }

?>