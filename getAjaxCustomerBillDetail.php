<?php 
include_once 'include/admin-functions.php';
$admin = new AdminFunctions();
$customer_id=$_POST['customer_id'];

$customer_invoice_details = $admin->getUniqueTaxInvoiceNoByCustomerID($customer_id); 
$counter=0;
$inv_amt=0;
$recd_amt=0;
$balance_amt=0;
while($row = $admin->fetch($customer_invoice_details)){
    $receivedDetails = $admin ->getUniqueInvoiceReceivedSumById($row['id']);

    if($admin->getUniqueCreditNoteDetailsById1($row['invoice_no'])){
        $creditDetails = $admin->getUniqueCreditNoteDetailsById1($row['invoice_no']);
    
        $inv_amt = $row['final_amt']-$creditDetails['total_after_disc_amt'];
 
    }else{
        $creditDetails = '';
        
    $inv_amt = $row['final_amt'];
 
    }
    $balance_amt = $inv_amt-$receivedDetails['recd_amt'];
    if($balance_amt!=0){
       
?>
<tr>
    <td style="display:none"><input type="text"  name="invoice_id[<?php echo $counter; ?>]" value="<?php echo $row['id'] ?>" class="form-control form-control-sm rounded-0 invoice_id" readonly></td>
    <td><input type="text"  name="invoice_id_no[<?php echo $counter; ?>]" value="<?php echo $row['invoice_no'] ?>" class="form-control form-control-sm rounded-0 invoice_id" readonly></td>
    <td><input type="text"  name="invoice_date[<?php echo $counter; ?>]" value="<?php echo $row['invoice_date']; ?>" class="form-control form-control-sm rounded-0 invoice_date" readonly></td>
    <td><input type="text" style="text-align:right"  name="disc_amt[<?php echo $counter; ?>]" value="<?php if( $creditDetails) { echo $creditDetails['total_disc_amt']; } else { echo '0.00';} ?>" class="form-control form-control-sm rounded-0 disc_amt" readonly></td>
    <td><input type="text" style="text-align:right"  name="after_disc_amt[<?php echo $counter; ?>]" value="<?php if( $creditDetails) {echo $creditDetails['total_after_disc_amt'];  } else { echo '0.00';} ?>" class="form-control form-control-sm rounded-0 after_disc_amt" readonly></td>
    <td><input type="text" style="text-align:right"  name="inv_amt[<?php echo $counter; ?>]" value="<?php echo $inv_amt;  ?>" class="form-control form-control-sm rounded-0 inv_amt" readonly></td>
    <td><input type="text" style="text-align:right"  name="recd_amt[<?php echo $counter; ?>]"  value="<?php if($receivedDetails['recd_amt']!='') { echo $receivedDetails['recd_amt'];} else { echo '0.00';}?>" class="form-control form-control-sm rounded-0 recd_amt" style="text-align:right"  readonly></td>
    <td><input type="text" style="text-align:right"  onkeyup="calc()" name="balance_amt[<?php echo $counter; ?>]" value="<?php echo $balance_amt; ?>" class="form-control form-control-sm rounded-0 balance_amt"  readonly></td>
    <td style="display:none"><input type="text" onkeyup="calc()" name="cal_amt[<?php echo $counter; ?>]" value="" class="form-control form-control-sm rounded-0 cal_amt" style="text-align:right"  ></td>
    <td><input type="text" onkeyup="calc()" name="net_amt[<?php echo $counter; ?>]" value="" class="form-control form-control-sm rounded-0 net_amt" style="text-align:right"  ></td>
    
</tr>
<?php $counter++; } }

?>