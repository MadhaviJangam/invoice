<?php

include_once 'include/config.php';
include_once 'include/admin-functions.php';
include_once 'include/classes/CSRF.class.php';
$admin = new AdminFunctions();

if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: index.php");
	exit();
}

$pageName = "Purchase Bill";
$parentPageURL = 'purchase-bill.php';
$senPageURL='compose-purchase-order.php';
$pageURL = 'purchase-bill-add.php';
$deleteURL = 'purchase-bill-add.php';


$item_details = $admin->getActiveItemDetails(); 
$getActiveSupplierDetails = $admin->getActiveSupplierDetails();

// $item_id=$_POST['item_id'];
// $item_details1111 = $admin->getUniqueItemMasterById($item_id);
// $itemRate=$admin->getActiveItemRateDetails($item_details1111['rate']);


 
$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);


if(isset($_POST['register'])){
	if($csrf->check_valid('post')) {
		$result = $admin->addPurchaseBill($_POST,$loggedInUserDetailsArr['id']);
		header("location:".$parentPageURL."?registersuccess&id=".$result);
	exit();
	}
}
if(isset($_POST['register_send'])){
	if($csrf->check_valid('post')) {
		$result = $admin->addPurchaseBill($_POST,$loggedInUserDetailsArr['id']);
		header("location:".$senPageURL."?registersuccess&id=".$result);
	exit();
	}
}
if(isset($_GET['edit'])){
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
	$data = $admin->getUniquePurchaseBillDetailsById($id);
}

if(isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['update'])) {
	if($csrf->check_valid('post')) {
		$id = trim($admin->escape_string($admin->strip_all($_POST['id'])));
		$result = $admin->updatePurchaseBill($_POST, $loggedInUserDetailsArr['id']);
		header("location:".$parentPageURL."?updatesuccess&id=".$result);
		exit();
  }
}

if(isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['update_send'])) {
  if($csrf->check_valid('post')) {
    $id = trim($admin->escape_string($admin->strip_all($_POST['id'])));
    $result = $admin->updatePurchaseBill($_POST, $loggedInUserDetailsArr['id']);
    header("location:".$senPageURL."?updatesuccess&id=".$id);
    exit();
  }
}

if(isset($_GET['edit'])){
  $getUniquePurchaseBillItemDetailsByTaxInvoiceId = $admin->getUniquePurchaseBillItemDetailsByTaxInvoiceId($_GET['id']);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title> Unique Invoice | <?php echo $pageName; ?></title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- chosen -->
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.min.css">
  <!-- Select2 -->

  <link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  
  <script src="../../plugins/notiflix/notiflix-aio-1.5.0.min.js"></script>
</head>
<body class="hold-transition sidebar-mini layout-footer-fixed sidebar-collapse" >
<!-- Site wrapper -->
<div class="wrapper">
 
  <?php 
  include('include/header.php');
  include('include/sidebar.php');
  ?>
<style>
    label{
        font-size:11px;
        padding:0px;
        margin:0px;
    }
    .form-control{
       border:1px solid #48544b;

    }
    .dataTables_wrapper {
    font-size: 14px;
}
em{
  color:red;
}
.mytable{
    font-size:14px;
    text-align:center;
}

.table td, .table th{
  padding: .13rem;
  border:1px solid #000;
}
.mytable td input{
    /* padding:1px; */
    text-align:center;
}

.mytable thead tr td{
    background:#ddd;
}
.select2-container .select2-selection--single {
            height: 25px;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            top: 31%;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 25px;
        }
        
    </style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
<br>
    <!-- Main content -->

    <section class="content" style="zoom: 90%;">
    
      <form action="" id="form" method="post" enctype="multipart/form-data" autocomplete="off">
      <!-- Default box -->
   <div id="cardeffect" style="display:none;">
      <div class="card" >
        <div class="card-header">
          <h3 class="card-title"> <?php echo $pageName; ?></h3>
          <a href="<?php echo $parentPageURL; ?>" align="right" style="font-size:14px;color:red;float:right;"> <i class="fas fa-hand-point-left"></i> Back</a>
        </div>
       
        <div class="card-body" style="padding-top:5px;padding-bottom:5px;">
          <div class="form-group row">
            <div class="col-md-2 fromerrorcheck">
              <label>Bill No<em>*</em> </label>
              <input type="text" name="supplier_bill_no" value="<?php if(isset($_GET['edit'])) { echo $data['supplier_bill_no']; }else{ echo $admin-> getUniuqePurchaseBillNo(); } ?>" class="form-control form-control-sm rounded-0" readonly>
            </div> 
           
            <div class="col-md-2 fromerrorcheck">
              <label>Date<em>*</em> </label>
              <input type="date" name="supplier_bill_date" id="supplier_bill_date" value="<?php if(isset($_GET['edit'])) { echo $data['supplier_bill_date']; }else{ echo date("Y-m-d"); } ?>" class="form-control form-control-sm rounded-0">
            </div> 

            <div class="col-md-2 fromerrorcheck">
              <label>Supplier Name<em>*</em> </label>
              <select class="form-control form-control-sm rounded-0 " onchange="fun_customer_details(this)" name="supplier_id" id="supplier_id" required>
              <option value="">Select Supplier Name</option>
                <?php while($row = $admin->fetch($getActiveSupplierDetails)){ ?>
                  <option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) and $data['supplier_id']==$row['id']) { echo 'selected'; } ?>><?php echo $row['supplier_name']; ?>
                  <?php } ?>
              </select>
            </div> 
            <div class="col-md-2 fromerrorcheck">
              <label>GST Applicable<em>*</em> </label>
              <select class="form-control form-control-sm rounded-0" name="gst_applicable" onchange="calc()" id="gst_applicable" disabled >
                <option value="2" <?php if(isset($_GET['edit']) and $data['gst_applicable']=='2') { echo 'selected'; } ?> read>CGST/SGST</option>
                <option value="3" <?php if(isset($_GET['edit']) and $data['gst_applicable']=='3') { echo 'selected'; } ?>>IGST</option>
              </select> 
            </div> 
           
            <div class="col-md-2 fromerrorcheck">
              <label>Transport GST % <em>*</em> </label>
              <select class="form-control form-control-sm rounded-0" name="gst_per" onchange="calc()" id="gst_per">
                <option value="0" <?php if(isset($_GET['edit']) and $data['gst_per']=='0') { echo 'selected'; } ?>>None</option>
                <option value="3" <?php if(isset($_GET['edit']) and $data['gst_per']=='3') { echo 'selected'; } ?>>3 %</option>
                <option value="5" <?php if(isset($_GET['edit']) and $data['gst_per']=='5') { echo 'selected'; } ?>>5 %</option>
                <option value="12" <?php if(isset($_GET['edit']) and $data['gst_per']=='12') { echo 'selected'; } ?>>12 %</option>
                <option value="18" <?php if(isset($_GET['edit']) and $data['gst_per']=='18') { echo 'selected'; } ?>>18 %</option>
                <option value="28" <?php if(isset($_GET['edit']) and $data['gst_per']=='28') { echo 'selected'; } ?>>28 %</option>
              </select> 
              </div> 
            <div class="col-md-2 fromerrorcheck">
              <label>Challan No<em>*</em> </label>
              <input type="text" name="delivery_challen_no" value="<?php if(isset($_GET['edit'])) { echo $data['delivery_challen_no']; } ?>" class="form-control form-control-sm rounded-0">
            </div> 

            <div class="col-md-2 fromerrorcheck">
              <label>Challan Date <em>*</em> </label>
              <input type="date" name="delivery_challen_date" value="<?php if(isset($_GET['edit'])) { echo $data['delivery_challen_date']; } ?>" class="form-control form-control-sm rounded-0">
            </div> 
            <div class="col-md-2 fromerrorcheck">
              <label>Purchase Bill No<em>*</em> </label>
              <input type="text" name="purchase_no" value="<?php if(isset($_GET['edit'])) { echo $data['purchase_no']; } ?>" class="form-control form-control-sm rounded-0">
            </div> 

            <div class="col-md-2 fromerrorcheck">
              <label>Purchase Date <em>*</em> </label>
              <input type="date" name="purchase_date" value="<?php if(isset($_GET['edit'])) { echo $data['purchase_date']; } ?>" class="form-control form-control-sm rounded-0">
            </div> 
        
            <div class="col-md-4 fromerrorcheck" style="display:none;">
              <label>Scanner Focuse<em>*</em> </label>
              <input type="text" style="border-bottom:2px solid blue;" class="form-control form-control-sm rounded-0" >
            </div> 

            <div class="col-md-12 " align="center">
            <b>Item Details</b>
            </div>
            
          </div>
          
          <div class=" row">
               <table border="1" class="table mytable">
                <thead>
                <tr>
                <td rowspan="2" width="15%">Item</td>
                <td rowspan="2">Rate</td>
                <td rowspan="2">Qty</td>
                <td rowspan="2">Unit</td>
                <td rowspan="2">Amt</td>
                <td rowspan="2">Disc %</td>
                <td rowspan="2">Disc Amt</td>
                <td rowspan="2">After Disc. Amt</td>
                <td colspan="2">CGST</td>
                <td colspan="2">SGST</td>
                <td colspan="2">IGST</td>
                <td rowspan="2">Net Amt</td>
                <td rowspan="2">Acn</td>
                </tr>
                <tr>
                <td>Rate</td>
                <td>Amt</td>
                <td>Rate</td>
                <td>Amt</td>
                <td>Rate</td>
                <td>Amt</td>
                </tr>
                <thead>
                <tbody>
                <?php
                     if(isset($_GET['edit'])){
                       $counter=0; 
                      while($rows = $admin->fetch($getUniquePurchaseBillItemDetailsByTaxInvoiceId)){ 
                        $item_details1 = $admin->getActiveItemDetails(); 
                        ?>
              <tr>
                <td>
              <select class="form-control form-control-sm rounded-0 item_id chosen" onchange="fun_item_details(this)" name="item_id[<?php echo $counter; ?>]">
                  <option value="">Select Item</option>
                  <?php while($row = $admin->fetch($item_details1)){
                   ?>
                  <option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) and $rows['item_id']==$row['id']) { echo 'selected'; } ?>><?php echo $row['item_name']; ?>
                  <?php } ?>
              </select>
               
                </td>
                <td><input type="text" onkeyup="calc()" name="rate[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['rate']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 rate"></td>
                <td><input type="text" onkeyup="calc()" name="qty[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['qty']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 qty"></td>
                <td><input type="text"  onkeyup="calc()" name="unit[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['unit']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 unit"></td>
                <td><input type="text"  onkeyup="calc()" name="amt[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 amt" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="dicountcalc(this)" name="disc_per[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['disc_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 disc_per"  ></td>
                <td><input type="text"  onkeyup="dicountcalc1(this)" name="disc_amt[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['disc_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 disc_amt" ></td>
                <td><input type="text"  onkeyup="calc()" name="after_disc_amt[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['after_disc_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 after_disc_amt" style="text-align:right"  readonly ></td>
                <td><input type="text"  onkeyup="calc()" name="cgst_per[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['cgst_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 cgst_per" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="cgst_amt[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['cgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 cgst_amt" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="sgst_per[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['sgst_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 sgst_per" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="sgst_amt[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['sgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 sgst_amt" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="igst_per[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['igst_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 igst_per" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="igst_amt[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['igst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 igst_amt"  style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="net_amt[<?php echo $counter; ?>]" value="<?php if(isset($_GET['edit'])) { echo $rows['net_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 net_amt" style="text-align:right"  readonly></td>
                <?php if($counter==0){ ?>
                  <td><a href="javascript:void(0);" onclick="addnewitem()" class="btn btn-sm btn-success">+</a></td>
               <?php }else{ ?>
                <td><a href="javascript:void(0);" onclick="removeitem(this)" class="btn btn-sm btn-danger">-</a></td>

               <?php } ?>
                </tr>

                    <?php  $counter++; }
                    }else{
                    ?>
              <tr>
                <td>
              <select class="form-control form-control-sm rounded-0 item_id chosen" required onchange="fun_item_details(this)" name="item_id[0]">
                  <option value="">Select Item</option>
                  <?php while($row = $admin->fetch($item_details)){
                   ?>
                  <option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) and $data['item_name']==$row['item_name']) { echo 'selected'; } ?>><?php echo $row['item_name']; ?>
                  <?php } ?>
              </select>
               
                </td>
                <td><input type="text" required onkeyup="calc()" name="rate[0]" value="<?php if(isset($_GET['edit'])) { echo $data['rate']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 rate"></td>
                <td><input type="text" required onkeyup="calc()" name="qty[0]" value="<?php if(isset($_GET['edit'])) { echo $data['qty']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 qty"></td>
                <td><input type="text"  onkeyup="calc()" name="unit[0]" value="<?php if(isset($_GET['edit'])) { echo $data['unit']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 unit" readonly></td>
                <td><input type="text"  onkeyup="calc()" name="amt[0]" value="<?php if(isset($_GET['edit'])) { echo $data['amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 amt" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="dicountcalc(this)" name="disc_per[0]" value="<?php if(isset($_GET['edit'])) { echo $data['disc_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 disc_per"  ></td>
                <td><input type="text"  onkeyup="dicountcalc1(this)" name="disc_amt[0]" value="<?php if(isset($_GET['edit'])) { echo $data['disc_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 disc_amt" ></td>
                <td><input type="text"  onkeyup="calc()" name="after_disc_amt[0]" value="<?php if(isset($_GET['edit'])) { echo $data['after_disc_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 after_disc_amt" style="text-align:right"  readonly ></td>
                <td><input type="text"  onkeyup="calc()" name="cgst_per[0]" value="<?php if(isset($_GET['edit'])) { echo $data['cgst_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 cgst_per" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="cgst_amt[0]" value="<?php if(isset($_GET['edit'])) { echo $data['cgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 cgst_amt" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="sgst_per[0]" value="<?php if(isset($_GET['edit'])) { echo $data['sgst_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 sgst_per" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="sgst_amt[0]" value="<?php if(isset($_GET['edit'])) { echo $data['sgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 sgst_amt" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="igst_per[0]" value="<?php if(isset($_GET['edit'])) { echo $data['igst_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 igst_per" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="igst_amt[0]" value="<?php if(isset($_GET['edit'])) { echo $data['igst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 igst_amt"  style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="net_amt[0]" value="<?php if(isset($_GET['edit'])) { echo $data['net_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 net_amt" style="text-align:right"  readonly></td>
                <td><a href="javascript:void(0);" onclick="addnewitem()" class="btn btn-sm btn-success">+</a></td>
                </tr>

                  <?php  }  ?>
               
                  
                <tr>
                <td style="border:1px solid #fff;">Total</td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"><input type="text" style="text-align:right"  id="total_amt" name="total_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"><input type="text" style="text-align:right"  id="total_disc_amt" name="total_disc_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_disc_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly></td>
                <td style="border:1px solid #fff;"><input type="text" style="text-align:right"  id="total_after_disc_amt" name="total_after_disc_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_after_disc_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly ></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"><input type="text" style="text-align:right"  id="total_cgst_amt" name="total_cgst_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_cgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"><input type="text" style="text-align:right"  id="total_sgst_amt" name="total_sgst_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_sgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly></td>
                <td style="border:1px solid #fff;"></td>
                <td style="border:1px solid #fff;"><input type="text"  style="text-align:right"  id="total_igst_amt" name="total_igst_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_igst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly></td>
                <td style="border:1px solid #fff;"><input type="text"  style="text-align:right" id="total_net_amt" name="total_net_amt" value="<?php if(isset($_GET['edit'])) { echo $data['total_net_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly></td>

                </tr>
                </tbody>
               </table>
               </div>
               <div class="form-group row">
               

            <div class="col-md-5 fromerrorcheck">
              <label>Note<em>*</em> </label>
              <textarea type="text" name="terms_of_payment" class="form-control form-control-sm rounded-0" ><?php if(isset($_GET['edit'])) { echo $data['terms_of_payment']; }else{ echo ''; } ?></textarea>
            </div> 

         
            <div class="col-md-1 fromerrorcheck">
              <label>Transport Chg.<em>*</em> </label>
              <input type="text" name="service_charges" id="service_charges"  onkeyup="calc1()" style="text-align:center"  value="<?php if(isset($_GET['edit'])) { echo $data['service_charges']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" >
            </div> 
        
            <div class="col-md-1 fromerrorcheck">
              <label>Transport CGST Amt<em>*</em> </label>
              <input type="text" name="service_charges_cgst_amt" id="service_charges_cgst_amt" onkeyup="calc1()" style="text-align:right"  value="<?php if(isset($_GET['edit'])) { echo $data['service_charges_cgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly>
            </div> 

            <div class="col-md-1 fromerrorcheck">
              <label>Transport SGST Amt<em>*</em> </label>
              <input type="text" name="service_charges_sgst_amt" id="service_charges_sgst_amt" onkeyup="calc1()" style="text-align:right"  value="<?php if(isset($_GET['edit'])) { echo $data['service_charges_sgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly >
            </div> 

            <div class="col-md-1 fromerrorcheck">
              <label>Transport IGST Amt<em>*</em> </label>
              <input type="text" name="service_charges_igst_amt" id="service_charges_igst_amt" onkeyup="calc1()" style="text-align:right"  value="<?php if(isset($_GET['edit'])) { echo $data['service_charges_igst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly >
            </div> 
           
            <div class="col-md-3 fromerrorcheck">
              <label>Final Paid Amt<em>*</em> </label>
              <input type="text" name="final_amt" id="final_amt" style="text-align:right" onkeyup="calc1()" value="<?php if(isset($_GET['edit'])) { echo $data['final_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0" readonly>
            </div> 
          </div>

        </div>
        <!-- /.card-body -->
        <div class="card-footer">
        <input type="hidden" name="<?php echo $token_id; ?>" value="<?php echo $token_value; ?>" />
        
        <div class="row">
            <?php if(isset($_GET['edit'])){ ?>
                <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id ?>"/>
                <div class="col-sm-6"><button type="submit" name="update" value="update" id="update" class="btn btn-warning btn-block"><i class="fas fa-save"></i> Update <?php echo $pageName; ?></button></div>
                <?php } else { ?>
                <div class="col-sm-6"><button type="submit" name="register" id="register" class="btn btn-success btn-block"><i class="fas fa-save"></i> Add <?php echo $pageName; ?></button></div>
               
            <?php } ?>
            <div class="col-sm-6"><a class="btn btn-danger btn-block" href="javascript:void(0);" onclick="clearall()"><i class="fas fa-broom"></i>Clear All</a></div>
        </div>

        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->
      </form> 
    </section>
   

    <!-- /.content -->
    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- jquery-validation -->
<script src="../../plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="../../plugins/jquery-validation/additional-methods.min.js"></script>
<script src="../../dist/js/demo.js"></script>
<!-- chossn -->
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.min.js"></script>
<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>
<!-- Page specific script -->
<script>

jQuery(document).ready(function(){
    
    jQuery(".chosen").chosen();
    
  });

  // $(function () {
  //   $("#example1").DataTable({
  //     "responsive": true, "lengthChange": false, "autoWidth": false,
  //     "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
  //   }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
  //   $('#example2').DataTable({
  //     "paging": true,
  //     "lengthChange": false,
  //     "searching": false,
  //     "ordering": true,
  //     "info": true,
  //     "autoWidth": false,
  //     "responsive": true,
  //   });
  // });

  <?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>
      $("#cardeffect").slideDown("slow");
      <?php } ?>
   
// $(function () {
//   $('#form').validate({
//     rules: {
//       ignore: [],
// 		  debug: false,
//       invoice_no : {
//          required: true,
//       },
//       invoice_date : {
//          required: true,
//       },
//       customer_id : {
//          required: true,
//       },
//       due_date : {
//          required: true,
//       },
//       gst_applicable : {
//          required: true,
//       },
//       service_charges : {
//          required: true,
//       },
//       service_charges_cgst_amt : {
//          required: true,
//       },
//       service_charges_sgst_amt : {
//          required: true,
//       },
//       service_charges_igst_amt : {
//          required: true,
//       },final_amt : {
//          required: true,
//       },
//     },
//     messages: {
//       email: {
//         required: "Please enter a email address",
//         email: "Please enter a vaild email address"
//       },
//     },
//     errorElement: 'span',
//      errorPlacement: function (error, element) {
//        error.addClass('invalid-feedback');
//        element.closest('.fromerrorcheck').append(error);
//      },
//      highlight: function (element, errorClass, validClass) {
//        $(element).addClass('is-invalid');
//      },
//      unhighlight: function (element, errorClass, validClass) {
//        $(element).removeClass('is-invalid');
//      }
//   });
// });

function sameasbillingaddress(){
    $('#shipping_address').val($('#billing_address').val());
}


function removeitem(e){
  var count = 0;
  $(e).parent().parent().remove();
    $('.mytable > tbody > tr').not(':last').each(function(){
        $(this).find('td').each(function(){
           var name = $(this).find('input, select').attr('name');
           if (name){
           name = name.split('[')[0];
           console.log(name);
           $(this).find('input, select').attr('name',name+'['+count+']');
           }
        });
        count++;    
    });
  
  calc();
}

function clearall(){
  $('input').val('');
  $('textarea').val('');
}

$('.loadstarter').on('click',function(){
      Notiflix.Loading.Init({});
      Notiflix.Loading.Hourglass();
    });
   
    $("#cardeffect").slideDown("slow");


  function addnewitem(){
    setTimeout(function(){
    var count=($('table > tbody > tr').length)-1;
    if(count!=35){

          $.ajax({
                    type: 'POST',
                    data: 'count='+count,
                    url: 'getAjaxPurchaseAddItem.php',
                    success: function (services_clone) {
                        $(".table > tbody tr:last").before(services_clone);    
                    }
          });
    }else{
      alert('More Then 30 Itema Not allowed to add in Invoice. More Information contact to customer care');
    }
  },200);
  }

  $(document).ready(function(){
    $(function () {
      $('.select2').select2()
    });

      //Initialize Select2 Elements
      $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

});

function fun_item_details(e){
  var item_id=$(e).val();
  var supplier_id=$('#supplier_id').val();

  if(item_id!=''){
    $.ajax({
                    type: 'POST',
                    data: 'item_id='+item_id+'&supplier_id='+supplier_id,
                    url: 'getAjaxPurchseItemDetails.php',
                    success: function (respone) {
                    var res = JSON.parse(respone);
                    console.log(res);
                      var gst_per = parseFloat(res['gst_percentage']);

                       $(e).parent().parent().find('.unit').val(res['stock_unit']);
                       if(res['rate']==null){
                        res['rate']=0;
                       }
                       $(e).parent().parent().find('.rate').val(res['rate']);
                       calc();
                       var gst_applicable = parseFloat($('#gst_applicable').val());
                       var gst_applicable = parseFloat($('#gst_applicable').val());
                       console.log();
      
      var cgst_per = 0;
      var sgst_per = 0;
      var igst_per = 0;
      if(gst_applicable==2){
      cgst_per=gst_per/2;
      sgst_per=gst_per/2;
      igst_per = 0;
      }else if(gst_applicable==3){
      cgst_per = 0;
      sgst_per = 0;
      igst_per = gst_per;
      }
      $(e).parent().parent().find('.cgst_per').val(cgst_per.toFixed(2));
      $(e).parent().parent().find('.sgst_per').val(sgst_per.toFixed(2));
      $(e).parent().parent().find('.igst_per').val(igst_per.toFixed(2));             
                    }
                });
  }
}

function fun_customer_details(e){
  var supplier_id=$(e).val();
  if(supplier_id!=''){
    $.ajax({
              type: 'POST',
              data: 'supplier_id='+supplier_id,
              url: 'getAjaxSupplierDetails.php',
              success: function (respone) {
              var res = JSON.parse(respone);
              console.log(res);
                if(res['statename']=='Maharashtra'){
                  $("#gst_applicable").val(2).trigger('change');
                }else{
                  $("#gst_applicable").val(3).trigger('change');
                  }
                }
          });
              
  }
}
function calc(){
  var total_amt=0;
  var total_disc_amt=0;
  var total_after_disc_amt=0;
  var total_cgst_amt=0;
  var total_sgst_amt=0;
  var total_igst_amt=0;
  var total_net_amt=0;

  $('.mytable > tbody > tr').not(':last').each(function (){
    var rate = parseFloat($(this).find('.rate').val());
    var qty = parseFloat($(this).find('.qty').val());
    var unit = $(this).find('.unit').val();
    var amt = rate*qty;
    $(this).find('.amt').val(amt.toFixed(2));
    var disc_per = parseFloat($(this).find('.disc_per').val());
    var disc_amt = amt * disc_per/100;
    $(this).find('.disc_amt').val(disc_amt);
    var after_disc_amt = amt-disc_amt;
      $(this).find('.after_disc_amt').val(after_disc_amt);

      
    var cgst_per = parseFloat($(this).find('.cgst_per').val());
    var sgst_per = parseFloat($(this).find('.sgst_per').val());
    var igst_per = parseFloat($(this).find('.igst_per').val());

    var cgst_amt = after_disc_amt * cgst_per/100;
    var sgst_amt = after_disc_amt * sgst_per/100;
    var igst_amt = after_disc_amt * igst_per/100;
      
    $(this).find('.cgst_amt').val(cgst_amt.toFixed(2));
    $(this).find('.sgst_amt').val(sgst_amt.toFixed(2));
    $(this).find('.igst_amt').val(igst_amt.toFixed(2));

    var net_amt = after_disc_amt + cgst_amt + sgst_amt +igst_amt;
    $(this).find('.net_amt').val(net_amt.toFixed(2));
      console.log(total_amt,amt);
total_amt = total_amt + amt;
total_disc_amt = total_disc_amt + disc_amt;
total_after_disc_amt = total_after_disc_amt + after_disc_amt;
total_cgst_amt = total_cgst_amt + cgst_amt;
total_sgst_amt = total_sgst_amt + sgst_amt;
total_igst_amt = total_igst_amt + igst_amt;
total_net_amt = total_net_amt + net_amt;
  })

$('#total_amt').val(total_amt.toFixed(2));
$('#total_disc_amt').val(total_disc_amt.toFixed(2));
$('#total_after_disc_amt').val(total_after_disc_amt.toFixed(2));
$('#total_cgst_amt').val(total_cgst_amt.toFixed(2));
$('#total_sgst_amt').val(total_sgst_amt.toFixed(2));
$('#total_igst_amt').val(total_igst_amt.toFixed(2));
$('#total_net_amt').val(total_net_amt.toFixed(2));
calc1();

}

function dicountcalc(e){
  var value = $(e).val();
  var getinputname = $(e).hasClass('disc_per');
  if(getinputname==true){
    var amt = $(e).parent().parent().find('.amt').val();
    var disc_amt = amt*value/100;
    $(e).parent().parent().find('.disc_amt').val(disc_amt);
  }
  calc();
}

function dicountcalc1(e){
  var value = $(e).val();
  var getinputname = $(e).hasClass('disc_per');

  if(getinputname==false){
    var amt = $(e).parent().parent().find('.amt').val();
    var disc_per=value*100/amt;
   
      $(e).parent().parent().find('.disc_per').val(disc_per);
  }
  calc();
}


function calc1(){

var service_charges = parseFloat($('#service_charges').val());

var total_net_amt = parseFloat($('#total_net_amt').val());

var gst_applicable = parseFloat($('#gst_applicable').val());
      
      var total_cgst_per = 0;
      var total_sgst_per = 0;
      var total_igst_per = 0;
      var gst_per = parseFloat($('#gst_per').val());

      if(gst_applicable==2){
        total_cgst_per=gst_per/2;
        total_sgst_per=gst_per/2;
        total_igst_per = 0;
      }else if(gst_applicable==3){
        total_cgst_per = 0;
        total_sgst_per = 0;
        total_igst_per = gst_per;
      }

    var total_cgst_amt = service_charges * total_cgst_per/100;
    var total_sgst_amt = service_charges * total_sgst_per/100;
    var total_igst_amt = service_charges * total_igst_per/100;
      
    $('#service_charges_cgst_amt').val(total_cgst_amt.toFixed(2));
    $('#service_charges_sgst_amt').val(total_sgst_amt.toFixed(2));
    $('#service_charges_igst_amt').val(total_igst_amt.toFixed(2));
    var final_amt =  total_net_amt +  service_charges + total_cgst_amt + total_sgst_amt + total_igst_amt;
    $('#final_amt').val(final_amt.toFixed(2));
}

</script>
</body>
</html>