<?php
include_once '../include/config.php';
include_once '../include/admin-functions.php';
$admin = new AdminFunctions();



//============================================================+
// File name   : example_006.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 006 for TCPDF class
//               WriteHTML and RTL support
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: WriteHTML and RTL support
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set some language-dependent strings (optional)

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);
// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}
$pdf->SetFont('courierB', 'B',9.3);
// ---------------------------------------------------------

// add a page
$pdf->AddPage();
include_once("../include/tax-invoice-pdf.inc.php"); // $invoiceMsg
include_once("../include/tax-invoice-email.inc.php"); // $invoiceMsg1

// == TEST ==
	// echo $invoiceMsg1; // TEST
		// exit;
	// == TEST ==
// Set some content to print
$html = $invoiceMsg;

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output($_GET['id'].'.pdf', 'I');


$attachment = $pdf->Output($_GET['id'].'.pdf', 'S');

$mail = new PHPMailer();
$mail->IsSMTP();
$mail->Host = "localhost";
$mail->SMTPAuth = true;
$mail->SMTPSecure = "ssl";
//$mail->Port = 587;
$mail->Username = "admin@invoice.usssoft.com";
$mail->Password = "Password@123";
//$mail->SMTPDebug = 2;
$mail->From = "admin@invoice.usssoft.com";
$mail->FromName = SITE_NAME;
$mail->AddAddress($_GET['send_to']);

//$mail->AddReplyTo("mail@mail.com");

$mail->IsHTML(true);
$mail->Subject = SITE_NAME." | ".$_GET['email_subject'];
$mail->Body = $invoiceMsg1;
$mail->addStringAttachment($attachment, $_GET['id'].'.pdf');

if(!$mail->Send())
{
echo "Message could not be sent. <p>";
echo "Mailer Error: " . $mail->ErrorInfo;
exit;
}

// echo "Message has been sent";

//============================================================+
// END OF FILE
//============================================================+
