<?php
include_once '../include/config.php';
include_once '../include/admin-functions.php';
$admin = new AdminFunctions();



//============================================================+
// File name   : example_006.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 006 for TCPDF class
//               WriteHTML and RTL support
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: WriteHTML and RTL support
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

class MYPDF extends TCPDF {
    public function Header() {
        $old_margin = $this->GetLeftMargin();
		$this->SetLeftMargin(4.3);
		$this->SetRightMargin(6);
		$headerData = $this->getHeaderData();
		$this->SetFont('courierB', 'B',10);
        //$this->SetFont('helvetica', '', 10);
        $this->writeHTML($headerData['string']);
    }
    public function GetLeftMargin() {
        return $this->lMargin;
    }
 
}
// create new PDF document
$pdf = new MYPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

//$pdf = new MYPDF('L', PDF_UNIT, 'Legal', true, 'UTF-8', false);
$headerhtml = '
<table border="1" style="width:100%">
<tr>
<td style="text-align:left;font-size:12px;border-top:2px solid #FFF;border-left:2px solid #FFF;" width="70%">Payment (Expense) Statement&nbsp;&nbsp;From Date:'; 

 if($_GET['from_date']!=''){ $headerhtml.= date("d-m-Y",strtotime($_GET['from_date']));}

$headerhtml .= '&nbsp;&nbsp;To Date:';
 if($_GET['to_date']!='') {$headerhtml.= date("d-m-Y",strtotime($_GET['to_date']));}

$headerhtml .= '</td>
<td style="text-align:right;font-size:10px;border-top:2px solid #FFF;border-right:2px solid #FFF;" width="30%">Printed On:';

$headerhtml .=  date("d-m-Y");

$headerhtml .= '</td>
</tr>
	<tr>
		<td align="center"  width="10%"> PAYMENT NO</td>
		<td align="center"  width="10%"> DATE</td>
		<td align="center"  width="30%"> PARTY NAME</td>
		<td align="center" width="10%"> BILL NO</td>
		<td align="center" width="10%"> INV AMT</td>
		<td align="center"  width="10%"> PAID AMT</td>
		<td align="center"  width="10%"> BAL AMT</td>
		<td align="center"  width="10%"> AMOUNT</td>
	</tr> 
</table>';
//$headerhtml = 'HII';
$pdf->setHeaderData($ln='', $lw=0, $ht='', $headerhtml, $tc=array(0,0,0), $lc=array(0,0,0));

// set default header data

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// remove default header/footer
$pdf->setPrintHeader(true);
$pdf->setPrintFooter(true);
// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(0, 14, 5, true);
//your HTML code
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
//$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->SetAutoPageBreak(TRUE, 10);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}
// ---------------------------------------------------------


// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
$pdf->SetFont('courierB', 'B',8);
//$pdf->SetFont('helvetica', '', 9, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();
include_once("../include/payment-details-pdf.php"); // $invoiceMsg
// == TEST ==
		// echo $invoiceMsg; // TEST
		// exit;
	// == TEST ==
// Set some content to print
$html = $invoiceMsg;

// Print text using writeHTMLCell()

//$htmlb = '<span style="background-color:yellow;color:blue;">&nbsp;PAGE 2&nbsp;</span>';

$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


//Close and output PDF document
$pdf->Output('example_006.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
