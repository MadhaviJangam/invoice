<?php

include_once 'include/config.php';
include_once 'include/admin-functions.php';
include_once 'include/classes/CSRF.class.php';
$admin = new AdminFunctions();

if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: index.php");
	exit();
}


$pageName = "Payment (Expense)";
$pageURL = 'payment-add.php';
$deleteURL = 'payment-add.php';
$tableName = 'customer_master';

$getstateDetails = $admin->getStateMasterByName();
$getExpenseHeadDetails = $admin->getExpenseHeadDetails($account_head);

$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);

$results = $admin->query("SELECT * FROM ui_customer_master WHERE deleted_time IS NULL GROUP BY id DESC");

if(isset($_GET['delId']) && !empty($_GET['delId'])){
	$id = $admin->escape_string($admin->strip_all($_GET['delId']));
	$delete = $admin->query("UPDATE ".PREFIX.$tableName." SET `deleted_by`=".$loggedInUserDetailsArr['id'].",`deleted_time`='".CURRENTMILIS."' WHERE id='".$id."'");
	header("location:".$pageURL."?deletesuccess");
	exit();
}

if(isset($_POST['register'])){
	if($csrf->check_valid('post')) {
		$result = $admin->addCustomerMaster($_POST,$loggedInUserDetailsArr['id']);
		header("location:".$pageURL."?registersuccess");
	exit();
	}
}

if(isset($_GET['edit'])){
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
	$data = $admin->getUniqueCustomerMasterById($id);
}

if(isset($_POST['id']) && !empty($_POST['id'])) {
	if($csrf->check_valid('post')) {
		$id = trim($admin->escape_string($admin->strip_all($_POST['id'])));
		$result = $admin->updateCustomerMaster($_POST, $loggedInUserDetailsArr['id']);
		header("location:".$pageURL."?updatesuccess");
		exit();
	}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title> Unique Invoice | <?php echo $pageName; ?></title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <script src="../../plugins/notiflix/notiflix-aio-1.5.0.min.js"></script>
</head>
<body class="hold-transition sidebar-mini layout-footer-fixed">
<!-- Site wrapper -->
<div class="wrapper">
 
  <?php 
  include('include/header.php');
  include('include/sidebar.php');
  ?>
<style>
    label{
        font-size:12px;
    }
    .dataTables_wrapper {
    font-size: 14px;
}
em{
  color:red;
}
    </style>
  <!-- Content Wrapper. Contains page content -->
  <?php if(isset($_GET['registersuccess'])){ ?>
  <script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Success('<?php echo $pageName; ?> successfully Added');
       </script>
<?php } ?>



<?php if(isset($_GET['updatesuccess'])){ ?>

<script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Warning('<?php echo $pageName; ?> successfully updated');
       </script>
<?php } ?>


<?php if(isset($_GET['deletesuccess'])){ ?>
<script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Failure('<?php echo $pageName; ?> successfully deleted');
       </script>

<?php } ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1> <?php echo $pageName; ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Master</a></li>
              <li class="breadcrumb-item active"> <?php echo $pageName; ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->

    <section class="content" >
    <?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>
      <form action="" id="form" method="post" enctype="multipart/form-data" autocomplete="off">
      <!-- Default box -->
   <div id="cardeffect" style="display:none;">
      <div class="card" >
        <div class="card-header">
          <h3 class="card-title"> <?php echo $pageName; ?></h3>
          <a href="<?php echo $pageURL; ?>" align="right" style="font-size:14px;color:red;float:right;"> <i class="fas fa-hand-point-left"></i> Back</a>
        </div>
       
        <div class="card-body">
          <div class="form-group row">
          <div class="col-md-3 fromerrorcheck">
              <label>Payment No<em>*</em> </label>
              <input type="text" name="credit_no" value="<?php if(isset($_GET['edit'])) { echo $data['credit_no']; }else{ echo $admin-> getUniuqeCreditNoteNo(); } ?>" class="form-control form-control-sm rounded-0" readonly>
            </div> 
            <div class="col-md-3 fromerrorcheck">
              <label>Payment Date<em>*</em> </label>
              <input type="date" name="credit_date" id="credit_date" value="<?php if(isset($_GET['edit'])) { echo $data['credit_date']; }else{ echo date("Y-m-d"); } ?>" class="form-control form-control-sm rounded-0">
            </div> 

            <div class="col-md-3 fromerrorcheck">
              <label>Payment Mode<em>*</em> </label>
              <select class="form-control form-control-sm rounded-0" name="transaction_mode" onchange="calc()" id="transaction_mode">
                <option value="Cash" <?php if(isset($_GET['edit']) and $data['transaction_mode']=='Cash') { echo 'selected'; } ?>>Cash</option>
                <option value="NEFT" <?php if(isset($_GET['edit']) and $data['transaction_mode']=='Cheque') { echo 'selected'; } ?>>NEFT</option>
                <option value="RTGS" <?php if(isset($_GET['edit']) and $data['transaction_mode']=='Bank Transfer') { echo 'selected'; } ?>>RTGS</option>
                <option value="Cheque" <?php if(isset($_GET['edit']) and $data['transaction_mode']=='"Credit Card') { echo 'selected'; } ?>>Cheque</option>
              </select> 
            </div>
            <div class="col-md-3 fromerrorcheck">
              <label>Transaction No<em>*</em> </label>
              <input type="text" name="transaction_no" id="transaction_no" value="<?php if(isset($_GET['edit'])) { echo $data['transaction_no']; }?>" class="form-control form-control-sm rounded-0">
            </div> 
            
            <div class="col-md-4 fromerrorcheck">
              <label>Account Head<em>*</em> </label>
              <select class="form-control form-control-sm rounded-0" name="account_head" onchange="calc()" id="account_head">
                  <?php while($row = $admin->fetch($getExpenseHeadDetails)){ ?>
                  <option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) and $data['account_head']==$row['id']) { echo 'selected'; } ?>><?php echo $row['description']; ?>
                  <?php } ?>
              </select>
            </div>

            <div class="col-md-4 fromerrorcheck">
              <label>#Reference<em>*</em> </label>
              <input type="text" name="reference" id="reference" value="<?php if(isset($_GET['edit'])) { echo $data['reference']; }?>" class="form-control form-control-sm rounded-0">
            </div>

            <div class="col-md-4 fromerrorcheck">
              <label>Payment Now<em>*</em> </label>
              <input type="text" name="payment_now" id="payment_now" value="<?php if(isset($_GET['edit'])) { echo $data['payment_now']; } ?>" class="form-control form-control-sm rounded-0">
            </div>
           <div class="col">
              <input type="checkbox" id="vehicle1" name="vehicle1" value="vehicle1">
              <label for="against_ref"> Against Bill</label><br>
              <input type="checkbox" id="Advance" name="Advance" value="Advance">
              <label for="Advance">Advance</label><br>
              <input type="checkbox" id="On Account" name="On Account" value="On Account">
              <label for="On Account"> On Account</label><br>
           </div>
          </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
        <input type="hidden" name="<?php echo $token_id; ?>" value="<?php echo $token_value; ?>" />

        <?php if(isset($_GET['edit'])){ ?>

            <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id ?>"/>

            <button type="submit" name="update" value="update" id="update" class="btn btn-warning"><i class="fas fa-save"></i> Update <?php echo $pageName; ?></button>

        <?php } else { ?>

            <button type="submit" name="register" id="register" class="btn btn-success"><i class="fas fa-save"></i> Add <?php echo $pageName; ?></button>

        <?php } ?>
        <a class="btn btn-danger" href="javascript:void(0);" onclick="clearall()"><i class="fas fa-broom"></i>Clear All</a>

        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->
      </form> 
    </section>
   
    <?php } ?>
    <!-- /.content -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><?php echo $pageName; ?> Details</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Sr. No.</th>
                    <th>Payment No</th>
                    <th>Payment Date</th>
                    <th>Company Email</th>
                    <th>Company PAN</th>
                    <th>Company GST</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                 <?php $x=1; while($row = $admin->fetch($results)){  ?>
                  <tr>
                    <td><?php echo $x; ?></td>
                    <td><?php echo $row['customer_name']; ?></td>
                    <td><?php echo $row['company_phone']; ?></td>
                    <td><?php echo $row['company_email']; ?></td>
                    <td><?php echo $row['company_pan']; ?></td>
                    <td> <?php echo $row['company_gst']; ?></td>
                    <td><div class="badge badge-<?php echo $row['active'] == '1'?'success':'danger'; ?> ml-2"><?php echo $row['active'] == '1'?'Active':'Inactive'; ?></div></td>
                    <td class="project-actions text-center">
                          <!-- <a class="btn btn-primary btn-sm" href="#">
                              <i class="fas fa-folder">
                              </i>
                              View
                          </a> -->
                          
                          <a class="btn btn-info btn-sm" href="<?php echo $pageURL; ?>?edit&id=<?php echo $row['id']; ?>">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a class="btn btn-danger btn-sm"  href="<?php echo $pageURL; ?>?delId=<?php echo $row['id']; ?>" onclick="return confirm('Are you sure you want to delete?');" >
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                      </td>                
                  </tr>
                  <?php $x++; } ?>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="float-left d-none d-sm-block">
      <a href="<?php echo $pageURL; ?>?add" class="btn btn-primary"> <i class="fas fa-plus-circle"></i> Create New <?php echo $pageName; ?></a>
    </div>
    <strong class="float-right">Copyright &copy; 2020-2021 <a href="https://usssoft.com">Unique Software System</a>. All rights reserved.</strong> 
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- jquery-validation -->
<script src="../../plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="../../plugins/jquery-validation/additional-methods.min.js"></script>
<script src="../../dist/js/demo.js"></script>
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });

  <?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>
      $("#cardeffect").slideDown("slow");
      <?php } ?>
   
$(function () {
 

  


  $('#form').validate({
    rules: {
      ignore: [],
		  debug: false,
      customer_name : {
         required: true,
      },
      company_phone : {
         required: true,
      },
      company_pan : {
         required: true,
      },
      company_email : {
         required: true,
      },
      company_gst : {
         required: true,
      },
      billing_address : {
         required: true,
      },
      shipping_address : {
         required: true,
      },
      statename: {
         required: true,
      },
      credit_days: {
         required: true,
      },
      contact_person_name : {
         required: true,
      },
      contact_person_phone : {
         required: true,
      },
      contact_person_whatsapp : {
         required: true,
      },
      contact_person_email : {
         required: true,
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
    },
    errorElement: 'span',
     errorPlacement: function (error, element) {
       error.addClass('invalid-feedback');
       element.closest('.fromerrorcheck').append(error);
     },
     highlight: function (element, errorClass, validClass) {
       $(element).addClass('is-invalid');
     },
     unhighlight: function (element, errorClass, validClass) {
       $(element).removeClass('is-invalid');
     }
  });
});

function sameasbillingaddress(){
    $('#shipping_address').val($('#billing_address').val());
}

function clearall(){
  $('input').val('');
  $('textarea').val('');
}

$('.loadstarter').on('click',function(){
      Notiflix.Loading.Init({});
      Notiflix.Loading.Hourglass();
    });
    
</script>

</body>
</html>