<?php

include_once 'include/config.php';
include_once 'include/admin-functions.php';
include_once 'include/classes/CSRF.class.php';
$admin = new AdminFunctions();

//    $debitDetails=$admin->getUniqueDebitNoteDetailsById($_GET['id']);
//    $supplierDetails=$admin->getUniqueSupplierMasterById($debitDetails['supplier_id']);
//    $getStateDetails=$admin->getStateCodeMasterByName($supplierDetails['statename']);
//    $getItemDetailTOTAL= $admin->getUniqueDebitNoteDetailsById($_GET['id']);
//    $getDebitItemDetails=$admin->getUniqueDebitNoteDetailsByTaxInvoiceId($_GET['id']);
//    $rowcount=mysqli_num_rows($getDebitItemDetails);
//    $getItemPerDetails=$admin->getUniqueDebitNoteDetailsByTaxInvoiceId($_GET['id']);
    //$companyInfo=$admin->getUniqueCompanyMasterById();

//    $arrayHSNcode=array();

// if ($debitDetails['debit_date'] == '0000-00-00') {
//    $debit_date = '';
// } else {
//     $debit_date = date("d-m-Y", strtotime($debitDetails['debit_date']));
// }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   
   <body>
   <table border="0" style="width:100%">
   <tr> 
            <td style="text-align:center;">Debit Note</td>
        </tr>
        
       
        <tr style="font-size:7px;">
            <td rowspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="4%">SR.NO</td>
            <td  rowspan="2" style="text-align:left;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="15%">DESCRIPTION OF GOODS AND SERVICE</td>
            <td   rowspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%">HSN CODE</td>
            <td  rowspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="5%">QTY</td>
            <td rowspan="2" style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="8%">RATE</td>
            <td rowspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="8%">UNIT</td>
            <td rowspan="2"style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="14%">TAXABLE VALUE </td>
            <td style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="12%">CGST</td>  
            <td style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="12%">SGST </td>  
            <td style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="12%">IGST </td>  
        </tr>
        <tr style="font-size:7px;">
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="5%">Rate</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="7%">Amount</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="5%">Rate</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="7%">Amount</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="5%">Rate</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="7%">Amount</td>
        </tr>
       
      </table>
   </body>
</html>
