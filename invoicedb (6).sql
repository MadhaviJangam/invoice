-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2021 at 12:37 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `invoicedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `company_info`
--

CREATE TABLE `company_info` (
  `id` int(11) NOT NULL,
  `company_name` varchar(300) DEFAULT NULL,
  `company_address` varchar(100) DEFAULT NULL,
  `factory_address` varchar(300) DEFAULT NULL,
  `gst_no` varchar(20) DEFAULT NULL,
  `state_code` int(11) NOT NULL DEFAULT 27,
  `mobile_no` varchar(20) DEFAULT NULL,
  `email_id` varchar(50) DEFAULT NULL,
  `bank_name` varchar(100) DEFAULT NULL,
  `bank_address` varchar(200) DEFAULT NULL,
  `account_no` varchar(20) DEFAULT NULL,
  `ifsc_code` varchar(30) DEFAULT NULL,
  `subject_to` varchar(50) DEFAULT NULL,
  `terms_and_condition` varchar(500) DEFAULT NULL,
  `designation` varchar(400) DEFAULT NULL,
  `terms_of_payment` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `company_info`
--

INSERT INTO `company_info` (`id`, `company_name`, `company_address`, `factory_address`, `gst_no`, `state_code`, `mobile_no`, `email_id`, `bank_name`, `bank_address`, `account_no`, `ifsc_code`, `subject_to`, `terms_and_condition`, `designation`, `terms_of_payment`) VALUES
(1, 'SSB Metaform', 'Office: J-103, Sector-3, Airoli, Navi Mumbai-400708.', 'SSB Metaform, Gala No. 6, Aaima Industrial Complex, Behind Quarter Deck Restaurent,\r\n\r\nTungareshwar Phata, Village-Sativali, Vasai (E) Dist.: Palghar-410208.', '27CDTPM2043M1Z0', 27, '9324262881', 'info@ssbmetaform.com,ssbmetaform@gmail.com', 'IDBI BANK LTD.\r\n', 'Parsik Nagar, Kalwa, Thane, Maharashtra-400 605.\r\n', '1609102000006965', 'IBKL0001609', 'Navi Mumbai', '1. Actual will be measured and billed.\r\n2. 30% advance.\r\n3. 60% during Execution.\r\n4. 10% after completion within 30 days.\r\n5. Above rates are only up to the height of 13\'.\r\n6. Above rates are only valid up to 6 months fron the date of issue.', 'Sales Manager', '1. Actual will be measured and billed.\r\n2. 30% advance.\r\n3. 60% during Execution.\r\n4. 10% after completion within 30 days.\r\n5. Above rates are only up to the height of 13\'.\r\n6. Above rates are only valid up to 6 months fron the date of issue.');

-- --------------------------------------------------------

--
-- Table structure for table `mstates`
--

CREATE TABLE `mstates` (
  `id` int(11) NOT NULL,
  `statecode` varchar(10) NOT NULL,
  `statename` varchar(40) NOT NULL,
  `stateigst` varchar(3) NOT NULL,
  `created_by` varchar(120) NOT NULL,
  `status` varchar(120) NOT NULL,
  `approved_by` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstates`
--

INSERT INTO `mstates` (`id`, `statecode`, `statename`, `stateigst`, `created_by`, `status`, `approved_by`) VALUES
(1, '1', 'Jammu and Kashmir', 'Y', 'Santosh', 'Created', 'Santosh'),
(2, '10', 'Bihar', 'Y', 'Santosh', 'Created', 'Santosh'),
(3, '11', 'Sikkim', 'Y', 'Santosh', 'Created', 'Santosh'),
(4, '12', 'Arunachal Pradesh', 'Y', 'Santosh', 'Created', 'Santosh'),
(5, '13', 'Nagaland', 'Y', 'Santosh', 'Created', 'Santosh'),
(6, '14', 'Manipur', 'Y', 'Santosh', 'Created', 'Santosh'),
(7, '15', 'Mizoram', 'Y', 'Santosh', 'Created', 'Santosh'),
(8, '16', 'Tripura', 'Y', 'Santosh', 'Created', 'Santosh'),
(9, '17', 'Meghalaya', 'Y', 'Santosh', 'Created', 'Santosh'),
(10, '18', 'Assam', 'Y', 'Santosh', 'Created', 'Santosh'),
(11, '19', 'West Bengal', 'Y', 'Santosh', 'Created', 'Santosh'),
(12, '2', 'Himachal Pradesh', 'Y', 'Santosh', 'Created', 'Santosh'),
(13, '20', 'Jharkhand', 'Y', 'Santosh', 'Created', 'Santosh'),
(14, '21', 'Orissa', 'Y', 'Santosh', 'Created', 'Santosh'),
(15, '22', 'Chhattisgarh', 'Y', 'Santosh', 'Created', 'Santosh'),
(16, '23', 'Madhya Pradesh', 'Y', 'Santosh', 'Created', 'Santosh'),
(17, '24', 'Gujarat', 'Y', 'Santosh', 'Created', 'Santosh'),
(18, '25', 'Daman and Diu', 'Y', 'Santosh', 'Created', 'Santosh'),
(19, '26', 'Dadra and Nagar Haveli', 'Y', 'Santosh', 'Created', 'Santosh'),
(20, '27', 'Maharashtra', 'N', 'Santosh', 'Created', 'Santosh'),
(21, '28', 'Andhra Pradesh', 'Y', 'Santosh', 'Created', 'Santosh'),
(22, '29', 'Karnataka', 'Y', 'Santosh', 'Created', 'Santosh'),
(23, '3', 'Punjab', 'Y', 'Santosh', 'Created', 'Santosh'),
(24, '30', 'Goa', 'Y', 'Santosh', 'Created', 'Santosh'),
(25, '31', 'Lakshadweep', 'Y', 'Santosh', 'Created', 'Santosh'),
(26, '32', 'Kerala', 'Y', 'Santosh', 'Created', 'Santosh'),
(27, '33', 'Tamil Nadu', 'Y', 'Santosh', 'Created', 'Santosh'),
(28, '34', 'Puducherry', 'Y', 'Santosh', 'Created', 'Santosh'),
(29, '35', 'Andaman and Nicobar Islands', 'Y', 'Santosh', 'Created', 'Santosh'),
(30, '4', 'Chandigarh', 'Y', 'Santosh', 'Created', 'Santosh');

-- --------------------------------------------------------

--
-- Table structure for table `ui_admin`
--

CREATE TABLE `ui_admin` (
  `id` int(100) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `user_role` varchar(10) DEFAULT NULL,
  `permissions` text DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `last_modified` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `ui_admin`
--

INSERT INTO `ui_admin` (`id`, `fname`, `lname`, `username`, `email`, `mobile`, `password`, `user_role`, `permissions`, `active`, `last_modified`, `created`) VALUES
(1, 'Sameer', 'Bayani', 'Sameer Bayani', 'admin@gmail.com', '9975701724', '$2y$10$JbepV9KNDeLneNqOeqQKh.AW57VeGIZcDUrM/u9FxI0HkAX6aOzZq', 'super', NULL, 1, '2020-12-30 12:07:51', '2020-12-30 05:04:48');

-- --------------------------------------------------------

--
-- Table structure for table `ui_credit_note_item_transaction`
--

CREATE TABLE `ui_credit_note_item_transaction` (
  `id` int(11) NOT NULL,
  `tit_id` int(11) NOT NULL DEFAULT 0,
  `credit_no` varchar(400) DEFAULT NULL,
  `item_id` int(11) NOT NULL DEFAULT 0,
  `rate` double(11,2) NOT NULL DEFAULT 0.00,
  `qty` int(11) NOT NULL DEFAULT 0,
  `unit` varchar(100) DEFAULT NULL,
  `amt` double(11,2) NOT NULL DEFAULT 0.00,
  `disc_per` double(11,2) NOT NULL DEFAULT 0.00,
  `disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `after_disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `cgst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `cgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `sgst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `sgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `igst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `igst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `net_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` date DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` date DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ui_credit_note_transaction`
--

CREATE TABLE `ui_credit_note_transaction` (
  `id` int(11) NOT NULL,
  `int_invoice_on` varchar(100) DEFAULT NULL,
  `credit_no` varchar(400) DEFAULT NULL,
  `credit_date` date DEFAULT NULL,
  `customer_id` int(11) NOT NULL DEFAULT 0,
  `invoice_id` varchar(400) DEFAULT NULL,
  `company_gst_no` varchar(100) DEFAULT NULL,
  `total_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_after_disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_cgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_sgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_igst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_net_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `service_charges` double(11,2) NOT NULL DEFAULT 0.00,
  `final_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `terms_of_payment` varchar(500) DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` date DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` date DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` date DEFAULT NULL,
  `service_charges_cgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `service_charges_sgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `service_charges_igst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `description` int(11) DEFAULT NULL,
  `gst_per` double(11,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ui_customer_master`
--

CREATE TABLE `ui_customer_master` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(100) DEFAULT NULL,
  `company_phone` varchar(20) DEFAULT NULL,
  `company_pan` varchar(20) DEFAULT NULL,
  `company_email` varchar(50) DEFAULT NULL,
  `company_gst` varchar(20) DEFAULT NULL,
  `billing_address` text DEFAULT NULL,
  `shipping_address` text DEFAULT NULL,
  `contact_person_name` varchar(100) DEFAULT NULL,
  `contact_person_phone` varchar(20) DEFAULT NULL,
  `contact_person_whatsapp` varchar(20) DEFAULT NULL,
  `contact_person_email` varchar(50) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` tinyint(4) NOT NULL DEFAULT 0,
  `created_time` datetime DEFAULT NULL,
  `updated_by` tinyint(4) NOT NULL DEFAULT 0,
  `updated_time` datetime DEFAULT NULL,
  `deleted_by` tinyint(4) NOT NULL DEFAULT 0,
  `deleted_time` datetime DEFAULT NULL,
  `statename` varchar(500) DEFAULT NULL,
  `credit_days` int(3) NOT NULL DEFAULT 0,
  `sales_person_name` varchar(100) DEFAULT NULL,
  `opening_balance` double(11,2) NOT NULL DEFAULT 0.00,
  `date_of_opening` date DEFAULT NULL,
  `fssai_no` varchar(100) DEFAULT NULL,
  `area_pincode` varchar(6) DEFAULT NULL,
  `upload_document` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ui_customer_master`
--

INSERT INTO `ui_customer_master` (`id`, `customer_name`, `company_phone`, `company_pan`, `company_email`, `company_gst`, `billing_address`, `shipping_address`, `contact_person_name`, `contact_person_phone`, `contact_person_whatsapp`, `contact_person_email`, `active`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`, `statename`, `credit_days`, `sales_person_name`, `opening_balance`, `date_of_opening`, `fssai_no`, `area_pincode`, `upload_document`) VALUES
(1, 'MAP FILTERS INDIA PVT. LTD. ', '', '', '', '27AAMCM3669A1ZD', 'UNIT NO.10, ROOP NAVAL\r\nIND. EST. FATHER WADI,\r\nVASAI EAST-401208.', 'UNIT NO.10, ROOP NAVAL\r\nIND. EST. FATHER WADI,\r\nVASAI EAST-401208.\r\n', '', '', '', '', 1, 1, '2021-04-02 18:29:16', 1, '2021-04-06 13:38:45', 0, NULL, 'Maharashtra', 0, '', 0.00, '0000-00-00', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ui_customer_rate_master`
--

CREATE TABLE `ui_customer_rate_master` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT 0,
  `item_id` int(11) NOT NULL DEFAULT 0,
  `rate` double(11,2) NOT NULL DEFAULT 0.00,
  `challan_rate` float(11,2) NOT NULL DEFAULT 0.00,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ui_customer_rate_master`
--

INSERT INTO `ui_customer_rate_master` (`id`, `customer_id`, `item_id`, `rate`, `challan_rate`, `created_by`, `created_time`) VALUES
(314, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:33:30'),
(315, 1, 0, 0.00, 0.00, 1, '2021-04-06 15:33:30');

-- --------------------------------------------------------

--
-- Table structure for table `ui_customer_rate_master_history`
--

CREATE TABLE `ui_customer_rate_master_history` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT 0,
  `item_id` int(11) NOT NULL DEFAULT 0,
  `rate` double(11,2) NOT NULL DEFAULT 0.00,
  `challan_rate` double(11,2) NOT NULL DEFAULT 0.00,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ui_customer_rate_master_history`
--

INSERT INTO `ui_customer_rate_master_history` (`id`, `customer_id`, `item_id`, `rate`, `challan_rate`, `created_by`, `created_time`) VALUES
(259, 1, 2, 5.00, 0.00, 1, '2021-04-03 13:30:06'),
(260, 1, 2, 5.00, 0.00, 1, '2021-04-05 12:10:01'),
(261, 1, 2, 5.00, 0.00, 1, '2021-04-05 12:11:58'),
(262, 1, 2, 5.00, 0.00, 1, '2021-04-05 12:18:13'),
(263, 1, 2, 5.00, 0.00, 1, '2021-04-05 12:20:23'),
(264, 1, 0, 0.00, 0.00, 1, '2021-04-05 12:20:23'),
(265, 1, 2, 5.00, 0.00, 1, '2021-04-05 12:21:42'),
(266, 1, 0, 0.00, 0.00, 1, '2021-04-05 12:21:42'),
(267, 1, 2, 5.00, 0.00, 1, '2021-04-05 12:22:02'),
(268, 1, 2, 5.00, 0.00, 1, '2021-04-05 12:22:02'),
(269, 1, 2, 5.00, 0.00, 1, '2021-04-05 14:37:58'),
(270, 1, 2, 5.00, 0.00, 1, '2021-04-06 12:24:45'),
(271, 1, 2, 5.00, 0.00, 1, '2021-04-06 12:24:45'),
(272, 1, 0, 0.00, 0.00, 1, '2021-04-06 12:24:45'),
(273, 1, 2, 5.00, 0.00, 1, '2021-04-06 12:28:02'),
(274, 1, 2, 5.00, 0.00, 1, '2021-04-06 12:28:02'),
(275, 1, 0, 0.00, 0.00, 1, '2021-04-06 12:28:02'),
(276, 1, 2, 5.00, 0.00, 1, '2021-04-06 12:35:39'),
(277, 1, 2, 5.00, 0.00, 1, '2021-04-06 12:35:39'),
(278, 1, 2, 5.00, 0.00, 1, '2021-04-06 12:35:39'),
(279, 1, 2, 45.00, 0.00, 1, '2021-04-06 13:42:40'),
(280, 1, 2, 45.00, 0.00, 1, '2021-04-06 13:42:40'),
(281, 1, 2, 45.00, 0.00, 1, '2021-04-06 13:42:40'),
(282, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:06:48'),
(283, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:06:48'),
(284, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:06:48'),
(285, 1, 0, 0.00, 0.00, 1, '2021-04-06 15:06:48'),
(286, 1, 0, 0.00, 0.00, 1, '2021-04-06 15:06:48'),
(287, 1, 0, 0.00, 0.00, 1, '2021-04-06 15:06:48'),
(288, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:12:11'),
(289, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:12:11'),
(290, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:12:11'),
(291, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:12:11'),
(292, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:12:11'),
(293, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:12:11'),
(294, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:13:26'),
(295, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:13:26'),
(296, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:13:26'),
(297, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:13:26'),
(298, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:13:26'),
(299, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:13:26'),
(300, 1, 0, 0.00, 0.00, 1, '2021-04-06 15:13:26'),
(301, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:31:52'),
(302, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:31:52'),
(303, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:31:52'),
(304, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:31:52'),
(305, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:31:52'),
(306, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:31:52'),
(307, 1, 0, 0.00, 0.00, 1, '2021-04-06 15:31:52'),
(308, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:33:30'),
(309, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:33:30'),
(310, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:33:30'),
(311, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:33:30'),
(312, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:33:30'),
(313, 1, 2, 45.00, 0.00, 1, '2021-04-06 15:33:30');

-- --------------------------------------------------------

--
-- Table structure for table `ui_debit_item_transaction`
--

CREATE TABLE `ui_debit_item_transaction` (
  `id` int(11) NOT NULL,
  `tit_id` int(11) NOT NULL DEFAULT 0,
  `debit_note_no` varchar(400) DEFAULT NULL,
  `item_id` int(11) NOT NULL DEFAULT 0,
  `rate` double(11,2) NOT NULL DEFAULT 0.00,
  `qty` int(11) NOT NULL DEFAULT 0,
  `unit` varchar(100) DEFAULT NULL,
  `amt` double(11,2) NOT NULL DEFAULT 0.00,
  `disc_per` double(11,2) NOT NULL DEFAULT 0.00,
  `disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `after_disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `cgst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `cgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `sgst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `sgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `igst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `igst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `net_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` date DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` date DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ui_debit_note_trasaction`
--

CREATE TABLE `ui_debit_note_trasaction` (
  `id` int(11) NOT NULL,
  `int_invoice_on` varchar(100) DEFAULT NULL,
  `debit_note_no` varchar(400) DEFAULT NULL,
  `debit_date` date DEFAULT NULL,
  `supplier_id` int(11) NOT NULL DEFAULT 0,
  `supplier_bill_id` varchar(400) DEFAULT NULL,
  `company_gst_no` varchar(100) DEFAULT NULL,
  `total_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_after_disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_cgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_sgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_igst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_net_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `service_charges` double(11,2) NOT NULL DEFAULT 0.00,
  `final_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `description` text DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` date DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` date DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` date DEFAULT NULL,
  `service_charges_cgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `service_charges_sgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `service_charges_igst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `gst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `terms_of_payment` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ui_delivery_challan_item_transaction`
--

CREATE TABLE `ui_delivery_challan_item_transaction` (
  `id` int(11) NOT NULL,
  `tit_id` int(11) NOT NULL DEFAULT 0,
  `delivery_challan_no` varchar(400) DEFAULT NULL,
  `item_id` int(11) NOT NULL DEFAULT 0,
  `rate` double(11,2) NOT NULL DEFAULT 0.00,
  `qty` int(11) NOT NULL DEFAULT 0,
  `unit` varchar(100) DEFAULT NULL,
  `amt` double(11,2) NOT NULL DEFAULT 0.00,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` date DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` date DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ui_delivery_challan_transaction`
--

CREATE TABLE `ui_delivery_challan_transaction` (
  `id` int(11) NOT NULL,
  `int_invoice_on` varchar(100) DEFAULT NULL,
  `delivery_challan_no` varchar(400) DEFAULT NULL,
  `challan_date` date DEFAULT NULL,
  `challan_type` int(11) NOT NULL DEFAULT 0,
  `customer_id` int(11) NOT NULL DEFAULT 0,
  `gst_applicable` int(11) NOT NULL DEFAULT 0,
  `gst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `total_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `address` text DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` date DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` date DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` date DEFAULT NULL,
  `terms_of_payment` text DEFAULT NULL,
  `challan_file` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ui_item_group_master`
--

CREATE TABLE `ui_item_group_master` (
  `id` int(11) NOT NULL,
  `group_name` varchar(100) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` tinyint(4) NOT NULL DEFAULT 0,
  `created_time` datetime DEFAULT NULL,
  `updated_by` tinyint(4) NOT NULL DEFAULT 0,
  `updated_time` datetime DEFAULT NULL,
  `deleted_by` tinyint(4) NOT NULL DEFAULT 0,
  `deleted_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ui_item_group_master`
--

INSERT INTO `ui_item_group_master` (`id`, `group_name`, `active`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(35, 'SS', 1, 1, '2021-04-03 12:13:54', 0, NULL, 0, NULL),
(36, 'MS', 1, 1, '2021-04-03 12:14:01', 0, NULL, 0, NULL),
(37, 'AL', 1, 1, '2021-04-03 12:14:07', 0, NULL, 0, NULL),
(38, 'SF', 1, 1, '2021-04-03 12:14:16', 0, NULL, 0, NULL),
(39, 'GI', 1, 1, '2021-04-03 12:14:24', 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ui_item_master`
--

CREATE TABLE `ui_item_master` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL DEFAULT 0,
  `item_name` varchar(500) DEFAULT NULL,
  `group_name` varchar(2121) DEFAULT NULL,
  `item_short_name` varchar(100) DEFAULT NULL,
  `stock_unit` varchar(50) DEFAULT NULL,
  `gst_percentage` double(11,2) NOT NULL DEFAULT 0.00,
  `cess_percentage` double(11,2) NOT NULL DEFAULT 0.00,
  `reorder_quantitiy` int(11) NOT NULL DEFAULT 0,
  `hsn_code` varchar(50) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` datetime DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ui_item_master`
--

INSERT INTO `ui_item_master` (`id`, `group_id`, `item_name`, `group_name`, `item_short_name`, `stock_unit`, `gst_percentage`, `cess_percentage`, `reorder_quantitiy`, `hsn_code`, `active`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(2, 35, '1.0MM SS304 PERFORATION', NULL, 'aa', 'NOS', 12.00, 0.00, 5, '55', 1, 1, '2021-04-03 13:27:34', 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ui_ledger_master`
--

CREATE TABLE `ui_ledger_master` (
  `id` int(11) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `exp_income_head` tinyint(4) NOT NULL DEFAULT 0,
  `dt_of_opening_bal` date DEFAULT NULL,
  `opening_balance` double(11,2) NOT NULL DEFAULT 0.00,
  `active` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` datetime DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ui_opening_balance_master`
--

CREATE TABLE `ui_opening_balance_master` (
  `id` int(11) NOT NULL,
  `item_id` int(10) DEFAULT 0,
  `date` date DEFAULT NULL,
  `rate` double(11,2) NOT NULL DEFAULT 0.00,
  `quantity` int(11) DEFAULT 0,
  `active` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` datetime DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ui_opening_balance_master`
--

INSERT INTO `ui_opening_balance_master` (`id`, `item_id`, `date`, `rate`, `quantity`, `active`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(11, 2, '0000-00-00', 100.00, 1, 1, 1, '2021-04-03 13:35:50', 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ui_opening_stock`
--

CREATE TABLE `ui_opening_stock` (
  `id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT 0,
  `opening_stock` int(11) NOT NULL DEFAULT 0,
  `qty_in` int(11) NOT NULL DEFAULT 0,
  `qty_out` int(11) NOT NULL DEFAULT 0,
  `adjustment_in` int(11) NOT NULL DEFAULT 0,
  `adjustment_out` int(11) NOT NULL DEFAULT 0,
  `current_stock` int(11) NOT NULL DEFAULT 0,
  `entry_date` date DEFAULT NULL,
  `user_by` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ui_opening_stock`
--

INSERT INTO `ui_opening_stock` (`id`, `item_id`, `opening_stock`, `qty_in`, `qty_out`, `adjustment_in`, `adjustment_out`, `current_stock`, `entry_date`, `user_by`) VALUES
(82, 1, 0, 1, 0, 0, 0, 1, '2021-04-02', 1),
(83, 2, 1, 0, 0, 0, 0, 1, '2021-04-03', 1),
(84, 2, 1, 0, 1, 0, 0, 0, '2021-04-05', 1),
(85, 2, 1, 0, 9, 0, 0, -8, '2021-04-05', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ui_payment_bill_transaction`
--

CREATE TABLE `ui_payment_bill_transaction` (
  `id` int(11) NOT NULL,
  `tit_id` int(11) NOT NULL DEFAULT 0,
  `payment_no` varchar(500) DEFAULT NULL,
  `supplier_bill_id` int(11) DEFAULT 0,
  `supplier_date` date DEFAULT NULL,
  `inv_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `paid_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `balance_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `net_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` date DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ui_payment_transaction`
--

CREATE TABLE `ui_payment_transaction` (
  `id` int(11) NOT NULL,
  `int_invoice_on` varchar(100) DEFAULT NULL,
  `payment_no` varchar(400) DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `account_head_id` int(11) NOT NULL DEFAULT 0,
  `payment_mode` varchar(100) DEFAULT NULL,
  `transaction_no` varchar(100) DEFAULT NULL,
  `payment_now` double(11,2) NOT NULL DEFAULT 0.00,
  `reference` varchar(100) DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` date DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` date DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` date DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `supplier_ledger_id` int(11) NOT NULL,
  `bank_name` varchar(400) DEFAULT NULL,
  `instrument_no` varchar(400) DEFAULT NULL,
  `remark` varchar(300) DEFAULT NULL,
  `total_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `radio_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ui_proforma_invoice_item_transaction`
--

CREATE TABLE `ui_proforma_invoice_item_transaction` (
  `id` int(11) NOT NULL,
  `tit_id` int(11) NOT NULL DEFAULT 0,
  `invoice_no` varchar(400) DEFAULT NULL,
  `item_id` int(11) NOT NULL DEFAULT 0,
  `rate` double(11,2) NOT NULL DEFAULT 0.00,
  `qty` int(11) NOT NULL DEFAULT 0,
  `unit` varchar(100) DEFAULT NULL,
  `amt` double(11,2) NOT NULL DEFAULT 0.00,
  `disc_per` double(11,2) NOT NULL DEFAULT 0.00,
  `disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `after_disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `cgst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `cgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `sgst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `sgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `igst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `igst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `net_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` date DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` date DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ui_proforma_invoice_item_transaction`
--

INSERT INTO `ui_proforma_invoice_item_transaction` (`id`, `tit_id`, `invoice_no`, `item_id`, `rate`, `qty`, `unit`, `amt`, `disc_per`, `disc_amt`, `after_disc_amt`, `cgst_per`, `cgst_amt`, `sgst_per`, `sgst_amt`, `igst_per`, `igst_amt`, `net_amt`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(28, 8, '', 2, 5.00, 1, 'NOS', 5.00, 0.00, 0.00, 5.00, 6.00, 0.30, 6.00, 0.30, 0.00, 0.00, 5.60, 0, NULL, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ui_proforma_invoice_transaction`
--

CREATE TABLE `ui_proforma_invoice_transaction` (
  `id` int(11) NOT NULL,
  `int_invoice_on` varchar(100) DEFAULT NULL,
  `proforma_no` varchar(400) DEFAULT NULL,
  `proforma_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `customer_id` int(11) NOT NULL DEFAULT 0,
  `gst_applicable` int(11) NOT NULL DEFAULT 0,
  `gst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `total_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_after_disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_cgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_sgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_igst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_net_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `service_charges` double(11,2) NOT NULL DEFAULT 0.00,
  `final_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `notes` varchar(500) DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` date DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` date DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` date DEFAULT NULL,
  `delivery_challen_no` varchar(50) DEFAULT NULL,
  `delivery_challen_date` date DEFAULT NULL,
  `buyer_order_on` varchar(20) DEFAULT NULL,
  `buyer_order_date` date DEFAULT NULL,
  `vehicle_no` varchar(20) DEFAULT NULL,
  `e_way_bill_no` varchar(20) DEFAULT NULL,
  `lr_no` varchar(20) DEFAULT NULL,
  `lr_date` date DEFAULT NULL,
  `terms_of_payment` text DEFAULT NULL,
  `service_charges_cgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `service_charges_sgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `service_charges_igst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `kind_atten` varchar(300) DEFAULT NULL,
  `terms_of_delivery` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ui_proforma_invoice_transaction`
--

INSERT INTO `ui_proforma_invoice_transaction` (`id`, `int_invoice_on`, `proforma_no`, `proforma_date`, `due_date`, `customer_id`, `gst_applicable`, `gst_per`, `total_amt`, `total_disc_amt`, `total_after_disc_amt`, `total_cgst_amt`, `total_sgst_amt`, `total_igst_amt`, `total_net_amt`, `service_charges`, `final_amt`, `notes`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`, `delivery_challen_no`, `delivery_challen_date`, `buyer_order_on`, `buyer_order_date`, `vehicle_no`, `e_way_bill_no`, `lr_no`, `lr_date`, `terms_of_payment`, `service_charges_cgst_amt`, `service_charges_sgst_amt`, `service_charges_igst_amt`, `kind_atten`, `terms_of_delivery`) VALUES
(8, '00001', 'PRO/20-21/00001', '2021-04-05', '2021-04-05', 1, 0, 0.00, 5.00, 0.00, 5.00, 0.30, 0.30, 0.00, 5.60, 0.00, 5.60, '', 1, '2021-04-05', 1, '2021-04-06', 0, NULL, '', '0000-00-00', 'PO/2021/001', '2021-04-04', '', '', '', '0000-00-00', '1. 30% advance.', 0.00, 0.00, 0.00, 'Madhavi Jnagm', '1. 30% advance.');

-- --------------------------------------------------------

--
-- Table structure for table `ui_purchase_bill_item_transaction`
--

CREATE TABLE `ui_purchase_bill_item_transaction` (
  `id` int(11) NOT NULL,
  `tit_id` int(11) NOT NULL DEFAULT 0,
  `supplier_bill_no` varchar(400) DEFAULT NULL,
  `item_id` int(11) NOT NULL DEFAULT 0,
  `rate` double(11,2) NOT NULL DEFAULT 0.00,
  `qty` int(11) NOT NULL DEFAULT 0,
  `unit` varchar(100) DEFAULT NULL,
  `amt` double(11,2) NOT NULL DEFAULT 0.00,
  `disc_per` double(11,2) NOT NULL DEFAULT 0.00,
  `disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `after_disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `cgst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `cgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `sgst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `sgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `igst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `igst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `net_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` date DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` date DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ui_purchase_bill_item_transaction`
--

INSERT INTO `ui_purchase_bill_item_transaction` (`id`, `tit_id`, `supplier_bill_no`, `item_id`, `rate`, `qty`, `unit`, `amt`, `disc_per`, `disc_amt`, `after_disc_amt`, `cgst_per`, `cgst_amt`, `sgst_per`, `sgst_amt`, `igst_per`, `igst_amt`, `net_amt`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(101, 39, 'PB/20-21/00001', 1, 100.00, 1, 'N', 100.00, 0.00, 0.00, 100.00, 6.00, 6.00, 6.00, 6.00, 0.00, 0.00, 112.00, 0, NULL, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ui_purchase_bill_transaction`
--

CREATE TABLE `ui_purchase_bill_transaction` (
  `id` int(11) NOT NULL,
  `int_invoice_on` varchar(100) DEFAULT NULL,
  `supplier_bill_no` varchar(400) DEFAULT NULL,
  `supplier_bill_date` date DEFAULT NULL,
  `supplier_id` int(11) NOT NULL DEFAULT 0,
  `gst_applicable` int(11) NOT NULL DEFAULT 0,
  `gst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `total_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_after_disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_cgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_sgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_igst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_net_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `service_charges` double(11,2) NOT NULL DEFAULT 0.00,
  `final_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `description` text DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` date DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` date DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` date DEFAULT NULL,
  `delivery_challen_no` varchar(50) DEFAULT NULL,
  `delivery_challen_date` date DEFAULT NULL,
  `purchase_no` varchar(20) DEFAULT NULL,
  `purchase_date` date DEFAULT NULL,
  `terms_of_payment` text DEFAULT NULL,
  `service_charges_cgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `service_charges_sgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `service_charges_igst_amt` double(11,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ui_purchase_bill_transaction`
--

INSERT INTO `ui_purchase_bill_transaction` (`id`, `int_invoice_on`, `supplier_bill_no`, `supplier_bill_date`, `supplier_id`, `gst_applicable`, `gst_per`, `total_amt`, `total_disc_amt`, `total_after_disc_amt`, `total_cgst_amt`, `total_sgst_amt`, `total_igst_amt`, `total_net_amt`, `service_charges`, `final_amt`, `description`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`, `delivery_challen_no`, `delivery_challen_date`, `purchase_no`, `purchase_date`, `terms_of_payment`, `service_charges_cgst_amt`, `service_charges_sgst_amt`, `service_charges_igst_amt`) VALUES
(39, '00001', 'PB/20-21/00001', '2021-04-02', 153, 0, 0.00, 100.00, 0.00, 100.00, 6.00, 6.00, 0.00, 112.00, 0.00, 112.00, NULL, 1, '2021-04-02', 0, NULL, 0, NULL, '', '0000-00-00', '', '0000-00-00', '', 0.00, 0.00, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `ui_purchase_order_item_transaction`
--

CREATE TABLE `ui_purchase_order_item_transaction` (
  `id` int(11) NOT NULL,
  `tit_id` int(11) NOT NULL DEFAULT 0,
  `purchase_no` varchar(400) DEFAULT NULL,
  `item_id` int(11) NOT NULL DEFAULT 0,
  `rate` double(11,2) NOT NULL DEFAULT 0.00,
  `qty` int(11) NOT NULL DEFAULT 0,
  `unit` varchar(100) DEFAULT NULL,
  `amt` double(11,2) NOT NULL DEFAULT 0.00,
  `disc_per` double(11,2) NOT NULL DEFAULT 0.00,
  `disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `after_disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `cgst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `cgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `sgst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `sgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `igst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `igst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `net_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` date DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` date DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ui_purchase_order_item_transaction`
--

INSERT INTO `ui_purchase_order_item_transaction` (`id`, `tit_id`, `purchase_no`, `item_id`, `rate`, `qty`, `unit`, `amt`, `disc_per`, `disc_amt`, `after_disc_amt`, `cgst_per`, `cgst_amt`, `sgst_per`, `sgst_amt`, `igst_per`, `igst_amt`, `net_amt`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(398, 17, 'PO/20-21/00001', 2, 121.00, 1, 'NOS', 121.00, 0.00, 0.00, 121.00, 6.00, 7.26, 6.00, 7.26, 0.00, 0.00, 135.52, 0, NULL, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ui_purchase_order_transaction`
--

CREATE TABLE `ui_purchase_order_transaction` (
  `id` int(11) NOT NULL,
  `int_invoice_on` varchar(100) DEFAULT NULL,
  `purchase_no` varchar(400) DEFAULT NULL,
  `purchase_date` date DEFAULT NULL,
  `supplier_id` int(11) NOT NULL DEFAULT 0,
  `gst_applicable` int(11) NOT NULL DEFAULT 0,
  `gst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `total_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_after_disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_cgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_sgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_igst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_net_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `other_charges` double(11,2) NOT NULL DEFAULT 0.00,
  `final_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `terms_of_payment` varchar(500) DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` date DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` date DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` date DEFAULT NULL,
  `supplier_ref_no` varchar(20) DEFAULT NULL,
  `supplier_date` date DEFAULT NULL,
  `kind_atten` text DEFAULT NULL,
  `round_off` double(11,2) NOT NULL DEFAULT 0.00,
  `prices_for` varchar(100) DEFAULT NULL,
  `delivery_at` varchar(200) DEFAULT NULL,
  `packing_inst` varchar(500) DEFAULT NULL,
  `transporter` varchar(500) DEFAULT NULL,
  `notes` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ui_purchase_order_transaction`
--

INSERT INTO `ui_purchase_order_transaction` (`id`, `int_invoice_on`, `purchase_no`, `purchase_date`, `supplier_id`, `gst_applicable`, `gst_per`, `total_amt`, `total_disc_amt`, `total_after_disc_amt`, `total_cgst_amt`, `total_sgst_amt`, `total_igst_amt`, `total_net_amt`, `other_charges`, `final_amt`, `terms_of_payment`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`, `supplier_ref_no`, `supplier_date`, `kind_atten`, `round_off`, `prices_for`, `delivery_at`, `packing_inst`, `transporter`, `notes`) VALUES
(17, '00001', 'PO/20-21/00001', '2021-04-05', 153, 0, 0.00, 121.00, 0.00, 121.00, 7.26, 7.26, 0.00, 135.52, 0.00, 135.52, '1. Actual will be measured and billed.\r\n2. 30% advance.\r\n3. 60% during Execution.\r\n4. 10% after completion within 30 days.\r\n5. Above rates are only up to the height of 13\'.\r\n6. Above rates are only valid up to 6 months fron the date of issue.', 1, '2021-04-05', 0, NULL, 0, NULL, '', '0000-00-00', '', 0.00, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ui_quotation_item_transaction`
--

CREATE TABLE `ui_quotation_item_transaction` (
  `id` int(11) NOT NULL,
  `tit_id` int(11) NOT NULL DEFAULT 0,
  `quotation_no` varchar(400) DEFAULT NULL,
  `item_id` int(11) NOT NULL DEFAULT 0,
  `item_desc` varchar(500) DEFAULT NULL,
  `rate` double(11,2) NOT NULL DEFAULT 0.00,
  `qty` int(11) NOT NULL DEFAULT 0,
  `unit` varchar(100) DEFAULT NULL,
  `amt` double(11,2) NOT NULL DEFAULT 0.00,
  `gst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `gst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `net_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` date DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` date DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ui_quotation_item_transaction`
--

INSERT INTO `ui_quotation_item_transaction` (`id`, `tit_id`, `quotation_no`, `item_id`, `item_desc`, `rate`, `qty`, `unit`, `amt`, `gst_per`, `gst_amt`, `net_amt`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(309, 47, 'QUO/20-21/00001', 2, 'aaaa', 5.00, 1, 'NOS', 5.00, 12.00, 0.60, 5.60, 0, NULL, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ui_quotation_transaction`
--

CREATE TABLE `ui_quotation_transaction` (
  `id` int(11) NOT NULL,
  `int_invoice_on` varchar(100) DEFAULT NULL,
  `quotation_no` varchar(400) DEFAULT NULL,
  `quotation_date` date DEFAULT NULL,
  `customer_id` int(11) NOT NULL DEFAULT 0,
  `verbal_by` varchar(100) DEFAULT NULL,
  `subject` varchar(300) DEFAULT NULL,
  `terms_and_condition` varchar(300) DEFAULT NULL,
  `gst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `total_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_gst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_net_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` date DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` date DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ui_quotation_transaction`
--

INSERT INTO `ui_quotation_transaction` (`id`, `int_invoice_on`, `quotation_no`, `quotation_date`, `customer_id`, `verbal_by`, `subject`, `terms_and_condition`, `gst_per`, `total_amt`, `total_gst_amt`, `total_net_amt`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(47, '00001', 'QUO/20-21/00001', '2021-04-05', 1, '', '              -     \r\n            ', '1. Actual will be measured and billed.\r\n2. 30% advance.\r\n3. 60% during Execution.\r\n4. 10% after completion within 30 days.\r\n5. Above rates are only up to the height of 13\'.\r\n6. Above rates are only valid up to 6 months fron the date of issue.', 0.00, 5.00, 0.60, 5.60, 1, '2021-04-05', 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ui_recevied_bil_transaction`
--

CREATE TABLE `ui_recevied_bil_transaction` (
  `id` int(11) NOT NULL,
  `tit_id` int(11) NOT NULL DEFAULT 0,
  `recevied_no` varchar(500) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT 0,
  `invoice_date` date DEFAULT NULL,
  `inv_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `recd_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `balance_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `net_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` date DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ui_recevied_transaction`
--

CREATE TABLE `ui_recevied_transaction` (
  `id` int(11) NOT NULL,
  `int_invoice_on` varchar(100) DEFAULT NULL,
  `recevied_no` varchar(400) DEFAULT NULL,
  `received_date` date DEFAULT NULL,
  `account_head_id` varchar(100) DEFAULT NULL,
  `payment_mode` varchar(100) DEFAULT NULL,
  `transaction_no` varchar(100) DEFAULT NULL,
  `received_now` double(11,2) NOT NULL DEFAULT 0.00,
  `reference` varchar(100) DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` date DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` date DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` date DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `ledger_id` int(11) NOT NULL,
  `bank_name` varchar(100) DEFAULT NULL,
  `instrument_no` varchar(100) DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  `total_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `radio_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ui_supplier_master`
--

CREATE TABLE `ui_supplier_master` (
  `id` int(11) NOT NULL,
  `supplier_name` varchar(100) DEFAULT NULL,
  `company_phone` varchar(20) DEFAULT NULL,
  `company_pan` varchar(20) DEFAULT NULL,
  `company_email` varchar(50) DEFAULT NULL,
  `company_gst` varchar(20) DEFAULT NULL,
  `billing_address` text DEFAULT NULL,
  `shipping_address` text DEFAULT NULL,
  `contact_person_name` varchar(100) DEFAULT NULL,
  `contact_person_phone` varchar(20) DEFAULT NULL,
  `contact_person_whatsapp` varchar(20) DEFAULT NULL,
  `contact_person_email` varchar(50) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` tinyint(4) NOT NULL DEFAULT 0,
  `created_time` datetime DEFAULT NULL,
  `updated_by` tinyint(4) NOT NULL DEFAULT 0,
  `updated_time` datetime DEFAULT NULL,
  `deleted_by` tinyint(4) NOT NULL DEFAULT 0,
  `deleted_time` datetime DEFAULT NULL,
  `statename` varchar(100) DEFAULT NULL,
  `purchase_person_name` varchar(100) DEFAULT NULL,
  `opening_balance` double(11,2) NOT NULL DEFAULT 0.00,
  `date_of_opening` date DEFAULT NULL,
  `fssai_no` varchar(100) DEFAULT NULL,
  `area_pincode` varchar(6) DEFAULT NULL,
  `upload_document` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ui_supplier_master`
--

INSERT INTO `ui_supplier_master` (`id`, `supplier_name`, `company_phone`, `company_pan`, `company_email`, `company_gst`, `billing_address`, `shipping_address`, `contact_person_name`, `contact_person_phone`, `contact_person_whatsapp`, `contact_person_email`, `active`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`, `statename`, `purchase_person_name`, `opening_balance`, `date_of_opening`, `fssai_no`, `area_pincode`, `upload_document`) VALUES
(14, '3 WISE MONKEYS (KHAR WEST)', NULL, '', 'mumbai@holylandgroup.com', '27AAAFZ9698R1ZQ', '10 A,RAJKUTIR,3RD ROAD,BESIDE UNICONTINENTAL', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(15, 'AAN  ENTERPRISES', NULL, '', '', '27DSGPS9433E1ZC', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(16, 'ABM Marketing Cell Pvt. Ltd', NULL, '', '', '27AAJCA9694G1Z3', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(17, 'ADITI FOODS INDIA PVT LTD', NULL, '', '', '27AAEFA7708D1ZP', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(18, 'Aditya Agencies', NULL, '', '', '27AAPFA3291A1ZM', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(19, 'A G ENTERPRISES', NULL, '', '', '27AAZFA0380J1Z1', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(20, 'AKSHAJ ENTERPRISE', NULL, '', '', '27AZZPD2683A1Z7', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(21, 'AMEE AGENCIES', NULL, '', '', '27AAEPT8810H1ZG', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(22, 'ANUSHTHAN ENTERPRISES', NULL, '', '', '27AXHPMO153C1ZV', '9,VIDYA NIWAS,SHIVTEKADI,JVLR,JOGESHWARI', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(23, 'Asian Sales', NULL, '', '', '27FVTPS1038B1ZC', 'Sh-8, Sai Pooja Chs, Pl-36, Sec-34', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(24, 'ASQUARE FOOD & BEVERAGES PVT.LTD.', NULL, '', '', '22AANCA8045R1Z3', 'URLA INDUSTRIAL AREA,CANAL ROAD,GONDWARA,', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(25, 'ATHARVA FOODS', NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(26, 'AUSTIN FOODS & BEVERAGES PVT LTD', NULL, '', '', '27AALCA5411D1ZO', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(27, 'AYRA ENTERPRISES', NULL, '', '', '27CGBPS6830F1ZA', '28/29,SKY INDUSTRIAL ESTATE,OLD DAL MILL COMPOUND,', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(28, 'BALAJI ENTERPRISES', NULL, '', '', '27BVWPS0023B1ZM', 'A-45,NEW EMPIRE IND ESTATE,KONDIVITA LANE,', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(29, 'Best in Taste', NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(30, 'BMS ENTERPRISES', NULL, '', '', '27AAWFB9309F1Z1', 'SHOP NO-1 DAMODAR SHANTI BLDG,PLOT NO-10', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(31, 'Cash Purchase', NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(32, 'CHENAB IMPEX PVT LTD', NULL, '', '', '27AAACC3509K1ZR', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(33, 'CHIC AGENCY GOREGAON (W)', NULL, '', '', '27CTWPP8297F1ZK', '7, UDYOG NAGAR, GALA NO. 17, MEZZANINE FLOOR,', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(34, 'CK ENTERPRISE', NULL, '', '', '27DFGFS3331C1ZM', 'SHREERAM NGR,VODARPADA,RD-NO1,AKURLI RD,', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(35, 'CONTINENTAL FOODS', NULL, '', '', '27AIMPB0987D1ZB', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(36, 'CONTINENTAL STORES', NULL, '', 'continentalstores@yahoo.com', '27AAACC8446K1ZD', 'Cyrus Yeast Pvt. Ltd.,', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(37, 'CUPS N CONES', NULL, '', '', '27AAEPM1662N1ZE', '72/580,,MOTILALNAGAR NO,1 NEAR SIDDHART', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(38, 'DISHA ENTERPISES', NULL, '', '', '27AEFPD6590P1ZX', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(39, 'DS SPICECO PRIVATE LIMITED', NULL, 'AAECD6959Q', '', '27AAECD6959Q1ZQ', 'C/O PARSHV LOGISTICS,BUILDING NO,A-10, GALA', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(40, 'ENIGMA COMMUNICATION', NULL, '', '', '27AEPP03701A12S', 'NO,43 ROAD NO-9 NUTAN LAXMI SOCIETY JUHU', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(41, 'FDSAFFDS', NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(42, 'FIDELITY INTERNATIONAL', NULL, '', '', '27BAHPS4293K1Z1', '4TH FLOOR,CLASSIQUE CENTER,26 MAHAL', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(43, 'FOODCOAST INTERNATIONAL', NULL, '', '', '27AABFF9886E1Z2', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(44, 'FOODSTUFF ENTERPRISE LLP', NULL, '', '', '27AAEFF0348N1Z9', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(45, 'FORTUNE GOURMET SPECIALITIES PRIVATE LIMITED', NULL, '', '', '27AAACF4952B1ZY', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(46, 'FUTURISTIC FOODS', NULL, '', '', '27AAEFF9962E1Z6', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(47, 'GALA SALES AGENCIES', NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(48, 'HAPPY & HEALTHY FOODS 20-21', NULL, '', '', '27ANIPS0345M1ZL', '402/403 INSIDE CRWFORD MARKET MUMBAI-400001', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(49, 'Harihar Oil Traders (Andheri E)', NULL, '', '', '27AAAFH2127H1ZR', 'A/10, Nandham Indl. Estate, Marol Maroshi Road,', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(50, 'HASMUKLAL & SONS', NULL, '', '', '27AAFPS2930N1ZA', 'A-20 Shreyas Industrial Est, A, Off Western Express', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(51, 'HDFC Ergo General Insurance Company Ltd', NULL, '', '', '27AABCL5045N1Z8', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(52, 'Holy-Land Marketing Pvt. Ltd.', NULL, '', '', '27AAACH1632E1Z3', 'Gala No. 119-122, AC Unit, Shree Rajlaxmi Complex,', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(53, 'IGST OUTPUT', NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(54, 'INDO FOODS', NULL, '', '', '27AQUPK2690L1Z1', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(55, 'IPATELS TRADING HUB (JOGESHWARU=I W)', NULL, '', '', '27AQJPP8007B2ZW', 'ID.44,BELOW J.V.L.R BRIDGE NEAR S.V ROAD', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(56, 'JAI AMBE AGENCY', NULL, '', '', '27AGTPJ9714D1Z2', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(57, 'JAIN DUGDHALAY', NULL, '', '', '27AIZPG6478P1ZY1', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(58, 'JAISWAL ENTERPRISES', NULL, '', '', '27ABNPG9626J1Z5', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(59, 'J M D ENTERPRISES', NULL, '', '', '27AOGPP0026F1Z9', 'ID NO 108/116 BELOW JOGESHWARI BRIG NR', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(60, 'KIARA FOODS', NULL, '', '', '27APXPB9809G1ZD', '402/A,PADMAVATI NAGAR DUMPING ROAD MULUND', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(61, 'KOLLUR FOODS PRODUCTS', NULL, '', '', '27AAMFK4573E1Z4', 'UNIT NO 1&2 GANESH NAGAR GEN A K VAIDYA MARG', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(62, 'LEAGUE FOODS PVT LTD', NULL, '', '', '27AACCL5380R1ZT', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(63, 'MAGPIE GLOBAL LIMITED', NULL, '', '', '27AAECM5127D1ZQ', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(64, 'MAHAKALI MASALA', NULL, 'AHVPG8833B', '', '27AHVPG8833B1Z3', '29,ARIHANT MANSION,KESHAVJI NAIK ROAD,', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(65, 'MAHAVIR MARKETING (MALAD EAST)', NULL, '', '', '27AJWPK6652K1ZD', 'SHOP 1/7,THE MALAD CO.OP.HSG SOCIETY LTD', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(66, 'MAHEER AGRO LLP', NULL, '', '', '27ABBFM2994B1ZC', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(67, 'MANAV ENTERPRISES', NULL, '', '', '27ARZPT6524C2Z6', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(68, 'MANGALIN AGRO FOODS PVT LTD', NULL, '', '', '27AAEFA7708D1ZP', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(69, 'MATAJEE TRADING COMPANY', NULL, '', '', '27ABGPC3914N1ZL', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(70, 'MEHTA SONS', NULL, '', '', '27ABKFM7068H1ZS', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(71, 'Mira Trading Co', NULL, '', '', '27APGPG5380J1ZO', '9/35, Yeshwant Nagar HSG. SOC, Opposite Apna Bazar, S V', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(72, 'MRK Foods Private Limited', NULL, '', '', '27AAGCM6925R1ZN', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(73, 'M/S FROZEN FOODS', NULL, '', '', '27ADIPA1974E1ZS', 'UNIT NO-223, SAGAR BLDG PRABHAT INDUSTRIAL', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(74, 'N.A.', NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(75, 'NARAYAN SALES CORPORATION', NULL, '', '', '27AOWPS9274E1Z0', 'Damji Shamji Ind  Premises CO-Op Society Ltd', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(76, 'New Ravechi Grain Stores', NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(77, 'NOBLE SALES (NS)', NULL, 'AISPL4097J', '', '27AISPL4097J1ZH', 'TAYSHEED APARTMENTS', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(78, 'NOOR NISHAN INC', NULL, '', '', '07AAMFN2833N2ZS', '218,KH NO.487/14 SCHOOL ROAD,PEERA GARHI', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(79, 'N S Enterprises', NULL, '', '', '27AADPM8514A1Z1', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(80, 'OM ENTERPRISES', NULL, '', '', '27BCAPSO798C1ZJ', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(81, 'PAREKH AGENCY', NULL, '', '', '27AABPP2937J1Z1', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(82, 'PARESH ENTERPRISES', NULL, '', '', '27BIWPS0566A1ZY', 'GALA NO 29/6,AMALGAMDED BLDG,HAJI BAPU RD', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(83, 'PATEL AGENCY', NULL, '', '', '27AAJPP2872L1Z4', 'BOSTON APTS SHOP NO 5 SAHKAR ROAD OPP DR BHARAT', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(84, 'Patel Enterprises', NULL, '', '', '27AAGPP4895E1ZB', 'Shop No.2,Periera Compt,Vaishali Nagar,', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(85, 'Prachi Enterprise', NULL, '', '', '27AABHM5986L1ZJ', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(86, 'PRASHANT TRADERS', NULL, '', '', '27AAAHL0718A1ZW', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(87, 'PRIYA ENTERPRISES', NULL, '', '', '27BDEPM3870Q1ZV', '3 KALIKA NIWAS,NEHRU ROAD SANTACRUZ EAST ,', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(88, 'RADHE RADHE SALES', NULL, '', '', '27COIPS9803R1ZU', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(89, 'RAJ IMPEX', NULL, '', '', '27AMQPS7541C1ZO', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(90, 'RAJKALA INDUSTRIES PVT LTD', NULL, '', '', '27AAACR2500B1Z6', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(91, 'RAJ SALES AGENCIES', NULL, '', '', '27AMTPS4865J1Z1', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(92, 'Rapid Carriers Pvt Ltd', NULL, '', '', '27AABCR9177P1ZH', 'Rapid House, 38, Narayan Dhuru Street,', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(93, 'RAVI MASALA SUPPLYING CO.', NULL, 'AFYPB7453D', '', '27AFYPB7453D1Z7', '2AGARVAL CHOWL 5 CARTER ROAD RAIDONGRI', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(94, 'Refton Agencies', NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(95, 'R.PATEL AND CO.', NULL, 'AGGPP6475J', 'rpatel.chandu@gmail.com', '27AGGPP6475J1ZS', 'E11,A.P.M.C.MARKET-1,PHASE-II,MUDI BAZAR,', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(96, 'RUPANDE FOOD PRODUCT', NULL, '', '', '27AAJPS1343K1ZF', '41,42 DEEN BULDING COMPOUND N M JOSHI MARG', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(97, 'Sablok International Foods LLP', NULL, '', '', '27AECFS1404G1ZE', 'C-112A, Shreyas Indl. Estate, Jaycoach', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(98, 'SAIKRIPA FOODS SERVICES PVT.LTD.', NULL, '', '', '27AAJCS6517J1ZY', 'THE ACERS CLUB,CHEMBUR,411/B,HEMU KALANI', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(99, 'Sai Service Pvt. Ltd', NULL, '', '', '27AABCS4998M1ZJ', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(100, 'Sams Food Products Pvt Ltd', NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(101, 'SARASWATI SALES AGENCY', NULL, '', '', '27ACOFS1071K1ZQ', '61/491,MOTILAL NAGAR NO.1,GROUND FLOOR,H.S.RUPWATE', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(102, 'Sarwar Food Products', NULL, '', '', '27AAEPS4303G1ZV', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(103, 'SFDF', NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(104, 'SHAH TRADING', NULL, '', '', '27AALPS6489A1Z9', '1228/154,ROAD NO-3,MOTILAL NAGAR-1', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(105, 'SHETH BROTHERS', NULL, '', '', '27AAAFS2872E1ZA', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(106, 'SHREE BALAJI ENTERPRISES', NULL, '', '', '27ADQPN4469F1Z1', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(107, 'SHREE ENTERPRISES', NULL, '', '', '27AJOPK3408P1ZN', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(108, 'SHREE GANESH ENTERPRISES', NULL, '', '', '27ARZPT6524C2Z6', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(109, 'Shree Ganesh Enterprises(Cash Purchase)', NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(110, 'SHREE GANESH TRADERS', NULL, '', '', '27AQNPS4008B1ZX', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(111, 'Shree Ganesh Trading Co.', NULL, '', '', '27CORPK3338C1ZU', 'Shop No 15, B Wing, Sai Shraddha Bldg,', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(112, 'SHREEJI INTERNATIONAL', NULL, '', '', '27ADHFS8859B1ZO', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(113, 'SHREEJI MARKETING', NULL, '', '', '27AAAPP3674M1Z9', '214/1712,MOTILAL NAGAR OPP GANESH', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(114, 'SHREE NAKODA DISTRIBUTORS', NULL, '', '', '27AHCPS2707D1ZL', '107,SHIV SAGAR INDUSTRIAL ESTATE KOTKAR COMPOUND', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(115, 'SHREENATH AGENCIES', NULL, '', '', '27AACPP4812F1ZW', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(116, 'Shree Puja Enterprises', NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(117, 'SHREE SAIRAJ ENTERPRISES', NULL, '', '', '27DYPPS1573C1Z2', 'SHOP NO.02,DEEP NARAYAN CHAWL,NEW LINK ROAD', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(118, 'SHREE SAI TRADERS', NULL, '', '', '27BRLPS3056H1ZE', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(119, 'SHRI RAM FOODS', NULL, '', 'shriramfoods76@gmail.com', '27ARBPM6241P1ZD', 'A-301, DENIS APARTMENT,', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(120, 'SHRISHTI AGENCIES', NULL, '', 'anandpandey2008@hotmail.com', '27BBKPP9792C1Z2', 'A WING, B-56/403 KAPIL CO. OP.', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(121, 'S K ENTERPRISES', NULL, '', '', '27AADPK6387R1ZT', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(122, 'S K FOODS', NULL, '', '', '27AADPK6387R1ZT', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(123, 'SKY ENTERPRISE PRIVATE LIMITED', NULL, '', '', '27AAICS9571K1ZL', 'SURVEY NO.171 (P),VILLAGE AAMGAON MANOR', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(124, 'SMS SALES', NULL, '', '', '27BSSPS7743H1ZV', 'M.G.ROAD,21/169 UNNAT NAGAR NO.4 GOREGAON', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(125, 'S.S.V DISTRIBUTORS INDIA PVT.LTD', NULL, '', '', '27AAXCS9818A1ZT', 'C-002,AMTOPHILL WAREHOUSING CO.LTD.BARKAT', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(126, 'Star Distributors', NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(127, 'STAR NET SYSTEM', NULL, '', '', '27ADWPD7189C1Z4', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(128, 'STAR SALES', NULL, '', '', '27AAOPS2351L1Z6', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(129, 'SUBHRA ENTERPRISE', NULL, '', '', '27AERPJ3417P1ZV', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(130, 'SUBHRA VICTUS SOLUTIONS', NULL, '', '', '27ACYPJ0718N2ZX', 'B-34,SHREE BALAJI INDL.ESTATE HANUMAN NAGAR,', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(131, 'SUPER ENTERPRISES', NULL, '', '', '27AAOFS9815R1Z0', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(132, 'SURUCHI INDIA FOODS', NULL, '', '', '27AAACE6179M1Z5', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(133, 'Swastik Traders (Cash Purchase)', NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(134, 'SYMCO SALES PVT LTD', NULL, '', '', '27AANCS1432L1Z2', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(135, 'TEXAS CORPORATION', NULL, '', '', '27AACFT1774E1Z8', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(136, 'The Berlin Cafe (Vashi)', NULL, '', 'Theberlincafelounge@gmail.com', '27AARFT0146K1ZS', 'Plot No. 25, Sector 19C, Near to Satra Plaza,', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(137, 'Tirumala Food Industries', NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(138, 'TJUK TRADENETWORKS PVT LTD', NULL, '', '', '27AACCT0200E1Z3', '172/c Shop No-1,Namu Bldg,Naigum Cross Road,', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(139, 'TOSHNIWAL TRADERS', NULL, '', '', '27AANFM9844R1Z2', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(140, 'TRIMURTI FOOD PRODUCTS', NULL, '', '', '27AAAPC9865B1ZW', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(141, 'UNI FOODS', NULL, '', '', '27AQUPK2723N1Z7', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(142, 'UNITED DISTRIBUTORS INCORPORATION', NULL, '', '', '27AAEFN3108Q1ZZ', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(143, 'VAIBHAV LAXMI VENTURE', NULL, '', '', '27AAJFV3526B1ZB', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(144, 'VEEKAY FOODS', NULL, '', '', '27AASFV9040K1ZE', 'GALA NO,4/5/6,DEVI DAYAL COMPOUND, PANNA', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(145, 'VEEKAY FOODS PVT LTD', NULL, '', '', '27AAECV1254E1ZJ', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(146, 'VIJAY DISTRIBUTORS', NULL, '', '', '27ACGPG0711L1ZU', '5/6 PUSHPAK BUILDING NO,3 KHETAN ESTATE', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(147, 'VIJAYLAXMI ENTERPRISES', NULL, '', '', '27BVRPD6001M1ZD', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(148, 'VIJAY SALES', NULL, '', '', '27AAHCV3778L1ZK', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(149, 'VITA SALES', NULL, '', '', '27AAAFV0726G1ZE', '55/1 VICTORIA BUILDING DR.B.A ROAD,BYCULLA EAST', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(150, 'WAHAJUDDIN SHAIKH', NULL, '', '', '', 'JUNAID NAGAR, CD BARFIWALA ROAD, JUHU LANE, ANDHERI', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(151, 'Western Marketing', NULL, '', '', '27AACFW8773G1ZO', '', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(152, 'ZAMAN FAST FOOD (AMBOLI)', NULL, '', '', '', 'SV ROAD', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, NULL, 'Maharashtra', NULL, 0.00, NULL, NULL, NULL, NULL),
(153, 'Zima - Valor Hospitality Pvt. Ltd Bandra(W)', '', '', '', '27AAFCV4395R1ZB', 'Zima, Ground Flr, Mangal Kunj,', 'aaa', '', '', '', '', 1, 0, NULL, 1, '2021-04-02 18:32:30', 0, NULL, 'Maharashtra', '', 0.00, '0000-00-00', '', '', 'chrysanthemum-1617368550-1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `ui_supplier_rate_master`
--

CREATE TABLE `ui_supplier_rate_master` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL DEFAULT 0,
  `item_id` int(11) NOT NULL DEFAULT 0,
  `rate` double(11,2) NOT NULL DEFAULT 0.00,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ui_supplier_rate_master`
--

INSERT INTO `ui_supplier_rate_master` (`id`, `supplier_id`, `item_id`, `rate`, `created_by`, `created_time`) VALUES
(176, 153, 1, 100.00, 1, '2021-04-02 18:32:48');

-- --------------------------------------------------------

--
-- Table structure for table `ui_supplier_rate_master_history`
--

CREATE TABLE `ui_supplier_rate_master_history` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL DEFAULT 0,
  `item_id` int(11) NOT NULL DEFAULT 0,
  `rate` double(11,2) NOT NULL DEFAULT 0.00,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ui_tax_invoice_item_transaction`
--

CREATE TABLE `ui_tax_invoice_item_transaction` (
  `id` int(11) NOT NULL,
  `tit_id` int(11) NOT NULL DEFAULT 0,
  `invoice_no` varchar(400) DEFAULT NULL,
  `item_id` int(11) NOT NULL DEFAULT 0,
  `sheet_type` int(11) NOT NULL DEFAULT 0,
  `length` double(11,2) NOT NULL DEFAULT 0.00,
  `width` double(11,2) NOT NULL DEFAULT 0.00,
  `thickness` double(11,2) NOT NULL DEFAULT 0.00,
  `pcs` double(11,2) NOT NULL DEFAULT 0.00,
  `description` text DEFAULT NULL,
  `rate` double(11,2) NOT NULL DEFAULT 0.00,
  `qty` int(11) NOT NULL DEFAULT 0,
  `unit` varchar(100) DEFAULT NULL,
  `amt` double(11,2) NOT NULL DEFAULT 0.00,
  `disc_per` double(11,2) NOT NULL DEFAULT 0.00,
  `disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `after_disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `cgst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `cgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `sgst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `sgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `igst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `igst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `net_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` date DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` date DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ui_tax_invoice_item_transaction`
--

INSERT INTO `ui_tax_invoice_item_transaction` (`id`, `tit_id`, `invoice_no`, `item_id`, `sheet_type`, `length`, `width`, `thickness`, `pcs`, `description`, `rate`, `qty`, `unit`, `amt`, `disc_per`, `disc_amt`, `after_disc_amt`, `cgst_per`, `cgst_amt`, `sgst_per`, `sgst_amt`, `igst_per`, `igst_amt`, `net_amt`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(257, 28, 'INV/20-21/00001', 2, 5, 122.00, 12.00, 1.00, 1.00, 'Size -:1920*1265*1.00MM=1.00NO', 45.00, 26, 'NOS', 1170.00, 0.00, 0.00, 1170.00, 6.00, 70.20, 6.00, 70.20, 0.00, 0.00, 1310.40, 0, NULL, 0, NULL, 0, NULL),
(258, 28, 'INV/20-21/00001', 2, 0, 1990.00, 475.00, 1.00, 1.00, 'Size -:1990*475*1.00MM=1.00NO', 45.00, 10, 'NOS', 450.00, 0.00, 0.00, 450.00, 6.00, 27.00, 6.00, 27.00, 0.00, 0.00, 504.00, 0, NULL, 0, NULL, 0, NULL),
(259, 28, 'INV/20-21/00001', 2, 0, 1926.00, 816.00, 1.00, 1.00, 'Size -:1926*816*1.00MM=1.00NO', 45.00, 17, 'NOS', 765.00, 0.00, 0.00, 765.00, 6.00, 45.90, 6.00, 45.90, 0.00, 0.00, 856.80, 0, NULL, 0, NULL, 0, NULL),
(260, 28, 'INV/20-21/00001', 2, 0, 1.00, 1.00, 1.00, 1.00, 'Size -:1*1*1MM=1NO', 45.00, 8, 'NOS', 360.00, 0.00, 0.00, 360.00, 6.00, 21.60, 6.00, 21.60, 0.00, 0.00, 403.20, 0, NULL, 0, NULL, 0, NULL),
(261, 28, 'INV/20-21/00001', 2, 0, 1.00, 2.00, 1.00, 1.00, 'Size -:1*2*1MM=1NO', 45.00, 16, 'NOS', 720.00, 0.00, 0.00, 720.00, 6.00, 43.20, 6.00, 43.20, 0.00, 0.00, 806.40, 0, NULL, 0, NULL, 0, NULL),
(262, 28, 'INV/20-21/00001', 2, 0, 1.00, 1.00, 1.00, 1.00, 'Size -:1*1*1MM=1NO', 45.00, 8, 'NOS', 360.00, 0.00, 0.00, 360.00, 6.00, 21.60, 6.00, 21.60, 0.00, 0.00, 403.20, 0, NULL, 0, NULL, 0, NULL),
(263, 28, 'INV/20-21/00001', 2, 0, 1.00, 1.00, 1.00, 1.00, 'Size -:1*1*1MM=1NO', 45.00, 8, 'NOS', 353.70, 0.00, 0.00, 353.70, 6.00, 21.22, 6.00, 21.22, 0.00, 0.00, 396.14, 0, NULL, 0, NULL, 0, NULL),
(264, 28, 'INV/20-21/00001', 0, 0, 0.00, 0.00, 0.00, 0.00, '', 0.00, 0, '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ui_tax_invoice_transaction`
--

CREATE TABLE `ui_tax_invoice_transaction` (
  `id` int(11) NOT NULL,
  `int_invoice_on` varchar(100) DEFAULT NULL,
  `invoice_no` varchar(400) DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `customer_id` int(11) NOT NULL DEFAULT 0,
  `gst_applicable` int(11) NOT NULL DEFAULT 0,
  `gst_per` double(11,2) NOT NULL DEFAULT 0.00,
  `total_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_after_disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_cgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_sgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_igst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `total_net_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `service_charges` double(11,2) NOT NULL DEFAULT 0.00,
  `final_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `final_disc_per` double(11,2) NOT NULL DEFAULT 0.00,
  `final_disc_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `after_disc_final_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `notes` varchar(500) DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` date DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` date DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` date DEFAULT NULL,
  `delivery_challen_no` varchar(50) DEFAULT NULL,
  `delivery_challen_date` date DEFAULT NULL,
  `buyer_order_on` varchar(20) DEFAULT NULL,
  `buyer_order_date` date DEFAULT NULL,
  `vehicle_no` varchar(20) DEFAULT NULL,
  `e_way_bill_no` varchar(20) DEFAULT NULL,
  `lr_no` varchar(20) DEFAULT NULL,
  `lr_date` date DEFAULT NULL,
  `terms_of_payment` text DEFAULT NULL,
  `service_charges_cgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `service_charges_sgst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `service_charges_igst_amt` double(11,2) NOT NULL DEFAULT 0.00,
  `ship_to` int(11) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ui_tax_invoice_transaction`
--

INSERT INTO `ui_tax_invoice_transaction` (`id`, `int_invoice_on`, `invoice_no`, `invoice_date`, `due_date`, `customer_id`, `gst_applicable`, `gst_per`, `total_amt`, `total_disc_amt`, `total_after_disc_amt`, `total_cgst_amt`, `total_sgst_amt`, `total_igst_amt`, `total_net_amt`, `service_charges`, `final_amt`, `final_disc_per`, `final_disc_amt`, `after_disc_final_amt`, `notes`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`, `delivery_challen_no`, `delivery_challen_date`, `buyer_order_on`, `buyer_order_date`, `vehicle_no`, `e_way_bill_no`, `lr_no`, `lr_date`, `terms_of_payment`, `service_charges_cgst_amt`, `service_charges_sgst_amt`, `service_charges_igst_amt`, `ship_to`, `address`) VALUES
(28, '00001', 'INV/20-21/00001', '2021-04-05', '2021-04-05', 1, 2, 0.00, 521995.50, 0.00, 521995.50, 250.72, 250.72, 0.00, 522496.94, 0.00, 522496.94, 0.00, 0.00, 0.00, '', 1, '2021-04-05', 1, '2021-04-06', 0, NULL, '', '0000-00-00', '', '0000-00-00', '', '', '', '0000-00-00', '', 0.00, 0.00, 0.00, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `ui_tax_master`
--

CREATE TABLE `ui_tax_master` (
  `id` int(11) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `cgst` double(11,2) NOT NULL DEFAULT 0.00,
  `sgst` double(11,2) DEFAULT 0.00,
  `igst` double(11,2) NOT NULL DEFAULT 0.00,
  `active` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_time` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_time` datetime DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_time` datetime DEFAULT NULL,
  `cess_tax` double(11,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ui_transporter_master`
--

CREATE TABLE `ui_transporter_master` (
  `id` int(11) NOT NULL,
  `transporter_name` varchar(100) DEFAULT NULL,
  `company_phone` varchar(20) DEFAULT NULL,
  `company_pan` varchar(20) DEFAULT NULL,
  `company_email` varchar(50) DEFAULT NULL,
  `company_gst` varchar(20) DEFAULT NULL,
  `billing_address` text DEFAULT NULL,
  `shipping_address` text DEFAULT NULL,
  `contact_person_name` varchar(100) DEFAULT NULL,
  `contact_person_phone` varchar(20) DEFAULT NULL,
  `contact_person_whatsapp` int(20) DEFAULT NULL,
  `contact_person_email` varchar(50) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` tinyint(4) NOT NULL DEFAULT 0,
  `created_time` datetime DEFAULT NULL,
  `updated_by` tinyint(4) NOT NULL DEFAULT 0,
  `updated_time` datetime DEFAULT NULL,
  `deleted_by` tinyint(4) NOT NULL DEFAULT 0,
  `deleted_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `company_info`
--
ALTER TABLE `company_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mstates`
--
ALTER TABLE `mstates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_admin`
--
ALTER TABLE `ui_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_credit_note_item_transaction`
--
ALTER TABLE `ui_credit_note_item_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_credit_note_transaction`
--
ALTER TABLE `ui_credit_note_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_customer_master`
--
ALTER TABLE `ui_customer_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_customer_rate_master`
--
ALTER TABLE `ui_customer_rate_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_customer_rate_master_history`
--
ALTER TABLE `ui_customer_rate_master_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_debit_item_transaction`
--
ALTER TABLE `ui_debit_item_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_debit_note_trasaction`
--
ALTER TABLE `ui_debit_note_trasaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_delivery_challan_item_transaction`
--
ALTER TABLE `ui_delivery_challan_item_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_delivery_challan_transaction`
--
ALTER TABLE `ui_delivery_challan_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_item_group_master`
--
ALTER TABLE `ui_item_group_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_item_master`
--
ALTER TABLE `ui_item_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_ledger_master`
--
ALTER TABLE `ui_ledger_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_opening_balance_master`
--
ALTER TABLE `ui_opening_balance_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_opening_stock`
--
ALTER TABLE `ui_opening_stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_payment_bill_transaction`
--
ALTER TABLE `ui_payment_bill_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_payment_transaction`
--
ALTER TABLE `ui_payment_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_proforma_invoice_item_transaction`
--
ALTER TABLE `ui_proforma_invoice_item_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_proforma_invoice_transaction`
--
ALTER TABLE `ui_proforma_invoice_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_purchase_bill_item_transaction`
--
ALTER TABLE `ui_purchase_bill_item_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_purchase_bill_transaction`
--
ALTER TABLE `ui_purchase_bill_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_purchase_order_item_transaction`
--
ALTER TABLE `ui_purchase_order_item_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_purchase_order_transaction`
--
ALTER TABLE `ui_purchase_order_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_quotation_item_transaction`
--
ALTER TABLE `ui_quotation_item_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_quotation_transaction`
--
ALTER TABLE `ui_quotation_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_recevied_bil_transaction`
--
ALTER TABLE `ui_recevied_bil_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_recevied_transaction`
--
ALTER TABLE `ui_recevied_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_supplier_master`
--
ALTER TABLE `ui_supplier_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_supplier_rate_master`
--
ALTER TABLE `ui_supplier_rate_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_supplier_rate_master_history`
--
ALTER TABLE `ui_supplier_rate_master_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_tax_invoice_item_transaction`
--
ALTER TABLE `ui_tax_invoice_item_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_tax_invoice_transaction`
--
ALTER TABLE `ui_tax_invoice_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_tax_master`
--
ALTER TABLE `ui_tax_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_transporter_master`
--
ALTER TABLE `ui_transporter_master`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company_info`
--
ALTER TABLE `company_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ui_admin`
--
ALTER TABLE `ui_admin`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ui_credit_note_item_transaction`
--
ALTER TABLE `ui_credit_note_item_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- AUTO_INCREMENT for table `ui_credit_note_transaction`
--
ALTER TABLE `ui_credit_note_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `ui_customer_master`
--
ALTER TABLE `ui_customer_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ui_customer_rate_master`
--
ALTER TABLE `ui_customer_rate_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=316;

--
-- AUTO_INCREMENT for table `ui_customer_rate_master_history`
--
ALTER TABLE `ui_customer_rate_master_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=314;

--
-- AUTO_INCREMENT for table `ui_debit_item_transaction`
--
ALTER TABLE `ui_debit_item_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `ui_debit_note_trasaction`
--
ALTER TABLE `ui_debit_note_trasaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `ui_delivery_challan_item_transaction`
--
ALTER TABLE `ui_delivery_challan_item_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `ui_delivery_challan_transaction`
--
ALTER TABLE `ui_delivery_challan_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `ui_item_group_master`
--
ALTER TABLE `ui_item_group_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `ui_item_master`
--
ALTER TABLE `ui_item_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ui_ledger_master`
--
ALTER TABLE `ui_ledger_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `ui_opening_balance_master`
--
ALTER TABLE `ui_opening_balance_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `ui_opening_stock`
--
ALTER TABLE `ui_opening_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `ui_payment_bill_transaction`
--
ALTER TABLE `ui_payment_bill_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `ui_payment_transaction`
--
ALTER TABLE `ui_payment_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `ui_proforma_invoice_item_transaction`
--
ALTER TABLE `ui_proforma_invoice_item_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `ui_proforma_invoice_transaction`
--
ALTER TABLE `ui_proforma_invoice_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ui_purchase_bill_item_transaction`
--
ALTER TABLE `ui_purchase_bill_item_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `ui_purchase_bill_transaction`
--
ALTER TABLE `ui_purchase_bill_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `ui_purchase_order_item_transaction`
--
ALTER TABLE `ui_purchase_order_item_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=399;

--
-- AUTO_INCREMENT for table `ui_purchase_order_transaction`
--
ALTER TABLE `ui_purchase_order_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `ui_quotation_item_transaction`
--
ALTER TABLE `ui_quotation_item_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=310;

--
-- AUTO_INCREMENT for table `ui_quotation_transaction`
--
ALTER TABLE `ui_quotation_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `ui_recevied_bil_transaction`
--
ALTER TABLE `ui_recevied_bil_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=217;

--
-- AUTO_INCREMENT for table `ui_recevied_transaction`
--
ALTER TABLE `ui_recevied_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `ui_supplier_master`
--
ALTER TABLE `ui_supplier_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;

--
-- AUTO_INCREMENT for table `ui_supplier_rate_master`
--
ALTER TABLE `ui_supplier_rate_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;

--
-- AUTO_INCREMENT for table `ui_supplier_rate_master_history`
--
ALTER TABLE `ui_supplier_rate_master_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;

--
-- AUTO_INCREMENT for table `ui_tax_invoice_item_transaction`
--
ALTER TABLE `ui_tax_invoice_item_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=265;

--
-- AUTO_INCREMENT for table `ui_tax_invoice_transaction`
--
ALTER TABLE `ui_tax_invoice_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `ui_tax_master`
--
ALTER TABLE `ui_tax_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ui_transporter_master`
--
ALTER TABLE `ui_transporter_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
