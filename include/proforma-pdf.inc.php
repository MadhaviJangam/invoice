<?php

$admin = new AdminFunctions();
	if(isset($admin)){
		$pdfObj = $admin;
   } 
   ob_start();

   $ProformaDetails=$admin->getUniqueProformaInvoiceDetailsById($_GET['id']);
   $customerDetails=$admin->getUniqueCustomerMasterById($ProformaDetails['customer_id']);
   $getStateDetails=$admin->getStateCodeMasterByName($customerDetails['statename']);
   $getItemDetailTOTAL= $admin->getUniqueProformaInvoiceDetailsById($_GET['id']);
   $getTaxInvoiceItemDetails=$admin->getUniqueProformaInvoiceItemDetailsByTaxInvoiceId($_GET['id']);
   $rowcount=mysqli_num_rows($getTaxInvoiceItemDetails);
   $getNumberToText=$admin->getIndianCurrency($getItemDetailTOTAL['final_amt']);
   $getItemPerDetails=$admin->getUniqueProformaInvoiceGSTDetailsByTaxInvoiceId($_GET['id']);
   $companyInfo=$admin->getUniqueCompanyMasterById();

   $arrayHSNcode=array();

if ($ProformaDetails['delivery_challen_date'] == '0000-00-00') {
   $delivery_challen_date = '';
} else {
    $delivery_challen_date = date("d-m-Y", strtotime($ProformaDetails['delivery_challen_date']));
}
if ($ProformaDetails['buyer_order_date'] == '0000-00-00') {
    $buyer_order_date = '';
 } else {
     $buyer_order_date = date("d-m-Y", strtotime($ProformaDetails['buyer_order_date']));
 }
 if ($ProformaDetails['lr_date'] == '0000-00-00') {
    $lr_date = '';
 } else {
     $lr_date = date("d-m-Y", strtotime($ProformaDetails['lr_date']));
 }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   
   <body>
   <table border="0" style="width:100%">
        <!-- <tr> 
            <td style="text-align:center;">Proforma Invoice</td>
        </tr> -->
        <tr>
            <td style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;font-size:20px;width:100%;text-align:center;"><b><?php echo $companyInfo['company_name'];?></b></td>
            
        </tr>
        <tr>
            <td colspan="6" style="text-align:center;border-left:1px solid #000;width:75%"><b>GSTN : <?php echo $companyInfo['gst_no'];?></b></td>
            <td rowspan="3" style="width:25%;border-right:1px solid #000;"><img src='html-pdf/images/car.jpg'></td>
        </tr>
        <tr>
            <td colspan="6" style="text-align:center;border-left:1px solid #000;"><?php echo $companyInfo['company_address'];?></td>
        </tr>
        <tr>
            <td colspan="6" style="text-align:left;border-left:1px solid #000;color:blue">Mob No:<?php echo $companyInfo['mobile_no'];?></td>
        </tr>
        <tr>
            <td  style="text-left:right;border-right:1px solid #000;border-left:1px solid #000;;color:blue;width:100%"><?php echo $companyInfo['email_id'];?></td>
        </tr>
        <tr style="font-size:12px;">
            <td style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"><b>Proforma Invoice</b></td>
        </tr>

        <tr>
            <td style="text-align:left;border-left:1px solid #000;" width="70%">To,</td>
            <td style="text-align:left;border-right:1px solid #000;" width="30%">Proforma No :  <?php echo $ProformaDetails['proforma_no']; ?></td>  
        </tr>
        <tr>
            <td style="text-align:left;border-left:1px solid #000;" width="70%"><b><?php echo substr($customerDetails['customer_name'] ,"0", 100); ?></b></td>  
            <td style="text-left:center;border-right:1px solid #000;" width="30%">Proforma Date : <?php echo date("d-m-Y", strtotime($ProformaDetails['proforma_date'])); ?></td>  
        </tr>
        <tr>
            <td style="text-align:left;border-left:1px solid #000;" width="70%"><?php echo $customerDetails['billing_address']; ?></td>  
            <td style="text-align:left;border-right:1px solid #000;" width="30%">Purchase Order No : <?php echo $ProformaDetails['buyer_order_on']; ?></td> 
        </tr>
        <tr>
        <td style="text-align:left;border-left:1px solid #000;" width="70%">State: <?php echo $customerDetails['statename'];?>&nbsp;&nbsp;&nbsp;State code:<?php echo $getStateDetails['statecode']; ?></td>
            <td style="text-align:left;border-right:1px solid #000;" width="30%">Purchase Order Date : <?php if($ProformaDetails['buyer_order_on']!='') {echo $buyer_order_date; }?></td>   
        </tr>
        <tr>
            <td style="text-align:left;border-left:1px solid #000;border-right:1px solid #000" width="100%">Contact No:<?php echo$customerDetails['contact_person_phone']; ?></td>
        </tr> 
        <tr>
            <td style="text-align:left;border-left:1px solid #000;border-right:1px solid #000" width="100%">Email ID:<?php echo$customerDetails['company_email']; ?></td>
        </tr> 
        <tr>
            <td style="text-align:left;border-left:1px solid #000;border-right:1px solid #000" width="100%">Kind Attn : <?php echo $ProformaDetails['kind_atten']; ?></td> 
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="100%"></td>    
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="100%">Dear Sir / Madam,</td>    
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="100%">In accordance with your purchase order no. <?php echo $ProformaDetails['buyer_order_on']; ?> dated <?php if($ProformaDetails['buyer_order_on']!='') {echo $buyer_order_date; }?> , we are pleased to submit our Proforma Invoice as follows;</td>    
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="100%"></td>    
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="100%">Terms Of Payment : <?php echo nl2br($ProformaDetails['terms_of_payment']); ?></td>    
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="100%"></td>    
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="100%">Terms Of Delivery : <?php echo nl2br($ProformaDetails['terms_of_delivery']); ?></td>    
        </tr>
        <tr>
            <td style="text-align:left;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="100%"></td>    
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"><b>SR.NO</b></td>
            <td  style="text-align:left;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="35%"><b>DESCRIPTION OF GOODS AND SERVICE</b></td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="13%"><b>HSN CODE</b></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><b>QTY</b></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><b>UNIT</b></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"><b>RATE</b></td>
            <td style="text-align:right;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="15%"><b>BASIC VALUE </b></td>  
        </tr>
        <?php
             $x= 1;   
             
            while ($row = $admin->fetch($getTaxInvoiceItemDetails)) {
                $getItemDetails= $admin->getUniqueItemMasterById($row['item_id']);
                array_push($arrayHSNcode,$getItemDetails['hsn_code']);
                $total_cgst_amt =  $ProformaDetails['total_cgst_amt']+ $ProformaDetails['total_sgst_amt'] +$ProformaDetails['total_igst_amt'];  
                
        ?>
        <tr style="font-size:10px;">
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"><?php echo $x; ?></td>
            <td  style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="35%"><?php  echo $getItemDetails['item_name'];?></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="13%"><?php  echo $getItemDetails['hsn_code'];?></td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $row['qty'];?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $row['unit'];?></td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"><?php  echo $row['rate'];?></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;" width="15%"><?php  echo $row['net_amt'];?></td>  
        </tr>

            <?php
            $x++;  
                }
            ?>
            <?php 
            $rowprint= 10 - $rowcount;
            for($i=1;$i<=$rowprint;$i++){ ?>
            <tr style="font-size:10px;">
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="35%"></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="13%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="15%"></td>
        </tr>
            <?php } ?>
 
        <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"></td>
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="35%"></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="13%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"></td>
            <td style="text-align:center;border-right:1px solid #000;border-left:1px solid #000;" width="15%"></td>  
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-top:1px solid #000;border-left:1px solid #000;" width="85%"><b>Basic Total</b></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;;border-top:1px solid #000;" width="15%"><?php echo $admin->formatAmount($getItemDetailTOTAL['final_amt']);?></td>  
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="85%">GST Total</td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;" width="15%"><?php echo $admin->formatAmount($total_cgst_amt);?></td>  
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;" width="85%"><b>Total Amount INR</b></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="15%"><?php echo $admin->formatAmount($getItemDetailTOTAL['final_amt']);?></td>  
        </tr>
        <tr style="font-size:10px;">
            <td colspa="5" style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="100%"><b>Total Invoice Value (In Figure)  : INR. <?php echo $admin->formatAmount($getItemDetailTOTAL['final_amt']);?></b></td>
        </tr>
        <tr style="font-size:10px;">
            <td colspan="6" style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="100%"><b>Total Invoice Value (In Words)  : <?php echo ($getNumberToText); ?> Only </b></td>
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="100%"></td>    
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:left;padding:0px;border-left:1px solid #000;" width="60%">Bank Details</td>
            <td style="text-align:left;padding:0px;border-right:1px solid #000;"  width="40%"><br><b>FOR&nbsp;&nbsp;<?php echo $companyInfo['company_name'];?></b></td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:left;padding:0px;border-left:1px solid #000;" width="60%">NAME :&nbsp;<?php echo $companyInfo['bank_name'];?>.</td>
            <td style="text-align:left;padding:0px;border-right:1px solid #000;"  width="40%"></td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:left;padding:0px;border-left:1px solid #000;" width="60%">ADDRESS :&nbsp;<?php echo $companyInfo['bank_address'];?></td>
            <td style="text-align:left;padding:0px;border-right:1px solid #000;"  width="40%"></td>
        </tr>
        
        <tr style="font-size:10px;">
            <td  style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="100%">A/c No. :&nbsp;<?php echo $companyInfo['account_no'];?></td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:left;padding:0px;border-left:1px solid #000;" width="60%">IFSC Code :&nbsp;<?php echo $companyInfo['ifsc_code'];?></td>
            <td style="text-align:left;padding:0px;border-right:1px solid #000;"  width="40%">Authorised Singnatory</td>
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="100%"></td>    
        </tr>
        <tr>
            <td colspan="2" style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;font-size:10px;" width="100%">Factory:<span style="font-size:8px;"><?php echo $companyInfo['factory_address'];?></span></td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:center;padding:0px;border-left:1px solid #000;border-bottom:1px solid #000;" width="100%"><b>This Is Computer Generated Proforma Invoice</b></td>
        </tr>
      </table>
   </body>
</html>
<?php 
	$invoiceMsg = ob_get_contents();
	ob_end_clean();
?>