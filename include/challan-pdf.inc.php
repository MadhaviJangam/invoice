<?php

$admin = new AdminFunctions();
	if(isset($admin)){
		$pdfObj = $admin;
   } 
   ob_start();

   $challanDetails=$admin->getUniqueChallanDetailsById($_GET['id']);
   $customerDetails=$admin->getUniqueCustomerMasterById($challanDetails['customer_id']);
   $getStateDetails=$admin->getStateCodeMasterByName($customerDetails['statename']);
   $getTaxInvoiceItemDetails=$admin->getUniqueChallanItemDetailsById($_GET['id']);
   $rowcount=mysqli_num_rows($getTaxInvoiceItemDetails);
   $getItemPerDetails=$admin->getUniqueTaxInvoiceGSTDetailsByTaxInvoiceId($_GET['id']);
   $companyInfo=$admin->getUniqueCompanyMasterById();

   

if ($challanDetails['challan_date'] == '0000-00-00') {
   $challan_date = '';
} else {
    $challan_date = date("d-m-Y", strtotime($challanDetails['challan_date']));
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   
   <body>
   <table border="0" style="width:100%">
        <tr> 
            <td style="text-align:center;">Delivery Challan</td>
        </tr>
        <tr>
            <td style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;color:#f26d00;font-size:20px;width:100%;text-align:center;"><?php echo $companyInfo['company_name'];?></td>
        </tr>
        <tr>
            <td colspan="6" style="text-align:center;border-left:1px solid #000;border-right:1px solid #000;"><?php echo $companyInfo['company_address'];?></td>
        </tr>
        <tr>
            <td colspan="3" style="text-align:left;border-left:1px solid #000;border-right:1px solid #000;">GSTN : <?php echo $companyInfo['gst_no'];?></td>
        </tr>
        <tr>
            <td style="text-align:left;border-left:1px solid #000;border-bottom:1px solid #000;" width="60%">Mob No:<?php echo $companyInfo['mobile_no'];?></td>
            <td  style="text-align:right;border-right:1px solid #000;border-bottom:1px solid #000;" width="40%"><?php echo $companyInfo['email_id'];?></td>
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;"  width="60%">Details Of Receiver ( Bill To)</td>  
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="40%"></td>   
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;"  width="60%"><?php echo substr($customerDetails['customer_name'] ,"0", 100); ?></td>  
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="40%">Challan No:<?php echo $challanDetails['delivery_challan_no']; ?></td>  
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;"  width="60%"><br><?php echo $customerDetails['billing_address']; ?></td>    
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="40%">Challan Date :<?php echo $challan_date; ?></td>  
        </tr>
        <tr>
            <td style="text-align:left;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="60%">GST No:</td>  
            <td style="text-align:left;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="40%"></td>   
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%">SR.NO</td>
            <td  style="text-align:left;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="40%">DESCRIPTION OF GOODS AND SERVICE</td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="15%">QTY</td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="15%">RATE</td>
            <td style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="20%">TOTAL</td>
            
        </tr>
        <?php
             $x= 1;   
             
            while ($row = $admin->fetch($getTaxInvoiceItemDetails)) {
                $getItemDetails= $admin->getUniqueItemMasterById($row['item_id']);
               
        ?>
        <tr style="font-size:10px;">
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $x; ?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="40%"><?php  echo $getItemDetails['item_name'];?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="15%"><?php  echo $row['qty'];?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="15%"><?php  echo $row['rate'];?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="20%"><?php  echo $row['amt'];?></td>
        </tr>
        <?php
            $x++;  
                }
            ?>
        <?php 
        $rowprint= 40 - $rowcount;
        for($i=1;$i<=$rowprint;$i++){ ?>
        <tr style="font-size:10px;">
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="40%"></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="15%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="15%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="20%"></td>
        </tr>
            <?php } ?>
        <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="80%">Total</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="20%"><?php echo $challanDetails['total_amt'];?></td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:left;padding:0px;border-left:1px solid #000;" width="64%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;" width="36%"></td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:left;padding:0px;border-left:1px solid #000;" width="64%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;" width="36%"></td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:left;padding:0px;border-left:1px solid #000;border-bottom:1px solid #000;" width="64%">Receiver Signature</td>
            <td style="text-align:left;padding:0px;border-right:1px solid #000;border-bottom:1px solid #000;" width="36%">Signature</td>
        </tr>
        
      </table>
   </body>
</html>
<?php 
	$invoiceMsg = ob_get_contents();
	ob_end_clean();
?>