<?php

$admin = new AdminFunctions();
	if(isset($admin)){
		$pdfObj = $admin;
   } 
   ob_start();
   if(isset($_GET['item_id'])){
       $item_id = $_GET['item_id'];
    }else{
        $item_id = '';
    }

   if(isset($_GET['from_date'])){  
       $from_date = $_GET['from_date'];
    }else{
        $from_date = '';
    }

   if(isset($_GET['to_date'])){
       $to_date = $_GET['to_date'];
    }else{
        $to_date = '';
    }
    $total_qty = 0;
    $total_sales_amt = 0;
    $total_purchase_amt = 0;
    $total_profit = 0;
    $total_profit_per=0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   
   <body>
   <table border="0" style="width:100%">
        <tr>
            <td style="text-align:left;font-size:10px;">Item Wise Profit  &nbsp;&nbsp;From Date: <?php if($from_date!=''){ echo date("d-m-Y",strtotime($from_date));}?>&nbsp;&nbsp;To Date: <?php if($to_date!=''){ echo date("d-m-Y",strtotime($to_date));}?></td>
        </tr>
        <tr style="">
            <td  rowspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="30%">ITEM NAME</td>
            <td colspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="24%">SALES</td>
            <td rowspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="15%">PURCHASE AMOUNT</td>
            <td rowspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"   width="11%">RATE</td>
            <td colspan="2"  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="20%">PROFIT</td>
        </tr>
        <tr style="">
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%">Quantity</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="14%">Amount</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%">Rs.</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%">%</td>
        </tr> 
       <?php 
        if($item_id!='' && $from_date == '' && $to_date == ''){
            $itemDetails = $admin->itemSalesByItemId($item_id);
            $purchaseDetails = $admin->getPurchaseRateByItemId($item_id);
            $proft =  ($itemDetails['amount'] - ($purchaseDetails['rate']*$itemDetails['qty']))/$itemDetails['amount']*100; 

            $total_qty = $total_qty + $itemDetails['qty'];
            $total_sales_amt =  $total_sales_amt + $itemDetails['amount'];
            $total_purchase_amt = $total_purchase_amt + $purchaseDetails['rate']*$itemDetails['qty'];
            $total_profit = $total_profit + $itemDetails['amount'] - ($purchaseDetails['rate']*$itemDetails['qty']);
            $total_profit_per = $total_profit_per + $proft;
            ?>

<tr>  
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="30%" ><?php echo $admin->getUniqueItemMasterById($itemDetails['item_id'])['item_name'];?></td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%" ><?php echo $itemDetails['qty']; ?></td>
            <td  style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="14%" ><?php echo $itemDetails['amount']; ?></td>

            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="15%"><?php echo ($purchaseDetails['rate']*$itemDetails['qty']); ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="11%"><?php echo $purchaseDetails['rate']; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo ($itemDetails['amount'] - ($purchaseDetails['rate']*$itemDetails['qty'])) ; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $admin->formatAmount($proft).'%'; ?></td>
        </tr>

            <?php }  ?>
            <?php   
            
        if($item_id=='' && $from_date != '' && $to_date != ''){
            $getAllSalesItemFromTodate = $admin->itemSalesAll($from_date,$to_date);
            while($row = $admin->fetch($getAllSalesItemFromTodate)) {
               $getpurchasebydate = $admin-> getPurchaseRateByItemIdAndDate($from_date,$to_date,$row['item_id']);
               $proft =  ($row['amount'] - ($getpurchasebydate['rate']*$row['qty']))/$row['amount']*100; 
           
               $total_qty = $total_qty + $row['qty'];
               $total_sales_amt =  $total_sales_amt + $row['amount'];
               $total_purchase_amt = $total_purchase_amt + $getpurchasebydate['rate']*$row['qty'];
               $total_profit = $total_profit + $row['amount'] - ($getpurchasebydate['rate']*$row['qty']);
               $total_profit_per = $total_profit_per + $proft;
           
           ?>
        <tr>  
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="30%" ><?php echo $admin->getUniqueItemMasterById($row['item_id'])['item_name'];?></td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%" ><?php echo $row['qty']; ?></td>
            <td  style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="14%" ><?php echo $row['amount']; ?></td>

            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="15%"><?php echo ($getpurchasebydate['rate']*$row['qty']); ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="11%"><?php echo $getpurchasebydate['rate']; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo ($row['amount'] - ($getpurchasebydate['rate']*$row['qty'])) ; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $admin->formatAmount($proft).'%'; ?></td>
        </tr>

        <?php } } ?>

        <?php  if($item_id!='' && $from_date != '' && $to_date != ''){
            $itemDetails = $admin->itemSales($from_date,$to_date,$item_id);

            $getpurchasebydate = $admin-> getPurchaseRateByItemIdAndDate($from_date,$to_date,$itemDetails['item_id']);
               
            $proft =  ($itemDetails['amount'] - ($getpurchasebydate['rate']*$itemDetails['qty']))/$itemDetails['amount']*100; 
            
            $total_qty = $total_qty + $itemDetails['qty'];
            $total_sales_amt =  $total_sales_amt + $itemDetails['amount'];
            $total_purchase_amt = $total_purchase_amt + $getpurchasebydate['rate']*$itemDetails['qty'];
            $total_profit = $total_profit + $itemDetails['amount'] - ($getpurchasebydate['rate']*$itemDetails['qty']);
            $total_profit_per = $total_profit_per + $proft;
            ?>
        <tr>  
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="30%" ><?php echo $admin->getUniqueItemMasterById($itemDetails['item_id'])['item_name'];?></td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%" ><?php echo $itemDetails['qty']; ?></td>
            <td  style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="14%" ><?php echo $itemDetails['amount']; ?></td>

            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="15%"><?php echo ($getpurchasebydate['rate']*$itemDetails['qty']); ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="11%"><?php echo $getpurchasebydate['rate']; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo ($itemDetails['amount'] - ($getpurchasebydate['rate']*$itemDetails['qty'])) ; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $admin->formatAmount($proft).'%'; ?></td>
        </tr>

        <?php } ?>
            
               <tr>
                <td width="100%" style="border-bottom:1px solid #000;"></td>
               </tr>
               <tr>
                  
                    <td  style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="30%" >Total</td>
                    <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%" ><?php echo $total_qty;?></td>
                    <td  style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="14%" ><?php echo $total_sales_amt; ?></td>
                    <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="15%"><?php echo $total_purchase_amt; ?></td>
                    <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="11%"></td>
                    <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $total_profit; ?></td>
                    <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $admin->formatAmount($total_profit_per).'%'; ?></td>
               </tr>
             
              
      </table>
   </body>
</html>
<?php 
	$invoiceMsg = ob_get_contents();
	ob_end_clean();
?>