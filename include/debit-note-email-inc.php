<?php

$admin = new AdminFunctions();
	if(isset($admin)){
		$pdfObj = $admin;
   } 
   ob_start();

   $debitDetails=$admin->getUniqueDebitNoteDetailsById($_GET['id']);
   $supplierDetails=$admin->getUniqueSupplierMasterById($debitDetails['supplier_id']);
   $getStateDetails=$admin->getStateCodeMasterByName($supplierDetails['statename']);
   $getItemDetailTOTAL= $admin->getUniqueDebitNoteDetailsById($_GET['id']);
   $getDebitItemDetails=$admin->getUniqueDebitNoteDetailsByTaxInvoiceId($_GET['id']);
   $rowcount=mysqli_num_rows($getDebitItemDetails);
   $getItemPerDetails=$admin->getUniqueDebitNoteDetailsByTaxInvoiceId($_GET['id']);
   $companyInfo=$admin->getUniqueCompanyMasterById();

   $arrayHSNcode=array();

if ($debitDetails['debit_date'] == '0000-00-00') {
   $debit_date = '';
} else {
    $debit_date = date("d-m-Y", strtotime($debitDetails['debit_date']));
}
?>
      
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   <body>
   <table border="1" style="width:100%">
    <tr>
        <td colspan="5" width="100%">
            Unique Software<br>
            office no 213, ambica complex,<br>
            vasai East.
    </td>
</tr>
    <tr>
        <td colspan="5" style="text-align:center" width="100%">
            <span>Debit Note</span>
    </td>
</tr>
<tr>
        <td  colspan="3" width="50%">
        Debit No :  <?php echo $debitDetails['debit_note_no']; ?> <br>
        Debit Date : <?php echo $debit_date; ?><br>
        </td>
        <td colspan="3" width="50%"></td>
        
</tr>
<tr>
        <td colspan="5" width="100%">Bill To : </td>
</tr>
<tr>
        <td width="10%">#</td>
        <td width="40%">Item & Description	</td>
        <td width="10%">Qty</td>
        <td width="20%">rate</td>
        <td width="20%">(Amount+GST)</td>
</tr>
<?php
$total=0;
             $x= 1;   
            while ($row = $admin->fetch($getDebitItemDetails)) {
                $getItemDetails= $admin->getUniqueItemMasterById($row['item_id']);
                
        ?>
<tr>
        <td width="10%"><?php echo $x; ?></td>
        <td width="40%"><?php  echo $getItemDetails['item_name'];?></td>
        <td width="10%"><?php  echo $row['qty'];?></td>
        <td width="20%"><?php  echo $row['rate'];?></td>
        <td width="20%"><?php  echo $row['amt'];?></td>
</tr>
<?php
$total+=$row['amt'];
            $x++;  
                }

            ?>
<tr>
        <td rowspan="2" colspan="3">
        Total In Words <br>
Indian Rupee <?php echo $admin->getIndianCurrency($getItemDetailTOTAL['total_net_amt']+$getItemDetailTOTAL['total_cgst_amt']+$getItemDetailTOTAL['total_sgst_amt']+$getItemDetailTOTAL['total_igst_amt']);?> Only <br>
Thanks for your business. 

        </td>
        <td colspan="2">
Total : <?php echo $admin->formatAmount($getItemDetailTOTAL['total_net_amt']+$getItemDetailTOTAL['total_cgst_amt']+$getItemDetailTOTAL['total_sgst_amt']+$getItemDetailTOTAL['total_igst_amt']);?><br>      
Sub Total :	 <?php echo $getItemDetailTOTAL['total_net_amt']; ?><br>




        </td>
       
</tr>

<tr>    

        <td colspan="2"><br>   <br>   <br>   <br>   <br>   <br>Authorized Signature
</td>
 
</tr>

      </table>
   </body>
</html>
<?php 
	$invoiceMsg1 = ob_get_contents();
	ob_end_clean();
?>