<?php

$admin = new AdminFunctions();
	if(isset($admin)){
		$pdfObj = $admin;
   } 
   ob_start();
   if(isset($_GET['supplier_id'])){
       $supplier_id = $_GET['supplier_id'];
    }else{
        $supplier_id = '';
    }

   if(isset($_GET['from_date'])){  
       $from_date = $_GET['from_date'];
    }else{
        $from_date = '';
    }

   if(isset($_GET['to_date'])){
       $to_date = $_GET['to_date'];
    }else{
        $to_date = '';
    }

   if(isset($_GET['supplier_bill_id'])){
       $supplier_bill_id = $_GET['supplier_bill_id'];
    }else{
        $supplier_bill_id = '';
    }
    $query='';
    if($supplier_id=='' && $from_date=='' && $to_date=='' ){
        $query="SELECT * FROM ".PREFIX."payment_transaction";
    }

    if($supplier_id!='' && $from_date=='' && $to_date=='' ){
        $query="SELECT * FROM ".PREFIX."payment_transaction WHERE supplier_id='".$supplier_id."' ";
    }
    
    if($supplier_id!=''  && $from_date!='' && $to_date!='' ){
        $query="SELECT * FROM ".PREFIX."payment_transaction WHERE supplier_id='".$supplier_id."' AND payment_date BETWEEN '".$from_date."' AND '".$to_date."' ";
    }

    if($supplier_id==''  && $from_date!='' && $to_date!='' ){
        $query="SELECT * FROM ".PREFIX."payment_transaction WHERE payment_date BETWEEN '".$from_date."' AND '".$to_date."' ";
    }

 
    $result=$admin->query($query);
    $num_rows1 = mysqli_num_rows($result);
    $total_inv_amt=0;
    $total_paid=0;
    $total_bal=0;
    $total_amount=0;

  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   
   <body>
   <table border="0" style="width:100%">
        <!-- <tr>
            <td style="text-align:left;font-size:10px;">Payment Statement &nbsp;&nbsp;From Date: <?php echo date("d-m-y",strtotime($from_date));?>&nbsp;&nbsp;To Date: <?php echo date("d-m-y",strtotime($to_date));?></td>
        </tr>
        <tr style="">
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="10%">PAYMENT NO</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="10%">DATE</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="30%">PARTY NAME</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"   width="10%">BILL NO</td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="10%">INV AMT</td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"   width="10%">PAID AMT</td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="10%">BAL AMT</td>
            <td style="text-align:right;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"               width="10%">AMOUNT</td>
        </tr> -->

        <?php  
            while($row = $admin->fetch($result)) {
            $id =  $row['id'];
            $billDetails = $admin -> getUniquePaymentDetailsById($row['id']);
              
        ?>
            <tr style="">
                <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php 
            

            ?><?php echo $row['payment_no'];?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo date("d-m-Y", strtotime($row['payment_date']));?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="30%"><?php echo $admin->getUniqueSupplierMasterById($row['supplier_id'])['supplier_name'];?></td>
            <td  colspan="12" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="50%"></td>
            </tr>
            <?php
            
                while($rows = $admin->fetch($billDetails)) { 

                $total_inv_amt= $total_inv_amt + $rows['inv_amt'];
                $total_paid= $total_paid + $rows['paid_amt'];
                $total_bal =$total_bal + $rows['balance_amt'];
                $total_amount=$total_amount + $rows['net_amt'];  
                ?>
               <tr>  
                    <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%" ></td>
                    <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%" ></td>
                    <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="30%" ></td>

                    <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $admin->getUniquePurchaseBillDetailsById($rows['supplier_bill_id'])['supplier_bill_no']; ?></td>
                    <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $rows['inv_amt'];?></td>
                    <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $rows['paid_amt'];?></td>
                    <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $rows['balance_amt'];?></td>
                    <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $rows['net_amt'];?></td>
               </tr>
               <?php } ?>
               <?php } ?>
               <tr>
                <td width="100%" style="border-bottom:1px solid #000;"></td>
               </tr>
               <tr>
                  
                    <td  style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="60%" >Total</td>
                    <td  style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%" ><?php  echo $total_inv_amt;?></td>
                    <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $total_paid; ?></td>
                    <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $total_bal; ?></td>
                    <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $total_amount; ?></td>
               </tr>
             
              
      </table>
   </body>
</html>
<?php 
	$invoiceMsg = ob_get_contents();
	ob_end_clean();
?>