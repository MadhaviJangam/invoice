<?php 
$basename = basename($_SERVER['REQUEST_URI']);	
$currentPage = pathinfo($_SERVER['PHP_SELF'], PATHINFO_BASENAME);
?>

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="dashboard.php" class="brand-link">
      <img src="https://usssoft.com/info/assets/img/logo.jpg" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Unique Invoice</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../../dist/img/avatar5.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="dashboard.php" class="d-block">Beta Version 0.1</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
			   with font-awesome or any other icon font library -->
			   <?php
					$masterPages = array(
						'customer-master.php',
						'customer-rate-master.php',
            'item-master.php',
            'item-group-master.php',
						'opening-balance-master.php',
						'supplier-master.php',
						'supplier-rate-master.php',
            'ledger-master.php',
            'tax-master.php',
						'transporter-master.php',
					);
				?>
         <?php
					$transactionPages = array(
            'quatation.php',
						'proforma.php',
						'tax-invoice.php',
            'purchase-order.php',
            'credit-note.php',
						'purchase-bill.php',
						'debit-note.php',
						'delivery-challan.php',
            'payment.php',
            'received.php',
           
            'compose-mail.php',
					);
				?>
				
          <li class="nav-item <?php if(in_array($currentPage, $masterPages)){ echo 'menu-is-opening menu-open'; } ?>">
            <a href="#" class="nav-link  <?php if(in_array($currentPage, $masterPages)){ echo 'active'; } ?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Master
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item loadstarter">
                <a href="customer-master.php" class="nav-link <?php if($currentPage == 'customer-master.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon "></i>
                  <p>Customer Master</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="item-group-master.php" class="nav-link <?php if($currentPage == 'item-group-master.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Item Group Master</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="item-master.php" class="nav-link <?php if($currentPage == 'item-master.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Item Master</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="customer-rate-master.php" class="nav-link <?php if($currentPage == 'customer-rate-master.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Customer Rate Master</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="opening-balance-master.php" class="nav-link <?php if($currentPage == 'opening-balance-master.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Opening Balance</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="supplier-master.php" class="nav-link <?php if($currentPage == 'supplier-master.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Supplier Master</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="supplier-rate-master.php" class="nav-link <?php if($currentPage == 'supplier-rate-master.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Supplier Rate Master</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="ledger-master.php" class="nav-link <?php if($currentPage == 'ledger-master.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Ledger Master</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="tax-master.php" class="nav-link <?php if($currentPage == 'tax-master.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Tax Master</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="transporter-master.php" class="nav-link <?php if($currentPage=='transporter-master.php'){ echo 'active';} ?>" >
                  <i class="far fa-circle nav-icon"></i>
                  <p>Transporter Master</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item <?php if(in_array($currentPage, $transactionPages)){ echo 'menu-is-opening menu-open'; } ?>">
            <a href="#" class="nav-link  <?php if(in_array($currentPage, $transactionPages)){ echo 'active'; } ?>">
              <i class="nav-icon fas fa-credit-card"></i>
              <p>
                Transaction
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item loadstarter">
                <a href="quatation.php" class="nav-link <?php if($currentPage == 'quatation.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon "></i>
                  <p>Quotation</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="delivery-challan.php" class="nav-link <?php if($currentPage == 'delivery-challan.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Delivery Challan</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="proforma.php" class="nav-link <?php if($currentPage == 'proforma.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Profoma Invoice</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="tax-invoice.php" class="nav-link <?php if($currentPage == 'tax-invoice.php' || $currentPage == 'tax-invoice-add.php' || $currentPage == 'compose-mail.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Tax Invoice</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="purchase-order.php" class="nav-link <?php if($currentPage == 'purchase-order.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Purchase Order</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="purchase-bill.php" class="nav-link <?php if($currentPage == 'purchase-bill.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Purchase Bill</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="credit-note.php" class="nav-link <?php if($currentPage == 'credit-note.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Credit Note</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="debit-note.php" class="nav-link <?php if($currentPage =='debit-note.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Debit Note</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="received.php" class="nav-link <?php if($currentPage == 'received.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Received(Income)</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="payment.php" class="nav-link <?php if($currentPage == 'payment.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Payment(Expense)</p>
                </a>
              </li>
            </ul>
          </li>
          <?php
					$stockReport = array(
            'stock.php',
            'bin-card.php',
					);
				?>
          <li class="nav-item <?php if(in_array($currentPage, $stockReport)){ echo 'menu-is-opening menu-open'; } ?>">
            <a href="#" class="nav-link  <?php if(in_array($currentPage, $stockReport)){ echo 'active'; } ?>">
              <i class="nav-icon fas fa-cubes"></i>
              <p>
                Stock
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item loadstarter">
                <a href="stock.php" class="nav-link <?php if($currentPage == 'stock.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Stock Report</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="bin-card.php"  class="nav-link <?php if($currentPage == 'bin-card.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon "></i>
                  <p>Bin Card</p>
                </a>
              </li>
            </ul>
          </li>
          <?php
          $ledgerPage = array(
            'customer-ledger.php',
						'supplier-ledger.php',
					);
				?>
          <li class="nav-item <?php if(in_array($currentPage, $ledgerPage)){ echo 'menu-is-opening menu-open'; } ?>">
            <a href="#" class="nav-link  <?php if(in_array($currentPage, $ledgerPage)){ echo 'active'; } ?>">
              <i class="nav-icon fas fa-file-invoice-dollar"></i>
              <p>
                Ledgers
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item loadstarter">
                <a href="customer-ledger.php" class="nav-link <?php if($currentPage == 'customer-ledger.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon "></i>
                  <p>Customer Ledger</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="supplier-ledger.php" class="nav-link <?php if($currentPage == 'supplier-ledger.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Supplier Ledger</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="profoma-invoice-transaction.php"  style="display:none;" class="nav-link <?php if($currentPage == 'profoma-invoice-transaction.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>General Ledger</p>
                </a>
              </li>
            </ul>
          </li>
          <?php
          $listingPage = array(
            'customer-price-list.php',
            'supplier-price-list.php',
            'sales-order-list.php',
					);
				?>
          <li class="nav-item <?php if(in_array($currentPage, $listingPage)){ echo 'menu-is-opening menu-open'; } ?>" >
            <a href="#" class="nav-link  <?php if(in_array($currentPage, $listingPage)){ echo 'active'; } ?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Master Listing
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item loadstarter">
                <a href="quatation.php" class="nav-link" style="display:none;" <?php if($currentPage == 'quatation.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon "></i>
                  <p>Customer List</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="supplier-price-list.php"  style="display:none;" class="nav-link <?php if($currentPage == 'supplier-price-list.php.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Supplier List</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="profoma-invoice-transaction.php"  style="display:none;" class="nav-link <?php if($currentPage == 'profoma-invoice-transaction.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Item List</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="customer-price-list.php"  class="nav-link <?php if($currentPage == 'customer-price-list.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Customer Price List</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="supplier-price-list.php" class="nav-link <?php if($currentPage == 'supplier-price-list.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Supplier Price List</p>
                </a>
              </li>
            </ul>
          </li>
          <?php
          $reportDetailsPages = array(
            'details-of-sales.php',
						'details-of-purchase.php',
						'sales-summary.php',
            'purchase-summary.php',
            'credit-note-register.php',
            'debit-note-register.php',
            'quotation-report.php',
            'delivery-challan-report.php',
            'received-statement.php',
            'payment-statement.php',
            'purchase-details-statement.php',
            'purchase-order-list.php',
            'purchase-bill-not-received.php',
            'gst-report.php',
            'item-profit-statement.php',
            'party-profit-statement.php',
        
					);
				?>
          <li class="nav-item <?php if(in_array($currentPage, $reportDetailsPages)){ echo 'menu-is-opening menu-open'; } ?>">
            <a href="#" class="nav-link  <?php if(in_array($currentPage, $reportDetailsPages)){ echo 'active'; } ?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Reports
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <li class="nav-item loadstarter">
                <a href="purchase-order-list.php" class="nav-link <?php if($currentPage == 'purchase-order-list.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Purchase Order List</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="sales-order-list.php" style="display:none;" class="nav-link <?php if($currentPage == 'sales-order-list.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sales Order List</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="details-of-sales.php" class="nav-link" style="display:none" <?php if($currentPage == 'details-of-sales.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon "></i>
                  <p>Details Of Sales</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="sales-summary.php" class="nav-link <?php if($currentPage == 'sales-summary.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sales Summary</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="details-of-purchase.php" style="display:none;" class="nav-link  <?php if($currentPage == 'details-of-purchase.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon "></i>
                  <p>Details Of Purchase</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="purchase-summary.php" class="nav-link <?php if($currentPage == 'purchase-summary.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Purchase Summary</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="purchase-bill-not-received.php"  class="nav-link <?php if($currentPage == 'purchase-bill-not-received.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Purchase Bill Not Received</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="credit-note-register.php"   class="nav-link <?php if($currentPage == 'credit-note-register.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Credit Note Register</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="debit-note-register.php"  class="nav-link <?php if($currentPage == 'debit-note-register.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Debit Note Register</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="delivery-challan-report.php" class="nav-link <?php if($currentPage == 'delivery-challan-report.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Delivery Challan Statement</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="quotation-report.php" class="nav-link <?php if($currentPage == 'quotation-report.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Quotation Statement</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="purchase-details-statement.php" style="display:none;" class="nav-link <?php if($currentPage == 'purchase-details-statement.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Payment Statement</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="payment-statement.php"  class="nav-link <?php if($currentPage == 'payment-statement.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Payment Statement</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="received-statement.php"  class="nav-link <?php if($currentPage == 'received-statement.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Received Statement</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="gst-report.php" class="nav-link <?php if($currentPage == 'gst-report.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>GST Report</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="item-profit-statement.php" class="nav-link <?php if($currentPage == 'item-profit-statement.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Item Wise Profit Statement</p>
                </a>
              </li>
              <li class="nav-item loadstarter">
                <a href="party-profit-statement.php" class="nav-link <?php if($currentPage == 'party-profit-statement.php') { echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Party Wise Profit Statement</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item loadstarter">
            <a href="logout.php" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
              Logout
              </p>
            </a>
          </li>
        
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  
