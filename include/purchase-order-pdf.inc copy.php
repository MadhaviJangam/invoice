<?php

$admin = new AdminFunctions();
	if(isset($admin)){
		$pdfObj = $admin;
   } 
   ob_start();

   $purchaseDetails=$admin->getUniquePurchaseOrderDetailsById($_GET['id']);
   $SupplierDetails=$admin->getUniqueSupplierMasterById($purchaseDetails['supplier_id']);
   $getStateDetails=$admin->getStateCodeMasterByName($SupplierDetails['statename']);
   $getItemDetailTOTAL= $admin->getUniquePurchaseOrderDetailsById($_GET['id']);
   $getPurchaseOrderItemDetails=$admin->getUniquePurchaseOrderItemDetailsByTaxInvoiceId($_GET['id']);
   $rowcount=mysqli_num_rows($getPurchaseOrderItemDetails);
   $getNumberToText=$admin->getIndianCurrency($getItemDetailTOTAL['final_amt']);
   $getItemPerDetails=$admin->getUniquePurchaseOrderGSTDetailsByTaxInvoiceId($_GET['id']);
   $companyInfo=$admin->getUniqueCompanyMasterById();

   $arrayHSNcode=array();

if ($purchaseDetails['purchase_date'] == '0000-00-00') {
   $purchase_date = '';
} else {
    $purchase_date = date("d-m-Y", strtotime($purchaseDetails['purchase_date']));
}
if ($purchaseDetails['supplier_date'] == '0000-00-00') {
    $supplier_date = '';
 } else {
     $supplier_date = date("d-m-Y", strtotime($purchaseDetails['supplier_date']));
 }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   
   <body>
   <table border="0" style="width:100%">
        <tr> 
            <td style="text-align:center;">Purchse Order</td>
        </tr>
        <tr>
            <td colspan="5" style="border-top:1px solid #000;border-left:1px solid #000;color:#f26d00;font-size:20px;width:75%;text-align:center;"><?php echo $companyInfo['company_name'];?></td>
            <td rowspan="2" style="width:25%;border-top:1px solid #000;border-right:1px solid #000;"><img style="border-top:10px solid #000;"></td>
        </tr>
        <tr>
            <td colspan="5" style="text-align:center;border-left:1px solid #000;">GSTN : <?php echo $companyInfo['gst_no'];?></td>
        </tr>
        <tr>
            <td colspan="6" style="text-align:center;border-left:1px solid #000;border-right:1px solid #000;"><?php echo $companyInfo['company_address'];?></td>
        </tr>
        <tr>
            <td colspan="3" style="text-align:left;border-left:1px solid #000;">Mob No:<?php echo $companyInfo['mobile_no'];?></td>
            <td colspan="6" style="text-align:right;border-right:1px solid #000;"><?php echo $companyInfo['email_id'];?></td>
        </tr>
        <tr>
            <td colspan="6" style="text-align:center;border-left:1px solid #000;border-right:1px solid #000;font-size:15px;border-bottom:1px solid #000;"><u>PURCHASE ORDER</u></td>
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;"width="50%">To, <?php echo substr($SupplierDetails['supplier_name'] ,"0", 100); ?></td>  
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="50%">Purchase No : <?php echo $purchaseDetails['purchase_no']; ?></td>  
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="50%"><br><?php echo $SupplierDetails['billing_address']; ?></td>  
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="50%">Purchase Date : <?php echo $purchase_date; ?></td>  
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="50%">GST No: <?php echo $SupplierDetails['company_gst']; ?></td>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="50%">Supplier Ref No. : <?php echo $purchaseDetails['supplier_ref_no']; ?></td>
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="50%">Contact No:<?php echo$SupplierDetails['contact_person_phone']; ?></td>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="50%">Supplier Date. : <?php echo $supplier_date; ?></td> 
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="50%">Email-ID:<?php echo $SupplierDetails['company_email']; ?></td>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="50%">Kind Attn : <?php echo $purchaseDetails['kind_atten']; ?></td> 
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;font-size:10px;"  width="100%">Subject to the conditions mentioned below, Please supply the following goods.</td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="5%">SR.NO</td>
            <td  style="text-align:left;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="40%">DESCRIPTION OF GOODS AND SERVICE</td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="13%">HSN CODE</td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%">QTY</td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="12%">RATE</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%">UNIT</td>
            <td style="text-align:right;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%">TAXABLE VALUE </td>  
        </tr>
        <?php
             $x= 1;   
             
            while ($row = $admin->fetch($getPurchaseOrderItemDetails)) {
                $getItemDetails= $admin->getUniqueItemMasterById($row['item_id']);
                array_push($arrayHSNcode,$getItemDetails['hsn_code']);
        ?>
        <tr style="font-size:10px;">
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="5%"><?php echo $x; ?></td>
            <td  style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="40%"><?php  echo $getItemDetails['item_name'];?></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="13%"><?php  echo $getItemDetails['hsn_code'];?></td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="10%"><?php  echo $row['qty'];?></td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="12%"><?php  echo $row['rate'];?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="10%"><?php  echo $row['unit'];?></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="10%"><?php  echo $row['amt'];?></td>  
        </tr>
        <?php
            $x++;  
                }
            ?>
        <?php 
        $rowprint= 25 - $rowcount;
        for($i=1;$i<=$rowprint;$i++){ ?>
        <tr style="font-size:10px;">
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"></td>
            <td  style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="40%"></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="13%"></td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:center;border-right:1px solid #000;border-left:1px solid #000;" width="10%"></td>  
        </tr>
        <?php } ?>
         <tr style="font-size:10px;">
            <td coslspan=3 style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="53%">Delivery Address<br><?php echo $SupplierDetails['billing_address']; ?></td>
            <td coslspan=3 style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="29%">Total Amount<br>Discount Amount<br>GST Amount</td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;" width="18%"><?php echo $getItemDetailTOTAL['total_amt'];?><br><?php echo $getItemDetailTOTAL['total_after_disc_amt'];?><br><?php echo $getItemDetailTOTAL['total_cgst_amt'];?></td>  
        </tr>
        <tr style="font-size:10px;">
            <td coslspan=3 style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="53%"></td>
            <td coslspan=3 style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="29%">Total Amount</td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="18%"><?php echo $getItemDetailTOTAL['total_net_amt']?></td>  
        </tr>
        <tr style="font-size:10px;">
            <td style="text-align:left;padding:0px;border-left:1px solid #000;border-bottom:1px solid #000;"  width="20%">Terms &<br>Conditions</td>
            <td style="text-align:left;padding:0px;border-right:1px solid #000;border-bottom:1px solid #000;"  width="80%"><?php echo nl2br($purchaseDetails['terms_of_payment']);?></td>
        </tr>
        <tr style="font-size:10px;">
            <td style="text-align:right;padding:0px;border-left:1px solid #000;border-right:1px solid #000;"  width="100%">FOR <?php echo $companyInfo['company_name'];?></td>
        </tr>
        <tr style="font-size:10px;">
            <td style="text-align:right;padding:0px;border-left:1px solid #000;border-right:1px solid #000;"  width="100%"></td>
        </tr>
        <tr style="font-size:10px;">
            <td style="text-align:right;padding:0px;border-left:1px solid #000;border-bottom:1px solid #000;border-right:1px solid #000;"  width="100%">AUTHORISED SIGNATORY</td>
        </tr>
      </table>
   </body>
</html>
<?php 
	$invoiceMsg = ob_get_contents();
	ob_end_clean();
?>