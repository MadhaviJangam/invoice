<?php

$admin = new AdminFunctions();
	if(isset($admin)){
		$pdfObj = $admin;
   } 
   ob_start();
   if(isset($_GET['customer_id'])){
       $customer_id = $_GET['customer_id'];
    }else{
        $customer_id = '';
    }

   if(isset($_GET['from_date'])){  
       $from_date = $_GET['from_date'];
    }else{
        $from_date = '';
    }

   if(isset($_GET['to_date'])){
       $to_date = $_GET['to_date'];
    }else{
        $to_date = '';
    }
    $total_qty = 0;
    $total_sales_amt = 0;
    $total_purchase_amt = 0;
    $total_profit = 0;
    $total_profit_per = 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   
   <body>
   <table border="0" style="width:100%">
        <tr>
            <td style="text-align:left;font-size:10px;">Party Wise Profit Statement &nbsp;&nbsp;From Date: <?php if($from_date!=''){ echo date("d-m-Y",strtotime($from_date));}?>&nbsp;&nbsp;To Date: <?php if($to_date!=''){ echo date("d-m-Y",strtotime($to_date));}?></td>
        </tr>
        <tr style="">
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="17%">BILL NO</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="12%">DATE</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="29%">PARTY NAME</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="12%">SALES AMOUNT</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="10%">PURCHASE AMOUNT</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"   width="10%">PROFIT</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="10%">%</td>
        </tr>
       <?php 
        if($customer_id!='' && $from_date == '' && $to_date == ''){
            $itemDetails = $admin->itemSalesByCustomerId($customer_id);
            while($row = $admin->fetch($itemDetails)) {
            $purchaseDetails = $admin->getPurchaseRateBySupplierId($row['invoice_date'],$row['item_id']);
            $proft =  ($row['amount'] - ($purchaseDetails['rate']*$row['qty']))/$row['amount']*100; 

            $total_qty = $total_qty + $row['qty'];
            $total_sales_amt =  $total_sales_amt + $row['amount'];
            $total_purchase_amt = $total_purchase_amt + $purchaseDetails['rate']*$row['qty'];
            $total_profit = $total_profit + $row['amount'] - ($purchaseDetails['rate']*$row['qty']);
            $total_profit_per = $total_profit_per + $proft;
            ?>

        <tr>  
            <td  style="text-align:left;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="17%" ><?php echo $row['bill_no']; ?></td>
            <td  style="text-align:left;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="12%" ><?php echo date("d-m-Y",strtotime($row['invoice_date'])); ?></td>
            <td  style="text-align:left;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="29%" ><?php echo $admin->getUniqueCustomerMasterById($row['customer_id'])['customer_name'];?></td>
            <td  style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="12%" ><?php echo $row['amount']; ?></td>

            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo ($purchaseDetails['rate']*$row['qty']); ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo ($row['amount'] - ($purchaseDetails['rate']*$row['qty'])) ;  ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $admin->formatAmount($proft).'%'; ?></td>
        </tr>
        <?php }  ?>
            <?php }  ?>
            <?php   
            
        if($customer_id=='' && $from_date != '' && $to_date != ''){
            $getAllSalesItemFromTodate = $admin->itemSalesByFromToDate($from_date,$to_date);
            while($row = $admin->fetch($getAllSalesItemFromTodate)) {
               $getpurchasebydate = $admin-> getPurchaseRateBySupplierId($row['invoice_date'],$row['item_id']);
               $proft =  ($row['amount'] - ($getpurchasebydate['rate']*$row['qty']))/$row['amount']*100; 
           
               $total_qty = $total_qty + $row['qty'];
               $total_sales_amt =  $total_sales_amt + $row['amount'];
               $total_purchase_amt = $total_purchase_amt + $getpurchasebydate['rate']*$row['qty'];
               $total_profit = $total_profit + $row['amount'] - ($getpurchasebydate['rate']*$row['qty']);
               $total_profit_per = $total_profit_per + $proft;
           
           ?>
       <tr>  
            <td  style="text-align:left;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="17%" ><?php echo $row['bill_no']; ?></td>
            <td  style="text-align:left;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="12%" ><?php echo date("d-m-Y",strtotime($row['invoice_date'])); ?></td>
            <td  style="text-align:left;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="29%" ><?php echo $admin->getUniqueCustomerMasterById($row['customer_id'])['customer_name'];?></td>
            <td  style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="12%" ><?php echo $row['amount']; ?></td>

            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo ($getpurchasebydate['rate']*$row['qty']); ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo ($row['amount'] - ($getpurchasebydate['rate']*$row['qty'])) ;?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $admin->formatAmount($proft).'%'; ?></td>
        </tr>

        <?php } } ?>

        <?php  if($customer_id!='' && $from_date != '' && $to_date != ''){
            $itemDetails = $admin->itemSalesByCustomerIdANDFromToDate($from_date,$to_date,$customer_id);

            while($row= $admin->fetch($itemDetails)){

            $getpurchasebydate = $admin-> getPurchaseRateBySupplierId($row['invoice_date'],$row['item_id']);
               
            $proft =  ($row['amount'] - ($getpurchasebydate['rate']*$row['qty']))/$row['amount']*100; 
            
            $total_qty = $total_qty + $row['qty'];
            $total_sales_amt =  $total_sales_amt + $row['amount'];
            $total_purchase_amt = $total_purchase_amt + $getpurchasebydate['rate']*$row['qty'];
            $total_profit = $total_profit + $row['amount'] - ($getpurchasebydate['rate']*$row['qty']);
            $total_profit_per = $total_profit_per + $proft;
            
            ?>
        <tr>  
            <td  style="text-align:left;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="17%" ><?php echo $row['bill_no']; ?></td>
            <td  style="text-align:left;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="12%" ><?php echo date("d-m-Y",strtotime($row['invoice_date'])); ?></td>
            <td  style="text-align:left;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="29%" ><?php echo $admin->getUniqueCustomerMasterById($row['customer_id'])['customer_name'];?></td>
            <td  style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="12%" ><?php echo $row['amount']; ?></td>

            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo ($getpurchasebydate['rate']*$row['qty']); ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo ($row['amount'] - ($getpurchasebydate['rate']*$row['qty'])) ; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $admin->formatAmount($proft).'%'; ?></td>
        </tr>
        <?php } ?>
        <?php } ?>
            
        <tr>
        <td width="100%" style="border-bottom:1px solid #000;"></td>
        </tr>
        <tr>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="58%" >Total</td>
            <td  style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="12%" ><?php echo $total_sales_amt; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $total_purchase_amt; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $total_profit; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;"  width="10%"><?php echo $admin->formatAmount($total_profit_per).'%'; ?></td>
        </tr>
             
              
      </table>
   </body>
</html>
<?php 
	$invoiceMsg = ob_get_contents();
	ob_end_clean();
?>