<?php

$admin = new AdminFunctions();
	if(isset($admin)){
		$pdfObj = $admin;
   } 
   ob_start();
   
   if(isset($_GET['from_date'])){  
       $from_date = $_GET['from_date'];
    }else{
        $from_date = '';
    }

   if(isset($_GET['to_date'])){
       $to_date = $_GET['to_date'];
    }else{
        $to_date = '';
    }

    $query='';
    
    if($from_date!='' && $to_date!='' ){
        $query="SELECT * FROM ".PREFIX."purchase_bill_transaction WHERE supplier_bill_date BETWEEN '".$from_date."' AND '".$to_date."' ";
    }
    if($from_date!='' && $to_date!='' ){
        $query="SELECT * FROM ".PREFIX."tax_invoice_transaction WHERE invoice_date BETWEEN '".$from_date."' AND '".$to_date."' ";
    }
   

   
    $gst_3Per = $admin-> gstReport($from_date,$to_date,3);
    $gst_5Per = $admin-> gstReport($from_date,$to_date,5);
    $gst_12Per = $admin-> gstReport($from_date,$to_date,12);
    $gst_18Per = $admin-> gstReport($from_date,$to_date,18);
    $gst_28Per = $admin-> gstReport($from_date,$to_date,28);

    $gst_sales_3Per = $admin-> gstSalesReport($from_date,$to_date,3);
    $gst_sales_5Per = $admin-> gstSalesReport($from_date,$to_date,5);
    $gst_sales_12Per = $admin-> gstSalesReport($from_date,$to_date,12);
    $gst_sales_18Per = $admin-> gstSalesReport($from_date,$to_date,18);
    $gst_sales_28Per = $admin-> gstSalesReport($from_date,$to_date,28);

    


    $total_gst_5Per = $gst_5Per['cgst_total'] - $gst_sales_5Per['cgst_total'];
    $total_gst_12Per = $gst_12Per['cgst_total'] - $gst_sales_12Per['cgst_total'];
    $total_gst_18Per = $gst_18Per['cgst_total'] - $gst_sales_18Per['cgst_total'];
    $total_gst_28Per = $gst_28Per['cgst_total'] - $gst_sales_28Per['cgst_total'];


    $total_igst_5Per = $gst_5Per['igst_total'] - $gst_sales_5Per['igst_total'];
    $total_igst_12Per = $gst_12Per['igst_total'] - $gst_sales_12Per['igst_total'];
    $total_igst_18Per = $gst_18Per['igst_total'] - $gst_sales_18Per['igst_total'];
    $total_igst_28Per = $gst_28Per['igst_total'] - $gst_sales_28Per['igst_total'];


  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   
   <body>
   <table border="0" style="width:100%">
        <tr>
            <td style="text-align:left;font-size:10px;" width="70%">GST REPORT &nbsp;&nbsp;From Date: <?php if($from_date!='') {echo date("d-m-Y",strtotime($from_date));}?>&nbsp;&nbsp;To Date: <?php if($to_date!=''){ echo date("d-m-Y",strtotime($to_date));}?></td>
            <td style="text-align:left;font-size:10px;" width="30%">Printed On &nbsp;&nbsp;<?php echo date("d-m-Y");?></td>
        </tr>
        <tr style="">
            <td colspan="3"  style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="6%"></td>
            <td colspan="3" style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000;"  width="14%">5%</td>
            <td colspan="3" style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000;"  width="14%">12%</td>
            <td colspan="3" style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000;"  width="14%">18%</td>
            <td colspan="3" style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000;"   width="15%">28%</td>
            <td colspan="3" style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000;" width="9%">5%</td>
            <td colspan="3" style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000;"  width="9%">12%</td>
            <td colspan="3" style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000;"   width="9%">18%</td>
            <td colspan="3" style="text-align:center;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000;"               width="9%">28%</td>
        </tr>
       
        <tr style="">
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;" width="6%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%">Taxable</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%">CGST</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="4%">SGST</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%">Taxable</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%">CGST</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="4%">SGST</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%">Taxable</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%">CGST</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="4%">SGST</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%">Taxable</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%">CGST</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%">SGST</td>
            <td  style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%">Taxable</td>
            <td  style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="4%">IGST</td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%">Taxable</td>
            <td  style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="4%">IGST</td>
            <td  style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%">Taxable</td>
            <td  style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="4%">IGST</td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%">Taxable</td>
            <td  style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="4%">IGST</td>
        </tr>
        <?php
       
    ?>
          
        <tr style="">
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="6%">Purchase</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"><?php if($gst_5Per['cgst_total']!=0.00){ echo $gst_5Per['total_amount'];} else{ echo '0.00';}  ?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"><?php echo $gst_5Per['cgst_total']; ?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="4%"><?php echo $gst_5Per['sgst_total']; ?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"><?php if($gst_12Per['cgst_total']!=0.00){ echo $gst_12Per['total_amount']; } else{ echo '0.00';}?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"><?php echo $gst_12Per['cgst_total']; ?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="4%"><?php echo $gst_12Per['sgst_total']; ?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"><?php if($gst_18Per['cgst_total']!=0.00){ echo $gst_18Per['total_amount'];} else{ echo '0.00';} ?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"><?php echo $gst_18Per['cgst_total'];?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="4%"><?php echo $gst_18Per['sgst_total'];?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"><?php if($gst_28Per['cgst_total']!=0.00){echo $gst_28Per['total_amount']; } else{ echo '0.00';}?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"><?php echo $gst_28Per['cgst_total']; ?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"><?php echo $gst_28Per['sgst_total']; ?></td>

            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="5%"><?php if($gst_5Per['igst_total']!=0.00){ echo $gst_5Per['total_amount'];} else{ echo '0.00';} ?></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="4%"><?php echo $gst_5Per['igst_total']; ?></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="5%"><?php if($gst_12Per['igst_total']!=0.00){ echo $gst_12Per['total_amount'];} else{ echo '0.00';} ?></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="4%"><?php echo $gst_12Per['igst_total']; ?></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="5%"><?php if($gst_18Per['igst_total']!=0.00){ echo $gst_18Per['total_amount']; } else{ echo '0.00';}?></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="4%"><?php echo $gst_18Per['igst_total']; ?></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="5%"><?php if($gst_28Per['igst_total']!=0.00){ echo $gst_28Per['total_amount']; } else{ echo '0.00';} ?></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="4%"><?php echo $gst_28Per['igst_total']; ?></td>
        </tr>

        <tr style="">
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="6%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="4%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="4%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="4%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"></td>
            <td  style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;" width="5%"></td>
            <td  style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;" width="4%"></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;" width="5%"></td>
            <td  style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;" width="4%"></td>
            <td  style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;" width="5%"></td>
            <td  style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;" width="4%"></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;" width="5%"></td>
            <td  style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;" width="4%"></td>
        </tr> 
        <tr style="">
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="6%">Sales</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"><?php if($gst_sales_5Per['cgst_total']!=0.00){ echo $gst_sales_5Per['total_amount'];} else{ echo '0.00';}  ?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"><?php echo $gst_sales_5Per['cgst_total']; ?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="4%"><?php echo $gst_sales_5Per['sgst_total']; ?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"><?php if($gst_sales_12Per['cgst_total']!=0.00){ echo $gst_sales_12Per['total_amount']; } else{ echo '0.00';}?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"><?php echo $gst_sales_12Per['cgst_total']; ?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="4%"><?php echo $gst_sales_12Per['sgst_total']; ?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"><?php if($gst_sales_18Per['cgst_total']!=0.00){ echo $gst_sales_18Per['total_amount'];} else{ echo '0.00';} ?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"><?php echo $gst_sales_18Per['cgst_total'];?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="4%"><?php echo $gst_sales_18Per['sgst_total'];?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"><?php if($gst_sales_28Per['cgst_total']!=0.00){echo $gst_sales_28Per['total_amount']; } else{ echo '0.00';}?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"><?php echo $gst_sales_28Per['cgst_total']; ?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="5%"><?php echo $gst_sales_28Per['sgst_total']; ?></td>

            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="5%"><?php if($gst_sales_5Per['igst_total']!=0.00){ echo $gst_sales_5Per['total_amount'];} else{ echo '0.00';} ?></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="4%"><?php echo $gst_sales_5Per['igst_total']; ?></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="5%"><?php if($gst_sales_12Per['igst_total']!=0.00){ echo $gst_sales_12Per['total_amount'];} else{ echo '0.00';} ?></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="4%"><?php echo $gst_sales_12Per['igst_total']; ?></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="5%"><?php if($gst_sales_18Per['igst_total']!=0.00){ echo $gst_sales_18Per['total_amount']; } else{ echo '0.00';}?></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="4%"><?php echo $gst_sales_18Per['igst_total']; ?></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="5%"><?php if($gst_sales_28Per['igst_total']!=0.00){ echo $gst_sales_28Per['total_amount']; } else{ echo '0.00';} ?></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="4%"><?php echo $gst_sales_28Per['igst_total']; ?></td>
        </tr>
           
        <tr style="">
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="6%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;" width="5%"><?php echo $total_gst_5Per;?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;" width="4%"><?php echo $total_gst_5Per;?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;" width="5%"><?php echo $total_gst_12Per;?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;" width="4%"><?php echo $total_gst_12Per;?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;" width="5%"><?php echo $total_gst_18Per;?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;" width="4%"><?php echo $total_gst_18Per;?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;" width="5%"><?php echo $total_gst_28Per;?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;" width="5%"><?php echo $total_gst_28Per;?></td>
            <td  style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%"></td>
            <td  style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;" width="4%"><?php if($total_igst_5Per!=0.00){ echo $total_igst_5Per;} else { echo '0.00';}?></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%"></td>
            <td  style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;" width="4%"><?php if($total_igst_12Per!=0.00){ $total_igst_12Per;} else { echo '0.00';}?></td>
            <td  style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%"></td>
            <td  style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;" width="4%"><?php if($total_igst_18Per!=0.00){ $total_igst_18Per;} else { echo '0.00';}?></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="5%"></td>
            <td  style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;" width="4%"><?php if($total_igst_28Per!=0.00){ $total_igst_28Per;} else { echo '0.00';}?></td>
        </tr>  
      </table>
   </body>
</html>
<?php 
	$invoiceMsg = ob_get_contents();
	ob_end_clean();
?>