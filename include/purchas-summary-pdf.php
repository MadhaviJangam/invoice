<?php

$admin = new AdminFunctions();
	if(isset($admin)){
		$pdfObj = $admin;
   } 
   ob_start();
   if(isset($_GET['supplier_id'])){
       $supplier_id = $_GET['supplier_id'];
    }else{
        $supplier_id = '';
    }

   if(isset($_GET['from_date'])){  
       $from_date = $_GET['from_date'];
    }else{
        $from_date = '';
    }

   if(isset($_GET['to_date'])){
       $to_date = $_GET['to_date'];
    }else{
        $to_date = '';
    }

   
    $query='';
    if($supplier_id=='' && $from_date=='' && $to_date=='' ){
        $query="SELECT * FROM ".PREFIX."purchase_bill_transaction";
    }

    if($supplier_id!='' && $from_date=='' && $to_date=='' ){
        $query="SELECT * FROM ".PREFIX."purchase_bill_transaction WHERE supplier_id='".$supplier_id."' ";
    }

    if($supplier_id=='' && $from_date!=='' && $to_date!=='' ){
        $query="SELECT * FROM ".PREFIX."purchase_bill_transaction WHERE supplier_bill_date BETWEEN '".$from_date."' AND '".$to_date."' ";

    }

    if($supplier_id!='' && $from_date!='' && $to_date!='' ){
        $query="SELECT * FROM ".PREFIX."purchase_bill_transaction WHERE supplier_id='".$supplier_id."' AND supplier_bill_date BETWEEN '".$from_date."' AND '".$to_date."' ";
    }
    

    $result=$admin->query($query);
    
    $num_rows1 = mysqli_num_rows($result);
   
    
  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   
   <body>
   <table border="0" style="width:100%">
        <!-- <tr style="">
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="10%">BILL NO</td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="10%">DATE</td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="40%">PARTY NAME</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"   width="10%">AMOUNT</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"   width="10%">GST</td>
            <td style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="20%">BILL AMOUNT</td>    
        </tr> -->
        <?php 
         $totalamount = 0;
         $gst=0;
         $total_gst_amt=0;
         $total_amt=0;
         $final_total=0;
      
           while($row = $admin->fetch($result)) {
            $totalamount = $row['total_after_disc_amt']+$row['service_charges'];
            $gst = $row['total_cgst_amt']+$row['total_sgst_amt'] + $row['total_igst_amt'] + $row['service_charges_cgst_amt']+ $row['service_charges_sgst_amt'] + $row['service_charges_igst_amt'];

            $final_total = $final_total + $row['final_amt'];
            $total_amt = $total_amt+($row['total_after_disc_amt']+$row['service_charges']);
            $total_gst_amt = $total_gst_amt+($row['total_cgst_amt']+$row['total_sgst_amt'] + $row['total_igst_amt'] + $row['service_charges_cgst_amt']+ $row['service_charges_sgst_amt'] + $row['service_charges_igst_amt']);
        ?>
        <tr style="">
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="15%"><?php echo $row['supplier_bill_no']; ?></td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="10%"><?php echo $row['supplier_bill_date']; ?></td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="40%"><?php echo $admin->getUniqueSupplierMasterById($row['supplier_id'])['supplier_name'];?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"   width="10%"><?php echo $totalamount; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"   width="10%"><?php echo $gst;?></td>
            <td style="text-align:right;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="15%"><?php echo $row['final_amt']; ?></td>    
        </tr>
           <?php } ?>
        <tr>
            <td width="100%" style="border-bottom:1px solid #000;"></td>
        </tr>
        <tr style="">
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="15%"></td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="10%"></td>
            <td  style="text-align:LEFT;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="40%"></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"   width="10%"><?php echo $total_amt; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"   width="10%"><?php echo $total_gst_amt;?></td>
            <td style="text-align:right;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="15%"><?php echo $final_total; ?></td>    
        </tr>
      </table>
   </body>
</html>
<?php 
	$invoiceMsg = ob_get_contents();
	ob_end_clean();
?>