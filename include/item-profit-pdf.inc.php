<?php

$admin = new AdminFunctions();
	if(isset($admin)){
		$pdfObj = $admin;
   } 
   ob_start();
   if(isset($_GET['item_id'])){
       $item_id = $_GET['item_id'];
    }else{
        $item_id = '';
    }

   if(isset($_GET['from_date'])){  
       $from_date = $_GET['from_date'];
    }else{
        $from_date = '';
    }

   if(isset($_GET['to_date'])){
       $to_date = $_GET['to_date'];
    }else{
        $to_date = '';
    }

    $query='';
    if($item_id=='' && $from_date=='' && $to_date=='' ){
        $query="SELECT * FROM ".PREFIX."tax_invoice_transaction";
    }

    if($item_id!='' && $from_date=='' && $to_date=='' ){
        $query="SELECT * FROM ".PREFIX."tax_invoice_transaction";
    }
    
    if($item_id!=''  && $from_date!='' && $to_date!='' ){
        $query="SELECT * FROM ".PREFIX."tax_invoice_transaction WHERE invoice_date BETWEEN '".$from_date."' AND '".$to_date."' ";
    }

    if($item_id==''  && $from_date!='' && $to_date!='' ){
        $query="SELECT * FROM ".PREFIX."tax_invoice_transaction WHERE invoice_date BETWEEN '".$from_date."' AND '".$to_date."' ";
    }
    

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   
   <body>
   <table border="0" style="width:100%">
        <tr>
            <td style="text-align:left;font-size:10px;">Payment Statement &nbsp;&nbsp;From Date: <?php if($from_date!=''){ echo date("d-m-Y",strtotime($from_date));}?>&nbsp;&nbsp;To Date: <?php if($to_date!=''){ echo date("d-m-Y",strtotime($to_date));}?></td>
        </tr>
        <tr style="">
            <td  rowspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="30%">ITEM NAME</td>
            <td colspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="24%">SALES</td>
            <td rowspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="15%">PURCHASE AMOUNT</td>
            <td rowspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"   width="11%">RATE</td>
            <td colspan="2"  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="20%">PROFIT</td>
        </tr>
        <tr style="">
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%">Quantity</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="14%">Amount</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%">Rs.</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%">%</td>
        </tr> 
        <?php
         
           $itemDetails = $admin->itemSales($from_date,$to_date,$item_id);
           $itemDetails = $admin->itemSalesByItemId($item_id);

            if($item_id!=''){
            while($row = $admin->fetch($itemDetails)) {
                $purchase = $admin->getPurchaseRateByItemId($from_date,$to_date,$item_id);
                $purchaseAmt = $row['qty'] * $purchase['rate'];
                $proftRs = $row['amount'] - $purchaseAmt;
                $proft =  $proftRs/$row['amount']*100; 
        ?>
            
        <tr>  
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="30%" ><?php echo $admin->getUniqueItemMasterById($row['item_id'])['item_name'];?></td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%" ><?php echo $row['qty']; ?></td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="14%" ><?php echo $row['amount']; ?></td>

            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="15%"><?php echo $purchaseAmt; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="11%"><?php echo $purchase['rate']; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo($proftRs); ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $admin->formatAmount($proft); ?></td>
        </tr>
        <?php } ?>
            <?php } else{
                
                $itemSalesAll111 = $admin->itemSalesAll($from_date,$to_date);
              
                while($row111 = $admin->fetch($itemSalesAll111)) {
                    $purchaseDetail =$admin->getPurchaseRate($from_date,$to_date);
                    while($rows = $admin->fetch($purchaseDetail)) {
                    $purchaseAmount = $row111['qty'] * $rows['rate'];
                    $proftRs = $row111['amount'] - $purchaseAmount;
                    $proft =  $proftRs/$row111['amount']*100;
                     
                ?>
                <tr>  
                <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="30%" ><?php echo $admin->getUniqueItemMasterById($row111['item_id'])['item_name'];?></td>
                <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%" ><?php echo $row111['qty']; ?></td>
                <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="14%" ><?php echo $row111['amount']; ?></td>

                <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="15%"><?php echo $purchaseAmount; ?></td>
                <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="11%"><?php echo $rows['rate']; ?></td>
                <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $proftRs; ?></td>
                <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $admin->formatAmount($proft); ?></td>
                </tr>
            <?php } } ?>
            <?php }  ?>
            
               <tr>
                <td width="100%" style="border-bottom:1px solid #000;"></td>
               </tr>
               <tr>
                  
                    <td  style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="30%" >Total</td>
                    <td  style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%" ><?php ?></td>
                    <td  style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="14%" ><?php ?></td>
                    <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="15%"><?php ?></td>
                    <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="11%"></td>
                    <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-left:1px solid #000;"  width="10%"><?php ?></td>
                    <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;"  width="10%"></td>
               </tr>
             
              
      </table>
   </body>
</html>
<?php 
	$invoiceMsg = ob_get_contents();
	ob_end_clean();
?>