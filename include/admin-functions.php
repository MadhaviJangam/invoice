<?php
// if(!isset($base)){
// 	$base='';
// }
	include_once 'config.php';
	include_once 'database.php';
	include_once 'Email.class.php';
	//include_once '../include/classes/Email.class.php';
	include_once 'SaveImage.class.php';
	// include_once 'include/classes/CSRF.class.php';
	// include_once "include/classes/Pagination.class.php";
	// include_once 'include/classes/HelperFunctions.class.php';

	/*
	 * AdminFunctions
	 * v1.0.0 - updated loginSession(), logoutSession(), adminLogin()
	 * v1.1.0 - integrated FCMNotification.class.php class for handling push notifications
	 */
	class AdminFunctions extends Database {
		private $userType = 'admin';

		/*===================== LOGIN BEGINS =====================*/
		function loginSession($userId, $userFirstName, $userLastName, $userType) {
			$_SESSION[SITE_NAME][$this->userType."UserId"] = $userId;
			$_SESSION[SITE_NAME][$this->userType."UserFirstName"] = $userFirstName;
			$_SESSION[SITE_NAME][$this->userType."UserLastName"] = $userLastName;
			$_SESSION[SITE_NAME][$this->userType."UserType"] = $this->userType;
		}
		function logoutSession() {
			if(isset($_SESSION[SITE_NAME])){
				if(isset($_SESSION[SITE_NAME][$this->userType."UserId"])){
					unset($_SESSION[SITE_NAME][$this->userType."UserId"]);
				}
				if(isset($_SESSION[SITE_NAME][$this->userType."UserFirstName"])){
					unset($_SESSION[SITE_NAME][$this->userType."UserFirstName"]);
				}
				if(isset($_SESSION[SITE_NAME][$this->userType."UserLastName"])){
					unset($_SESSION[SITE_NAME][$this->userType."UserLastName"]);
				}
				if(isset($_SESSION[SITE_NAME][$this->userType."UserType"])){
					unset($_SESSION[SITE_NAME][$this->userType."UserType"]);
				}
				return true;
			} else {
				return false;
			}
		}
		function adminLogin($data, $successURL, $failURL = "index.php?failed") {
			$username = $this->escape_string($this->strip_all($data['username']));
			$password = $this->escape_string($this->strip_all($data['password']));
			$query = "select * from ".PREFIX."admin where email='".$username."' AND active=1";
			$result = $this->query($query);

			if($this->num_rows($result) == 1) { // only one unique user should be present in the system
				$row = $this->fetch($result);
				if(password_verify($password, $row['password'])) {
					$this->loginSession($row['id'], $row['fname'], $row['lname'], $this->userType);
					$this->close_connection();
					header("location: ".$successURL);
					exit;
				} else {
					$this->close_connection();
					header("location: ".$failURL);
					exit;
				}
			} else {
				$this->close_connection();
				header("location: ".$failURL);
				exit;
			}
		}
		function sessionExists(){
			if($this->isUserLoggedIn()){
				return $loggedInUserDetailsArr = $this->getLoggedInUserDetails();
				// return true; // DEPRECATED
			} else {
				return false;
			}
		}
		function isUserLoggedIn(){
			if( isset($_SESSION[SITE_NAME]) && 
				isset($_SESSION[SITE_NAME][$this->userType.'UserId']) && 
				isset($_SESSION[SITE_NAME][$this->userType.'UserType']) && 
				!empty($_SESSION[SITE_NAME][$this->userType.'UserId']) &&
				$_SESSION[SITE_NAME][$this->userType.'UserType']==$this->userType){
				return true;
			} else {
				return false;
			}
		}

		function getSystemUserType() {
			return $this->userType;
		}

		function getLoggedInUserDetails(){
			$loggedInID = $this->escape_string($this->strip_all($_SESSION[SITE_NAME][$this->userType.'UserId']));
			$loggedInUserDetailsArr = $this->getUniqueAdminById($loggedInID);
			return $loggedInUserDetailsArr;
		}

		function getUniqueAdminById($userId) {
			$userId = $this->escape_string($this->strip_all($userId));
			$query = "select * from ".PREFIX."admin where id='".$userId."'";
			$sql = $this->query($query);
			return $this->fetch($sql);
		}

		/** * Function to get details of admin */
		function getFirstAdminDetails(){
			$query = "select fname, lname, email from ".PREFIX."admin where user_role = 'super' limit 0, 1";
			$sql = $this->fetch($this->query($query));
			return $sql;
		}
		/*===================== LOGIN ENDS =====================*/


		
		/*===================== EXTRA FUNCTIONS BEGINS =====================*/
		
		/** * Function to create permalink */
		function getValidatedPermalink($permalink){ // v2.0.0
			$permalink = trim($permalink, '()');
			$replace_keywords = array("-:-", "-:", ":-", " : ", " :", ": ", ":",
				"-@-", "-@", "@-", " @ ", " @", "@ ", "@", 
				"-.-", "-.", ".-", " . ", " .", ". ", ".", 
				"-\\-", "-\\", "\\-", " \\ ", " \\", "\\ ", "\\",
				"-/-", "-/", "/-", " / ", " /", "/ ", "/", 
				"-&-", "-&", "&-", " & ", " &", "& ", "&", 
				"-,-", "-,", ",-", " , ", " ,", ", ", ",", 
				" ",
				"---", "--", " - ", " -", "- ",
				"-#-", "-#", "#-", " # ", " #", "# ", "#",
				"-$-", "-$", "$-", " $ ", " $", "$ ", "$",
				"-%-", "-%", "%-", " % ", " %", "% ", "%",
				"-^-", "-^", "^-", " ^ ", " ^", "^ ", "^",
				"-*-", "-*", "*-", " * ", " *", "* ", "*",
				"-(-", "-(", "(-", " ( ", " (", "( ", "(",
				"-)-", "-)", ")-", " ) ", " )", ") ", ")",
				"-;-", "-;", ";-", " ; ", " ;", "; ", ";",
				"-'-", "-'", "'-", " ' ", " '", "' ", "'",
				"-?-", "-?", "?-", " ? ", " ?", "? ", "?",
				'-"-', '-"', '"-', ' " ', ' "', '" ', '"',
				"-!-", "-!", "!-", " ! ", " !", "! ", "!");
			$escapedPermalink = str_replace($replace_keywords, '-', $permalink); 
			return strtolower($escapedPermalink);
		}

		/** * Function to get value in yes/no */
		function getActiveLabel($isActive){
			if($isActive){
				return 'Yes';
			} else {
				return 'No';
			}
		}

		/** * Function to get image url */
		function getImageDir($imageFor){
			switch($imageFor){
				case "banner":
					return "../img/content/banner/"; // add / at end
					break;
				case "amenities":
					return "../img/content/amenities/"; // add / at end
					break;
				case "payment_plan_ad":
					return "../img/content/payment_plan_ad/"; // add / at end
					break;
				case "home_offer":
					return "../img/content/home_offer/"; // add / at end
					break;
				case "listing_offer":
					return "../img/content/listing_offer/"; // add / at end
					break;
				case "property_photographs":
					return "../img/content/property_photographs/"; // add / at end
					break;
				case "property_floor_plans":
					return "../img/content/property_floor_plans/"; // add / at end
					break;
				case "news_events":
					return "../img/content/news_events/"; // add / at end
					break;
				default:
					return false;
					break;
			}
		}

		/** * Function to get image url */
		function getImageUrl($imageFor, $fileName, $imageSuffix, $dirPrefix = ""){
			$fileDir = $this->getImageDir($imageFor, $dirPrefix);
			if($fileDir === false){ // custom directory not found, error!
				$fileDir = "../img/"; // add / at end
				$defaultImageUrl = $fileDir."default.jpg";
				return $defaultImageUrl;
			} else { // process custom directory
				$defaultImageUrl = $fileDir."default.jpg";
				if(empty($fileName)){
					return $defaultImageUrl;
				} else {
					$image_name = strtolower(pathinfo($fileName, PATHINFO_FILENAME));
					$image_ext = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
					if(!empty($imageSuffix)){
						$imageUrl = $fileDir.$image_name."_".$imageSuffix.".".$image_ext;
					} else {
						$imageUrl = $fileDir.$image_name.".".$image_ext;
					}
					if(file_exists($imageUrl)){
						return $imageUrl;
					} else {
						return $defaultImageUrl;
					}
				}
			}
		}

		/** * Function to delete/unlink image file */
		function unlinkImage($imageFor, $fileName, $imageSuffix, $dirPrefix = ""){
			$fileDir = $this->getImageDir($imageFor, $dirPrefix);
			if($fileDir === false){ // custom directory not found, error!
				return false;
			} else { // process custom directory
				$defaultImageUrl = $fileDir."default.jpg";

				$imagePath = $this->getImageUrl($imageFor, $fileName, $imageSuffix, $dirPrefix);
				if($imagePath != $defaultImageUrl){
					$status = unlink($imagePath);
					return $status;
				} else {
					return false;
				}
			}
		}

		/** * Function to get remaining time/ elapsed time */
		function formatTimeRemainingInText($dateTime, $isComplete = false){
			if($isComplete){
				return "<strong>Complete!</strong>";
			} else if(!empty($dateTime)){
				$timestampDiff = strtotime($dateTime) - time();
				if($timestampDiff <=0 ){ // over due
					$then = new DateTime($dateTime);
					$now = new DateTime();
					$sinceThen = $now->diff($then);

					if($sinceThen->y > 0){
						return '<strong class="text-danger">'.$sinceThen->y." year(s) over due</strong>";
					}
					if($sinceThen->m > 0){
						return '<strong class="text-danger">'.$sinceThen->m." month(s) over due</strong>";
					}
					if($sinceThen->d > 0){
						return '<strong class="text-danger">'.$sinceThen->d." day(s) over due</strong>";
					}
					if($sinceThen->h > 0){
						return '<strong class="text-danger">'.$sinceThen->h." hour(s) over due</strong>";
					}
					if($sinceThen->i > 0){
						return '<strong class="text-danger">'.$sinceThen->i." minutes(s) over due</strong>";
					}
				} else { // time remaining
					$then = new DateTime($dateTime);
					$now = new DateTime();
					$sinceThen = $now->diff($then);

					if($sinceThen->y > 0){
						return $sinceThen->y." year(s) left";
					}
					if($sinceThen->m > 0){
						return $sinceThen->m." month(s) left";
					}
					if($sinceThen->d > 0){
						return $sinceThen->d." day(s) left";
					}
					if($sinceThen->h > 0){
						return '<strong class="text-danger">'.$sinceThen->h."</strong> hour(s) remaining";
					}
					if($sinceThen->i > 0){
						return '<strong class="text-danger">'.$sinceThen->i."</strong> minutes(s) remaining";
					}
				}

			} else {
				return "-";
			}
		}

		/** * Function to format date and time */
		function returnFormatTimeRemainingArray($dateTime, $isComplete = false){
			$resultArray = array();
			$resultArray['year'] = "0";
			$resultArray['month'] = "0";
			$resultArray['day'] = "0";
			$resultArray['hour'] = "0";
			$resultArray['minute'] = "0";
			$resultArray['second'] = "0";

			if($isComplete){
				$resultArray['overDue'] = false; // +
				return $resultArray;
			} else if(!empty($dateTime)){
				$then = new DateTime($dateTime);
				$now = new DateTime();
				$sinceThen = $now->diff($then);
				$resultArray['year'] = $sinceThen->y;
				$resultArray['month'] = $sinceThen->m;
				$resultArray['day'] = $sinceThen->d;
				$resultArray['hour'] = $sinceThen->h;
				$resultArray['minute'] = $sinceThen->i;
				$resultArray['second'] = $sinceThen->s;

				$timestampDiff = strtotime($dateTime) - time();
				if($timestampDiff <=0 ){ // over due
					$resultArray['overDue'] = true; // -
				} else { // time remaining
					$resultArray['overDue'] = false; // +
				}
				return $resultArray;
			} else {
				$resultArray['overDue'] = false; // +
				return $resultArray;
			}
		}

		/** * Function to format date and time */
		function formatDateTime($dateTime, $defaultFormat = "d M, Y h:i a T"){
			if(empty($dateTime)){
				return "-";
			} else {
				return date($defaultFormat, strtotime($dateTime));
			}
		}

		/** * Function to format date */
		function formatDate($dateTime, $defaultFormat = "d M, Y T"){
			if(empty($dateTime)){
				return "-";
			} else {
				return date($defaultFormat, strtotime($dateTime));
			}
		}

		/** * Function to format time */
		function formatTime($dateTime, $defaultFormat = "h:i a T"){
			if(empty($dateTime)){
				return "-";
			} else {
				return date($defaultFormat, strtotime($dateTime));
			}
		}

		/** * Function to limit text of description */
		function limitDescText($content, $charLength){
			if(strlen($content) > $charLength){
				return substr($content, 0, $charLength).'...';
			} else {
				return $content;
			}
		}

		/** * Function to format number in amount */
		function formatAmount($amount){
			$amount = (float) $amount;
			return number_format($amount,  2, '.', ',');
		}

		/** * Function to format number as text () */
		function formatNumberAsText($number){
			$numberLength = strlen($number);

			if($numberLength > 3){
				$number = (float) $number;
				$multiplier = 1;
				$suffix = "";
				switch($numberLength){
					case 4:
					case 5:
					case 6:
						$multiplier = 1000;
						$suffix = "K";
						break;
					case 7:
					case 8:
					case 9:
						$multiplier = 1000000;
						$suffix = "M";
						break;
					case 10:
					case 11:
					case 12:
						$multiplier = 1000000000;
						$suffix = "B";
						break;
				}
				$number = $number / $multiplier;
				$number = number_format($number,  1, '.', '');
				$number = $number.$suffix;
			}
			return $number;
		}

		/** * Function to validate numbers */
		function isNumericValue($value){

			return is_numeric($value);
		}

		/** * Function to validate percentage value */
		function isPercentValue($value){

			return ($value >=0 && $value <= 100);
		}

		/** * Function to check whether user has certain permissions as per given */
		function checkUserPermissions($permission,$loggedInUserDetailsArr) {
			$userPermissionsArray = explode(',',$loggedInUserDetailsArr['permissions']);
			if(!in_array($permission,$userPermissionsArray) and $loggedInUserDetailsArr['user_role']!='super') {
				header("location: index.php");
				exit;
			}
		}

		/** * Function to generate random unique number for particular column of particular table */
		function generate_id($prefix, $randomNo, $tableName, $columnName){
			$chkprofile=$this->query("select ".$columnName." from ".PREFIX.$tableName." where ".$columnName." = '".$prefix.$randomNo."'");
			if($this->num_rows($chkprofile)>0){
				$randomNo = str_shuffle('1234567890123456789012345678901234567890');
				$randomNo = substr($randomNo,0,8);
				$this->generate_id($prefix, $randomNo, $tableName, $columnName);
			}else{
				return  $prefix.$randomNo;
			}
		}

		/** * Function to get title of youtube video by its video id */
		function get_youtube_title($ref) {
	      	$json = file_get_contents('http://www.youtube.com/oembed?url=http://www.youtube.com/watch?v=' . $ref . '&format=json'); //get JSON video details
	      	$details = json_decode($json, true); //parse the JSON into an array
	      	return $details['title']; //return the video title
	    }

		/** * Function to get ordinal with number */
	    function ordinal($number) {
		    $ends = array('th','st','nd','rd','th','th','th','th','th','th');
		    if ((($number % 100) >= 11) && (($number%100) <= 13))
		        return $number. 'th';
		    else
		        return $number. $ends[$number % 10];
		}
		function getIndianCurrency($final_amt) {
			$decimal = round($final_amt - ($no = floor($final_amt)), 2) * 100;
			$hundred = null;
			$digits_length = strlen($no);
			$i = 0;
			$str = array();
			$words = array(0 => '', 1 => 'one', 2 => 'two',
				3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
				7 => 'seven', 8 => 'eight', 9 => 'nine',
				10 => 'ten', 11 => 'eleven', 12 => 'twelve',
				13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
				16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
				19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
				40 => 'forty', 50 => 'fifty', 60 => 'sixty',
				70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
			$digits = array('', 'hundred','thousand','lakh', 'crore');
			while( $i < $digits_length ) {
				$divider = ($i == 2) ? 10 : 100;
				$final_amt = floor($no % $divider);
				$no = floor($no / $divider);
				$i += $divider == 10 ? 1 : 2;
				if ($final_amt) {
					$plural = (($counter = count($str)) && $final_amt > 9) ? '' : null;
					//$plural = (($counter = count($str)) && $number > 9) ? 's' : null;
					$hundred = ($counter == 1 && $str[0]) ? '' : null;
					$str [] = ($final_amt < 21) ? $words[$final_amt].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($final_amt / 10) * 10].' '.$words[$final_amt % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
				} else $str[] = null;
			}
			$Rupees = 'Rupees '.implode('', array_reverse($str));
			$paise = ($decimal) ? " and " . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
			//return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise;
			return ucfirst(($Rupees ? $Rupees . '' : '') . $paise);
		}
		/*===================== EXTRA FUNCTIONS ENDS =====================*/


		function getListOfCities(){
			$query = "select distinct districtname from ".PREFIX."pincode order by districtname asc";
			return $this->query($query);
		}
		function getListOfStates(){
			$query = "select distinct statename from ".PREFIX."pincode order by statename asc";
			return $this->query($query);
		}


		/* ============================= ENTITY MODULE STARTS===================================*/

		function getUniqueEntityById($id) {
			$id = $this->escape_string($this->strip_all($id));
			$query = "select * from ".PREFIX."entity_master where id='".$id."'";
			$sql = $this->query($query);
			return $this->fetch($sql);
		}


		function addEntity($data) {
			$name 			= $this->escape_string($this->strip_all($data['name']));
			$active 		= $this->escape_string($this->strip_all($data['active']));
			$created		= date("Y-m-d H:i:s");

			$query = "insert into ".PREFIX."entity_master(name, active, created) values ('".$name."', '".$active."', '".$created."')";
			return $this->query($query);
		}

		function updateEntity($data){
			$id 				= $this->escape_string($this->strip_all($data['id']));
			$name		= $this->escape_string($this->strip_all($data['name']));
			$active 			= $this->escape_string($this->strip_all($data['active']));

			$query = "update ".PREFIX."entity_master set name = '".$name."' , active = '".$active."' where id='".$id."'";
			return $this->query($query);
		}

		/* ============================= ENTITY MODULE ENDS===================================*/

		/* ============================= COMPANY MASTER STARTS===================================*/

		function getUniqueCompanyMasterById() {
			//$id = $this->escape_string($this->strip_all($id));
			$query = "select * from company_info";
			$sql = $this->query($query);
			return $this->fetch($sql);
		}
		
		/* ============================= CUSTOMER MASTER STARTS===================================*/

		function getUniqueCustomerMasterById($id) {
			$id = $this->escape_string($this->strip_all($id));
			$query = "select * from ".PREFIX."customer_master where id='".$id."' AND deleted_time IS NULL";
			$sql = $this->query($query);
			return $this->fetch($sql);
		}
		function getStateMasterByName() {
			$query = "select * from mstates";
			return $this->query($query);
		}
		function getStateCodeMasterByName($statename) {
			$query = "select statecode from mstates where statename= '".$statename."' ";
			$sql = $this->query($query);
			return $this->fetch($sql);
				}
		function getActiveCustomerDetails() {
			$query = "select * from ".PREFIX."customer_master where active='1' AND deleted_time IS NULL";
			return $this->query($query);
			
		}
		

		function addCustomerMaster($data,$user_by,$file) {

		$customer_name = $this->escape_string($this->strip_all($data['customer_name']));
		$company_phone = $this->escape_string($this->strip_all($data['company_phone']));
		$company_pan = $this->escape_string($this->strip_all($data['company_pan']));
		$company_email = $this->escape_string($this->strip_all($data['company_email']));
		$company_gst = $this->escape_string($this->strip_all($data['company_gst']));
		$billing_address = $this->escape_string($this->strip_all($data['billing_address']));
		$shipping_address = $this->escape_string($this->strip_all($data['shipping_address']));
		$contact_person_name = $this->escape_string($this->strip_all($data['contact_person_name']));
		$contact_person_phone = $this->escape_string($this->strip_all($data['contact_person_phone']));
		$contact_person_whatsapp = $this->escape_string($this->strip_all($data['contact_person_whatsapp']));
		$contact_person_email = $this->escape_string($this->strip_all($data['contact_person_email']));
		$statename= $this->escape_string($this->strip_all($data['statename']));
		$credit_days= $this->escape_string($this->strip_all($data['credit_days']));
		$opening_balance= $this->escape_string($this->strip_all($data['opening_balance']));
		$sales_person_name= $this->escape_string($this->strip_all($data['sales_person_name']));
		$date_of_opening= $this->escape_string($this->strip_all($data['date_of_opening']));
		$fssai_no= $this->escape_string($this->strip_all($data['fssai_no']));
		$area_pincode= $this->escape_string($this->strip_all($data['area_pincode']));
		$active = $this->escape_string($this->strip_all($data['active']));

		$SaveImage = new SaveImage();
				$imgDir = 'images/';

			
		if(!empty($file['thumbnail_image']['name'])) {		
		
			$cropData = $this->strip_all($data['cropData1']);
			$file_name = strtolower( pathinfo($file['thumbnail_image']['name'], PATHINFO_FILENAME));
		
			$thumbnail_image = $SaveImage->uploadCroppedImageFileFromForm($file['thumbnail_image'], 210, $cropData, $imgDir, $file_name.'-'.time().'-1',false);

		
		} else {
			$thumbnail_image = "";
		}

		$query = "insert into ".PREFIX."customer_master(customer_name, company_phone, company_pan, company_email, company_gst, billing_address, shipping_address, contact_person_name, contact_person_phone, contact_person_whatsapp, contact_person_email, active, created_by, created_time,statename,credit_days,sales_person_name,opening_balance,date_of_opening,fssai_no,area_pincode,upload_document) values ('".$customer_name."', '".$company_phone."', '".$company_pan."', '".$company_email."', '".$company_gst."', '".$billing_address."', '".$shipping_address."', '".$contact_person_name."', '".$contact_person_phone."', '".$contact_person_whatsapp."', '".$contact_person_email."', '".$active."', '".$user_by."', '".CURRENTMILIS."','".$statename."','".$credit_days."','".$sales_person_name."','".$opening_balance."','".$date_of_opening."','".$fssai_no."','".$area_pincode."','".$thumbnail_image."')";
		return $this->query($query);
		}

		function updateCustomerMaster($data,$user_by,$file){
			$id = $this->escape_string($this->strip_all($data['id']));
			$customer_name = $this->escape_string($this->strip_all($data['customer_name']));
			$company_phone = $this->escape_string($this->strip_all($data['company_phone']));
			$company_pan = $this->escape_string($this->strip_all($data['company_pan']));
			$company_email = $this->escape_string($this->strip_all($data['company_email']));
			$company_gst = $this->escape_string($this->strip_all($data['company_gst']));
			$billing_address = $this->escape_string($this->strip_all($data['billing_address']));
			$shipping_address = $this->escape_string($this->strip_all($data['shipping_address']));
			$contact_person_name = $this->escape_string($this->strip_all($data['contact_person_name']));
			$contact_person_phone = $this->escape_string($this->strip_all($data['contact_person_phone']));
			$contact_person_whatsapp = $this->escape_string($this->strip_all($data['contact_person_whatsapp']));
			$contact_person_email = $this->escape_string($this->strip_all($data['contact_person_email']));
			$statename= $this->escape_string($this->strip_all($data['statename']));
			$credit_days= $this->escape_string($this->strip_all($data['credit_days']));
			$sales_person_name= $this->escape_string($this->strip_all($data['sales_person_name']));
			$active = $this->escape_string($this->strip_all($data['active']));
			$opening_balance= $this->escape_string($this->strip_all($data['opening_balance']));
			$date_of_opening= $this->escape_string($this->strip_all($data['date_of_opening']));
			$fssai_no= $this->escape_string($this->strip_all($data['fssai_no']));
			$area_pincode= $this->escape_string($this->strip_all($data['area_pincode']));
			
				
			$SaveImage = new SaveImage();
					$imgDir = 'images/';

				
			if(!empty($file['thumbnail_image']['name'])) {		
				$cropData = $this->strip_all($data['cropData1']);
				$file_name = strtolower( pathinfo($file['thumbnail_image']['name'], PATHINFO_FILENAME));
			
				$thumbnail_image = $SaveImage->uploadCroppedImageFileFromForm($file['thumbnail_image'], 210, $cropData, $imgDir, $file_name.'-'.time().'-1',false);

			
			} else {
				$thumbnail_image = "";
			}
			$query = "update ".PREFIX."customer_master set customer_name='".$customer_name."', company_phone='".$company_phone."' , company_pan='".$company_pan."', company_email='".$company_email."' , company_gst='".$company_gst."' , billing_address='".$billing_address."' , shipping_address='".$shipping_address."' , contact_person_name='".$contact_person_name."' , contact_person_phone='".$contact_person_phone."' , contact_person_whatsapp='".$contact_person_whatsapp."' , contact_person_email='".$contact_person_email."', active='".$active."', updated_by='".$user_by."', updated_time='".CURRENTMILIS."', statename='".$statename."',credit_days='".$credit_days."',sales_person_name='".$sales_person_name."',opening_balance='".$opening_balance."',date_of_opening='".$date_of_opening."',fssai_no='".$fssai_no."',area_pincode='".$area_pincode."',upload_document='".$thumbnail_image."' WHERE id='".$id."' ";
			return $this->query($query);
		}

		/* ============================= CUSTOMER MASTER ENDS===================================*/

		
		/* ============================= ITEM GROUP MASTER STARTS===================================*/

		function getUniqueItemGroupMasterById($id) {
			$id = $this->escape_string($this->strip_all($id));
			$query = "select * from ".PREFIX."item_group_master where id='".$id."' AND deleted_time IS NULL";
			$sql = $this->query($query);
			return $this->fetch($sql);
		}

		function getActiveItemGroupMaster() {
			$query = "select * from ".PREFIX."item_group_master where deleted_time IS NULL AND active='1'";
			return $this->query($query);
		}

		function addItemGroupMaster($data,$user_by) {
			$group_name = $this->escape_string($this->strip_all($data['group_name']));
			$active = $this->escape_string($this->strip_all($data['active']));
			$query = "insert into ".PREFIX."item_group_master(group_name, active, created_by, created_time) values ('".$group_name."', '".$active."', '".$user_by."', '".CURRENTMILIS."')";
			return $this->query($query);
		}

		function updateItemGroupMaster($data,$user_by){
				$id = $this->escape_string($this->strip_all($data['id']));
				$group_name = $this->escape_string($this->strip_all($data['group_name']));
				$active = $this->escape_string($this->strip_all($data['active']));
				$query = "update ".PREFIX."item_group_master SET group_name='".$group_name."',active='".$active."' WHERE id='".$id."'";
				return $this->query($query);
		}

		/* ============================= ITEM GROUP MASTER ENDS===================================*/

		/* ============================= CUSTOMER RATE STARTS===================================*/
		function getUniqueItemMasterById($id) {
			$id = $this->escape_string($this->strip_all($id));
			$query = "select * from ".PREFIX."item_master where id='".$id."' AND deleted_time IS NULL";
			
			$sql = $this->query($query);
			return $this->fetch($sql);
		}
		function getUniqueItemAndRateMasterById($id,$customer_id) {
			$id = $this->escape_string($this->strip_all($id));
			$query = "SELECT *,(SELECT rate FROM ".PREFIX."customer_rate_master WHERE ".PREFIX."customer_rate_master.item_id=".PREFIX."item_master.id AND ".PREFIX."customer_rate_master.customer_id='".$customer_id."') as rate FROM ".PREFIX."item_master WHERE id='".$id."'";
			$sql = $this->query($query);
			return $this->fetch($sql);
		}
		function getUniqueRateById($customer_id,$item_id) {
			
			$query = "SELECT * FROM ".PREFIX."customer_rate_master WHERE customer_id='".$customer_id."' AND item_id='".$item_id."'" ;
			$sql = $this->query($query);
			return $this->fetch($sql);
		}
		function getUniqueSupplierRateById($supplier_id,$item_id) {
			$query = "SELECT * FROM ".PREFIX."supplier_rate_master WHERE supplier_id='".$supplier_id."' AND item_id='".$item_id."'" ;
			$sql = $this->query($query);
			return $this->fetch($sql);
		}
		function getUniqueChallanItemAndRateMasterById($id,$customer_id) {
			$id = $this->escape_string($this->strip_all($id));
			$query = "SELECT *,(SELECT challan_rate FROM ".PREFIX."customer_rate_master WHERE ".PREFIX."customer_rate_master.item_id=".PREFIX."item_master.id AND ".PREFIX."customer_rate_master.customer_id='".$customer_id."') as rate FROM ".PREFIX."item_master WHERE id='".$id."'";
			$sql = $this->query($query);
			return $this->fetch($sql);
		}
		function getUniquePurchaseRateMasterById($id,$supplier_id) {
			$id = $this->escape_string($this->strip_all($id));
			$query = "SELECT *,(SELECT rate FROM ".PREFIX."supplier_rate_master WHERE ".PREFIX."supplier_rate_master.item_id=".PREFIX."item_master.id AND ".PREFIX."supplier_rate_master.supplier_id='".$supplier_id."') as rate FROM ".PREFIX."item_master WHERE id='".$id."'";
			$sql = $this->query($query);
			return $this->fetch($sql);
		}
		function getActiveItemUnitMaster() {
			$query = "select stock_unit from ".PREFIX."item_master where active='1' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getActiveUnitMaster($id) {
			$query = "select stock_unit from ".PREFIX."item_master where id='".$id."' AND active='1' AND deleted_time IS NULL";
			$sql = $this->query($query);
			return $this->fetch($sql);
		}
		function getActiveItemTaxMaster() {
			$query = "select item_name from ".PREFIX."item_master where active='1' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getActiveGSTTaxMaster($id) {
			$query = "select gst_percentage from ".PREFIX."item_master where id='".$id."' active='1' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getActiveItemDetails() {
			$query = "select * from ".PREFIX."item_master where active='1' AND deleted_time IS NULL";
			return $this->query($query);
		}
		
		function addItemMaster($data,$user_by) {
			$group_id = $this->escape_string($this->strip_all($data['group_id']));
			$item_name = $this->escape_string($this->strip_all($data['item_name']));
			$item_short_name = $this->escape_string($this->strip_all($data['item_short_name']));
			$stock_unit = $this->escape_string($this->strip_all($data['stock_unit']));
			$gst_percentage = $this->escape_string($this->strip_all($data['gst_percentage']));
			$cess_percentage = $this->escape_string($this->strip_all($data['cess_percentage']));
			$reorder_quantitiy = $this->escape_string($this->strip_all($data['reorder_quantitiy']));
			$hsn_code = $this->escape_string($this->strip_all($data['hsn_code']));
			$active = $this->escape_string($this->strip_all($data['active']));
			
			$query = "insert into ".PREFIX."item_master(group_id, item_name, item_short_name, stock_unit, gst_percentage,cess_percentage, reorder_quantitiy, hsn_code, active, created_by, created_time) values ('".$group_id."', '".$item_name."', '".$item_short_name."', '".strtoupper($stock_unit)."', '".$gst_percentage."', '".$cess_percentage."','".$reorder_quantitiy."', '".$hsn_code."', '".$active."', '".$user_by."', '".CURRENTMILIS."')";
			return $this->query($query);
		}

		function updateItemMaster($data,$user_by){
			$id = $this->escape_string($this->strip_all($data['id']));
			$group_id = $this->escape_string($this->strip_all($data['group_id']));
			$item_name = $this->escape_string($this->strip_all($data['item_name']));
			$item_short_name = $this->escape_string($this->strip_all($data['item_short_name']));
			$stock_unit = $this->escape_string($this->strip_all($data['stock_unit']));
			$gst_percentage = $this->escape_string($this->strip_all($data['gst_percentage']));
			$cess_percentage = $this->escape_string($this->strip_all($data['cess_percentage']));
			$reorder_quantitiy = $this->escape_string($this->strip_all($data['reorder_quantitiy']));
			$hsn_code = $this->escape_string($this->strip_all($data['hsn_code']));
			$active = $this->escape_string($this->strip_all($data['active']));
 
			$query = "update ".PREFIX."item_master SET group_id='".$group_id."', item_name='".$item_name."', item_short_name='".$item_short_name."', stock_unit='".strtoupper($stock_unit)."', gst_percentage='".$gst_percentage."', cess_percentage='".$cess_percentage."',reorder_quantitiy='".$reorder_quantitiy."', hsn_code='".$hsn_code."', active='".$active."' WHERE id='".$id."'";
			return $this->query($query);
	}
		/* ============================= ITEM MASTER ENDS===================================*/

		/* ============================= ITEM MASTER STARTS===================================*/
		
		function addCustomerRateMaster($data,$user_by) {
			$customer_id = $this->escape_string($this->strip_all($data['customer_id']));
			$item_id = $this->escape_string($this->strip_all($data['item_id']));
			$rate = $this->escape_string($this->strip_all($data['rate']));
			$challan_rate = $this->escape_string($this->strip_all($data['challan_rate']));
			$check = $this->fetch($this->query("select * from ".PREFIX."customer_rate_master where customer_id='".$customer_id."' and item_id='".$item_id."' "));

			if ($check) {
				$move = $this->query("INSERT INTO ".PREFIX."customer_rate_master_history SELECT * FROM ".PREFIX."customer_rate_master WHERE customer_id = '".$customer_id."' and item_id='".$item_id."'");
				$delete = $this->query("DELETE FROM ".PREFIX."customer_rate_master WHERE customer_id = '".$customer_id."' and item_id='".$item_id."'");
				
				$query = "insert into ".PREFIX."customer_rate_master (customer_id, item_id, rate,challan_rate, created_by, created_time) values ('".$customer_id."', '".$item_id."', '".$rate."', '".$challan_rate."', '".$user_by."', '".CURRENTMILIS."')";
			} else {
				
				$query = "insert into ".PREFIX."customer_rate_master (customer_id, item_id, rate,challan_rate, created_by, created_time) values ('".$customer_id."', '".$item_id."', '".$rate."', '".$challan_rate."', '".$user_by."', '".CURRENTMILIS."')";
			}
				return $this->query($query);

		}
				/* ============================= ITEM MASTER ENDS===================================*/
	
	/* ============================= SUPPLIER MASTER STARTS===================================*/

	function getUniqueSupplierMasterById($id) {
		$id = $this->escape_string($this->strip_all($id));
		$query = "select * from ".PREFIX."supplier_master where id='".$id."' AND deleted_time IS NULL";
		$sql = $this->query($query);
		return $this->fetch($sql);
	}

	function getActiveSupplierDetails() {
		$query = "select * from ".PREFIX."supplier_master where active='1' AND deleted_time IS NULL";
		return $this->query($query);
	}
	

	function addSupplierMaster($data,$user_by,$file) {
	$supplier_name = $this->escape_string($this->strip_all($data['supplier_name']));
	$company_phone = $this->escape_string($this->strip_all($data['company_phone']));
	$company_pan = $this->escape_string($this->strip_all($data['company_pan']));
	$company_email = $this->escape_string($this->strip_all($data['company_email']));
	$company_gst = $this->escape_string($this->strip_all($data['company_gst']));
	$billing_address = $this->escape_string($this->strip_all($data['billing_address']));
	$shipping_address = $this->escape_string($this->strip_all($data['shipping_address']));
	$contact_person_name = $this->escape_string($this->strip_all($data['contact_person_name']));
	$contact_person_phone = $this->escape_string($this->strip_all($data['contact_person_phone']));
	$contact_person_whatsapp = $this->escape_string($this->strip_all($data['contact_person_whatsapp']));
	$contact_person_email = $this->escape_string($this->strip_all($data['contact_person_email']));
	$statename= $this->escape_string($this->strip_all($data['statename']));
	$purchase_person_name= $this->escape_string($this->strip_all($data['purchase_person_name']));
	$date_of_opening= $this->escape_string($this->strip_all($data['date_of_opening']));
	$opening_balance= $this->escape_string($this->strip_all($data['opening_balance']));
	$fssai_no= $this->escape_string($this->strip_all($data['fssai_no']));
	$area_pincode= $this->escape_string($this->strip_all($data['area_pincode']));
	$active = $this->escape_string($this->strip_all($data['active']));

	$SaveImage = new SaveImage();
	$imgDir = 'supplier_images/';

		if(!empty($file['thumbnail_image']['name'])) {		
			$cropData = $this->strip_all($data['cropData1']);
			$file_name = strtolower( pathinfo($file['thumbnail_image']['name'], PATHINFO_FILENAME));
		
			$thumbnail_image = $SaveImage->uploadCroppedImageFileFromForm($file['thumbnail_image'], 210, $cropData, $imgDir, $file_name.'-'.time().'-1',false);

		} else {
			$thumbnail_image = "";
		}
	$query = "insert into ".PREFIX."supplier_master(supplier_name, company_phone, company_pan, company_email, company_gst, billing_address, shipping_address, contact_person_name, contact_person_phone, contact_person_whatsapp, contact_person_email, active, created_by, created_time,statename,purchase_person_name,date_of_opening,opening_balance,area_pincode,fssai_no,upload_document) values ('".$supplier_name."', '".$company_phone."', '".$company_pan."', '".$company_email."', '".$company_gst."', '".$billing_address."', '".$shipping_address."', '".$contact_person_name."', '".$contact_person_phone."', '".$contact_person_whatsapp."', '".$contact_person_email."', '".$active."', '".$user_by."', '".CURRENTMILIS."','".$statename."','".$purchase_person_name."','".$date_of_opening."','".$opening_balance."','".$area_pincode."','".$fssai_no."','".$thumbnail_image."')";
	return $this->query($query);
	}

	function updateSupplierMaster($data,$user_by,$file){
		$id = $this->escape_string($this->strip_all($data['id']));
		$supplier_name = $this->escape_string($this->strip_all($data['supplier_name']));
		$company_phone = $this->escape_string($this->strip_all($data['company_phone']));
		$company_pan = $this->escape_string($this->strip_all($data['company_pan']));
		$company_email = $this->escape_string($this->strip_all($data['company_email']));
		$company_gst = $this->escape_string($this->strip_all($data['company_gst']));
		$billing_address = $this->escape_string($this->strip_all($data['billing_address']));
		$shipping_address = $this->escape_string($this->strip_all($data['shipping_address']));
		$contact_person_name = $this->escape_string($this->strip_all($data['contact_person_name']));
		$contact_person_phone = $this->escape_string($this->strip_all($data['contact_person_phone']));
		$contact_person_whatsapp = $this->escape_string($this->strip_all($data['contact_person_whatsapp']));
		$contact_person_email = $this->escape_string($this->strip_all($data['contact_person_email']));
		$statename= $this->escape_string($this->strip_all($data['statename']));
		$purchase_person_name= $this->escape_string($this->strip_all($data['purchase_person_name']));
		$date_of_opening= $this->escape_string($this->strip_all($data['date_of_opening']));
		$opening_balance= $this->escape_string($this->strip_all($data['opening_balance']));
		$fssai_no= $this->escape_string($this->strip_all($data['fssai_no']));
		$area_pincode= $this->escape_string($this->strip_all($data['area_pincode']));
		$active = $this->escape_string($this->strip_all($data['active']));
		
		$SaveImage = new SaveImage();
		$imgDir = 'supplier_images/';

		if(!empty($file['thumbnail_image']['name'])) {		
			$cropData = $this->strip_all($data['cropData1']);
			$file_name = strtolower( pathinfo($file['thumbnail_image']['name'], PATHINFO_FILENAME));
		
			$thumbnail_image = $SaveImage->uploadCroppedImageFileFromForm($file['thumbnail_image'], 210, $cropData, $imgDir, $file_name.'-'.time().'-1',false);

		} else {
		$thumbnail_image = "";
		}
		$query = "update ".PREFIX."supplier_master set supplier_name='".$supplier_name."', company_phone='".$company_phone."' , company_pan='".$company_pan."', company_email='".$company_email."' , company_gst='".$company_gst."' , billing_address='".$billing_address."' , shipping_address='".$shipping_address."' , contact_person_name='".$contact_person_name."' , contact_person_phone='".$contact_person_phone."' , contact_person_whatsapp='".$contact_person_whatsapp."' , contact_person_email='".$contact_person_email."', active='".$active."', updated_by='".$user_by."', updated_time='".CURRENTMILIS."',statename='".$statename."',purchase_person_name='".$purchase_person_name."',date_of_opening='".$date_of_opening."',opening_balance='".$opening_balance."',fssai_no='".$fssai_no."',area_pincode='".$area_pincode."',upload_document='".$thumbnail_image."' WHERE id='".$id."' ";
		return $this->query($query);
	}

	/* ============================= SUPPLIER MASTER ENDS===================================*/

	/* ============================= SUPPLIER RATE MASTER STARTS===================================*/
	function updateRateMaster($data,$user_by) {
	if(isset($data['item_id'])) {
		$item_id = $data['item_id'];
		$supplier_id = $data['supplier_id'];
		$rate = $data['rate'];

		$rate_value = $this->escape_string($this->strip_all($rate[$i])); 
		$item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
		$supplier_id_value = $this->escape_string($this->strip_all($supplier_id[$i])); 
		$check = $this->fetch($this->query("select * from ".PREFIX."supplier_rate_master where supplier_id='".$supplier_id."' and item_id='".$item_id."' "));
		if ($check) {
			$move = $this->query("INSERT INTO ".PREFIX."supplier_rate_master_history SELECT * FROM ".PREFIX."supplier_rate_master WHERE supplier_id = '".$supplier_id."' and item_id='".$item_id."'");
			$delete = $this->query("DELETE FROM ".PREFIX."supplier_rate_master WHERE supplier_id = '".$supplier_id."' and item_id='".$item_id."'");
			
			$query = "insert into ".PREFIX."supplier_rate_master (supplier_id, item_id, rate, created_by, created_time) values ('".$supplier_id."', '".$item_id."', '".$rate."', '".$user_by."', '".CURRENTMILIS."')";
		} else {
			
			$query = "insert into ".PREFIX."supplier_rate_master (supplier_id, item_id, rate, created_by, created_time) values ('".$supplier_id."', '".$item_id."', '".$rate."', '".$user_by."', '".CURRENTMILIS."')";
		}
			return $this->query($query);
		}
	}
	function getUniqueSupplierRateMasterById($id,$supplier_id) {
		$id = $this->escape_string($this->strip_all($id));
		$query = "SELECT *,(SELECT rate FROM ".PREFIX."supplier_rate_master WHERE ".PREFIX."supplier_rate_master.item_id=".PREFIX."item_master.id AND ".PREFIX."supplier_rate_master.supplier_id='".$supplier_id."') as rate FROM ".PREFIX."item_master WHERE id='".$id."'";
		$sql = $this->query($query);
		return $this->fetch($sql);
	}
	function addsupplierRateMaster($data,$user_by) {
		$supplier_id = $this->escape_string($this->strip_all($data['supplier_id']));
		$item_id = $this->escape_string($this->strip_all($data['item_id']));
		$rate = $this->escape_string($this->strip_all($data['rate']));
		$check = $this->fetch($this->query("select * from ".PREFIX."supplier_rate_master where supplier_id='".$supplier_id."' and item_id='".$item_id."' "));

		if ($check) {
			$move = $this->query("INSERT INTO ".PREFIX."supplier_rate_master_history SELECT * FROM ".PREFIX."supplier_rate_master WHERE supplier_id = '".$supplier_id."' and item_id='".$item_id."'");
			$delete = $this->query("DELETE FROM ".PREFIX."supplier_rate_master WHERE supplier_id = '".$supplier_id."' and item_id='".$item_id."'");
			
			$query = "insert into ".PREFIX."supplier_rate_master (supplier_id, item_id, rate, created_by, created_time) values ('".$supplier_id."', '".$item_id."', '".$rate."', '".$user_by."', '".CURRENTMILIS."')";
		} else {
			
			$query = "insert into ".PREFIX."supplier_rate_master (supplier_id, item_id, rate, created_by, created_time) values ('".$supplier_id."', '".$item_id."', '".$rate."', '".$user_by."', '".CURRENTMILIS."')";
		}
			return $this->query($query);

	}
	/* ============================= SUPPLIER RATE MASTER ENDS===================================*/

		/* ============================= TRANSPORTER MASTER STARTS===================================*/

		function getUniqueTransporterMasterById($id) {
			$id = $this->escape_string($this->strip_all($id));
			$query = "select * from ".PREFIX."transporter_master where id='".$id."' AND deleted_time IS NULL";
			$sql = $this->query($query);
			return $this->fetch($sql);
		}
	
		function getActiveTransporterDetails() {
			$query = "select * from ".PREFIX."transporter_master where active='1' AND deleted_time IS NULL";
			return $this->query($query);
		}
		
	
		function addTransporterMaster($data,$user_by) {
		$transporter_name = $this->escape_string($this->strip_all($data['transporter_name']));
		$company_phone = $this->escape_string($this->strip_all($data['company_phone']));
		$company_pan = $this->escape_string($this->strip_all($data['company_pan']));
		$company_email = $this->escape_string($this->strip_all($data['company_email']));
		$company_gst = $this->escape_string($this->strip_all($data['company_gst']));
		$billing_address = $this->escape_string($this->strip_all($data['billing_address']));
		$shipping_address = $this->escape_string($this->strip_all($data['shipping_address']));
		$contact_person_name = $this->escape_string($this->strip_all($data['contact_person_name']));
		$contact_person_phone = $this->escape_string($this->strip_all($data['contact_person_phone']));
		$contact_person_whatsapp = $this->escape_string($this->strip_all($data['contact_person_whatsapp']));
		$contact_person_email = $this->escape_string($this->strip_all($data['contact_person_email']));
		$active = $this->escape_string($this->strip_all($data['active']));
		$query = "insert into ".PREFIX."transporter_master(transporter_name, company_phone, company_pan, company_email, company_gst, billing_address, shipping_address, contact_person_name, contact_person_phone, contact_person_whatsapp, contact_person_email, active, created_by, created_time) values ('".$transporter_name."', '".$company_phone."', '".$company_pan."', '".$company_email."', '".$company_gst."', '".$billing_address."', '".$shipping_address."', '".$contact_person_name."', '".$contact_person_phone."', '".$contact_person_whatsapp."', '".$contact_person_email."', '".$active."', '".$user_by."', '".CURRENTMILIS."')";
		return $this->query($query);
		}
	
		function updateTransporterMaster($data,$user_by){
			$id = $this->escape_string($this->strip_all($data['id']));
			$transporter_name = $this->escape_string($this->strip_all($data['transporter_name']));
			$company_phone = $this->escape_string($this->strip_all($data['company_phone']));
			$company_pan = $this->escape_string($this->strip_all($data['company_pan']));
			$company_email = $this->escape_string($this->strip_all($data['company_email']));
			$company_gst = $this->escape_string($this->strip_all($data['company_gst']));
			$billing_address = $this->escape_string($this->strip_all($data['billing_address']));
			$shipping_address = $this->escape_string($this->strip_all($data['shipping_address']));
			$contact_person_name = $this->escape_string($this->strip_all($data['contact_person_name']));
			$contact_person_phone = $this->escape_string($this->strip_all($data['contact_person_phone']));
			$contact_person_whatsapp = $this->escape_string($this->strip_all($data['contact_person_whatsapp']));
			$contact_person_email = $this->escape_string($this->strip_all($data['contact_person_email']));
			$active = $this->escape_string($this->strip_all($data['active']));
			$query = "update ".PREFIX."transporter_master set transporter_name='".$transporter_name."', company_phone='".$company_phone."' , company_pan='".$company_pan."', company_email='".$company_email."' , company_gst='".$company_gst."' , billing_address='".$billing_address."' , shipping_address='".$shipping_address."' , contact_person_name='".$contact_person_name."' , contact_person_phone='".$contact_person_phone."' , contact_person_whatsapp='".$contact_person_whatsapp."' , contact_person_email='".$contact_person_email."', active='".$active."', updated_by='".$user_by."', updated_time='".CURRENTMILIS."' WHERE id='".$id."' ";
			return $this->query($query);
		}
	
		/* ============================= TRANSPORTER MASTER ENDS===================================*/

		/* ============================= LEDGER MASTER STARTS===================================*/

		function getUniqueLedgerMasterById($id) {
			$id = $this->escape_string($this->strip_all($id));
			$query = "select * from ".PREFIX."ledger_master where id='".$id."' AND deleted_time IS NULL";
			$sql = $this->query($query);
			return $this->fetch($sql);
		}
	
		function getActiveLedgerDetails() {
			$query = "select * from ".PREFIX."ledger_master where active='1' AND deleted_time IS NULL";
			return $this->query($query);
		}
		
	
		function addLedgerMaster($data,$user_by) {
		$description = $this->escape_string($this->strip_all($data['description']));
		$exp_income_head = $this->escape_string($this->strip_all($data['exp_income_head']));
		$dt_of_opening_bal = $this->escape_string($this->strip_all($data['dt_of_opening_bal']));
		$opening_balance = $this->escape_string($this->strip_all($data['opening_balance']));
		$active = $this->escape_string($this->strip_all($data['active']));
		$query = "insert into ".PREFIX."ledger_master(description, exp_income_head, dt_of_opening_bal, opening_balance,active, created_by, created_time) values ('".$description."', '".$exp_income_head."', '".$dt_of_opening_bal."', '".$opening_balance."', '".$active."', '".$user_by."', '".CURRENTMILIS."')";
		return $this->query($query);
		}
	
		function updateLedgerMaster($data,$user_by){
			$id = $this->escape_string($this->strip_all($data['id']));
			$description = $this->escape_string($this->strip_all($data['description']));
			$exp_income_head = $this->escape_string($this->strip_all($data['exp_income_head']));
			$dt_of_opening_bal = $this->escape_string($this->strip_all($data['dt_of_opening_bal']));
			$opening_balance = $this->escape_string($this->strip_all($data['opening_balance']));
			$active = $this->escape_string($this->strip_all($data['active']));
			$query = "update ".PREFIX."ledger_master set description='".$description."', exp_income_head='".$exp_income_head."' , dt_of_opening_bal='".$dt_of_opening_bal."', opening_balance='".$opening_balance."' , active='".$active."', updated_by='".$user_by."', updated_time='".CURRENTMILIS."' WHERE id='".$id."' ";
			return $this->query($query);
		}
	
		/* ============================= LEDGER MASTER ENDS===================================*/
				/* ============================= OPENING BALANCE MASTER STARTS===================================*/

				function getUniqueOpeningBalanceMasterById($id) {
					$id = $this->escape_string($this->strip_all($id));
					$query = "select * from ".PREFIX."opening_balance_master where id='".$id."' AND deleted_time IS NULL";
					$sql = $this->query($query);
					return $this->fetch($sql);
				}
			
				function getActiveOpeningBalanceDetails($id) {
					$query = "select * from ".PREFIX."opening_balance_master where active='1' AND id='".$id."' AND deleted_time IS NULL";
					$sql = $this->query($query);
					return $this->fetch($sql);				}
				
			
				function addOpeningBalanceMaster($data,$user_by) {
					$item_id = $this->escape_string($this->strip_all($data['item_id']));
					$date = $this->escape_string($this->strip_all($data['date']));
					$quantity = $this->escape_string($this->strip_all($data['quantity']));
					$rate = $this->escape_string($this->strip_all($data['rate']));
					$active = $this->escape_string($this->strip_all($data['active']));
					$query = "insert into ".PREFIX."opening_balance_master(item_id, date, quantity,rate,active, created_by, created_time) values ('".$item_id."', '".$date."', '".$quantity."','".$rate."', '".$active."', '".$user_by."', '".CURRENTMILIS."')";
					$this -> addStock($item_id,$quantity,'','','','',$user_by);
					return $this->query($query);
				
				}
			
				function updateOpeningBalanceMaster($data,$user_by){
					$id = $this->escape_string($this->strip_all($data['id']));
					$item_id = $this->escape_string($this->strip_all($data['item_id']));
					$date = $this->escape_string($this->strip_all($data['date']));
					$quantity = $this->escape_string($this->strip_all($data['quantity']));
					$rate = $this->escape_string($this->strip_all($data['rate']));
					$active = $this->escape_string($this->strip_all($data['active']));
					$query = "update ".PREFIX."opening_balance_master set item_id='".$item_id."', date='".$date."' , quantity='".$quantity."',rate='".$rate."', active='".$active."', updated_by='".$user_by."', updated_time='".CURRENTMILIS."' WHERE id='".$id."' ";
					return $this->query($query);
				}
			
				
				/* ============================= OPENING BALANCE MASTER ENDS===================================*/
				/* =============================  Tax MASTER STARTS===================================*/

				function getUniqueTaxMasterById($id) {
					$id = $this->escape_string($this->strip_all($id));
					$query = "select * from ".PREFIX."tax_master where id='".$id."' AND deleted_time IS NULL";
					$sql = $this->query($query);
					return $this->fetch($sql);
				}
			
				function getActiveTaxDetails() {
					$query = "select * from ".PREFIX."tax_master where active='1' AND deleted_time IS NULL";
					return $this->query($query);
				}
				
			
				function addTaxMaster($data,$user_by) {
					$description = $this->escape_string($this->strip_all($data['description']));
					$cgst = $this->escape_string($this->strip_all($data['cgst']));
					$sgst = $this->escape_string($this->strip_all($data['sgst']));
					$igst = $this->escape_string($this->strip_all($data['igst']));
					$cess_tax = $this->escape_string($this->strip_all($data['cess_tax']));
					$active = $this->escape_string($this->strip_all($data['active']));
					$query = "insert into ".PREFIX."tax_master(description, cgst, sgst,igst,active, created_by, created_time,cess_tax) values ('".$description."', '".$cgst."', '".$sgst."', '".$igst."', '".$active."', '".$user_by."', '".CURRENTMILIS."','".$cess_tax."')";
					return $this->query($query);
				}
			
				function updateTaxMaster($data,$user_by){
					$id = $this->escape_string($this->strip_all($data['id']));
					$description = $this->escape_string($this->strip_all($data['description']));
					$cgst = $this->escape_string($this->strip_all($data['cgst']));
					$sgst = $this->escape_string($this->strip_all($data['sgst']));
					$igst = $this->escape_string($this->strip_all($data['igst']));
					$cess_tax = $this->escape_string($this->strip_all($data['cess_tax']));
					$active = $this->escape_string($this->strip_all($data['active']));
					$query = "update ".PREFIX."tax_master set description='".$description."', cgst='".$cgst."' , sgst='".$sgst."', igst='".$igst."',  active='".$active."', updated_by='".$user_by."', updated_time='".CURRENTMILIS."',cess_tax='".$cess_tax."' WHERE id='".$id."' ";
					return $this->query($query);
				}
			
				/* ============================= Tax BALANCE MASTER ENDS===================================*/
				/* ============================= ENTITY MODULE ENDS===================================*/


		/* ============================= TAX INVOICE TRANSACTION STARTS===================================*/
		function getUniuqeTaxInvoiceNo(){
			$int_invoice_on=$this->fetch($this->query("select MAX(int_invoice_on) x from ".PREFIX."tax_invoice_transaction ORDER BY int_invoice_on DESC"))['x'];
			
			if(!$int_invoice_on){
					$int_invoice_on='00001';
				}else{
					$int_invoice_on++;
					$int_invoice_on=sprintf('%05s', $int_invoice_on);
				}

				if (date('m') <= 4) {
					$financial_year = (date('y')-1) . '-' . date('y');
				} else {
					$financial_year = date('y') . '-' . (date('y') + 1);
				}
			return 'INV/'.$financial_year.'/'.$int_invoice_on;
		}

		function getUniuqeTaxIntInvoiceNo(){
			$int_invoice_on=$this->fetch($this->query("select MAX(int_invoice_on) x from ".PREFIX."tax_invoice_transaction ORDER BY int_invoice_on DESC"))['x'];
			
				$int_invoice_on++;
				$int_invoice_on=sprintf('%05s', $int_invoice_on);

			return $int_invoice_on;
		}

		function getUniqueTaxInvoiceDetailsById($id) {
			$sql = $this->query("select * from ".PREFIX."tax_invoice_transaction where id='".$id."' AND deleted_time IS NULL");
			return $this->fetch($sql);
		}
		function getUniqueTaxInvoiceDetailsById1($id,$from_date,$to_date) {
		
			$sql = $this->query("select * from ".PREFIX."tax_invoice_transaction where invoice_no='".$id."' AND(".PREFIX."tax_invoice_transaction.invoice_date BETWEEN '$from_date' and '$to_date') AND deleted_time IS NULL");
			return $this->fetch($sql);
		}
		function gstSalesReport($from_date,$to_date,$gst_per) {
			$sql = $this->query("SELECT cgst_per,SUM(cgst_amt) as cgst_total ,SUM(sgst_amt) as sgst_total,SUM(igst_amt) as igst_total,SUM(net_amt) as total_amount FROM  (select  A.tit_id,A.cgst_per,A.cgst_amt,A.sgst_per,A.sgst_amt,A.igst_per,A.igst_amt,A.net_amt,B.service_charges,B.gst_per,B.service_charges_cgst_amt,B.service_charges_sgst_amt,B.service_charges_igst_amt from ".PREFIX."tax_invoice_item_transaction A INNER JOIN ".PREFIX."tax_invoice_transaction B ON A.tit_id=B.id AND B.invoice_date BETWEEN '".$from_date."' AND '".$to_date."') as U WHERE cgst_per='".$gst_per."'");
			return $this->fetch($sql);
		}
		function getUniqueTaxInvoiceDetailsCustomerId($customer_id) {
			$sql = "select * from ".PREFIX."tax_invoice_transaction where customer_id='".$customer_id."' AND deleted_time IS NULL";
			return $this->fetch($sql);
		}
		function getUniqueTaxInvoiceNoByCustomerID($customer_id) {
			$query = "select * from ".PREFIX."tax_invoice_transaction where customer_id='".$customer_id."' AND deleted_time IS NULL";
			return $this->query($query);

		}function getUniqueItemDetailsByCustomerID($customer_id,$item_id) {
			
			$query = "select item_id,qty,unit,rate,amt,disc_per,disc_amt,after_disc_amt,customer_id from ".PREFIX."tax_invoice_transaction A INNER JOIN ".PREFIX."tax_invoice_item_transaction B ON A.id=B.tit_id AND customer_id='".$customer_id."' AND item_id='".$item_id."' LIMIT 5";
			return $this->query($query);
		}
		function itemReceivedPurchaseRateBYTaxInvoice($description,$priority){
			$query = "SELECT * FROM ".PREFIX."tax_invoice_item_transaction where ".PREFIX."tax_invoice_item_transaction.invoice_no IN ('".$description."')";
			$sql = $this->query($query);
			return $this->fetch($sql);
		
			}
		function itemSales($from_date,$to_date,$item_id){
			$query = "SELECT item_id as item_id,sum(qty) as qty,sum(after_disc_amt) as amount FROM (select A.tit_id,A.item_id,A.qty,A.after_disc_amt from ".PREFIX."tax_invoice_item_transaction A INNER JOIN ".PREFIX."tax_invoice_transaction B ON A.tit_id=B.id AND B.invoice_date BETWEEN '$from_date' AND '$to_date') as U where item_id='".$item_id."' GROUP BY item_id";
			$sql = $this->query($query);
			return $this->fetch($sql);
		}
		function itemSalesAll($from_date,$to_date){
			$query = "SELECT item_id as item_id,sum(qty) as qty,sum(after_disc_amt) as amount FROM (select A.tit_id,A.item_id,A.qty,A.after_disc_amt from ".PREFIX."tax_invoice_item_transaction A INNER JOIN ".PREFIX."tax_invoice_transaction B ON A.tit_id=B.id AND B.invoice_date BETWEEN '$from_date' AND '$to_date') as U GROUP BY item_id";
			return $this->query($query);
		}
		function itemSalesByItemId($item_id){
			$query = " SELECT item_id as item_id,sum(qty) as qty,sum(after_disc_amt) as amount FROM (select A.tit_id,A.item_id,A.qty,A.after_disc_amt from ".PREFIX."tax_invoice_item_transaction A INNER JOIN ".PREFIX."tax_invoice_transaction B ON A.tit_id=B.id) as U where item_id='".$item_id."' GROUP BY item_id";
			return $this->fetch($this->query($query));
		}
		function itemSalesByCustomerIdANDFromToDate($from_date,$to_date,$customer_id){
			$query = "SELECT customer_id as customer_id,sum(after_disc_amt) as amount ,invoice_no as bill_no,invoice_date as invoice_date,item_id as item_id,qty as qty  FROM (select A.tit_id,A.item_id,A.qty,A.after_disc_amt,B.customer_id,B.invoice_no,B.invoice_date from ".PREFIX."tax_invoice_item_transaction A INNER JOIN ".PREFIX."tax_invoice_transaction B ON A.tit_id=B.id AND B.invoice_date BETWEEN '$from_date' AND '$to_date') as U where customer_id='".$customer_id."'  GROUP BY invoice_no ORDER BY invoice_date DESC";
			return $this->query($query);
		}
		function itemSalesByFromToDate($from_date,$to_date){
			$query = "SELECT customer_id as customer_id,sum(after_disc_amt) as amount ,invoice_no as bill_no,invoice_date as invoice_date,item_id as item_id,qty as qty FROM (select A.tit_id,A.item_id,A.qty,A.after_disc_amt,B.customer_id,B.invoice_no,B.invoice_date from ".PREFIX."tax_invoice_item_transaction A INNER JOIN ".PREFIX."tax_invoice_transaction B ON A.tit_id=B.id AND B.invoice_date BETWEEN '$from_date' AND '$to_date') as U  GROUP BY invoice_no ORDER BY invoice_date ASC";
			return $this->query($query);
		}
		function itemSalesByCustomerId($customer_id){
			$query = "SELECT customer_id as customer_id,sum(after_disc_amt) as amount ,invoice_no as bill_no,invoice_date as invoice_date,item_id as item_id,qty as qty FROM (select A.tit_id,A.item_id,A.qty,A.after_disc_amt,B.customer_id,B.invoice_no,B.invoice_date from ".PREFIX."tax_invoice_item_transaction A INNER JOIN ".PREFIX."tax_invoice_transaction B ON A.tit_id=B.id) as U where customer_id='".$customer_id."'  GROUP BY invoice_no ORDER BY invoice_date DESC";
			return $this->query($query);
		}
		
		function getUniqueTaxInvoiceItemDetailsByTaxInvoiceId($tit_id) {
			$query = "select * from ".PREFIX."tax_invoice_item_transaction where tit_id='".$tit_id."'  AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniqueTaxInvoiceItemDetailsByTaxInvoiceId123($tit_id,$item_id) {
			$query = "select * from ".PREFIX."tax_invoice_item_transaction where tit_id='".$tit_id."' AND item_id='".$item_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniqueTaxInvoiceItemDetails($tit_id) {
			$query = "select * from ".PREFIX."tax_invoice_item_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
			$sql = $this->query($query);
			return $this->fetch($sql);
		}
		function getUniqueTaxInvoiceItemDetailsByTaxInvoiceId11111($tit_id) {
			$query = "select * from ".PREFIX."tax_invoice_item_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL ORDER BY item_id";
			return $this->query($query);
		}
		function getUniqueTaxInvoiceItemDetailsByTaxInvoiceId1($tit_id,$item_id) {
			$query = "select * from ".PREFIX."tax_invoice_item_transaction where tit_id='".$tit_id."' AND item_id='".$item_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniqueTaxInvoiceItemDetailsByTaxInvoiceId2($item_id) {
			$query = "select * from ".PREFIX."tax_invoice_item_transaction where item_id='".$item_id."' AND deleted_time IS NULL ORDER BY tit_id ASC";
			return $this->query($query);
		}
		function getUniqueInvoiceItemDetailsByInvoiceId($tit_id) {
			$query = "select * from ".PREFIX."tax_invoice_item_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniqueTaxInvoiceGSTDetailsByTaxInvoiceId($tit_id) {
			$query = "select * from ".PREFIX."tax_invoice_item_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
			$sql = $this->query($query);
			return $this->fetch($sql);
		 }
		 function getItemDescription($sheet_type,$length,$width,$thickness,$pcs){
			 if($sheet_type = '0'){
				$qty = '0';
			 }else{
				If($sheet_type = '1') {
					$qty = $length * $width * $thickness * $pcs * 7.86;
					}ElseIf($sheet_type = '2') {
						$qty = $length * $width * $thickness * $pcs * 7.85;
					}ElseIf($sheet_type = '3') {
						$qty = $txtLength * $width * $thickness * $pcs * 7.85;
					}ElseIf($sheet_type = '4') {
						$qty = $txtLength * $width * $thickness * $pcs * 2.61;
					}ElseIf($sheet_type = '5') {
						$qty = $txtLength * $width / 92903 * $pcs;
					}
			 }
			
			return $qty;
		}
		function addTaxInvoice($data,$user_by) {
		$invoice_no = $this-> getUniuqeTaxInvoiceNo();
		$int_invoice_on = $this-> getUniuqeTaxIntInvoiceNo();
		$invoice_date = $this->escape_string($this->strip_all($data['invoice_date']));
		$due_date = $this->escape_string($this->strip_all($data['due_date']));
		$customer_id = $this->escape_string($this->strip_all($data['customer_id']));
		$gst_applicable = $this->escape_string($this->strip_all($data['gst_applicable']));
		$gst_per = $this->escape_string($this->strip_all($data['gst_per']));
		$total_amt = $this->escape_string($this->strip_all($data['total_amt']));
		$total_disc_amt = $this->escape_string($this->strip_all($data['total_disc_amt']));
		$total_after_disc_amt = $this->escape_string($this->strip_all($data['total_after_disc_amt']));
		$total_cgst_amt = $this->escape_string($this->strip_all($data['total_cgst_amt']));
		$total_sgst_amt = $this->escape_string($this->strip_all($data['total_sgst_amt']));
		$total_igst_amt = $this->escape_string($this->strip_all($data['total_igst_amt']));
		$total_net_amt = $this->escape_string($this->strip_all($data['total_net_amt']));
		$service_charges = $this->escape_string($this->strip_all($data['service_charges']));
		$final_amt = $this->escape_string($this->strip_all($data['final_amt']));
		$terms_of_payment = $this->escape_string($this->strip_all($data['terms_of_payment']));
		$delivery_challen_no = $this->escape_string($this->strip_all($data['delivery_challen_no']));
		$delivery_challen_date = $this->escape_string($this->strip_all($data['delivery_challen_date']));
		$buyer_order_on = $this->escape_string($this->strip_all($data['buyer_order_on']));
		$buyer_order_date = $this->escape_string($this->strip_all($data['buyer_order_date']));
		$vehicle_no = $this->escape_string($this->strip_all($data['vehicle_no']));
		$e_way_bill_no = $this->escape_string($this->strip_all($data['e_way_bill_no']));
		$lr_no = $this->escape_string($this->strip_all($data['lr_no']));
		$lr_date = $this->escape_string($this->strip_all($data['lr_date']));
		$service_charges_cgst_amt = $this->escape_string($this->strip_all($data['service_charges_cgst_amt']));
		$service_charges_sgst_amt = $this->escape_string($this->strip_all($data['service_charges_sgst_amt']));
		$service_charges_igst_amt = $this->escape_string($this->strip_all($data['service_charges_igst_amt']));
		$notes = $this->escape_string($this->strip_all($data['notes']));
		$ship_to = $this->escape_string($this->strip_all($data['ship_to']));
		$address = $this->escape_string($this->strip_all($data['address']));
		// $final_disc_per = $this->escape_string($this->strip_all($data['final_disc_per']));
		// $final_disc_amt = $this->escape_string($this->strip_all($data['final_disc_amt']));
		// $after_disc_final_amt = $this->escape_string($this->strip_all($data['after_disc_final_amt']));
	
		
		$insertIntoTable = $this->query("INSERT INTO ".PREFIX."tax_invoice_transaction (int_invoice_on,invoice_no, invoice_date, due_date, customer_id, gst_applicable, gst_per, total_amt, total_disc_amt, total_after_disc_amt, total_cgst_amt, total_sgst_amt, total_igst_amt, total_net_amt, service_charges, final_amt,terms_of_payment,delivery_challen_no,delivery_challen_date,buyer_order_on,buyer_order_date,vehicle_no,e_way_bill_no,lr_no,lr_date,service_charges_cgst_amt,service_charges_sgst_amt,service_charges_igst_amt,notes,created_by,created_time,ship_to,address) values ('".$int_invoice_on."','".$invoice_no."' ,'".$invoice_date."' ,'".$due_date."' ,'".$customer_id."' ,'".$gst_applicable."' ,'".$gst_per."' ,'".$total_amt."' ,'".$total_disc_amt."' ,'".$total_after_disc_amt."' ,'".$total_cgst_amt."' ,'".$total_sgst_amt."' ,'".$total_igst_amt."' ,'".$total_net_amt."' ,'".$service_charges."' ,'".$final_amt."','".$terms_of_payment."','".$delivery_challen_no."','".$delivery_challen_date."','".$buyer_order_on."','".$buyer_order_date."','".$vehicle_no."','".$e_way_bill_no."','".$lr_no."','".$lr_date."','".$service_charges_cgst_amt."','".$service_charges_sgst_amt."','".$service_charges_igst_amt."','".$notes."','".$user_by."','".CURRENTMILIS."','".$ship_to."','".$address."')"); 
		$last_id = $this->last_insert_id();
		if(isset($data['item_id'])) {
				$item_id = $data['item_id'];
				$rate = $data['rate'];
				$qty = $data['qty'];
				$unit = $data['unit'];
				$amt = $data['amt'];
				$disc_per = $data['disc_per'];
				$disc_amt = $data['disc_amt'];
				$after_disc_amt = $data['after_disc_amt'];
				$cgst_per = $data['cgst_per'];
				$cgst_amt = $data['cgst_amt'];
				$sgst_per = $data['sgst_per'];
				$sgst_amt = $data['sgst_amt'];
				$igst_per = $data['igst_per'];
				$igst_amt = $data['igst_amt'];
				$net_amt = $data['net_amt'];
				$sheet_type = $data['sheet_type'];
				$length = $data['length'];
				$width = $data['width'];
				$thickness = $data['thickness'];
				$pcs = $data['pcs'];
				$description = $data['description'];
				

				for ($i = 0; $i < count($data['item_id']); $i++) {

					$item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
					$unit_value = $this->escape_string($this->strip_all($unit[$i])); 
					
					// $itemDetails = $this-> getUniqueItemMasterById($item_id_value);
					// if(!$itemDetails){
					// 	 $this->query("INSERT INTO ".PREFIX."item_master (item_name, active, stock_unit, created_by,created_time) values ('".$item_id_value."', '1', '".$unit_value."','".$user_by."','".CURRENTMILIS."')");
					// 	 $item_id_value = $this->last_insert_id();
					// }

					$rate_value = $this->escape_string($this->strip_all($rate[$i])); 
					$qty_value = $this->escape_string($this->strip_all($qty[$i])); 
					$check = $this->fetch($this->query("select * from ".PREFIX."customer_rate_master where customer_id='".$customer_id."' and item_id='".$item_id_value."' "));

					if ($check) {
						$move = $this->query("INSERT INTO ".PREFIX."customer_rate_master_history SELECT * FROM ".PREFIX."customer_rate_master WHERE customer_id = '".$customer_id."' and item_id='".$item_id_value."'");
						$delete = $this->query("DELETE FROM ".PREFIX."customer_rate_master WHERE customer_id = '".$customer_id."' and item_id='".$item_id_value."'");
						
						$query = $this->query("insert into ".PREFIX."customer_rate_master (customer_id, item_id, rate,challan_rate, created_by, created_time) values ('".$customer_id."', '".$item_id_value."', '".$rate_value."', '".$check['challan_rate']."', '".$user_by."', '".CURRENTMILIS."')");
					} else {
						
						$query = $this->query("insert into ".PREFIX."customer_rate_master (customer_id, item_id, rate,challan_rate, created_by, created_time) values ('".$customer_id."', '".$item_id_value."', '".$rate_value."', '0', '".$user_by."', '".CURRENTMILIS."')");
					}
					$this-> addStock($item_id_value,'','',$qty_value,'','',$user_by);

					$amt_value = $this->escape_string($this->strip_all($amt[$i])); 
					$disc_per_value = $this->escape_string($this->strip_all($disc_per[$i])); 
					$disc_amt_value = $this->escape_string($this->strip_all($disc_amt[$i])); 
					$after_disc_amt_value = $this->escape_string($this->strip_all($after_disc_amt[$i])); 
					$cgst_per_value = $this->escape_string($this->strip_all($cgst_per[$i])); 
					$cgst_amt_value = $this->escape_string($this->strip_all($cgst_amt[$i]));
					$sgst_per_value = $this->escape_string($this->strip_all($sgst_per[$i]));
					$sgst_amt_value = $this->escape_string($this->strip_all($sgst_amt[$i]));
					$igst_per_value = $this->escape_string($this->strip_all($igst_per[$i]));
					$igst_amt_value = $this->escape_string($this->strip_all($igst_amt[$i]));
					$net_amt_value = $this->escape_string($this->strip_all($net_amt[$i])); 
					$sheet_type_value = $this->escape_string($this->strip_all($sheet_type[$i]));
					$length_value = $this->escape_string($this->strip_all($length[$i]));
					$description_value = $this->escape_string($this->strip_all($description[$i]));
					$width_value = $this->escape_string($this->strip_all($width[$i]));
					$thickness_value = $this->escape_string($this->strip_all($thickness[$i]));
					$pcs_value = $this->escape_string($this->strip_all($pcs[$i])); 
					
				   $insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."tax_invoice_item_transaction(tit_id, invoice_no, item_id, rate, qty, unit, amt, disc_per, disc_amt, after_disc_amt, cgst_per, cgst_amt, sgst_per, sgst_amt, igst_per,igst_amt,net_amt,sheet_type,length,description,width,thickness,pcs) values ('".$last_id."' ,'".$invoice_no."' ,'".$item_id_value."' ,'".$rate_value."' ,'".$qty_value."' ,'".$unit_value."' ,'".$amt_value."' ,'".$disc_per_value."' ,'".$disc_amt_value."','".$after_disc_amt_value."' ,'".$cgst_per_value."' ,'".$cgst_amt_value."' ,'".$sgst_per_value."' ,'".$sgst_amt_value."' ,'".$igst_per_value."' ,'".$igst_amt_value."','".$net_amt_value."','".$sheet_type_value."','".$length_value."','".$description_value."','".$width_value."','".$thickness_value."','".$pcs_value."')");
					
				}

		}
		return $last_id;
		}
		function updateTaxInvoice($data,$user_by) {

			
			$last_id = $this->escape_string($this->strip_all($data['id']));
			$invoice_no = $this->escape_string($this->strip_all($data['invoice_no']));
			$invoice_date = $this->escape_string($this->strip_all($data['invoice_date']));
			$due_date = $this->escape_string($this->strip_all($data['due_date']));
			$customer_id = $this->escape_string($this->strip_all($data['customer_id']));
			$gst_applicable = $this->escape_string($this->strip_all($data['gst_applicable']));
			$gst_per = $this->escape_string($this->strip_all($data['gst_per']));
			$total_amt = $this->escape_string($this->strip_all($data['total_amt']));
			$total_disc_amt = $this->escape_string($this->strip_all($data['total_disc_amt']));
			$total_after_disc_amt = $this->escape_string($this->strip_all($data['total_after_disc_amt']));
			$total_cgst_amt = $this->escape_string($this->strip_all($data['total_cgst_amt']));
			$total_sgst_amt = $this->escape_string($this->strip_all($data['total_sgst_amt']));
			$total_igst_amt = $this->escape_string($this->strip_all($data['total_igst_amt']));
			$total_net_amt = $this->escape_string($this->strip_all($data['total_net_amt']));
			$service_charges = $this->escape_string($this->strip_all($data['service_charges']));
			$final_amt = $this->escape_string($this->strip_all($data['final_amt']));
			$terms_of_payment = $this->escape_string($this->strip_all($data['terms_of_payment']));
			$delivery_challen_no = $this->escape_string($this->strip_all($data['delivery_challen_no']));
			$delivery_challen_date = $this->escape_string($this->strip_all($data['delivery_challen_date']));
			$buyer_order_on = $this->escape_string($this->strip_all($data['buyer_order_on']));
			$buyer_order_date = $this->escape_string($this->strip_all($data['buyer_order_date']));
			$vehicle_no = $this->escape_string($this->strip_all($data['vehicle_no']));
			$e_way_bill_no = $this->escape_string($this->strip_all($data['e_way_bill_no']));
			$lr_no = $this->escape_string($this->strip_all($data['lr_no']));
			$lr_date = $this->escape_string($this->strip_all($data['lr_date']));
			$service_charges_cgst_amt = $this->escape_string($this->strip_all($data['service_charges_cgst_amt']));
			$service_charges_sgst_amt = $this->escape_string($this->strip_all($data['service_charges_sgst_amt']));
			$service_charges_igst_amt = $this->escape_string($this->strip_all($data['service_charges_igst_amt']));
			$notes = $this->escape_string($this->strip_all($data['notes']));
			$ship_to = $this->escape_string($this->strip_all($data['ship_to']));
			$address = $this->escape_string($this->strip_all($data['address']));
			// $final_disc_per = $this->escape_string($this->strip_all($data['final_disc_per']));
			// $final_disc_amt = $this->escape_string($this->strip_all($data['final_disc_amt']));
			// $after_disc_final_amt = $this->escape_string($this->strip_all($data['after_disc_final_amt']));
	
			$updateIntoTable = $this->query("UPDATE  ".PREFIX."tax_invoice_transaction set invoice_date='".$invoice_date."', due_date='".$due_date."', customer_id='".$customer_id."', gst_applicable='".$gst_applicable."',  total_amt='".$total_amt."',  gst_per='".$gst_per."', total_disc_amt='".$total_disc_amt."', total_after_disc_amt='".$total_after_disc_amt."', total_cgst_amt='".$total_cgst_amt."', total_sgst_amt='".$total_sgst_amt."', total_igst_amt='".$total_igst_amt."',total_net_amt='".$total_net_amt."',service_charges='".$service_charges."',final_amt='".$final_amt."',terms_of_payment='".$terms_of_payment."',delivery_challen_no='".$delivery_challen_no."',delivery_challen_date='".$delivery_challen_date."',buyer_order_on='".$buyer_order_on."',buyer_order_date='".$buyer_order_date."',vehicle_no='".$vehicle_no."',e_way_bill_no='".$e_way_bill_no."',lr_no='".$lr_no."',lr_date='".$lr_date."',service_charges_cgst_amt='".$service_charges_cgst_amt."',service_charges_sgst_amt='".$service_charges_sgst_amt."',service_charges_igst_amt='".$service_charges_igst_amt."',notes='".$notes."',ship_to='".$ship_to."',address='".$address."',updated_by='".$user_by."', updated_time='".CURRENTMILIS."' WHERE id='".$last_id."' ");
			$deletetable = $this->query("DELETE FROM ".PREFIX."tax_invoice_item_transaction where tit_id='".$last_id."' ");
			if(isset($data['item_id'])) {
					$item_id = $data['item_id'];
					$rate = $data['rate'];
					$qty = $data['qty'];
					$unit = $data['unit'];
					$amt = $data['amt'];
					$disc_per = $data['disc_per'];
					$disc_amt = $data['disc_amt'];
					$after_disc_amt = $data['after_disc_amt'];
					$cgst_per = $data['cgst_per'];
					$cgst_amt = $data['cgst_amt'];
					$sgst_per = $data['sgst_per'];
					$sgst_amt = $data['sgst_amt'];
					$igst_per = $data['igst_per'];
					$igst_amt = $data['igst_amt'];
					$net_amt = $data['net_amt'];
					$sheet_type = $data['sheet_type'];
					$length = $data['length'];
					$width = $data['width'];
					$thickness = $data['thickness'];
					$pcs = $data['pcs'];
					$description = $data['description'];
					
	
					for ($i = 0; $i < count($data['item_id']); $i++) {

						// $item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
						// $unit_value = $this->escape_string($this->strip_all($unit[$i])); 
			
						//$itemDetails = $this-> getUniqueItemMasterById($item_id_value);
						// if(!$itemDetails){
						// 	 $this->query("INSERT INTO ".PREFIX."item_master (item_name, active, stock_unit, created_by,created_time) values ('".$item_id_value."', '1', '".$unit_value."','".$user_by."','".CURRENTMILIS."')");
						// 	 $item_id_value = $this->last_insert_id();
							
						// }
	
						$item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
						$rate_value = $this->escape_string($this->strip_all($rate[$i])); 
						
						$check = $this->fetch($this->query("select * from ".PREFIX."customer_rate_master where customer_id='".$customer_id."' and item_id='".$item_id_value."' "));

						if ($check) {

							
							 $move = $this->query("INSERT INTO ".PREFIX."customer_rate_master_history SELECT * FROM ".PREFIX."customer_rate_master WHERE customer_id = '".$customer_id."' and item_id='".$item_id_value."'");
							 $delete = $this->query("DELETE FROM ".PREFIX."customer_rate_master WHERE customer_id = '".$customer_id."' and item_id='".$item_id_value."'");
							
							 $query = $this->query("insert into ".PREFIX."customer_rate_master (customer_id, item_id, rate,challan_rate, created_by, created_time) values ('".$customer_id."', '".$item_id_value."', '".$rate_value."', '".$check['challan_rate']."', '".$user_by."', '".CURRENTMILIS."')");
						} else {
							
							$query = $this->query("insert into ".PREFIX."customer_rate_master (customer_id, item_id, rate,challan_rate, created_by, created_time) values ('".$customer_id."', '".$item_id_value."', '".$rate_value."', '0', '".$user_by."', '".CURRENTMILIS."')");
						}

						$qty_value = $this->escape_string($this->strip_all($qty[$i])); 
						$unit_value = $this->escape_string($this->strip_all($unit[$i])); 
						$amt_value = $this->escape_string($this->strip_all($amt[$i])); 
						$disc_per_value = $this->escape_string($this->strip_all($disc_per[$i])); 
						$disc_amt_value = $this->escape_string($this->strip_all($disc_amt[$i])); 
						$after_disc_amt_value = $this->escape_string($this->strip_all($after_disc_amt[$i])); 
						$cgst_per_value = $this->escape_string($this->strip_all($cgst_per[$i])); 
						$cgst_amt_value = $this->escape_string($this->strip_all($cgst_amt[$i]));
						$sgst_per_value = $this->escape_string($this->strip_all($sgst_per[$i]));
						$sgst_amt_value = $this->escape_string($this->strip_all($sgst_amt[$i]));
						$igst_per_value = $this->escape_string($this->strip_all($igst_per[$i]));
						$igst_amt_value = $this->escape_string($this->strip_all($igst_amt[$i]));
						$net_amt_value = $this->escape_string($this->strip_all($net_amt[$i]));
						$sheet_type_value = $this->escape_string($this->strip_all($sheet_type[$i]));
						$length_value = $this->escape_string($this->strip_all($length[$i]));
						$description_value = $this->escape_string($this->strip_all($description[$i]));
						$width_value = $this->escape_string($this->strip_all($width[$i]));
						$thickness_value = $this->escape_string($this->strip_all($thickness[$i]));
						$pcs_value = $this->escape_string($this->strip_all($pcs[$i]));  
	
						$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."tax_invoice_item_transaction(tit_id, invoice_no, item_id, rate, qty, unit, amt, disc_per, disc_amt, after_disc_amt, cgst_per, cgst_amt, sgst_per, sgst_amt, igst_per,igst_amt,net_amt,sheet_type,length,description,width,thickness,pcs) values ('".$last_id."' ,'".$invoice_no."' ,'".$item_id_value."' ,'".$rate_value."' ,'".$qty_value."' ,'".$unit_value."' ,'".$amt_value."' ,'".$disc_per_value."' ,'".$disc_amt_value."','".$after_disc_amt_value."' ,'".$cgst_per_value."' ,'".$cgst_amt_value."' ,'".$sgst_per_value."' ,'".$sgst_amt_value."' ,'".$igst_per_value."' ,'".$igst_amt_value."','".$net_amt_value."','".$sheet_type_value."','".$length_value."','".$description_value."','".$width_value."','".$thickness_value."','".$pcs_value."')");
	
					}
	
			}
			return $last_id;
			}
	/* ============================= QUOTATION INVOICE TRANSACTION STARTS===================================*/
		function getUniuqeQuotationNo(){
			$int_invoice_on=$this->fetch($this->query("select MAX(int_invoice_on) x from ".PREFIX."quotation_transaction ORDER BY int_invoice_on DESC"))['x'];
			
			if(!$int_invoice_on){
					$int_invoice_on='00001';
				}else{
					$int_invoice_on++;
					$int_invoice_on=sprintf('%05s', $int_invoice_on);
				}

				if (date('m') <= 4) {
					$financial_year = (date('y')-1) . '-' . date('y');
				} else {
					$financial_year = date('y') . '-' . (date('y') + 1);
				}
			return 'QUO/'.$financial_year.'/'.$int_invoice_on;
		}

		function getUniuqeQuotationIntNo(){
			$int_invoice_on=$this->fetch($this->query("select MAX(int_invoice_on) x from ".PREFIX."quotation_transaction ORDER BY int_invoice_on DESC"))['x'];
			
				$int_invoice_on++;
				$int_invoice_on=sprintf('%05s', $int_invoice_on);

			return $int_invoice_on;
		}

		function getUniqueQuotationDetailsById($id) {
			$sql = $this->query("select * from ".PREFIX."quotation_transaction where id='".$id."' AND deleted_time IS NULL");
			return $this->fetch($sql);
		}

		function getUniqueQuotationItemDetailsByTaxInvoiceId($tit_id) {
			$query = "select * from ".PREFIX."quotation_item_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniqueQuotationItemDetailsByTaxInvoiceId1($tit_id,$item_id) {
			$query = "select * from ".PREFIX."quotation_item_transaction where tit_id='".$tit_id."' AND item_id='".$item_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniqueQuotationItemDetailsByTaxInvoiceId2($item_id) {
			$query = "select * from ".PREFIX."quotation_item_transaction where item_id='".$item_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniqueQuotationGSTDetailsByTaxInvoiceId($tit_id) {
			$query = "select * from ".PREFIX."quotation_item_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
			$sql = $this->query($query);
			return $this->fetch($sql);
		 }
		
		function addQuotation($data,$user_by) {
		$int_invoice_on = $this-> getUniuqeQuotationIntNo();
		$quotation_no = $this-> getUniuqeQuotationNo();
		$quotation_date = $this->escape_string($this->strip_all($data['quotation_date']));
		$customer_id = $this->escape_string($this->strip_all($data['customer_id']));
		$verbal_by = $this->escape_string($this->strip_all($data['verbal_by']));
		$subject = $this->escape_string($this->strip_all($data['subject']));
		$terms_and_condition = $this->escape_string($this->strip_all($data['terms_and_condition']));
		$total_amt = $this->escape_string($this->strip_all($data['total_amt']));
		$total_gst_amt = $this->escape_string($this->strip_all($data['total_gst_amt']));
		$total_net_amt = $this->escape_string($this->strip_all($data['total_net_amt']));
		
		// echo ("INSERT INTO ".PREFIX."quotation_transaction (int_invoice_on,quotation_no,quotation_date,customer_id, gst_per, total_amt, total_gst_amt, total_net_amt,created_by,created_time) values ('".$int_invoice_on."','".$quotation_no."','".$quotation_date."','".$customer_id."','".$gst_per."' ,'".$total_amt."' ,'".$total_gst_amt."' ,'".$total_net_amt."' ,'".$user_by."','".CURRENTMILIS."')"); 
		$insertIntoTable = $this->query("INSERT INTO ".PREFIX."quotation_transaction (int_invoice_on,quotation_no,quotation_date,customer_id,verbal_by,subject, total_amt, total_gst_amt, total_net_amt,terms_and_condition,created_by,created_time) values ('".$int_invoice_on."','".$quotation_no."','".$quotation_date."','".$customer_id."', '".$verbal_by."','".$subject."','".$total_amt."' ,'".$total_gst_amt."' ,'".$total_net_amt."','".$terms_and_condition."','".$user_by."','".CURRENTMILIS."')"); 
		$last_id = $this->last_insert_id();
		if(isset($data['item_id'])) {
				$item_id = $data['item_id'];
				$item_desc = $data['item_desc'];
				$rate = $data['rate'];
				$qty = $data['qty'];
				$unit = $data['unit'];
				$amt = $data['amt'];
				$gst_per = $data['gst_per'];
				$gst_amt = $data['gst_amt'];
				$net_amt = $data['net_amt'];
				

				for ($i = 0; $i < count($data['item_id']); $i++) {

					$item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
					$item_desc_value = $this->escape_string($this->strip_all($item_desc[$i])); 
					$unit_value = $this->escape_string($this->strip_all($unit[$i])); 
					$rate_value = $this->escape_string($this->strip_all($rate[$i])); 
					$qty_value = $this->escape_string($this->strip_all($qty[$i])); 
					$amt_value = $this->escape_string($this->strip_all($amt[$i]));
					$gst_per_value = $this->escape_string($this->strip_all($gst_per[$i]));
					$gst_amt_value = $this->escape_string($this->strip_all($gst_amt[$i]));
					$net_amt_value = $this->escape_string($this->strip_all($net_amt[$i])); 
					$check = $this->fetch($this->query("select * from ".PREFIX."customer_rate_master where customer_id='".$customer_id."' and item_id='".$item_id_value."' "));
					if ($check) {
						$move = $this->query("INSERT INTO ".PREFIX."customer_rate_master_history SELECT * FROM ".PREFIX."customer_rate_master WHERE customer_id = '".$customer_id."' and item_id='".$item_id_value."'");
						$delete = $this->query("DELETE FROM ".PREFIX."customer_rate_master WHERE customer_id = '".$customer_id."' and item_id='".$item_id_value."'");
						
						$query = $this->query("insert into ".PREFIX."customer_rate_master (customer_id, item_id, rate,challan_rate, created_by, created_time) values ('".$customer_id."', '".$item_id_value."', '".$rate_value."', '0', '".$user_by."', '".CURRENTMILIS."')");
					} else {
						
						$query = $this->query("insert into ".PREFIX."customer_rate_master (customer_id, item_id, rate,challan_rate, created_by, created_time) values ('".$customer_id."', '".$item_id_value."', '".$rate_value."', '0', '".$user_by."', '".CURRENTMILIS."')");
					}
					//echo ("INSERT INTO ".PREFIX."quotation_item_transaction(tit_id, quotation_no, item_id, item_desc, rate, qty, unit, amt,gst_amt,net_amt,gst_per) values ('".$last_id."' ,'".$quotation_no."' ,'".$item_id_value."' ,'".$item_desc_value."', '".$rate_value."' ,'".$qty_value."' ,'".$unit_value."' ,'".$amt_value."' ,'".$gst_amt_value."','".$net_amt_value."','".$gst_per_value."')");
				    $insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."quotation_item_transaction(tit_id, quotation_no, item_id, item_desc, rate, qty, unit, amt,gst_amt,net_amt,gst_per) values ('".$last_id."' ,'".$quotation_no."' ,'".$item_id_value."' ,'".$item_desc_value."', '".$rate_value."' ,'".$qty_value."' ,'".$unit_value."' ,'".$amt_value."' ,'".$gst_amt_value."','".$net_amt_value."','".$gst_per_value."')");
					
				}

		}
		return $last_id;
		}
		function updateQuotation($data,$user_by) {
			
			$last_id = $this->escape_string($this->strip_all($data['id']));
			$quotation_no = $this-> getUniuqeQuotationIntNo();
			$quotation_date = $this->escape_string($this->strip_all($data['quotation_date']));
			$customer_id = $this->escape_string($this->strip_all($data['customer_id']));
			$verbal_by = $this->escape_string($this->strip_all($data['verbal_by']));
			$subject = $this->escape_string($this->strip_all($data['subject']));
			$terms_and_condition = $this->escape_string($this->strip_all($data['terms_and_condition']));
			$total_amt = $this->escape_string($this->strip_all($data['total_amt']));
			$total_gst_amt = $this->escape_string($this->strip_all($data['total_gst_amt']));
			$total_net_amt = $this->escape_string($this->strip_all($data['total_net_amt']));

			//echo "UPDATE ".PREFIX."quotation_transaction set quotation_date='".$quotation_date."', customer_id='".$customer_id."',verbal_by='".$verbal_by."',subject='".$subject."', terms_and_condition='".$terms_and_condition."', total_amt='".$total_amt."',  gst_per='".$gst_per."', total_gst_amt='".$total_gst_amt."', total_net_amt='".$total_net_amt."',updated_by='".$user_by."', updated_time='".CURRENTMILIS."' WHERE id='".$last_id."' ";
			$updateIntoTable = $this->query("UPDATE  ".PREFIX."quotation_transaction set quotation_date='".$quotation_date."', customer_id='".$customer_id."',verbal_by='".$verbal_by."',subject='".$subject."', terms_and_condition='".$terms_and_condition."', total_amt='".$total_amt."', total_gst_amt='".$total_gst_amt."', total_net_amt='".$total_net_amt."',updated_by='".$user_by."', updated_time='".CURRENTMILIS."' WHERE id='".$last_id."' ");
			$deletetable = $this->query("DELETE FROM ".PREFIX."quotation_item_transaction where tit_id='".$last_id."' ");
			if(isset($data['item_id'])) {
				$item_id = $data['item_id'];
				$item_desc = $data['item_desc'];
				$rate = $data['rate'];
				$qty = $data['qty'];
				$unit = $data['unit'];
				$amt = $data['amt'];
				$gst_amt = $data['gst_amt'];
				$gst_per = $data['gst_per'];
				$net_amt = $data['net_amt'];
					
	
					for ($i = 0; $i < count($data['item_id']); $i++) {
	
						$item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
						$item_desc_value = $this->escape_string($this->strip_all($item_desc[$i])); 
						$unit_value = $this->escape_string($this->strip_all($unit[$i])); 
						$rate_value = $this->escape_string($this->strip_all($rate[$i])); 
						$qty_value = $this->escape_string($this->strip_all($qty[$i])); 
						$amt_value = $this->escape_string($this->strip_all($amt[$i])); 
						$gst_per_value = $this->escape_string($this->strip_all($gst_per[$i]));
						$gst_amt_value = $this->escape_string($this->strip_all($gst_amt[$i]));
						$net_amt_value = $this->escape_string($this->strip_all($net_amt[$i]));  
						
						$check = $this->fetch($this->query("select * from ".PREFIX."customer_rate_master where customer_id='".$customer_id."' and item_id='".$item_id_value."' "));
						if ($check) {
							
							$move = $this->query("INSERT INTO ".PREFIX."customer_rate_master_history SELECT * FROM ".PREFIX."customer_rate_master WHERE customer_id = '".$customer_id."' and item_id='".$item_id_value."'");
							$delete = $this->query("DELETE FROM ".PREFIX."customer_rate_master WHERE customer_id = '".$customer_id."' and item_id='".$item_id_value."'");
							
							$query = $this->query("insert into ".PREFIX."customer_rate_master (customer_id, item_id, rate,challan_rate, created_by, created_time) values ('".$customer_id."', '".$item_id_value."', '".$rate_value."', '0', '".$user_by."', '".CURRENTMILIS."')");
						} else {
							
							$query = $this->query("insert into ".PREFIX."customer_rate_master (customer_id, item_id, rate,challan_rate, created_by, created_time) values ('".$customer_id."', '".$item_id_value."', '".$rate_value."', '0', '".$user_by."', '".CURRENTMILIS."')");
						}
						//echo ("INSERT INTO ".PREFIX."quotation_item_transaction(tit_id, quotation_no, item_id, item_desc, rate, qty, unit, amt,gst_amt,net_amt) values ('".$last_id."' ,'".$quotation_no."' ,'".$item_id_value."' ,'".$item_desc_value."', '".$rate_value."' ,'".$qty_value."' ,'".$unit_value."' ,'".$amt_value."' ,'".$gst_amt_value."','".$net_amt_value."')");
						$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."quotation_item_transaction(tit_id, quotation_no, item_id, item_desc, rate, qty, unit, amt,gst_amt,net_amt,gst_per) values ('".$last_id."' ,'".$quotation_no."' ,'".$item_id_value."' ,'".$item_desc_value."', '".$rate_value."' ,'".$qty_value."' ,'".$unit_value."' ,'".$amt_value."' ,'".$gst_amt_value."','".$net_amt_value."','".$gst_per_value."')");
	
					}
	
			}
			return $last_id;
			}
/* ============================= PROFOMA INVOICE TRANSACTION STARTS===================================*/

		function getUniuqeProformaInvoiceNo(){
			$int_invoice_on=$this->fetch($this->query("select MAX(int_invoice_on) x from ".PREFIX."proforma_invoice_transaction ORDER BY int_invoice_on DESC"))['x'];
			
			if(!$int_invoice_on){
					$int_invoice_on='00001';
				}else{
					$int_invoice_on++;
					$int_invoice_on=sprintf('%05s', $int_invoice_on);
				}

				if (date('m') <= 4) {
					$financial_year = (date('y')-1) . '-' . date('y');
				} else {
					$financial_year = date('y') . '-' . (date('y') + 1);
				}
			return 'PRO/'.$financial_year.'/'.$int_invoice_on;
		}

		function getUniuqeProformaIntInvoiceNo(){
			$int_invoice_on=$this->fetch($this->query("select MAX(int_invoice_on) x from ".PREFIX."proforma_invoice_transaction ORDER BY int_invoice_on DESC"))['x'];
			
				$int_invoice_on++;
				$int_invoice_on=sprintf('%05s', $int_invoice_on);

			return $int_invoice_on;
		}

		function getUniqueProformaInvoiceDetailsById($id) {
			$sql = $this->query("select * from ".PREFIX."proforma_invoice_transaction where id='".$id."' AND deleted_time IS NULL");
			return $this->fetch($sql);
		}

		function getUniqueProformaInvoiceItemDetailsByTaxInvoiceId($tit_id) {
			$query = "select * from ".PREFIX."proforma_invoice_item_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniqueProformaInvoiceGSTDetailsByTaxInvoiceId($tit_id) {
			$query = "select * from ".PREFIX."proforma_invoice_item_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
			$sql = $this->query($query);
			return $this->fetch($sql);
		}

		function addProformaInvoice($data,$user_by) {
		$proforma_no = $this-> getUniuqeProformaInvoiceNo();
		$int_invoice_on = $this-> getUniuqeProformaIntInvoiceNo();
		$proforma_date = $this->escape_string($this->strip_all($data['proforma_date']));
		$due_date = $this->escape_string($this->strip_all($data['due_date']));
		$customer_id = $this->escape_string($this->strip_all($data['customer_id']));
		$gst_applicable = $this->escape_string($this->strip_all($data['gst_applicable']));
		$gst_per = $this->escape_string($this->strip_all($data['gst_per']));
		$total_amt = $this->escape_string($this->strip_all($data['total_amt']));
		$total_disc_amt = $this->escape_string($this->strip_all($data['total_disc_amt']));
		$total_after_disc_amt = $this->escape_string($this->strip_all($data['total_after_disc_amt']));
		$total_cgst_amt = $this->escape_string($this->strip_all($data['total_cgst_amt']));
		$total_sgst_amt = $this->escape_string($this->strip_all($data['total_sgst_amt']));
		$total_igst_amt = $this->escape_string($this->strip_all($data['total_igst_amt']));
		$total_net_amt = $this->escape_string($this->strip_all($data['total_net_amt']));
		$service_charges = $this->escape_string($this->strip_all($data['service_charges']));
		$final_amt = $this->escape_string($this->strip_all($data['final_amt']));
		$terms_of_payment = $this->escape_string($this->strip_all($data['terms_of_payment']));
		$delivery_challen_no = $this->escape_string($this->strip_all($data['delivery_challen_no']));
		$delivery_challen_date = $this->escape_string($this->strip_all($data['delivery_challen_date']));
		$buyer_order_on = $this->escape_string($this->strip_all($data['buyer_order_on']));
		$buyer_order_date = $this->escape_string($this->strip_all($data['buyer_order_date']));
		$vehicle_no = $this->escape_string($this->strip_all($data['vehicle_no']));
		$e_way_bill_no = $this->escape_string($this->strip_all($data['e_way_bill_no']));
		$lr_no = $this->escape_string($this->strip_all($data['lr_no']));
		$lr_date = $this->escape_string($this->strip_all($data['lr_date']));
		$service_charges_cgst_amt = $this->escape_string($this->strip_all($data['service_charges_cgst_amt']));
		$service_charges_sgst_amt = $this->escape_string($this->strip_all($data['service_charges_sgst_amt']));
		$service_charges_igst_amt = $this->escape_string($this->strip_all($data['service_charges_igst_amt']));
		$notes = $this->escape_string($this->strip_all($data['notes']));
		$kind_atten = $this->escape_string($this->strip_all($data['kind_atten']));
		$terms_of_delivery = $this->escape_string($this->strip_all($data['terms_of_delivery']));

		$insertIntoTable = $this->query("INSERT INTO ".PREFIX."proforma_invoice_transaction (int_invoice_on,proforma_no, proforma_date, due_date, customer_id, gst_applicable, gst_per, total_amt, total_disc_amt, total_after_disc_amt, total_cgst_amt, total_sgst_amt, total_igst_amt, total_net_amt, service_charges, final_amt,terms_of_payment,delivery_challen_no,delivery_challen_date,buyer_order_on,buyer_order_date,vehicle_no,e_way_bill_no,lr_no,lr_date,service_charges_cgst_amt,service_charges_sgst_amt,service_charges_igst_amt,notes,kind_atten,terms_of_delivery,created_by,created_time) values ('".$int_invoice_on."','".$proforma_no."' ,'".$proforma_date."' ,'".$due_date."' ,'".$customer_id."' ,'".$gst_applicable."' ,'".$gst_per."' ,'".$total_amt."' ,'".$total_disc_amt."' ,'".$total_after_disc_amt."' ,'".$total_cgst_amt."' ,'".$total_sgst_amt."' ,'".$total_igst_amt."' ,'".$total_net_amt."' ,'".$service_charges."' ,'".$final_amt."','".$terms_of_payment."','".$delivery_challen_no."','".$delivery_challen_date."','".$buyer_order_on."','".$buyer_order_date."','".$vehicle_no."','".$e_way_bill_no."','".$lr_no."','".$lr_date."','".$service_charges_cgst_amt."','".$service_charges_sgst_amt."','".$service_charges_igst_amt."','".$notes."','".$kind_atten."','".$terms_of_delivery."','".$user_by."','".CURRENTMILIS."')"); 
		$last_id = $this->last_insert_id();
		if(isset($data['item_id'])) {
				$item_id = $data['item_id'];
				$rate = $data['rate'];
				$qty = $data['qty'];
				$unit = $data['unit'];
				$amt = $data['amt'];
				$disc_per = $data['disc_per'];
				$disc_amt = $data['disc_amt'];
				$after_disc_amt = $data['after_disc_amt'];
				$cgst_per = $data['cgst_per'];
				$cgst_amt = $data['cgst_amt'];
				$sgst_per = $data['sgst_per'];
				$sgst_amt = $data['sgst_amt'];
				$igst_per = $data['igst_per'];
				$igst_amt = $data['igst_amt'];
				$net_amt = $data['net_amt'];
				

				for ($i = 0; $i < count($data['item_id']); $i++) {

					$item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
					$unit_value = $this->escape_string($this->strip_all($unit[$i])); 
					
					// $itemDetails = $this-> getUniqueItemMasterById($item_id_value);
					// if(!$itemDetails){
					// 	 $this->query("INSERT INTO ".PREFIX."item_master (item_name, active, stock_unit, created_by,created_time) values ('".$item_id_value."', '1', '".$unit_value."','".$user_by."','".CURRENTMILIS."')");
					// 	 $item_id_value = $this->last_insert_id();
					// }

					$rate_value = $this->escape_string($this->strip_all($rate[$i])); 
					$qty_value = $this->escape_string($this->strip_all($qty[$i])); 
					$amt_value = $this->escape_string($this->strip_all($amt[$i])); 
					$disc_per_value = $this->escape_string($this->strip_all($disc_per[$i])); 
					$disc_amt_value = $this->escape_string($this->strip_all($disc_amt[$i])); 
					$after_disc_amt_value = $this->escape_string($this->strip_all($after_disc_amt[$i])); 
					$cgst_per_value = $this->escape_string($this->strip_all($cgst_per[$i])); 
					$cgst_amt_value = $this->escape_string($this->strip_all($cgst_amt[$i]));
					$sgst_per_value = $this->escape_string($this->strip_all($sgst_per[$i]));
					$sgst_amt_value = $this->escape_string($this->strip_all($sgst_amt[$i]));
					$igst_per_value = $this->escape_string($this->strip_all($igst_per[$i]));
					$igst_amt_value = $this->escape_string($this->strip_all($igst_amt[$i]));
					$net_amt_value = $this->escape_string($this->strip_all($net_amt[$i])); 
					
				$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."proforma_invoice_item_transaction(tit_id, invoice_no, item_id, rate, qty, unit, amt, disc_per, disc_amt, after_disc_amt, cgst_per, cgst_amt, sgst_per, sgst_amt, igst_per,igst_amt,net_amt) values ('".$last_id."' ,'".$invoice_no."' ,'".$item_id_value."' ,'".$rate_value."' ,'".$qty_value."' ,'".$unit_value."' ,'".$amt_value."' ,'".$disc_per_value."' ,'".$disc_amt_value."','".$after_disc_amt_value."' ,'".$cgst_per_value."' ,'".$cgst_amt_value."' ,'".$sgst_per_value."' ,'".$sgst_amt_value."' ,'".$igst_per_value."' ,'".$igst_amt_value."','".$net_amt_value."')");
					
				}

		}
		return $last_id;
		}
		function updateProformaInvoice($data,$user_by) {
			$last_id = $this->escape_string($this->strip_all($data['id']));
			$proforma_no = $this->escape_string($this->strip_all($data['proforma_no']));
			$proforma_date = $this->escape_string($this->strip_all($data['proforma_date']));
			$due_date = $this->escape_string($this->strip_all($data['due_date']));
			$customer_id = $this->escape_string($this->strip_all($data['customer_id']));
			$gst_applicable = $this->escape_string($this->strip_all($data['gst_applicable']));
			$gst_per = $this->escape_string($this->strip_all($data['gst_per']));
			$total_amt = $this->escape_string($this->strip_all($data['total_amt']));
			$total_disc_amt = $this->escape_string($this->strip_all($data['total_disc_amt']));
			$total_after_disc_amt = $this->escape_string($this->strip_all($data['total_after_disc_amt']));
			$total_cgst_amt = $this->escape_string($this->strip_all($data['total_cgst_amt']));
			$total_sgst_amt = $this->escape_string($this->strip_all($data['total_sgst_amt']));
			$total_igst_amt = $this->escape_string($this->strip_all($data['total_igst_amt']));
			$total_net_amt = $this->escape_string($this->strip_all($data['total_net_amt']));
			$service_charges = $this->escape_string($this->strip_all($data['service_charges']));
			$final_amt = $this->escape_string($this->strip_all($data['final_amt']));
			$terms_of_payment = $this->escape_string($this->strip_all($data['terms_of_payment']));
			$delivery_challen_no = $this->escape_string($this->strip_all($data['delivery_challen_no']));
			$delivery_challen_date = $this->escape_string($this->strip_all($data['delivery_challen_date']));
			$buyer_order_on = $this->escape_string($this->strip_all($data['buyer_order_on']));
			$buyer_order_date = $this->escape_string($this->strip_all($data['buyer_order_date']));
			$vehicle_no = $this->escape_string($this->strip_all($data['vehicle_no']));
			$e_way_bill_no = $this->escape_string($this->strip_all($data['e_way_bill_no']));
			$lr_no = $this->escape_string($this->strip_all($data['lr_no']));
			$lr_date = $this->escape_string($this->strip_all($data['lr_date']));
			$service_charges_cgst_amt = $this->escape_string($this->strip_all($data['service_charges_cgst_amt']));
			$service_charges_sgst_amt = $this->escape_string($this->strip_all($data['service_charges_sgst_amt']));
			$service_charges_igst_amt = $this->escape_string($this->strip_all($data['service_charges_igst_amt']));
			$notes = $this->escape_string($this->strip_all($data['notes']));
			$kind_atten = $this->escape_string($this->strip_all($data['kind_atten']));
			$terms_of_delivery = $this->escape_string($this->strip_all($data['terms_of_delivery']));
			
		//	echo ("UPDATE  ".PREFIX."tax_invoice_transaction set invoice_date='".$invoice_date."', due_date='".$due_date."', customer_id='".$customer_id."', gst_applicable='".$gst_applicable."',  total_amt='".$total_amt."',  gst_per='".$gst_per."', total_disc_amt='".$total_disc_amt."', total_after_disc_amt='".$total_after_disc_amt."', total_cgst_amt='".$total_cgst_amt."', total_sgst_amt='".$total_sgst_amt."', total_igst_amt='".$total_igst_amt."',total_net_amt='".$total_net_amt."',service_charges='".$service_charges."',final_amt='".$final_amt."',terms_of_payment='".$terms_of_payment."',delivery_challen_no='".$delivery_challen_no."',delivery_challen_date='".$delivery_challen_date."',buyer_order_on='".$buyer_order_on."',buyer_order_date='".$buyer_order_date."',vehicle_no='".$vehicle_no."',e_way_bill_no='".$e_way_bill_no."',lr_no='".$lr_no."',lr_date='".$lr_date."',service_charges_cgst_amt='".$service_charges_cgst_amt."',service_charges_sgst_amt='".$service_charges_sgst_amt."',service_charges_igst_amt='".$service_charges_igst_amt."',updated_by='".$user_by."', updated_time='".CURRENTMILIS."' WHERE id='".$last_id."' ");
			$updateIntoTable = $this->query("UPDATE  ".PREFIX."proforma_invoice_transaction set proforma_date='".$proforma_date."', due_date='".$due_date."', customer_id='".$customer_id."', gst_applicable='".$gst_applicable."',  total_amt='".$total_amt."',  gst_per='".$gst_per."', total_disc_amt='".$total_disc_amt."', total_after_disc_amt='".$total_after_disc_amt."', total_cgst_amt='".$total_cgst_amt."', total_sgst_amt='".$total_sgst_amt."', total_igst_amt='".$total_igst_amt."',total_net_amt='".$total_net_amt."',service_charges='".$service_charges."',final_amt='".$final_amt."',terms_of_payment='".$terms_of_payment."',delivery_challen_no='".$delivery_challen_no."',delivery_challen_date='".$delivery_challen_date."',buyer_order_on='".$buyer_order_on."',buyer_order_date='".$buyer_order_date."',vehicle_no='".$vehicle_no."',e_way_bill_no='".$e_way_bill_no."',lr_no='".$lr_no."',lr_date='".$lr_date."',service_charges_cgst_amt='".$service_charges_cgst_amt."',service_charges_sgst_amt='".$service_charges_sgst_amt."',service_charges_igst_amt='".$service_charges_igst_amt."',notes='".$notes."',kind_atten='".$kind_atten."',terms_of_delivery='".$terms_of_delivery."',updated_by='".$user_by."', updated_time='".CURRENTMILIS."' WHERE id='".$last_id."' ");
			$deletetable = $this->query("DELETE FROM ".PREFIX."proforma_invoice_item_transaction where tit_id='".$last_id."' ");
			if(isset($data['item_id'])) {
					$item_id = $data['item_id'];
					$rate = $data['rate'];
					$qty = $data['qty'];
					$unit = $data['unit'];
					$amt = $data['amt'];
					$disc_per = $data['disc_per'];
					$disc_amt = $data['disc_amt'];
					$after_disc_amt = $data['after_disc_amt'];
					$cgst_per = $data['cgst_per'];
					$cgst_amt = $data['cgst_amt'];
					$sgst_per = $data['sgst_per'];
					$sgst_amt = $data['sgst_amt'];
					$igst_per = $data['igst_per'];
					$igst_amt = $data['igst_amt'];
					$net_amt = $data['net_amt'];
					

					for ($i = 0; $i < count($data['item_id']); $i++) {

				// $item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
				// $unit_value = $this->escape_string($this->strip_all($unit[$i])); 
	
				//$itemDetails = $this-> getUniqueItemMasterById($item_id_value);
				// if(!$itemDetails){
				// 	 $this->query("INSERT INTO ".PREFIX."item_master (item_name, active, stock_unit, created_by,created_time) values ('".$item_id_value."', '1', '".$unit_value."','".$user_by."','".CURRENTMILIS."')");
				// 	 $item_id_value = $this->last_insert_id();
					
				// }

					$item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
					$rate_value = $this->escape_string($this->strip_all($rate[$i])); 
					$qty_value = $this->escape_string($this->strip_all($qty[$i])); 
					$unit_value = $this->escape_string($this->strip_all($unit[$i])); 
					$amt_value = $this->escape_string($this->strip_all($amt[$i])); 
					$disc_per_value = $this->escape_string($this->strip_all($disc_per[$i])); 
					$disc_amt_value = $this->escape_string($this->strip_all($disc_amt[$i])); 
					$after_disc_amt_value = $this->escape_string($this->strip_all($after_disc_amt[$i])); 
					$cgst_per_value = $this->escape_string($this->strip_all($cgst_per[$i])); 
					$cgst_amt_value = $this->escape_string($this->strip_all($cgst_amt[$i]));
					$sgst_per_value = $this->escape_string($this->strip_all($sgst_per[$i]));
					$sgst_amt_value = $this->escape_string($this->strip_all($sgst_amt[$i]));
					$igst_per_value = $this->escape_string($this->strip_all($igst_per[$i]));
					$igst_amt_value = $this->escape_string($this->strip_all($igst_amt[$i]));
					$net_amt_value = $this->escape_string($this->strip_all($net_amt[$i])); 

					$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."proforma_invoice_item_transaction(tit_id, invoice_no, item_id, rate, qty, unit, amt, disc_per, disc_amt, after_disc_amt, cgst_per, cgst_amt, sgst_per, sgst_amt, igst_per,igst_amt,net_amt) values ('".$last_id."' ,'".$invoice_no."' ,'".$item_id_value."' ,'".$rate_value."' ,'".$qty_value."' ,'".$unit_value."' ,'".$amt_value."' ,'".$disc_per_value."' ,'".$disc_amt_value."','".$after_disc_amt_value."' ,'".$cgst_per_value."' ,'".$cgst_amt_value."' ,'".$sgst_per_value."' ,'".$sgst_amt_value."' ,'".$igst_per_value."' ,'".$igst_amt_value."','".$net_amt_value."')"); 

				}

		}
		return $last_id;
		}

		/* ============================= Purchase Order TRANSACTION STARTS===================================*/
		function getUniuqePurchaseOrderNo(){
			$int_invoice_on=$this->fetch($this->query("select MAX(int_invoice_on) x from ".PREFIX."purchase_order_transaction ORDER BY int_invoice_on DESC"))['x'];
			
			if(!$int_invoice_on){
					$int_invoice_on='00001';
				}else{
					$int_invoice_on++;
					$int_invoice_on=sprintf('%05s', $int_invoice_on);
				}

				if (date('m') <= 4) {
					$financial_year = (date('y')-1) . '-' . date('y');
				} else {
					$financial_year = date('y') . '-' . (date('y') + 1);
				}
			return 'PO/'.$financial_year.'/'.$int_invoice_on;
		}

		function getUniuqePurchaseIntInvoiceNo(){
			$int_invoice_on=$this->fetch($this->query("select MAX(int_invoice_on) x from ".PREFIX."purchase_order_transaction ORDER BY int_invoice_on DESC"))['x'];
			
				$int_invoice_on++;
				$int_invoice_on=sprintf('%05s', $int_invoice_on);

			return $int_invoice_on;
		}

		function getUniquePurchaseOrderDetailsById($id) {
			$sql = $this->query("select * from ".PREFIX."purchase_order_transaction where id='".$id."' AND deleted_time IS NULL");
			return $this->fetch($sql);
		}

		function gstReport($from_date,$to_date,$gst_per) {
			$sql = $this->query("SELECT cgst_per,SUM(cgst_amt) as cgst_total ,SUM(sgst_amt) as sgst_total,SUM(igst_amt) as igst_total,SUM(net_amt) as total_amount FROM  (select  A.tit_id,A.cgst_per,A.cgst_amt,A.sgst_per,A.sgst_amt,A.igst_per,A.igst_amt,A.net_amt,B.service_charges,B.gst_per,B.service_charges_cgst_amt,B.service_charges_sgst_amt,B.service_charges_igst_amt from ".PREFIX."purchase_bill_item_transaction A INNER JOIN ".PREFIX."purchase_bill_transaction B ON A.tit_id=B.id AND B.supplier_bill_date BETWEEN '".$from_date."' AND '".$to_date."') as U WHERE cgst_per='".$gst_per."'");
			return $this->fetch($sql);
		}

		function getUniquePurchaseOrderItemDetailsByTaxInvoiceId($tit_id) {
			$query = "select * from ".PREFIX."purchase_order_item_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniquePurchaseOrderItemDetailsByTaxInvoiceId1($tit_id,$item_id) {
			$query = "select * from ".PREFIX."purchase_order_item_transaction where item_id='".$item_id."' AND tit_id='".$tit_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniquePurchaseOrderItemDetailsByTaxInvoiceId2($item_id) {
			$query = "select * from ".PREFIX."purchase_order_item_transaction where item_id='".$item_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniquePurchaseOrderGSTDetailsByTaxInvoiceId($tit_id) {
			$query = "select * from ".PREFIX."purchase_order_item_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
			$sql = $this->query($query);
			return $this->fetch($sql);
		 }
		
		function addPurchaseOrder($data,$user_by) {
		$purchase_no = $this-> getUniuqePurchaseOrderNo();
		$int_invoice_on = $this-> getUniuqePurchaseIntInvoiceNo();
		$purchase_date = $this->escape_string($this->strip_all($data['purchase_date']));
		$supplier_id = $this->escape_string($this->strip_all($data['supplier_id']));
		$gst_applicable = $this->escape_string($this->strip_all($data['gst_applicable']));
		$gst_per = $this->escape_string($this->strip_all($data['gst_per']));
		$total_amt = $this->escape_string($this->strip_all($data['total_amt']));
		$total_disc_amt = $this->escape_string($this->strip_all($data['total_disc_amt']));
		$total_after_disc_amt = $this->escape_string($this->strip_all($data['total_after_disc_amt']));
		$total_cgst_amt = $this->escape_string($this->strip_all($data['total_cgst_amt']));
		$total_sgst_amt = $this->escape_string($this->strip_all($data['total_sgst_amt']));
		$total_igst_amt = $this->escape_string($this->strip_all($data['total_igst_amt']));
		$total_net_amt = $this->escape_string($this->strip_all($data['total_net_amt']));
		$final_amt = $this->escape_string($this->strip_all($data['final_amt']));
		$kind_atten = $this->escape_string($this->strip_all($data['kind_atten']));
		$supplier_ref_no = $this->escape_string($this->strip_all($data['supplier_ref_no']));
		$supplier_date = $this->escape_string($this->strip_all($data['supplier_date']));
		$terms_of_payment = $this->escape_string($this->strip_all($data['terms_of_payment']));
		$round_off = $this->escape_string($this->strip_all($data['round_off']));
		$delivery_at = $this->escape_string($this->strip_all($data['delivery_at']));
		$packing_inst = $this->escape_string($this->strip_all($data['packing_inst']));
		$transporter = $this->escape_string($this->strip_all($data['transporter']));
		$notes = $this->escape_string($this->strip_all($data['notes']));
		$other_charges = $this->escape_string($this->strip_all($data['other_charges']));
		$prices_for = $this->escape_string($this->strip_all($data['prices_for']));
	
		$insertIntoTable = $this->query("INSERT INTO ".PREFIX."purchase_order_transaction (int_invoice_on,purchase_no, purchase_date, supplier_id, gst_applicable, gst_per, total_amt, total_disc_amt, total_after_disc_amt, total_cgst_amt, total_sgst_amt, total_igst_amt, total_net_amt, terms_of_payment, final_amt,kind_atten,supplier_ref_no,supplier_date,round_off,delivery_at,packing_inst,transporter,notes,other_charges,prices_for,created_by,created_time) values ('".$int_invoice_on."','".$purchase_no."' ,'".$purchase_date."' ,'".$supplier_id."' ,'".$gst_applicable."' ,'".$gst_per."' ,'".$total_amt."' ,'".$total_disc_amt."' ,'".$total_after_disc_amt."' ,'".$total_cgst_amt."' ,'".$total_sgst_amt."' ,'".$total_igst_amt."' ,'".$total_net_amt."' ,'".$terms_of_payment."' ,'".$final_amt."','".$kind_atten."','".$supplier_ref_no."','".$supplier_date."','".$round_off."','".$delivery_at."','".$packing_inst."','".$transporter."','".$notes."','".$other_charges."','".$prices_for."','".$user_by."','".CURRENTMILIS."')"); 
		$last_id = $this->last_insert_id();
		if(isset($data['item_id'])) {
				$item_id = $data['item_id'];
				$rate = $data['rate'];
				$qty = $data['qty'];
				$unit = $data['unit'];
				$amt = $data['amt'];
				$disc_per = $data['disc_per'];
				$disc_amt = $data['disc_amt'];
				$after_disc_amt = $data['after_disc_amt'];
				$cgst_per = $data['cgst_per'];
				$cgst_amt = $data['cgst_amt'];
				$sgst_per = $data['sgst_per'];
				$sgst_amt = $data['sgst_amt'];
				$igst_per = $data['igst_per'];
				$igst_amt = $data['igst_amt'];
				$net_amt = $data['net_amt'];
				

				for ($i = 0; $i < count($data['item_id']); $i++) {

					$item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
					$unit_value = $this->escape_string($this->strip_all($unit[$i])); 
					
					// $itemDetails = $this-> getUniqueItemMasterById($item_id_value);
					// if(!$itemDetails){
					// 	 $this->query("INSERT INTO ".PREFIX."item_master (item_name, active, stock_unit, created_by,created_time) values ('".$item_id_value."', '1', '".$unit_value."','".$user_by."','".CURRENTMILIS."')");
					// 	 $item_id_value = $this->last_insert_id();
					// }

					$rate_value = $this->escape_string($this->strip_all($rate[$i])); 
					$qty_value = $this->escape_string($this->strip_all($qty[$i])); 
					$amt_value = $this->escape_string($this->strip_all($amt[$i])); 
					$disc_per_value = $this->escape_string($this->strip_all($disc_per[$i])); 
					$disc_amt_value = $this->escape_string($this->strip_all($disc_amt[$i])); 
					$after_disc_amt_value = $this->escape_string($this->strip_all($after_disc_amt[$i])); 
					$cgst_per_value = $this->escape_string($this->strip_all($cgst_per[$i])); 
					$cgst_amt_value = $this->escape_string($this->strip_all($cgst_amt[$i]));
					$sgst_per_value = $this->escape_string($this->strip_all($sgst_per[$i]));
					$sgst_amt_value = $this->escape_string($this->strip_all($sgst_amt[$i]));
					$igst_per_value = $this->escape_string($this->strip_all($igst_per[$i]));
					$igst_amt_value = $this->escape_string($this->strip_all($igst_amt[$i]));
					$net_amt_value = $this->escape_string($this->strip_all($net_amt[$i])); 

				   $insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."purchase_order_item_transaction(tit_id, purchase_no, item_id, rate, qty, unit, amt, disc_per, disc_amt, after_disc_amt, cgst_per, cgst_amt, sgst_per, sgst_amt, igst_per,igst_amt,net_amt) values ('".$last_id."' ,'".$purchase_no."' ,'".$item_id_value."' ,'".$rate_value."' ,'".$qty_value."' ,'".$unit_value."' ,'".$amt_value."' ,'".$disc_per_value."' ,'".$disc_amt_value."','".$after_disc_amt_value."' ,'".$cgst_per_value."' ,'".$cgst_amt_value."' ,'".$sgst_per_value."' ,'".$sgst_amt_value."' ,'".$igst_per_value."' ,'".$igst_amt_value."','".$net_amt_value."')");
					
				}

		}
		return $last_id;
		}
		function updatePurchaseOrder($data,$user_by) {
			$last_id = $this->escape_string($this->strip_all($data['id']));
			$purchase_no = $this->escape_string($this->strip_all($data['purchase_no']));
			$purchase_date = $this->escape_string($this->strip_all($data['purchase_date']));
			$supplier_id = $this->escape_string($this->strip_all($data['supplier_id']));
			$gst_applicable = $this->escape_string($this->strip_all($data['gst_applicable']));
			$gst_per = $this->escape_string($this->strip_all($data['gst_per']));
			$total_amt = $this->escape_string($this->strip_all($data['total_amt']));
			$total_disc_amt = $this->escape_string($this->strip_all($data['total_disc_amt']));
			$total_after_disc_amt = $this->escape_string($this->strip_all($data['total_after_disc_amt']));
			$total_cgst_amt = $this->escape_string($this->strip_all($data['total_cgst_amt']));
			$total_sgst_amt = $this->escape_string($this->strip_all($data['total_sgst_amt']));
			$total_igst_amt = $this->escape_string($this->strip_all($data['total_igst_amt']));
			$total_net_amt = $this->escape_string($this->strip_all($data['total_net_amt']));
			$service_charges = $this->escape_string($this->strip_all($data['service_charges']));
			$final_amt = $this->escape_string($this->strip_all($data['final_amt']));
			$kind_atten = $this->escape_string($this->strip_all($data['kind_atten']));
			$supplier_ref_no = $this->escape_string($this->strip_all($data['supplier_ref_no']));
			$supplier_date = $this->escape_string($this->strip_all($data['supplier_date']));
			$terms_of_payment = $this->escape_string($this->strip_all($data['terms_of_payment']));
			$round_off = $this->escape_string($this->strip_all($data['round_off']));
			$delivery_at = $this->escape_string($this->strip_all($data['delivery_at']));
			$packing_inst = $this->escape_string($this->strip_all($data['packing_inst']));
			$transporter = $this->escape_string($this->strip_all($data['transporter']));
			$notes = $this->escape_string($this->strip_all($data['notes']));
			$other_charges = $this->escape_string($this->strip_all($data['other_charges']));
			$prices_for = $this->escape_string($this->strip_all($data['prices_for']));
		
			$updateIntoTable = $this->query("UPDATE  ".PREFIX."purchase_order_transaction set purchase_date='".$purchase_date."', supplier_id='".$supplier_id."', gst_applicable='".$gst_applicable."',  total_amt='".$total_amt."',  gst_per='".$gst_per."', total_disc_amt='".$total_disc_amt."', total_after_disc_amt='".$total_after_disc_amt."', total_cgst_amt='".$total_cgst_amt."', total_sgst_amt='".$total_sgst_amt."', total_igst_amt='".$total_igst_amt."',total_net_amt='".$total_net_amt."',terms_of_payment='".$terms_of_payment."',final_amt='".$final_amt."',kind_atten='".$kind_atten."',supplier_ref_no='".$supplier_ref_no."',supplier_date='".$supplier_date."',round_off='".$round_off."',delivery_at='".$delivery_at."',packing_inst='".$packing_inst."',transporter='".$transporter."',notes='".$notes."',other_charges='".$other_charges."',prices_for='".$prices_for."',updated_by='".$user_by."', updated_time='".CURRENTMILIS."' WHERE id='".$last_id."' ");
			$deletetable = $this->query("DELETE FROM ".PREFIX."purchase_order_item_transaction where tit_id='".$last_id."' ");
			if(isset($data['item_id'])) {
					$item_id = $data['item_id'];
					$rate = $data['rate'];
					$qty = $data['qty'];
					$unit = $data['unit'];
					$amt = $data['amt'];
					$disc_per = $data['disc_per'];
					$disc_amt = $data['disc_amt'];
					$after_disc_amt = $data['after_disc_amt'];
					$cgst_per = $data['cgst_per'];
					$cgst_amt = $data['cgst_amt'];
					$sgst_per = $data['sgst_per'];
					$sgst_amt = $data['sgst_amt'];
					$igst_per = $data['igst_per'];
					$igst_amt = $data['igst_amt'];
					$net_amt = $data['net_amt'];
					
	
					for ($i = 0; $i < count($data['item_id']); $i++) {

						// $item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
						// $unit_value = $this->escape_string($this->strip_all($unit[$i])); 
			
						//$itemDetails = $this-> getUniqueItemMasterById($item_id_value);
						// if(!$itemDetails){
						// 	 $this->query("INSERT INTO ".PREFIX."item_master (item_name, active, stock_unit, created_by,created_time) values ('".$item_id_value."', '1', '".$unit_value."','".$user_by."','".CURRENTMILIS."')");
						// 	 $item_id_value = $this->last_insert_id();
							
						// }
	
						$item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
						$rate_value = $this->escape_string($this->strip_all($rate[$i])); 
						$qty_value = $this->escape_string($this->strip_all($qty[$i])); 
						$unit_value = $this->escape_string($this->strip_all($unit[$i])); 
						$amt_value = $this->escape_string($this->strip_all($amt[$i])); 
						$disc_per_value = $this->escape_string($this->strip_all($disc_per[$i])); 
						$disc_amt_value = $this->escape_string($this->strip_all($disc_amt[$i])); 
						$after_disc_amt_value = $this->escape_string($this->strip_all($after_disc_amt[$i])); 
						$cgst_per_value = $this->escape_string($this->strip_all($cgst_per[$i])); 
						$cgst_amt_value = $this->escape_string($this->strip_all($cgst_amt[$i]));
						$sgst_per_value = $this->escape_string($this->strip_all($sgst_per[$i]));
						$sgst_amt_value = $this->escape_string($this->strip_all($sgst_amt[$i]));
						$igst_per_value = $this->escape_string($this->strip_all($igst_per[$i]));
						$igst_amt_value = $this->escape_string($this->strip_all($igst_amt[$i]));
						$net_amt_value = $this->escape_string($this->strip_all($net_amt[$i])); 

						
							
						$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."purchase_order_item_transaction(tit_id, purchase_no, item_id, rate, qty, unit, amt, disc_per, disc_amt, after_disc_amt, cgst_per, cgst_amt, sgst_per, sgst_amt, igst_per,igst_amt,net_amt) values ('".$last_id."' ,'".$purchase_no."' ,'".$item_id_value."' ,'".$rate_value."' ,'".$qty_value."' ,'".$unit_value."' ,'".$amt_value."' ,'".$disc_per_value."' ,'".$disc_amt_value."','".$after_disc_amt_value."' ,'".$cgst_per_value."' ,'".$cgst_amt_value."' ,'".$sgst_per_value."' ,'".$sgst_amt_value."' ,'".$igst_per_value."' ,'".$igst_amt_value."','".$net_amt_value."')"); 
	
					}
	
			}
			return $last_id;
			}


		/* ============================= Purchase Bill TRANSACTION STARTS===================================*/
		function getUniuqePurchaseBillNo(){
			$int_invoice_on=$this->fetch($this->query("select MAX(int_invoice_on) x from ".PREFIX."purchase_bill_transaction ORDER BY int_invoice_on DESC"))['x'];
			
			if(!$int_invoice_on){
					$int_invoice_on='00001';
				}else{
					$int_invoice_on++;
					$int_invoice_on=sprintf('%05s', $int_invoice_on);
				}

				if (date('m') <= 4) {
					$financial_year = (date('y')-1) . '-' . date('y');
				} else {
					$financial_year = date('y') . '-' . (date('y') + 1);
				}
			return 'PB/'.$financial_year.'/'.$int_invoice_on;
		}

		function getUniuqePurchaseBillIntInvoiceNo(){
			$int_invoice_on=$this->fetch($this->query("select MAX(int_invoice_on) x from ".PREFIX."purchase_bill_transaction ORDER BY int_invoice_on DESC"))['x'];
			
				$int_invoice_on++;
				$int_invoice_on=sprintf('%05s', $int_invoice_on);

			return $int_invoice_on;
		}

		function getUniquePurchaseBillDetailsById($id) {
			$sql = $this->query("select * from ".PREFIX."purchase_bill_transaction where id='".$id."' AND deleted_time IS NULL");
			return $this->fetch($sql);
		}
		function getUniquePurchaseBillDetailsById1($id) {
			
			$sql = $this->query("select * from ".PREFIX."purchase_bill_transaction where id='".$id."' AND (purchase_no='' or ISNULL(purchase_no))  AND deleted_time IS NULL");
			return $this->fetch($sql);
		}
		function getUniquePurchaseBillDetailsBySupplierId($supplier_id) {
			$sql = $this->query("select * from ".PREFIX."purchase_bill_transaction where supplier_id='".$supplier_id."' AND deleted_time IS NULL");
			return $sql;
		}
		function getUniquePurchaseBillNoBySupplierId($supplier_id) {
			$query = "select * from ".PREFIX."purchase_bill_transaction where supplier_id='".$supplier_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniquePurchaseBillItemDetailsByTaxInvoiceId($tit_id) {
			$query = "select * from ".PREFIX."purchase_bill_item_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniquePurchaseBillItemDetailsByTaxInvoiceId1($tit_id,$item_id) {
			$query = "select * from ".PREFIX."purchase_bill_item_transaction where tit_id='".$tit_id."' AND item_id='".$item_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniquePurchaseBillItemDetailsByTaxInvoiceId2($item_id) {
			$query = "select * from ".PREFIX."purchase_bill_item_transaction where item_id='".$item_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function itemReceivedPurchaseRate($description,$priority){
			$query = "SELECT * FROM ".PREFIX."purchase_bill_item_transaction where ".PREFIX."purchase_bill_item_transaction.supplier_bill_no IN ('".$description."')";
			$sql = $this->query($query);
			return $this->fetch($sql);
		}
		function getUniquePurchaseBillGSTDetailsByTaxInvoiceId($tit_id) {
			$query = "select * from ".PREFIX."purchase_bill_item_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
			return $this->query($query);
		 }
		
		function addPurchaseBill($data,$user_by) {
		$supplier_bill_no = $this-> getUniuqePurchaseBillNo();
		$int_invoice_on = $this-> getUniuqePurchaseBillIntInvoiceNo();
		$supplier_bill_date = $this->escape_string($this->strip_all($data['supplier_bill_date']));
		$supplier_id = $this->escape_string($this->strip_all($data['supplier_id']));
		$gst_applicable = $this->escape_string($this->strip_all($data['gst_applicable']));
		$gst_per = $this->escape_string($this->strip_all($data['gst_per']));
		$total_amt = $this->escape_string($this->strip_all($data['total_amt']));
		$total_disc_amt = $this->escape_string($this->strip_all($data['total_disc_amt']));
		$total_after_disc_amt = $this->escape_string($this->strip_all($data['total_after_disc_amt']));
		$total_cgst_amt = $this->escape_string($this->strip_all($data['total_cgst_amt']));
		$total_sgst_amt = $this->escape_string($this->strip_all($data['total_sgst_amt']));
		$total_igst_amt = $this->escape_string($this->strip_all($data['total_igst_amt']));
		$total_net_amt = $this->escape_string($this->strip_all($data['total_net_amt']));
		$service_charges = $this->escape_string($this->strip_all($data['service_charges']));
		$final_amt = $this->escape_string($this->strip_all($data['final_amt']));
		$terms_of_payment = $this->escape_string($this->strip_all($data['terms_of_payment']));
		$delivery_challen_no = $this->escape_string($this->strip_all($data['delivery_challen_no']));
		$delivery_challen_date = $this->escape_string($this->strip_all($data['delivery_challen_date']));
		$purchase_no = $this->escape_string($this->strip_all($data['purchase_no']));
		$purchase_date = $this->escape_string($this->strip_all($data['purchase_date']));
		$service_charges_cgst_amt = $this->escape_string($this->strip_all($data['service_charges_cgst_amt']));
		$service_charges_sgst_amt = $this->escape_string($this->strip_all($data['service_charges_sgst_amt']));
		$service_charges_igst_amt = $this->escape_string($this->strip_all($data['service_charges_igst_amt']));
	
		//echo "INSERT INTO ".PREFIX."purchase_bill_transaction (int_invoice_on,supplier_bill_no, supplier_bill_date, supplier_id, gst_applicable, gst_per, total_amt, total_disc_amt, total_after_disc_amt, total_cgst_amt, total_sgst_amt, total_igst_amt, total_net_amt, service_charges, final_amt,terms_of_payment,delivery_challen_no,delivery_challen_date,service_charges_cgst_amt,service_charges_sgst_amt,service_charges_igst_amt,created_by,created_time,purchase_no,purchase_date) values ('".$int_invoice_on."','".$supplier_bill_no."' ,'".$supplier_bill_date."' ,'".$supplier_id."' ,'".$gst_applicable."' ,'".$gst_per."' ,'".$total_amt."' ,'".$total_disc_amt."' ,'".$total_after_disc_amt."' ,'".$total_cgst_amt."' ,'".$total_sgst_amt."' ,'".$total_igst_amt."' ,'".$total_net_amt."' ,'".$service_charges."' ,'".$final_amt."','".$terms_of_payment."','".$delivery_challen_no."','".$delivery_challen_date."','".$service_charges_cgst_amt."','".$service_charges_sgst_amt."','".$service_charges_igst_amt."','".$user_by."','".CURRENTMILIS."','".$purchase_no."','".$purchase_date."')"; 
		$insertIntoTable = $this->query("INSERT INTO ".PREFIX."purchase_bill_transaction (int_invoice_on,supplier_bill_no, supplier_bill_date, supplier_id, gst_applicable, gst_per, total_amt, total_disc_amt, total_after_disc_amt, total_cgst_amt, total_sgst_amt, total_igst_amt, total_net_amt, service_charges, final_amt,terms_of_payment,delivery_challen_no,delivery_challen_date,service_charges_cgst_amt,service_charges_sgst_amt,service_charges_igst_amt,created_by,created_time,purchase_no,purchase_date) values ('".$int_invoice_on."','".$supplier_bill_no."' ,'".$supplier_bill_date."' ,'".$supplier_id."' ,'".$gst_applicable."' ,'".$gst_per."' ,'".$total_amt."' ,'".$total_disc_amt."' ,'".$total_after_disc_amt."' ,'".$total_cgst_amt."' ,'".$total_sgst_amt."' ,'".$total_igst_amt."' ,'".$total_net_amt."' ,'".$service_charges."' ,'".$final_amt."','".$terms_of_payment."','".$delivery_challen_no."','".$delivery_challen_date."','".$service_charges_cgst_amt."','".$service_charges_sgst_amt."','".$service_charges_igst_amt."','".$user_by."','".CURRENTMILIS."','".$purchase_no."','".$purchase_date."')"); 
		$last_id = $this->last_insert_id();
		if(isset($data['item_id'])) {
				$item_id = $data['item_id'];
				$rate = $data['rate'];
				$qty = $data['qty'];
				$unit = $data['unit'];
				$amt = $data['amt'];
				$disc_per = $data['disc_per'];
				$disc_amt = $data['disc_amt'];
				$after_disc_amt = $data['after_disc_amt'];
				$cgst_per = $data['cgst_per'];
				$cgst_amt = $data['cgst_amt'];
				$sgst_per = $data['sgst_per'];
				$sgst_amt = $data['sgst_amt'];
				$igst_per = $data['igst_per'];
				$igst_amt = $data['igst_amt'];
				$net_amt = $data['net_amt'];
				

				for ($i = 0; $i < count($data['item_id']); $i++) {

					$item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
					$unit_value = $this->escape_string($this->strip_all($unit[$i])); 
					
					// $itemDetails = $this-> getUniqueItemMasterById($item_id_value);
					// if(!$itemDetails){
					// 	 $this->query("INSERT INTO ".PREFIX."item_master (item_name, active, stock_unit, created_by,created_time) values ('".$item_id_value."', '1', '".$unit_value."','".$user_by."','".CURRENTMILIS."')");
					// 	 $item_id_value = $this->last_insert_id();
					// }

					$rate_value = $this->escape_string($this->strip_all($rate[$i])); 
					$qty_value = $this->escape_string($this->strip_all($qty[$i]));
					$this-> addStock($item_id_value,'',$qty_value,'','','',$user_by); 
					$amt_value = $this->escape_string($this->strip_all($amt[$i])); 
					$disc_per_value = $this->escape_string($this->strip_all($disc_per[$i])); 
					$disc_amt_value = $this->escape_string($this->strip_all($disc_amt[$i])); 
					$after_disc_amt_value = $this->escape_string($this->strip_all($after_disc_amt[$i])); 
					$cgst_per_value = $this->escape_string($this->strip_all($cgst_per[$i])); 
					$cgst_amt_value = $this->escape_string($this->strip_all($cgst_amt[$i]));
					$sgst_per_value = $this->escape_string($this->strip_all($sgst_per[$i]));
					$sgst_amt_value = $this->escape_string($this->strip_all($sgst_amt[$i]));
					$igst_per_value = $this->escape_string($this->strip_all($igst_per[$i]));
					$igst_amt_value = $this->escape_string($this->strip_all($igst_amt[$i]));
					$net_amt_value = $this->escape_string($this->strip_all($net_amt[$i])); 
					
					$check = $this->fetch($this->query("select * from ".PREFIX."supplier_rate_master where supplier_id='".$supplier_id."' and item_id='".$item_id_value."' "));
						if ($check) {
							$move = $this->query("INSERT INTO ".PREFIX."supplier_rate_master_history SELECT * FROM ".PREFIX."supplier_rate_master WHERE supplier_id = '".$supplier_id."' and item_id='".$item_id."'");
							$delete = $this->query("DELETE FROM ".PREFIX."supplier_rate_master WHERE supplier_id = '".$supplier_id."' and item_id='".$item_id_value."'");
							
							$query = $this->query("insert into ".PREFIX."supplier_rate_master (supplier_id, item_id, rate, created_by, created_time) values ('".$supplier_id."', '".$item_id_value."', '".$rate_value."', '".$user_by."', '".CURRENTMILIS."')");
						} else {
							
							$query = $this->query("insert into ".PREFIX."supplier_rate_master (supplier_id, item_id, rate, created_by, created_time) values ('".$supplier_id."', '".$item_id_value."', '".$rate_value."', '".$user_by."', '".CURRENTMILIS."')");
						}

				   $insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."purchase_bill_item_transaction(tit_id, supplier_bill_no, item_id, rate, qty, unit, amt, disc_per, disc_amt, after_disc_amt, cgst_per, cgst_amt, sgst_per, sgst_amt, igst_per,igst_amt,net_amt) values ('".$last_id."' ,'".$supplier_bill_no."' ,'".$item_id_value."' ,'".$rate_value."' ,'".$qty_value."' ,'".$unit_value."' ,'".$amt_value."' ,'".$disc_per_value."' ,'".$disc_amt_value."','".$after_disc_amt_value."' ,'".$cgst_per_value."' ,'".$cgst_amt_value."' ,'".$sgst_per_value."' ,'".$sgst_amt_value."' ,'".$igst_per_value."' ,'".$igst_amt_value."','".$net_amt_value."')");
					
				}

		}
		return $last_id;
		}
		function updatePurchaseBill($data,$user_by) {
			$last_id = $this->escape_string($this->strip_all($data['id']));
			$supplier_bill_no = $this->escape_string($this->strip_all($data['supplier_bill_no']));
			$supplier_bill_date = $this->escape_string($this->strip_all($data['supplier_bill_date']));
			$supplier_id = $this->escape_string($this->strip_all($data['supplier_id']));
			$gst_applicable = $this->escape_string($this->strip_all($data['gst_applicable']));
			$gst_per = $this->escape_string($this->strip_all($data['gst_per']));
			$total_amt = $this->escape_string($this->strip_all($data['total_amt']));
			$total_disc_amt = $this->escape_string($this->strip_all($data['total_disc_amt']));
			$total_after_disc_amt = $this->escape_string($this->strip_all($data['total_after_disc_amt']));
			$total_cgst_amt = $this->escape_string($this->strip_all($data['total_cgst_amt']));
			$total_sgst_amt = $this->escape_string($this->strip_all($data['total_sgst_amt']));
			$total_igst_amt = $this->escape_string($this->strip_all($data['total_igst_amt']));
			$total_net_amt = $this->escape_string($this->strip_all($data['total_net_amt']));
			$service_charges = $this->escape_string($this->strip_all($data['service_charges']));
			$final_amt = $this->escape_string($this->strip_all($data['final_amt']));
			$terms_of_payment = $this->escape_string($this->strip_all($data['terms_of_payment']));
			$delivery_challen_no = $this->escape_string($this->strip_all($data['delivery_challen_no']));
			$delivery_challen_date = $this->escape_string($this->strip_all($data['delivery_challen_date']));
			$purchase_no = $this->escape_string($this->strip_all($data['purchase_no']));
			$purchase_date = $this->escape_string($this->strip_all($data['purchase_date']));
			$service_charges_cgst_amt = $this->escape_string($this->strip_all($data['service_charges_cgst_amt']));
			$service_charges_sgst_amt = $this->escape_string($this->strip_all($data['service_charges_sgst_amt']));
			$service_charges_igst_amt = $this->escape_string($this->strip_all($data['service_charges_igst_amt']));
			

			$updateIntoTable = $this->query("UPDATE  ".PREFIX."purchase_bill_transaction set supplier_bill_date='".$supplier_bill_date."', supplier_id='".$supplier_id."', gst_applicable='".$gst_applicable."',  total_amt='".$total_amt."',  gst_per='".$gst_per."', total_disc_amt='".$total_disc_amt."', total_after_disc_amt='".$total_after_disc_amt."', total_cgst_amt='".$total_cgst_amt."', total_sgst_amt='".$total_sgst_amt."', total_igst_amt='".$total_igst_amt."',total_net_amt='".$total_net_amt."',service_charges='".$service_charges."',final_amt='".$final_amt."',terms_of_payment='".$terms_of_payment."',delivery_challen_no='".$delivery_challen_no."',delivery_challen_date='".$delivery_challen_date."',service_charges_cgst_amt='".$service_charges_cgst_amt."',service_charges_sgst_amt='".$service_charges_sgst_amt."',service_charges_igst_amt='".$service_charges_igst_amt."',purchase_no='".$purchase_no."',purchase_date='".$purchase_date."',updated_by='".$user_by."', updated_time='".CURRENTMILIS."' WHERE id='".$last_id."' ");
			$deletetable = $this->query("DELETE FROM ".PREFIX."purchase_bill_item_transaction where tit_id='".$last_id."' ");
			if(isset($data['item_id'])) {
					$item_id = $data['item_id'];
					$rate = $data['rate'];
					$qty = $data['qty'];
					$unit = $data['unit'];
					$amt = $data['amt'];
					$disc_per = $data['disc_per'];
					$disc_amt = $data['disc_amt'];
					$after_disc_amt = $data['after_disc_amt'];
					$cgst_per = $data['cgst_per'];
					$cgst_amt = $data['cgst_amt'];
					$sgst_per = $data['sgst_per'];
					$sgst_amt = $data['sgst_amt'];
					$igst_per = $data['igst_per'];
					$igst_amt = $data['igst_amt'];
					$net_amt = $data['net_amt'];
					
	
					for ($i = 0; $i < count($data['item_id']); $i++) {

						// $item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
						// $unit_value = $this->escape_string($this->strip_all($unit[$i])); 
			
						//$itemDetails = $this-> getUniqueItemMasterById($item_id_value);
						// if(!$itemDetails){
						// 	 $this->query("INSERT INTO ".PREFIX."item_master (item_name, active, stock_unit, created_by,created_time) values ('".$item_id_value."', '1', '".$unit_value."','".$user_by."','".CURRENTMILIS."')");
						// 	 $item_id_value = $this->last_insert_id();
							
						// }
	
						$item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
						$rate_value = $this->escape_string($this->strip_all($rate[$i])); 
						$qty_value = $this->escape_string($this->strip_all($qty[$i])); 
						$unit_value = $this->escape_string($this->strip_all($unit[$i])); 
						$amt_value = $this->escape_string($this->strip_all($amt[$i])); 
						$disc_per_value = $this->escape_string($this->strip_all($disc_per[$i])); 
						$disc_amt_value = $this->escape_string($this->strip_all($disc_amt[$i])); 
						$after_disc_amt_value = $this->escape_string($this->strip_all($after_disc_amt[$i])); 
						$cgst_per_value = $this->escape_string($this->strip_all($cgst_per[$i])); 
						$cgst_amt_value = $this->escape_string($this->strip_all($cgst_amt[$i]));
						$sgst_per_value = $this->escape_string($this->strip_all($sgst_per[$i]));
						$sgst_amt_value = $this->escape_string($this->strip_all($sgst_amt[$i]));
						$igst_per_value = $this->escape_string($this->strip_all($igst_per[$i]));
						$igst_amt_value = $this->escape_string($this->strip_all($igst_amt[$i]));
						$net_amt_value = $this->escape_string($this->strip_all($net_amt[$i])); 

						$check = $this->fetch($this->query("select * from ".PREFIX."supplier_rate_master where supplier_id='".$supplier_id."' and item_id='".$item_id_value."' "));
						if ($check) {
							$move = $this->query("INSERT INTO ".PREFIX."supplier_rate_master_history SELECT * FROM ".PREFIX."supplier_rate_master WHERE supplier_id = '".$supplier_id."' and item_id='".$item_id."'");
							$delete = $this->query("DELETE FROM ".PREFIX."supplier_rate_master WHERE supplier_id = '".$supplier_id."' and item_id='".$item_id_value."'");
							
							$query = $this->query("insert into ".PREFIX."supplier_rate_master (supplier_id, item_id, rate, created_by, created_time) values ('".$supplier_id."', '".$item_id_value."', '".$rate_value."', '".$user_by."', '".CURRENTMILIS."')");
						} else {
							
							$query = $this->query("insert into ".PREFIX."supplier_rate_master (supplier_id, item_id, rate, created_by, created_time) values ('".$supplier_id."', '".$item_id_value."', '".$rate_value."', '".$user_by."', '".CURRENTMILIS."')");
						}
								
						$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."purchase_bill_item_transaction(tit_id, supplier_bill_no, item_id, rate, qty, unit, amt, disc_per, disc_amt, after_disc_amt, cgst_per, cgst_amt, sgst_per, sgst_amt, igst_per,igst_amt,net_amt) values ('".$last_id."' ,'".$supplier_bill_no."' ,'".$item_id_value."' ,'".$rate_value."' ,'".$qty_value."' ,'".$unit_value."' ,'".$amt_value."' ,'".$disc_per_value."' ,'".$disc_amt_value."','".$after_disc_amt_value."' ,'".$cgst_per_value."' ,'".$cgst_amt_value."' ,'".$sgst_per_value."' ,'".$sgst_amt_value."' ,'".$igst_per_value."' ,'".$igst_amt_value."','".$net_amt_value."')"); 
	
					}
	
			}
			return $last_id;
			}
			function getActiveInvoiceDetails() {
				$query = "select * from ".PREFIX."tax_invoice_transaction";
				return $this->query($query);
				
			}
			/* ============================= DELIVERY CHALLAN TRANSACTION STARTS===================================*/
		function getUniuqeChallanNo(){
			$int_invoice_on=$this->fetch($this->query("select MAX(int_invoice_on) x from ".PREFIX."delivery_challan_transaction ORDER BY int_invoice_on DESC"))['x'];
			
			if(!$int_invoice_on){
					$int_invoice_on='00001';
				}else{
					$int_invoice_on++;
					$int_invoice_on=sprintf('%05s', $int_invoice_on);
				}

				if (date('m') <= 4) {
					$financial_year = (date('y')-1) . '-' . date('y');
				} else {
					$financial_year = date('y') . '-' . (date('y') + 1);
				}
			return 'DC/'.$financial_year.'/'.$int_invoice_on;
		}

		function getUniuqeIntChallanNo(){
			$int_invoice_on=$this->fetch($this->query("select MAX(int_invoice_on) x from ".PREFIX."delivery_challan_transaction ORDER BY int_invoice_on DESC"))['x'];
			
				$int_invoice_on++;
				$int_invoice_on=sprintf('%05s', $int_invoice_on);

			return $int_invoice_on;
		}

		function getUniqueChallanDetailsById($id) {
			$sql = $this->query("select * from ".PREFIX."delivery_challan_transaction where id='".$id."' AND deleted_time IS NULL");
			return $this->fetch($sql);
		}
		function getUniqueChallanItemDetailsById($tit_id) {
			$query = "select * from ".PREFIX."delivery_challan_item_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniqueChallanItemDetailsById1($tit_id,$item_id) {
			$query = "select * from ".PREFIX."delivery_challan_item_transaction where tit_id='".$tit_id."' AND item_id='".$item_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniqueChallanItemDetailsById2($item_id) {
			$query = "select * from ".PREFIX."delivery_challan_item_transaction where item_id='".$item_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function addChallan($data,$user_by) {
		$delivery_challan_no = $this-> getUniuqeChallanNo();
		$int_invoice_on = $this-> getUniuqeIntChallanNo();
		$challan_date = $this->escape_string($this->strip_all($data['challan_date']));
		$customer_id = $this->escape_string($this->strip_all($data['customer_id']));
		$total_amt = $this->escape_string($this->strip_all($data['total_amt']));
		//$terms_of_payment = $this->escape_string($this->strip_all($data['terms_of_payment']));
		$address = $this->escape_string($this->strip_all($data['address']));
		$challan_type = $this->escape_string($this->strip_all($data['challan_type']));
		
	
		$insertIntoTable = $this->query("INSERT INTO ".PREFIX."delivery_challan_transaction (int_invoice_on,delivery_challan_no, challan_date, customer_id,total_amt,address,challan_type,created_by,created_time) values ('".$int_invoice_on."','".$delivery_challan_no."' ,'".$challan_date."' ,'".$customer_id."','".$total_amt."','".$address."','".$challan_type."','".$user_by."','".CURRENTMILIS."')"); 
		$last_id = $this->last_insert_id();
		if(isset($data['item_id'])) {
				$item_id = $data['item_id'];
				$rate = $data['rate'];
				$qty = $data['qty'];
				$unit = $data['unit'];
				$amt = $data['amt'];
				
				for ($i = 0; $i < count($data['item_id']); $i++) {

					$item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
					$unit_value = $this->escape_string($this->strip_all($unit[$i])); 
					
					// $itemDetails = $this-> getUniqueItemMasterById($item_id_value);
					// if(!$itemDetails){
					// 	 $this->query("INSERT INTO ".PREFIX."item_master (item_name, active, stock_unit, created_by,created_time) values ('".$item_id_value."', '1', '".$unit_value."','".$user_by."','".CURRENTMILIS."')");
					// 	 $item_id_value = $this->last_insert_id();
					// }

					$rate_value = $this->escape_string($this->strip_all($rate[$i])); 
					$qty_value = $this->escape_string($this->strip_all($qty[$i])); 
					$amt_value = $this->escape_string($this->strip_all($amt[$i])); 
					
					$check = $this->fetch($this->query("select * from ".PREFIX."customer_rate_master where customer_id='".$customer_id."' and item_id='".$item_id_value."' "));

					if ($check) {
						
						$move = $this->query("INSERT INTO ".PREFIX."customer_rate_master_history SELECT * FROM ".PREFIX."customer_rate_master WHERE customer_id = '".$customer_id."' and item_id='".$item_id_value."'");
						$delete = $this->query("DELETE FROM ".PREFIX."customer_rate_master WHERE customer_id = '".$customer_id."' and item_id='".$item_id_value."'");
						
						$query = $this->query("insert into ".PREFIX."customer_rate_master (customer_id, item_id, rate,challan_rate, created_by, created_time) values ('".$customer_id."', '".$item_id_value."', '".$check['rate']."', '".$rate_value."', '".$user_by."', '".CURRENTMILIS."')");
					} else {
						
						$query = $this->query("insert into ".PREFIX."customer_rate_master (customer_id, item_id, rate,challan_rate, created_by, created_time) values ('".$customer_id."', '".$item_id_value."', '0', '".$rate_value."', '".$user_by."', '".CURRENTMILIS."')");
					}

				   $insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."delivery_challan_item_transaction(tit_id, delivery_challan_no, item_id, rate, qty, unit, amt) values ('".$last_id."' ,'".$delivery_challan_no."' ,'".$item_id_value."' ,'".$rate_value."' ,'".$qty_value."' ,'".$unit_value."' ,'".$amt_value."')");
					
				}

		}
		return $last_id;
		}
		function updateChallan($data,$user_by) {
			$last_id = $this->escape_string($this->strip_all($data['id']));
			$delivery_challan_no = $this->escape_string($this->strip_all($data['delivery_challan_no']));
			$challan_date = $this->escape_string($this->strip_all($data['challan_date']));
			$customer_id = $this->escape_string($this->strip_all($data['customer_id']));
			$total_amt = $this->escape_string($this->strip_all($data['total_amt']));
			//$terms_of_payment = $this->escape_string($this->strip_all($data['terms_of_payment']));
			$address = $this->escape_string($this->strip_all($data['address']));
			$challan_type = $this->escape_string($this->strip_all($data['challan_type']));

			$updateIntoTable = $this->query("UPDATE  ".PREFIX."delivery_challan_transaction set challan_date='".$challan_date."',delivery_challan_no='".$delivery_challan_no."', customer_id='".$customer_id."',  total_amt='".$total_amt."',address='".$address."',challan_type='".$challan_type."',updated_by='".$user_by."', updated_time='".CURRENTMILIS."' WHERE id='".$last_id."' ");
			$deletetable = $this->query("DELETE FROM ".PREFIX."delivery_challan_item_transaction where tit_id='".$last_id."' ");
			if(isset($data['item_id'])) {
					$item_id = $data['item_id'];
					$rate = $data['rate'];
					$qty = $data['qty'];
					$unit = $data['unit'];
					$amt = $data['amt'];
					
	
					for ($i = 0; $i < count($data['item_id']); $i++) {

						// $item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
						// $unit_value = $this->escape_string($this->strip_all($unit[$i])); 
			
						//$itemDetails = $this-> getUniqueItemMasterById($item_id_value);
						// if(!$itemDetails){
						// 	 $this->query("INSERT INTO ".PREFIX."item_master (item_name, active, stock_unit, created_by,created_time) values ('".$item_id_value."', '1', '".$unit_value."','".$user_by."','".CURRENTMILIS."')");
						// 	 $item_id_value = $this->last_insert_id();
							
						// }
	
						$item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
						$rate_value = $this->escape_string($this->strip_all($rate[$i])); 
						$qty_value = $this->escape_string($this->strip_all($qty[$i])); 
						$unit_value = $this->escape_string($this->strip_all($unit[$i])); 
						$amt_value = $this->escape_string($this->strip_all($amt[$i])); 
	
						$check = $this->fetch($this->query("select * from ".PREFIX."customer_rate_master where customer_id='".$customer_id."' and item_id='".$item_id_value."' "));
					

						//$rate = ($check['rate']);
						if ($check) {
							
							$move = $this->query("INSERT INTO ".PREFIX."customer_rate_master_history SELECT * FROM ".PREFIX."customer_rate_master WHERE customer_id = '".$customer_id."' and item_id='".$item_id_value."'");
							$delete = $this->query("DELETE FROM ".PREFIX."customer_rate_master WHERE customer_id = '".$customer_id."' and item_id='".$item_id_value."'");
							
							$query = $this->query("insert into ".PREFIX."customer_rate_master (customer_id, item_id, rate,challan_rate, created_by, created_time) values ('".$customer_id."', '".$item_id_value."', '".$check['rate']."', '".$rate_value."', '".$user_by."', '".CURRENTMILIS."')");
						} else {
							
							$query = $this->query("insert into ".PREFIX."customer_rate_master (customer_id, item_id, rate,challan_rate, created_by, created_time) values ('".$customer_id."', '".$item_id_value."', '0', '".$rate_value."', '".$user_by."', '".CURRENTMILIS."')");
						
					}

						$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."delivery_challan_item_transaction(tit_id, delivery_challan_no, item_id, rate, qty, unit, amt) values ('".$last_id."' ,'".$delivery_challan_no."','".$item_id_value."','".$rate_value."','".$qty_value."','".$unit_value."','".$amt_value."')"); 
	
					}
	
			}
			return $last_id;
			}
			/* ============================= CREDIT NOTE TRANSACTION STARTS===================================*/
		function getUniuqeCreditNoteNo(){
			$int_invoice_on=$this->fetch($this->query("select MAX(int_invoice_on) x from ".PREFIX."credit_note_transaction ORDER BY int_invoice_on DESC"))['x'];
			
			if(!$int_invoice_on){
					$int_invoice_on='00001';
				}else{
					$int_invoice_on++;
					$int_invoice_on=sprintf('%05s', $int_invoice_on);
				}

				if (date('m') <= 4) {
					$financial_year = (date('y')-1) . '-' . date('y');
				} else {
					$financial_year = date('y') . '-' . (date('y') + 1);
				}
			return 'Cr/'.$financial_year.'/'.$int_invoice_on;
		}

		function getUniuqeCreditNoteIntInvoiceNo(){
			$int_invoice_on=$this->fetch($this->query("select MAX(int_invoice_on) x from ".PREFIX."credit_note_transaction ORDER BY int_invoice_on DESC"))['x'];
			
				$int_invoice_on++;
				$int_invoice_on=sprintf('%05s', $int_invoice_on);

			return $int_invoice_on;
		}
		
		function getUniqueCreditNoteDetailsById($id) {
			$sql = $this->query("select * from ".PREFIX."credit_note_transaction where id='".$id."' AND deleted_time IS NULL");
			return $this->fetch($sql);
		}
		function getUniqueCreditNoteDetailsById1($invoice_id) {
			//echo ("select * from ".PREFIX."credit_note_transaction where invoice_id='".$invoice_id."' AND deleted_time IS NULL");
			$sql = $this->query("select * from ".PREFIX."credit_note_transaction where invoice_id='".$invoice_id."' AND deleted_time IS NULL");
			return $this->fetch($sql);
		}
		function getUniqueCreditNoteDetailsByTitID($tit_id) {
			//echo "select sum(disc_amt),sum(after_disc_amt) from ".PREFIX."credit_note_item_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
			$query = "select sum(disc_amt),sum(after_disc_amt) from ".PREFIX."credit_note_item_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniqueCreditNoteDetailsByTaxInvoiceId($tit_id) {
			$query = "select * from ".PREFIX."credit_note_item_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniqueCreditNoteDetailsByTaxInvoiceId1($tit_id,$item_id) {
			$query = "select * from ".PREFIX."credit_note_item_transaction where tit_id='".$tit_id."' AND item_id='".$item_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniqueCreditNoteDetailsByTaxInvoiceId2($item_id) {
			$query = "select * from ".PREFIX."credit_note_item_transaction where item_id='".$item_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniqueCreditNoteGSTDetailsByTaxInvoiceId($tit_id) {
			$query = "select * from ".PREFIX."credit_note_item_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
			$sql = $this->query($query);
			return $this->fetch($sql);
		 }
		
		function addCreditNote($data,$user_by) {
		$credit_no = $this-> getUniuqeCreditNoteNo();
		$int_invoice_on = $this-> getUniuqeCreditNoteIntInvoiceNo();
		$credit_date = $this->escape_string($this->strip_all($data['credit_date']));
		$customer_id = $this->escape_string($this->strip_all($data['customer_id']));
		$invoice_id = $this->escape_string($this->strip_all($data['invoice_id']));
		$company_gst_no = $this->escape_string($this->strip_all($data['company_gst_no']));
		$total_amt = $this->escape_string($this->strip_all($data['total_amt']));
		$total_disc_amt = $this->escape_string($this->strip_all($data['total_disc_amt']));
		$total_after_disc_amt = $this->escape_string($this->strip_all($data['total_after_disc_amt']));
		$total_cgst_amt = $this->escape_string($this->strip_all($data['total_cgst_amt']));
		$total_sgst_amt = $this->escape_string($this->strip_all($data['total_sgst_amt']));
		$total_igst_amt = $this->escape_string($this->strip_all($data['total_igst_amt']));
		$total_net_amt = $this->escape_string($this->strip_all($data['total_net_amt']));
		$service_charges = $this->escape_string($this->strip_all($data['service_charges']));
		$final_amt = $this->escape_string($this->strip_all($data['final_amt']));
		$terms_of_payment = $this->escape_string($this->strip_all($data['terms_of_payment']));
		$description = $this->escape_string($this->strip_all($data['description']));
		$gst_per = $this->escape_string($this->strip_all($data['gst_per']));
		$service_charges_cgst_amt = $this->escape_string($this->strip_all($data['service_charges_cgst_amt']));
		$service_charges_sgst_amt = $this->escape_string($this->strip_all($data['service_charges_sgst_amt']));
		$service_charges_igst_amt = $this->escape_string($this->strip_all($data['service_charges_igst_amt']));
	
		
		$insertIntoTable = $this->query("INSERT INTO ".PREFIX."credit_note_transaction (int_invoice_on,credit_no,credit_date,customer_id, invoice_id, company_gst_no, total_amt, total_disc_amt, total_after_disc_amt, total_cgst_amt, total_sgst_amt, total_igst_amt, total_net_amt, service_charges, final_amt,service_charges_cgst_amt,service_charges_sgst_amt,service_charges_igst_amt,created_by,created_time,terms_of_payment,description,gst_per) values ('".$int_invoice_on."','".$credit_no."' ,'".$credit_date."' ,'".$customer_id."' ,'".$invoice_id."' ,'".$company_gst_no."' ,'".$total_amt."' ,'".$total_disc_amt."' ,'".$total_after_disc_amt."' ,'".$total_cgst_amt."' ,'".$total_sgst_amt."' ,'".$total_igst_amt."' ,'".$total_net_amt."' ,'".$service_charges."' ,'".$final_amt."','".$service_charges_cgst_amt."','".$service_charges_sgst_amt."','".$service_charges_igst_amt."','".$user_by."','".CURRENTMILIS."','".$terms_of_payment."','".$description."','".$gst_per."')"); 
		$last_id = $this->last_insert_id();
		
		if(isset($data['item_id'])) {
				$item_id = $data['item_id'];
				$rate = $data['rate'];
				$qty = $data['qty'];
				$unit = $data['unit'];
				$amt = $data['amt'];
				$disc_per = $data['disc_per'];
				$disc_amt = $data['disc_amt'];
				$after_disc_amt = $data['after_disc_amt'];
				$cgst_per = $data['cgst_per'];
				$cgst_amt = $data['cgst_amt'];
				$sgst_per = $data['sgst_per'];
				$sgst_amt = $data['sgst_amt'];
				$igst_per = $data['igst_per'];
				$igst_amt = $data['igst_amt'];
				$net_amt = $data['net_amt'];
				
				
				 for ($i = 0; $i < count($data['item_id']); $i++) {

					$item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
					$unit_value = $this->escape_string($this->strip_all($unit[$i])); 
					$rate_value = $this->escape_string($this->strip_all($rate[$i])); 
					$qty_value = $this->escape_string($this->strip_all($qty[$i])); 
					$amt_value = $this->escape_string($this->strip_all($amt[$i])); 
					$disc_per_value = $this->escape_string($this->strip_all($disc_per[$i])); 
					$disc_amt_value = $this->escape_string($this->strip_all($disc_amt[$i])); 
					$after_disc_amt_value = $this->escape_string($this->strip_all($after_disc_amt[$i])); 
					$cgst_per_value = $this->escape_string($this->strip_all($cgst_per[$i])); 
					$cgst_amt_value = $this->escape_string($this->strip_all($cgst_amt[$i]));
					$sgst_per_value = $this->escape_string($this->strip_all($sgst_per[$i]));
					$sgst_amt_value = $this->escape_string($this->strip_all($sgst_amt[$i]));
					$igst_per_value = $this->escape_string($this->strip_all($igst_per[$i]));
					$igst_amt_value = $this->escape_string($this->strip_all($igst_amt[$i]));
					$net_amt_value = $this->escape_string($this->strip_all($net_amt[$i])); 
				   $insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."credit_note_item_transaction (tit_id, credit_no, item_id, rate, qty, unit, amt, disc_per, disc_amt, after_disc_amt, cgst_per, cgst_amt, sgst_per, sgst_amt, igst_per,igst_amt,net_amt) values ('".$last_id."' ,'".$credit_no."' ,'".$item_id_value."' ,'".$rate_value."' ,'".$qty_value."' ,'".$unit_value."' ,'".$amt_value."' ,'".$disc_per_value."' ,'".$disc_amt_value."','".$after_disc_amt_value."' ,'".$cgst_per_value."' ,'".$cgst_amt_value."' ,'".$sgst_per_value."' ,'".$sgst_amt_value."' ,'".$igst_per_value."' ,'".$igst_amt_value."','".$net_amt_value."')");
					
				 }
		}
		return $last_id;
		}
		function updateCreditNote($data,$user_by) {
			$last_id = $this->escape_string($this->strip_all($data['id']));
			$credit_no = $this-> getUniuqeCreditNoteNo();
			$credit_date = $this->escape_string($this->strip_all($data['credit_date']));
			$customer_id = $this->escape_string($this->strip_all($data['customer_id']));
			$invoice_id = $this->escape_string($this->strip_all($data['invoice_id']));
			$company_gst_no = $this->escape_string($this->strip_all($data['company_gst_no']));
			$total_amt = $this->escape_string($this->strip_all($data['total_amt']));
			$total_disc_amt = $this->escape_string($this->strip_all($data['total_disc_amt']));
			$total_after_disc_amt = $this->escape_string($this->strip_all($data['total_after_disc_amt']));
			$total_cgst_amt = $this->escape_string($this->strip_all($data['total_cgst_amt']));
			$total_sgst_amt = $this->escape_string($this->strip_all($data['total_sgst_amt']));
			$total_igst_amt = $this->escape_string($this->strip_all($data['total_igst_amt']));
			$total_net_amt = $this->escape_string($this->strip_all($data['total_net_amt']));
			$service_charges = $this->escape_string($this->strip_all($data['service_charges']));
			$final_amt = $this->escape_string($this->strip_all($data['final_amt']));
			$terms_of_payment = $this->escape_string($this->strip_all($data['terms_of_payment']));
			$description = $this->escape_string($this->strip_all($data['description']));
			$gst_per = $this->escape_string($this->strip_all($data['gst_per']));
			$service_charges_cgst_amt = $this->escape_string($this->strip_all($data['service_charges_cgst_amt']));
			$service_charges_sgst_amt = $this->escape_string($this->strip_all($data['service_charges_sgst_amt']));
			$service_charges_igst_amt = $this->escape_string($this->strip_all($data['service_charges_igst_amt']));
				
			$updateIntoTable = $this->query("UPDATE  ".PREFIX."credit_note_transaction set credit_date='".$credit_date."', customer_id='".$customer_id."', invoice_id='".$invoice_id."',  total_amt='".$total_amt."',  company_gst_no='".$company_gst_no."', total_disc_amt='".$total_disc_amt."', total_after_disc_amt='".$total_after_disc_amt."', total_cgst_amt='".$total_cgst_amt."', total_sgst_amt='".$total_sgst_amt."', total_igst_amt='".$total_igst_amt."',total_net_amt='".$total_net_amt."',service_charges='".$service_charges."',final_amt='".$final_amt."',terms_of_payment='".$terms_of_payment."',service_charges_cgst_amt='".$service_charges_cgst_amt."',service_charges_sgst_amt='".$service_charges_sgst_amt."',service_charges_igst_amt='".$service_charges_igst_amt."',description='".$description."',updated_by='".$user_by."',gst_per='".$gst_per."', updated_time='".CURRENTMILIS."' WHERE id='".$last_id."' ");
			$deletetable = $this->query("DELETE FROM ".PREFIX."credit_note_item_transaction where tit_id='".$last_id."' ");
			if(isset($data['item_id'])) {
					$item_id = $data['item_id'];
					$rate = $data['rate'];
					$qty = $data['qty'];
					$unit = $data['unit'];
					$amt = $data['amt'];
					$disc_per = $data['disc_per'];
					$disc_amt = $data['disc_amt'];
					$after_disc_amt = $data['after_disc_amt'];
					$cgst_per = $data['cgst_per'];
					$cgst_amt = $data['cgst_amt'];
					$sgst_per = $data['sgst_per'];
					$sgst_amt = $data['sgst_amt'];
					$igst_per = $data['igst_per'];
					$igst_amt = $data['igst_amt'];
					$net_amt = $data['net_amt'];
					
	
					for ($i = 0; $i < count($data['item_id']); $i++) {

						// $item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
						// $unit_value = $this->escape_string($this->strip_all($unit[$i])); 
			
						//$itemDetails = $this-> getUniqueItemMasterById($item_id_value);
						// if(!$itemDetails){
						// 	 $this->query("INSERT INTO ".PREFIX."item_master (item_name, active, stock_unit, created_by,created_time) values ('".$item_id_value."', '1', '".$unit_value."','".$user_by."','".CURRENTMILIS."')");
						// 	 $item_id_value = $this->last_insert_id();
							
						// }
	
						$item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
						$rate_value = $this->escape_string($this->strip_all($rate[$i])); 
						$qty_value = $this->escape_string($this->strip_all($qty[$i])); 
						$unit_value = $this->escape_string($this->strip_all($unit[$i])); 
						$amt_value = $this->escape_string($this->strip_all($amt[$i])); 
						$disc_per_value = $this->escape_string($this->strip_all($disc_per[$i])); 
						$disc_amt_value = $this->escape_string($this->strip_all($disc_amt[$i])); 
						$after_disc_amt_value = $this->escape_string($this->strip_all($after_disc_amt[$i])); 
						$cgst_per_value = $this->escape_string($this->strip_all($cgst_per[$i])); 
						$cgst_amt_value = $this->escape_string($this->strip_all($cgst_amt[$i]));
						$sgst_per_value = $this->escape_string($this->strip_all($sgst_per[$i]));
						$sgst_amt_value = $this->escape_string($this->strip_all($sgst_amt[$i]));
						$igst_per_value = $this->escape_string($this->strip_all($igst_per[$i]));
						$igst_amt_value = $this->escape_string($this->strip_all($igst_amt[$i]));
						$net_amt_value = $this->escape_string($this->strip_all($net_amt[$i])); 
								//echo ("INSERT INTO ".PREFIX."purchase_order_item_transaction(tit_id, purchase_no, item_id, rate, qty, unit, amt, disc_per, disc_amt, after_disc_amt, cgst_per, cgst_amt, sgst_per, sgst_amt, igst_per,igst_amt,net_amt) values ('".$last_id."' ,'".$purchase_no."' ,'".$item_id_value."' ,'".$rate_value."' ,'".$qty_value."' ,'".$unit_value."' ,'".$amt_value."' ,'".$disc_per_value."' ,'".$disc_amt_value."','".$after_disc_amt_value."' ,'".$cgst_per_value."' ,'".$cgst_amt_value."' ,'".$sgst_per_value."' ,'".$sgst_amt_value."' ,'".$igst_per_value."' ,'".$igst_amt_value."','".$net_amt_value."')"); 
						$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."credit_note_item_transaction(tit_id, credit_no, item_id, rate, qty, unit, amt, disc_per, disc_amt, after_disc_amt, cgst_per, cgst_amt, sgst_per, sgst_amt, igst_per,igst_amt,net_amt) values ('".$last_id."' ,'".$credit_no."' ,'".$item_id_value."' ,'".$rate_value."' ,'".$qty_value."' ,'".$unit_value."' ,'".$amt_value."' ,'".$disc_per_value."' ,'".$disc_amt_value."','".$after_disc_amt_value."' ,'".$cgst_per_value."' ,'".$cgst_amt_value."' ,'".$sgst_per_value."' ,'".$sgst_amt_value."' ,'".$igst_per_value."' ,'".$igst_amt_value."','".$net_amt_value."')"); 
	
					}
	
			}
			return $last_id;
			}

		/* ============================= DEBIT NOTE TRANSACTION STARTS===================================*/
		function getUniuqeDebitNoteNo(){
			$int_invoice_on=$this->fetch($this->query("select MAX(int_invoice_on) x from ".PREFIX."debit_note_trasaction ORDER BY int_invoice_on DESC"))['x'];
			
			if(!$int_invoice_on){
					$int_invoice_on='00001';
				}else{
					$int_invoice_on++;
					$int_invoice_on=sprintf('%05s', $int_invoice_on);
				}

				if (date('m') <= 4) {
					$financial_year = (date('y')-1) . '-' . date('y');
				} else {
					$financial_year = date('y') . '-' . (date('y') + 1);
				}
			return 'Dr/'.$financial_year.'/'.$int_invoice_on;
		}

		function getUniuqeDebitNoteIntInvoiceNo(){
			$int_invoice_on=$this->fetch($this->query("select MAX(int_invoice_on) x from ".PREFIX."debit_note_trasaction ORDER BY int_invoice_on DESC"))['x'];
			
				$int_invoice_on++;
				$int_invoice_on=sprintf('%05s', $int_invoice_on);

			return $int_invoice_on;
		}

		function getUniqueDebitNoteDetailsById($id) {
			$sql = $this->query("select * from ".PREFIX."debit_note_trasaction where id='".$id."' AND deleted_time IS NULL");
			return $this->fetch($sql);
		}
		function getUniqueDebitNoteDetailsById1($supplier_bill_id) {
			
			$sql = $this->query("select * from ".PREFIX."debit_note_trasaction where supplier_bill_id='".$supplier_bill_id."' AND deleted_time IS NULL");
			return $this->fetch($sql);
		}
		function getUniqueDebitNoteDetailsByTaxInvoiceId($tit_id) {
			$query = "select * from ".PREFIX."debit_item_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniqueDebitNoteDetailsByTaxInvoiceId1($tit_id,$item_id) {
			$query = "select * from ".PREFIX."debit_item_transaction where tit_id='".$tit_id."' AND item_id='".$item_id."'AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniqueDebitNoteDetailsByTaxInvoiceId2($item_id) {
			$query = "select * from ".PREFIX."debit_item_transaction where item_id='".$item_id."' AND deleted_time IS NULL";
			return $this->query($query);
		}
		function getUniqueDebitNoteGSTDetailsByTaxInvoiceId($tit_id) {
			$query = "select * from ".PREFIX."debit_item_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
			$sql = $this->query($query);
			return $this->fetch($sql);
		 }
		
		function addDebitNote($data,$user_by) {
		$debit_note_no = $this-> getUniuqeDebitNoteNo();
		$int_invoice_on = $this-> getUniuqeDebitNoteIntInvoiceNo();
		$debit_date = $this->escape_string($this->strip_all($data['debit_date']));
		$supplier_id = $this->escape_string($this->strip_all($data['supplier_id']));
		$supplier_bill_id = $this->escape_string($this->strip_all($data['supplier_bill_id']));
		$company_gst_no = $this->escape_string($this->strip_all($data['company_gst_no']));
		$total_amt = $this->escape_string($this->strip_all($data['total_amt']));
		$total_disc_amt = $this->escape_string($this->strip_all($data['total_disc_amt']));
		$total_after_disc_amt = $this->escape_string($this->strip_all($data['total_after_disc_amt']));
		$total_cgst_amt = $this->escape_string($this->strip_all($data['total_cgst_amt']));
		$total_sgst_amt = $this->escape_string($this->strip_all($data['total_sgst_amt']));
		$total_igst_amt = $this->escape_string($this->strip_all($data['total_igst_amt']));
		$total_net_amt = $this->escape_string($this->strip_all($data['total_net_amt']));
		$service_charges = $this->escape_string($this->strip_all($data['service_charges']));
		$final_amt = $this->escape_string($this->strip_all($data['final_amt']));
		$description = $this->escape_string($this->strip_all($data['description']));
		$gst_per = $this->escape_string($this->strip_all($data['gst_per']));
		$terms_of_payment = $this->escape_string($this->strip_all($data['terms_of_payment']));
		$service_charges_cgst_amt = $this->escape_string($this->strip_all($data['service_charges_cgst_amt']));
		$service_charges_sgst_amt = $this->escape_string($this->strip_all($data['service_charges_sgst_amt']));
		$service_charges_igst_amt = $this->escape_string($this->strip_all($data['service_charges_igst_amt']));
	
		
		$insertIntoTable = $this->query("INSERT INTO ".PREFIX."debit_note_trasaction (int_invoice_on,debit_note_no,debit_date,supplier_id, supplier_bill_id, company_gst_no, total_amt, total_disc_amt, total_after_disc_amt, total_cgst_amt, total_sgst_amt, total_igst_amt, total_net_amt, service_charges, final_amt,service_charges_cgst_amt,service_charges_sgst_amt,service_charges_igst_amt,created_by,created_time,description,gst_per,terms_of_payment) values ('".$int_invoice_on."','".$debit_note_no."' ,'".$debit_date."' ,'".$supplier_id."' ,'".$supplier_bill_id."' ,'".$company_gst_no."' ,'".$total_amt."' ,'".$total_disc_amt."' ,'".$total_after_disc_amt."' ,'".$total_cgst_amt."' ,'".$total_sgst_amt."' ,'".$total_igst_amt."' ,'".$total_net_amt."' ,'".$service_charges."' ,'".$final_amt."','".$service_charges_cgst_amt."','".$service_charges_sgst_amt."','".$service_charges_igst_amt."','".$user_by."','".CURRENTMILIS."','".$description."','".$gst_per."','".$terms_of_payment."')"); 
		$last_id = $this->last_insert_id();
		
		if(isset($data['item_id'])) {
				$item_id = $data['item_id'];
				$rate = $data['rate'];
				$qty = $data['qty'];
				$unit = $data['unit'];
				$amt = $data['amt'];
				$disc_per = $data['disc_per'];
				$disc_amt = $data['disc_amt'];
				$after_disc_amt = $data['after_disc_amt'];
				$cgst_per = $data['cgst_per'];
				$cgst_amt = $data['cgst_amt'];
				$sgst_per = $data['sgst_per'];
				$sgst_amt = $data['sgst_amt'];
				$igst_per = $data['igst_per'];
				$igst_amt = $data['igst_amt'];
				$net_amt = $data['net_amt'];
				
				
				 for ($i = 0; $i < count($data['item_id']); $i++) {

					$item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
					$unit_value = $this->escape_string($this->strip_all($unit[$i])); 
					$rate_value = $this->escape_string($this->strip_all($rate[$i])); 
					$qty_value = $this->escape_string($this->strip_all($qty[$i])); 
					$amt_value = $this->escape_string($this->strip_all($amt[$i])); 
					$disc_per_value = $this->escape_string($this->strip_all($disc_per[$i])); 
					$disc_amt_value = $this->escape_string($this->strip_all($disc_amt[$i])); 
					$after_disc_amt_value = $this->escape_string($this->strip_all($after_disc_amt[$i])); 
					$cgst_per_value = $this->escape_string($this->strip_all($cgst_per[$i])); 
					$cgst_amt_value = $this->escape_string($this->strip_all($cgst_amt[$i]));
					$sgst_per_value = $this->escape_string($this->strip_all($sgst_per[$i]));
					$sgst_amt_value = $this->escape_string($this->strip_all($sgst_amt[$i]));
					$igst_per_value = $this->escape_string($this->strip_all($igst_per[$i]));
					$igst_amt_value = $this->escape_string($this->strip_all($igst_amt[$i]));
					$net_amt_value = $this->escape_string($this->strip_all($net_amt[$i])); 
				   $insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."debit_item_transaction (tit_id, debit_note_no, item_id, rate, qty, unit, amt, disc_per, disc_amt, after_disc_amt, cgst_per, cgst_amt, sgst_per, sgst_amt, igst_per,igst_amt,net_amt) values ('".$last_id."' ,'".$debit_note_no."' ,'".$item_id_value."' ,'".$rate_value."' ,'".$qty_value."' ,'".$unit_value."' ,'".$amt_value."' ,'".$disc_per_value."' ,'".$disc_amt_value."','".$after_disc_amt_value."' ,'".$cgst_per_value."' ,'".$cgst_amt_value."' ,'".$sgst_per_value."' ,'".$sgst_amt_value."' ,'".$igst_per_value."' ,'".$igst_amt_value."','".$net_amt_value."')");
					
				 }
		}
		return $last_id;
		}
		function updateDebitNote($data,$user_by) {
			$last_id = $this->escape_string($this->strip_all($data['id']));
			$debit_note_no = $this-> getUniuqeDebitNoteNo();
			$debit_date = $this->escape_string($this->strip_all($data['debit_date']));
			$supplier_id = $this->escape_string($this->strip_all($data['supplier_id']));
			$supplier_bill_id = $this->escape_string($this->strip_all($data['supplier_bill_id']));
			$company_gst_no = $this->escape_string($this->strip_all($data['company_gst_no']));
			$company_gst_no = $this->escape_string($this->strip_all($data['company_gst_no']));
			$total_amt = $this->escape_string($this->strip_all($data['total_amt']));
			$total_disc_amt = $this->escape_string($this->strip_all($data['total_disc_amt']));
			$total_after_disc_amt = $this->escape_string($this->strip_all($data['total_after_disc_amt']));
			$total_cgst_amt = $this->escape_string($this->strip_all($data['total_cgst_amt']));
			$total_sgst_amt = $this->escape_string($this->strip_all($data['total_sgst_amt']));
			$total_igst_amt = $this->escape_string($this->strip_all($data['total_igst_amt']));
			$total_net_amt = $this->escape_string($this->strip_all($data['total_net_amt']));
			$service_charges = $this->escape_string($this->strip_all($data['service_charges']));
			$final_amt = $this->escape_string($this->strip_all($data['final_amt']));
			$description = $this->escape_string($this->strip_all($data['description']));
			$gst_per = $this->escape_string($this->strip_all($data['gst_per']));
			$terms_of_payment = $this->escape_string($this->strip_all($data['terms_of_payment']));
			$service_charges_cgst_amt = $this->escape_string($this->strip_all($data['service_charges_cgst_amt']));
			$service_charges_sgst_amt = $this->escape_string($this->strip_all($data['service_charges_sgst_amt']));
			$service_charges_igst_amt = $this->escape_string($this->strip_all($data['service_charges_igst_amt']));
				
			$updateIntoTable = $this->query("UPDATE  ".PREFIX."debit_note_trasaction set debit_date='".$debit_date."', supplier_id='".$supplier_id."', supplier_bill_id='".$supplier_bill_id."',  total_amt='".$total_amt."',  company_gst_no='".$company_gst_no."', total_disc_amt='".$total_disc_amt."', total_after_disc_amt='".$total_after_disc_amt."', total_cgst_amt='".$total_cgst_amt."', total_sgst_amt='".$total_sgst_amt."', total_igst_amt='".$total_igst_amt."',total_net_amt='".$total_net_amt."',service_charges='".$service_charges."',final_amt='".$final_amt."',description='".$description."',service_charges_cgst_amt='".$service_charges_cgst_amt."',service_charges_sgst_amt='".$service_charges_sgst_amt."',service_charges_igst_amt='".$service_charges_igst_amt."',gst_per='".$gst_per."',terms_of_payment='".$terms_of_payment."',updated_by='".$user_by."', updated_time='".CURRENTMILIS."' WHERE id='".$last_id."' ");
			$deletetable = $this->query("DELETE FROM ".PREFIX."debit_item_transaction where tit_id='".$last_id."' ");
			if(isset($data['item_id'])) {
					$item_id = $data['item_id'];
					$rate = $data['rate'];
					$qty = $data['qty'];
					$unit = $data['unit'];
					$amt = $data['amt'];
					$disc_per = $data['disc_per'];
					$disc_amt = $data['disc_amt'];
					$after_disc_amt = $data['after_disc_amt'];
					$cgst_per = $data['cgst_per'];
					$cgst_amt = $data['cgst_amt'];
					$sgst_per = $data['sgst_per'];
					$sgst_amt = $data['sgst_amt'];
					$igst_per = $data['igst_per'];
					$igst_amt = $data['igst_amt'];
					$net_amt = $data['net_amt'];
					
	
					for ($i = 0; $i < count($data['item_id']); $i++) {

						// $item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
						// $unit_value = $this->escape_string($this->strip_all($unit[$i])); 
			
						//$itemDetails = $this-> getUniqueItemMasterById($item_id_value);
						// if(!$itemDetails){
						// 	 $this->query("INSERT INTO ".PREFIX."item_master (item_name, active, stock_unit, created_by,created_time) values ('".$item_id_value."', '1', '".$unit_value."','".$user_by."','".CURRENTMILIS."')");
						// 	 $item_id_value = $this->last_insert_id();
							
						// }
	
						$item_id_value = $this->escape_string($this->strip_all($item_id[$i])); 
						$rate_value = $this->escape_string($this->strip_all($rate[$i])); 
						$qty_value = $this->escape_string($this->strip_all($qty[$i])); 
						$unit_value = $this->escape_string($this->strip_all($unit[$i])); 
						$amt_value = $this->escape_string($this->strip_all($amt[$i])); 
						$disc_per_value = $this->escape_string($this->strip_all($disc_per[$i])); 
						$disc_amt_value = $this->escape_string($this->strip_all($disc_amt[$i])); 
						$after_disc_amt_value = $this->escape_string($this->strip_all($after_disc_amt[$i])); 
						$cgst_per_value = $this->escape_string($this->strip_all($cgst_per[$i])); 
						$cgst_amt_value = $this->escape_string($this->strip_all($cgst_amt[$i]));
						$sgst_per_value = $this->escape_string($this->strip_all($sgst_per[$i]));
						$sgst_amt_value = $this->escape_string($this->strip_all($sgst_amt[$i]));
						$igst_per_value = $this->escape_string($this->strip_all($igst_per[$i]));
						$igst_amt_value = $this->escape_string($this->strip_all($igst_amt[$i]));
						$net_amt_value = $this->escape_string($this->strip_all($net_amt[$i])); 
								//echo ("INSERT INTO ".PREFIX."purchase_order_item_transaction(tit_id, purchase_no, item_id, rate, qty, unit, amt, disc_per, disc_amt, after_disc_amt, cgst_per, cgst_amt, sgst_per, sgst_amt, igst_per,igst_amt,net_amt) values ('".$last_id."' ,'".$purchase_no."' ,'".$item_id_value."' ,'".$rate_value."' ,'".$qty_value."' ,'".$unit_value."' ,'".$amt_value."' ,'".$disc_per_value."' ,'".$disc_amt_value."','".$after_disc_amt_value."' ,'".$cgst_per_value."' ,'".$cgst_amt_value."' ,'".$sgst_per_value."' ,'".$sgst_amt_value."' ,'".$igst_per_value."' ,'".$igst_amt_value."','".$net_amt_value."')"); 
						$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."debit_item_transaction(tit_id, debit_note_no, item_id, rate, qty, unit, amt, disc_per, disc_amt, after_disc_amt, cgst_per, cgst_amt, sgst_per, sgst_amt, igst_per,igst_amt,net_amt) values ('".$last_id."' ,'".$debit_note_no."' ,'".$item_id_value."' ,'".$rate_value."' ,'".$qty_value."' ,'".$unit_value."' ,'".$amt_value."' ,'".$disc_per_value."' ,'".$disc_amt_value."','".$after_disc_amt_value."' ,'".$cgst_per_value."' ,'".$cgst_amt_value."' ,'".$sgst_per_value."' ,'".$sgst_amt_value."' ,'".$igst_per_value."' ,'".$igst_amt_value."','".$net_amt_value."')"); 
			
					}
	
			}
			return $last_id;
			}

			function getHeadDetails($account_head_id) {
				$query = "select * from ".PREFIX."ledger_master where exp_income_head='".$account_head_id."' AND deleted_time IS NULL";
				return $this->query($query);
			}
			
		/* ====================================RECEVIED ======================================================*/
		function getUniuqeReceviedNo(){
			$int_invoice_on=$this->fetch($this->query("select MAX(int_invoice_on) x from ".PREFIX."recevied_transaction ORDER BY int_invoice_on DESC"))['x'];
			
			if(!$int_invoice_on){
					$int_invoice_on='00001';
				}else{
					$int_invoice_on++;
					$int_invoice_on=sprintf('%05s', $int_invoice_on);
				}

				if (date('m') <= 4) {
					$financial_year = (date('y')-1) . '-' . date('y');
				} else {
					$financial_year = date('y') . '-' . (date('y') + 1);
				}
			return 'RE/'.$financial_year.'/'.$int_invoice_on;
		}

		function getUniuqeReceviedNoInt(){
			$int_invoice_on=$this->fetch($this->query("select MAX(int_invoice_on) x from ".PREFIX."recevied_transaction ORDER BY int_invoice_on DESC"))['x'];
			
				$int_invoice_on++;
				$int_invoice_on=sprintf('%05s', $int_invoice_on);

			return $int_invoice_on;
		}
			function getUniqueReceviedNoById($id) {
				$id = $this->escape_string($this->strip_all($id));
				$query = "select * from ".PREFIX."recevied_transaction where id='".$id."' AND deleted_time IS NULL";
				$sql = $this->query($query);
				return $this->fetch($sql);
			}
		
			function getActiveReceviedDetails() {
				$query = "select * from ".PREFIX."recevied_transaction where active='1' AND deleted_time IS NULL";
				return $this->query($query);
			}
			function getUniqueInvoiceReceivedSumById($invoice_id) {
				echo ("select sum(net_amt) as recd_amt from ".PREFIX."recevied_bil_transaction where invoice_id='".$invoice_id."' AND deleted_time IS NULL");
				$sql = $this->query("select sum(net_amt) as recd_amt from ".PREFIX."recevied_bil_transaction where invoice_id='".$invoice_id."' AND deleted_time IS NULL");
				return $this->fetch($sql);
			}
		
			function getUniqueReceivedDetailsById($tit_id) {
				$query = "select * from ".PREFIX."recevied_bil_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
				return $this->query($query);
			}
			
			function addRecevied($data,$user_by) {
				$recevied_no = $this-> getUniuqeReceviedNo();
				$int_invoice_on = $this-> getUniuqeReceviedNoInt();
				$received_date = $this->escape_string($this->strip_all($data['received_date']));
				$account_head_id = $this->escape_string($this->strip_all($data['account_head_id']));
				$payment_mode = $this->escape_string($this->strip_all($data['payment_mode']));
				$transaction_no = $this->escape_string($this->strip_all($data['transaction_no']));
				$received_now = $this->escape_string($this->strip_all($data['received_now']));
				$reference = $this->escape_string($this->strip_all($data['reference']));
				$customer_id = $this->escape_string($this->strip_all($data['customer_id']));
				$ledger_id = $this->escape_string($this->strip_all($data['ledger_id']));
				$bank_name = $this->escape_string($this->strip_all($data['bank_name']));
				$instrument_no = $this->escape_string($this->strip_all($data['instrument_no']));
				$remark = $this->escape_string($this->strip_all($data['remark']));
				$total_amt = $this->escape_string($this->strip_all($data['total_amt']));
				$radio_name = $this->escape_string($this->strip_all($data['radio_name']));
				
				//echo ("INSERT INTO  ".PREFIX."recevied_transaction(int_invoice_on,recevied_no, received_date, account_head_id, payment_mode, transaction_no, received_now, reference,created_by, created_time,customer_id,ledger_id,bank_name,instrument_no,remark,total_amt) values ('".$int_invoice_on."' ,'".$recevied_no."', '".$received_date."', '".$account_head_id."', '".$payment_mode."', '".$transaction_no."', '".$received_now."', '".$reference."', '".$user_by."', '".CURRENTMILIS."','".$customer_id."','".$ledger_id."','".$bank_name."','".$instrument_no."','".$remark."','".$total_amt."')");
				$insertIntoDetailsTable = $this->query("INSERT INTO  ".PREFIX."recevied_transaction(int_invoice_on,recevied_no, received_date, account_head_id, payment_mode, transaction_no, received_now, reference,created_by, created_time,customer_id,ledger_id,bank_name,instrument_no,remark,total_amt,radio_name) values ('".$int_invoice_on."' ,'".$recevied_no."', '".$received_date."', '".$account_head_id."', '".$payment_mode."', '".$transaction_no."', '".$received_now."', '".$reference."', '".$user_by."', '".CURRENTMILIS."','".$customer_id."','".$ledger_id."','".$bank_name."','".$instrument_no."','".$remark."','".$total_amt."','".$radio_name."')");
				$last_id = $this->last_insert_id();
				
				
				if(isset($data['invoice_id'])) {
					$invoice_id = $data['invoice_id'];
					$invoice_date = $data['invoice_date'];
					$inv_amt = $data['inv_amt'];
					$recd_amt = $data['recd_amt'];
					$balance_amt = $data['balance_amt'];
					$net_amt = $data['net_amt'];
					
					for ($i = 0; $i < count($data['invoice_id']); $i++) {

						$invoice_id_value = $this->escape_string($this->strip_all($invoice_id[$i])); 
						$invoice_date_value = $this->escape_string($this->strip_all($invoice_date[$i])); 
						$inv_amt_value = $this->escape_string($this->strip_all($inv_amt[$i])); 
						$recd_amt_value = $this->escape_string($this->strip_all($recd_amt[$i])); 
						$balance_amt_value = $this->escape_string($this->strip_all($balance_amt[$i])); 
						$net_amt_value = $this->escape_string($this->strip_all($net_amt[$i])); 
						
				//echo ("INSERT INTO ".PREFIX."recevied_bil_transaction (tit_id, recevied_no, invoice_id, invoice_date, inv_amt, recd_amt, balance_amt, net_amt) values ('".$last_id."' ,'".$recevied_no."' ,'".$invoice_id_value."' ,'".$invoice_date."','".$inv_amt_value."' ,'".$recd_amt_value."' ,'".$balance_amt_value."' ,'".$net_amt_value."')");
			 $insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."recevied_bil_transaction (tit_id, recevied_no, invoice_id, invoice_date, inv_amt, recd_amt, balance_amt, net_amt) values ('".$last_id."' ,'".$recevied_no."' ,'".$invoice_id_value."' ,'".$invoice_date."','".$inv_amt_value."' ,'".$recd_amt_value."' ,'".$balance_amt_value."' ,'".$net_amt_value."')");
					
			 }
		}
		return $last_id;
		}
		function updateRecevied($data,$user_by) {
			$last_id = $this->escape_string($this->strip_all($data['id']));
			$received_date = $this->escape_string($this->strip_all($data['received_date']));
			$account_head_id = $this->escape_string($this->strip_all($data['account_head_id']));
			$payment_mode = $this->escape_string($this->strip_all($data['payment_mode']));
			$transaction_no = $this->escape_string($this->strip_all($data['transaction_no']));
			$received_now = $this->escape_string($this->strip_all($data['received_now']));
			$reference = $this->escape_string($this->strip_all($data['reference']));
			$customer_id = $this->escape_string($this->strip_all($data['customer_id']));
			$ledger_id = $this->escape_string($this->strip_all($data['ledger_id']));
			$bank_name = $this->escape_string($this->strip_all($data['bank_name']));
			$instrument_no = $this->escape_string($this->strip_all($data['instrument_no']));
			$remark = $this->escape_string($this->strip_all($data['remark']));
			$total_amt = $this->escape_string($this->strip_all($data['total_amt']));
			$radio_name = $this->escape_string($this->strip_all($data['radio_name']));
				
			$updateIntoTable = $this->query("UPDATE  ".PREFIX."recevied_transaction set received_date='".$received_date."', account_head_id='".$account_head_id."', payment_mode='".$payment_mode."',  transaction_no='".$transaction_no."',  received_now='".$received_now."', reference='".$reference."', customer_id='".$customer_id."', ledger_id='".$ledger_id."', bank_name='".$bank_name."',instrument_no='".$instrument_no."',remark='".$remark."',total_amt='".$total_amt."',radio_name='".$radio_name."',updated_by='".$user_by."', updated_time='".CURRENTMILIS."' WHERE id='".$last_id."' ");
		//	$deletetable = $this->query("DELETE FROM ".PREFIX."recevied_bil_transaction where tit_id='".$last_id."' ");
			if(isset($data['invoice_id'])) {
				$invoice_id = $data['invoice_id'];
				$invoice_date = $data['invoice_date'];
				$inv_amt = $data['inv_amt'];
				$recd_amt = $data['recd_amt'];
				$balance_amt = $data['balance_amt'];
				$net_amt = $data['net_amt'];
					
	
					for ($i = 0; $i < count($data['invoice_id']); $i++) {

						$invoice_id_value = $this->escape_string($this->strip_all($invoice_id[$i])); 
						$invoice_date_value = $this->escape_string($this->strip_all($invoice_date[$i])); 
						$inv_amt_value = $this->escape_string($this->strip_all($inv_amt[$i])); 
						$recd_amt_value = $this->escape_string($this->strip_all($recd_amt[$i])); 
						$balance_amt_value = $this->escape_string($this->strip_all($balance_amt[$i])); 
						$net_amt_value = $this->escape_string($this->strip_all($net_amt[$i]));  
								
						$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."recevied_bil_transaction (tit_id, recevied_no, invoice_id, invoice_date, inv_amt, recd_amt, balance_amt, net_amt) values ('".$last_id."' ,'".$recevied_no."' ,'".$invoice_id_value."' ,'".$invoice_date."','".$inv_amt_value."' ,'".$recd_amt_value."' ,'".$balance_amt_value."' ,'".$net_amt_value."')");
					}
			}
			return $last_id;
			}
	
			/* ====================================PAYMENT ======================================================*/
			
		function getUniuqePaymentNo(){
			$int_invoice_on=$this->fetch($this->query("select MAX(int_invoice_on) x from ".PREFIX."payment_transaction ORDER BY int_invoice_on DESC"))['x'];
			
			if(!$int_invoice_on){
					$int_invoice_on='00001';
				}else{
					$int_invoice_on++;
					$int_invoice_on=sprintf('%05s', $int_invoice_on);
				}

				if (date('m') <= 4) {
					$financial_year = (date('y')-1) . '-' . date('y');
				} else {
					$financial_year = date('y') . '-' . (date('y') + 1);
				}
			return 'PAY/'.$financial_year.'/'.$int_invoice_on;
		}

		function getUniuqePaymentNoInt(){
			$int_invoice_on=$this->fetch($this->query("select MAX(int_invoice_on) x from ".PREFIX."payment_transaction ORDER BY int_invoice_on DESC"))['x'];
			
				$int_invoice_on++;
				$int_invoice_on=sprintf('%05s', $int_invoice_on);

			return $int_invoice_on;
		}
			function getUniquePaymentById($id) {
				$id = $this->escape_string($this->strip_all($id));
				$query = "select * from ".PREFIX."payment_transaction where id='".$id."' AND deleted_time IS NULL";
				$sql = $this->query($query);
				return $this->fetch($sql);
			}
			function getUniquePaymentById1($id) {
				$id = $this->escape_string($this->strip_all($id));
				$query = "select * from ".PREFIX."payment_transaction where id='".$id."' AND deleted_time IS NULL";
				return $this->query($query);
			}
			function getActivePaymentDetails() {
				$query = "select * from ".PREFIX."payment_transaction where active='1' AND deleted_time IS NULL";
				return $this->query($query);
			}
			function getUniqueInvoicePaymentSumById($supplier_bill_id) {
				
				$sql = $this->query("select sum(net_amt) as paid_amt from ".PREFIX."payment_bill_transaction where supplier_bill_id='".$supplier_bill_id."' AND deleted_time IS NULL");
				return $this->fetch($sql);
			}
			function getUniquePaymentDetailsById($tit_id) {
				$query = "select * from ".PREFIX."payment_bill_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
				return $this->query($query);
			}
			function getUniquePaymentDetailsById2($tit_id) {
				$query = "select * from ".PREFIX."payment_bill_transaction where tit_id='".$tit_id."' AND deleted_time IS NULL";
				$sql = $this->query($query);
				return $this->fetch($sql);
			}
			function getUniquePaymentDetailsById1($tit_id,$supplier_bill_id) {
				$query = "select * from ".PREFIX."payment_bill_transaction where tit_id='".$tit_id."'AND supplier_bill_id='".$supplier_bill_id."' AND deleted_time IS NULL";
				return $this->query($query);
			}
			function addPayment($data,$user_by) {
				$payment_no = $this-> getUniuqePaymentNo();
				$int_invoice_on = $this-> getUniuqePaymentNoInt();
				$payment_date = $this->escape_string($this->strip_all($data['payment_date']));
				$account_head_id = $this->escape_string($this->strip_all($data['account_head_id']));
				$payment_mode = $this->escape_string($this->strip_all($data['payment_mode']));
				$transaction_no = $this->escape_string($this->strip_all($data['transaction_no']));
				$payment_now = $this->escape_string($this->strip_all($data['payment_now']));
				$reference = $this->escape_string($this->strip_all($data['reference']));
				$supplier_id = $this->escape_string($this->strip_all($data['supplier_id']));
				$supplier_ledger_id = $this->escape_string($this->strip_all($data['supplier_ledger_id']));
				$bank_name = $this->escape_string($this->strip_all($data['bank_name']));
				$instrument_no = $this->escape_string($this->strip_all($data['instrument_no']));
				$remark = $this->escape_string($this->strip_all($data['remark']));
				$total_amt = $this->escape_string($this->strip_all($data['total_amt']));
				$radio_name = $this->escape_string($this->strip_all($data['radio_name']));
				
				
				$insertIntoDetailsTable = $this->query("INSERT INTO  ".PREFIX."payment_transaction(int_invoice_on,payment_no, payment_date, account_head_id, payment_mode, transaction_no, payment_now, reference,created_by, created_time,supplier_id,supplier_ledger_id,bank_name,instrument_no,remark,total_amt,radio_name) values ('".$int_invoice_on."' ,'".$payment_no."', '".$payment_date."', '".$account_head_id."', '".$payment_mode."', '".$transaction_no."', '".$payment_now."', '".$reference."', '".$user_by."', '".CURRENTMILIS."','".$supplier_id."','".$supplier_ledger_id."','".$bank_name."','".$instrument_no."','".$remark."','".$total_amt."','".$radio_name."')");
				$last_id = $this->last_insert_id();
				
				
				if(isset($data['supplier_bill_id'])) {
					$supplier_bill_id = $data['supplier_bill_id'];
					$supplier_date = $data['supplier_date'];
					$inv_amt = $data['inv_amt'];
					$paid_amt = $data['paid_amt'];
					$balance_amt = $data['balance_amt'];
					$net_amt = $data['net_amt'];
					
					for ($i = 0; $i < count($data['supplier_bill_id']); $i++) {

						$supplier_bill_id_value = $this->escape_string($this->strip_all($supplier_bill_id[$i])); 
						$supplier_date_value = $this->escape_string($this->strip_all($supplier_date[$i])); 
						$inv_amt_value = $this->escape_string($this->strip_all($inv_amt[$i])); 
						$paid_amt_value = $this->escape_string($this->strip_all($paid_amt[$i])); 
						$balance_amt_value = $this->escape_string($this->strip_all($balance_amt[$i])); 
						$net_amt_value = $this->escape_string($this->strip_all($net_amt[$i])); 
						
				
			 $insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."payment_bill_transaction (tit_id, payment_no, supplier_bill_id, supplier_date, inv_amt, paid_amt, balance_amt, net_amt) values ('".$last_id."' ,'".$payment_no."' ,'".$supplier_bill_id_value."' ,'".$payment_date_value."','".$inv_amt_value."' ,'".$paid_amt_value."' ,'".$balance_amt_value."' ,'".$net_amt_value."')");
					
			 }
		}
		return $last_id;
		}
		function updatePayment($data,$user_by) {
			$last_id = $this->escape_string($this->strip_all($data['id']));
			$payment_date = $this->escape_string($this->strip_all($data['payment_date']));
			$account_head_id = $this->escape_string($this->strip_all($data['account_head_id']));
			$payment_mode = $this->escape_string($this->strip_all($data['payment_mode']));
			$transaction_no = $this->escape_string($this->strip_all($data['transaction_no']));
			$payment_now = $this->escape_string($this->strip_all($data['payment_now']));
			$reference = $this->escape_string($this->strip_all($data['reference']));
			$supplier_id = $this->escape_string($this->strip_all($data['supplier_id']));
			$supplier_ledger_id = $this->escape_string($this->strip_all($data['supplier_ledger_id']));
			$bank_name = $this->escape_string($this->strip_all($data['bank_name']));
			$instrument_no = $this->escape_string($this->strip_all($data['instrument_no']));
			$remark = $this->escape_string($this->strip_all($data['remark']));
			$total_amt = $this->escape_string($this->strip_all($data['total_amt']));
			$radio_name = $this->escape_string($this->strip_all($data['radio_name']));
				
			$updateIntoTable = $this->query("UPDATE  ".PREFIX."payment_transaction set payment_date='".$payment_date."', account_head_id='".$account_head_id."', payment_mode='".$payment_mode."',  transaction_no='".$transaction_no."',  payment_now='".$payment_now."', reference='".$reference."', supplier_id='".$supplier_id."', supplier_ledger_id='".$supplier_ledger_id."', bank_name='".$bank_name."',instrument_no='".$instrument_no."',remark='".$remark."',total_amt='".$total_amt."',radio_name='".$radio_name."',updated_by='".$user_by."', updated_time='".CURRENTMILIS."' WHERE id='".$last_id."' ");
		//	$deletetable = $this->query("DELETE FROM ".PREFIX."payment_bill_transaction where tit_id='".$last_id."' ");
			if(isset($data['invoice_id'])) {
				$supplier_bill_id = $data['supplier_bill_id'];
				$supplier_date = $data['supplier_date'];
				$inv_amt = $data['inv_amt'];
				$paid_amt = $data['paid_amt'];
				$balance_amt = $data['balance_amt'];
				$net_amt = $data['net_amt'];
					
	
					for ($i = 0; $i < count($data['invoice_id']); $i++) {

						$supplier_bill_id_value = $this->escape_string($this->strip_all($supplier_bill_id[$i])); 
						$supplier_date_value = $this->escape_string($this->strip_all($supplier_date[$i])); 
						$inv_amt_value = $this->escape_string($this->strip_all($inv_amt[$i])); 
						$paid_amt_value = $this->escape_string($this->strip_all($paid_amt[$i])); 
						$balance_amt_value = $this->escape_string($this->strip_all($balance_amt[$i])); 
						$net_amt_value = $this->escape_string($this->strip_all($net_amt[$i]));  
								 
						$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."payment_bill_transaction (tit_id, payment_no, supplier_bill_id, supplier_date, inv_amt, paid_amt, balance_amt, net_amt) values ('".$last_id."' ,'".$payment_no."' ,'".$supplier_bill_id_value."' ,'".$supplier_date_value."','".$inv_amt_value."' ,'".$paid_amt_value."' ,'".$balance_amt_value."' ,'".$net_amt_value."')");
					}
			}
			return $last_id;
			}
			//===================================STOCK REPORT===========================================================//
			function getLiveStockDetails($item_id){
				$sql=$this->fetch($this->query("SELECT * FROM ".PREFIX."opening_stock WHERE item_id='".$item_id."' ORDER BY id DESC LIMIT 1"));
				return $sql;
			}

			function addStock($item_id,$opening_stock,$qty_in,$qty_out,$adjustment_in,$adjustment_out,$user_by){
				$entry_date = date("Y-m-d");
				
				$itemStockDetails=$this->fetch($this->query("SELECT * FROM ".PREFIX."opening_stock WHERE item_id='".$item_id."' ORDER BY id DESC LIMIT 1"));
						
				if($itemStockDetails){
					
					if($opening_stock==''){
						$opening_stock=$itemStockDetails['opening_stock'];
					}else{
						$opening_stock = $opening_stock + $itemStockDetails['opening_stock'];
					}

					if($qty_in==''){
						$qty_in=$itemStockDetails['qty_in'];
					}else{
						$qty_in = $qty_in + $itemStockDetails['qty_in'];
					}

					if($qty_out==''){
						$qty_out=$itemStockDetails['qty_out'];
					}else{
						$qty_out = $qty_out + $itemStockDetails['qty_out'];
					}

					if($adjustment_in==''){
						$adjustment_in=$itemStockDetails['adjustment_in'];
					}else{
						$adjustment_in = $adjustment_in + $itemStockDetails['adjustment_in'];
					}

					if($adjustment_out==''){
						$adjustment_out=$itemStockDetails['adjustment_out'];
					}else{
						$adjustment_out = $adjustment_out + $itemStockDetails['adjustment_out'];
					}
				

					$current_stock = ($opening_stock + $qty_in + $adjustment_in) - ($qty_out + $adjustment_out);

					$this->query("INSERT INTO ".PREFIX."opening_stock (item_id,opening_stock,qty_in,qty_out,adjustment_in,adjustment_out,current_stock,entry_date,user_by) VALUES ('".$item_id."', '".$opening_stock."', '".$qty_in."', '".$qty_out."', '".$adjustment_in."', '".$adjustment_out."', '".$current_stock."', '".$entry_date."', '".$user_by."')");
				
				}else{ 	
					
					if($opening_stock==''){
						$opening_stock=0;
					}
					if($qty_in==''){
						$qty_in=0;
					}
					if($qty_out==''){
						$qty_out=0;
					}
					if($adjustment_in==''){
						$adjustment_in=0;
					}
					if($adjustment_out==''){
						$adjustment_out=0;
					}

					$current_stock = ($opening_stock + $qty_in + $adjustment_in) - ($qty_out + $adjustment_out);

					$this->query("INSERT INTO ".PREFIX."opening_stock (item_id,opening_stock,qty_in,qty_out,adjustment_in,adjustment_out,current_stock,entry_date,user_by) VALUES ('".$item_id."', '".$opening_stock."', '".$qty_in."', '".$qty_out."', '".$adjustment_in."', '".$adjustment_out."', '".$current_stock."', '".$entry_date."', '".$user_by."')");
				
				}

				return true;
			}
			function getAllCreditDataByID($customer_id,$from_date,$to_date){
				$query=
					"select * from (SELECT ".PREFIX."customer_master.date_of_opening as tr_date, 'Opening Balanace' as description, '' as debit,'' as credit, ".PREFIX."customer_master.opening_balance as opening,".PREFIX."customer_master.id as id, 0 AS priority 
					FROM ".PREFIX."customer_master  WHERE ".PREFIX."customer_master.id='".$customer_id."' 
					UNION
						
					SELECT ".PREFIX."tax_invoice_transaction.invoice_date as tr_date ,".PREFIX."tax_invoice_transaction.invoice_no as description, ".PREFIX."tax_invoice_transaction.final_amt as debit,  '' as credit, '' as opening,".PREFIX."tax_invoice_transaction.id as id, 1 AS priority
					FROM ".PREFIX."tax_invoice_transaction  WHERE ".PREFIX."tax_invoice_transaction.customer_id='".$customer_id."' AND (".PREFIX."tax_invoice_transaction.invoice_date BETWEEN '$from_date' AND '$to_date')
					UNION
					
					SELECT ".PREFIX."delivery_challan_transaction.challan_date,".PREFIX."delivery_challan_transaction.delivery_challan_no as description, ".PREFIX."delivery_challan_transaction.total_amt as debit, '' as credit, '' as opening,".PREFIX."delivery_challan_transaction.id as id, 2 AS priority
					FROM ".PREFIX."delivery_challan_transaction WHERE ".PREFIX."delivery_challan_transaction.customer_id='".$customer_id."' AND (".PREFIX."delivery_challan_transaction.challan_date BETWEEN '$from_date' AND '$to_date' )
					UNION
					
					SELECT ".PREFIX."credit_note_transaction.credit_date,".PREFIX."credit_note_transaction.credit_no as description, '' as debit, ".PREFIX."credit_note_transaction.final_amt as credit,'' as opening,".PREFIX."credit_note_transaction.id as id,3 AS priority
					FROM ".PREFIX."credit_note_transaction WHERE ".PREFIX."credit_note_transaction.customer_id='".$customer_id."' AND (".PREFIX."credit_note_transaction.credit_date BETWEEN '$from_date' AND '$to_date' )) u ORDER by tr_date ASC"  ;
				return $this->query($query);

			}
			function PreviousClosing($customer_id,$from_date,$to_date){
				
				$onedayMinusDate    = date('Y-m-d', strtotime('-1 day', strtotime($from_date)));
				$query= "SELECT SUM(".PREFIX."tax_invoice_transaction.final_amt) as plus,(SELECT SUM(".PREFIX."credit_note_transaction.final_amt) FROM ".PREFIX."credit_note_transaction WHERE ".PREFIX."credit_note_transaction.customer_id='".$customer_id."' AND (".PREFIX."credit_note_transaction.credit_date BETWEEN '1990-01-01' and '$onedayMinusDate')) as minus,
				(SELECT ".PREFIX."customer_master.opening_balance FROM ".PREFIX."customer_master WHERE ".PREFIX."customer_master.id='".$customer_id."') as opening FROM ".PREFIX."tax_invoice_transaction WHERE customer_id='".$customer_id."'  AND (".PREFIX."tax_invoice_transaction.invoice_date BETWEEN '1990-01-01' and '$onedayMinusDate')";
				$sql=$this->query($query);
				return $this->fetch($sql);
			}
			function getAllDebititDataByID($supplier_id,$from_date,$to_date){

				$query = "select * from (SELECT ".PREFIX."supplier_master.date_of_opening as tr_date, 'Opening Balanace' as description, '' as debit,'' as credit, ".PREFIX."supplier_master.opening_balance as opening,".PREFIX."supplier_master.id as id, 0 AS priority FROM ".PREFIX."supplier_master  WHERE ".PREFIX."supplier_master.id='".$supplier_id."' 
				UNION
				SELECT ".PREFIX."purchase_bill_transaction.supplier_bill_date as tr_date ,".PREFIX."purchase_bill_transaction.supplier_bill_no as description, ".PREFIX."purchase_bill_transaction.final_amt as credit,  '' as debit, '' as opening,".PREFIX."purchase_bill_transaction.id as id, 1 AS priority FROM ".PREFIX."purchase_bill_transaction  WHERE  ".PREFIX."purchase_bill_transaction.supplier_id='".$supplier_id."'AND (".PREFIX."purchase_bill_transaction.supplier_bill_date BETWEEN '$from_date' AND '$to_date')
				UNION
				SELECT ".PREFIX."debit_note_trasaction.debit_date,".PREFIX."debit_note_trasaction.debit_note_no as description, '' as credit,  ".PREFIX."debit_note_trasaction.final_amt as debit,'' as opening,".PREFIX."debit_note_trasaction.id as id, 2 AS priority FROM ".PREFIX."debit_note_trasaction WHERE ".PREFIX."debit_note_trasaction.supplier_id='".$supplier_id."'AND (".PREFIX."debit_note_trasaction.debit_date BETWEEN '$from_date' AND '$to_date' )) u ORDER by tr_date ASC";

				
				return $this->query($query);
			}
			function PreviousSupplierClosing($supplier_id,$from_date,$to_date){
				
				$onedayMinusDate    = date('Y-m-d', strtotime('-1 day', strtotime($from_date)));

				$query= "SELECT SUM(".PREFIX."purchase_bill_transaction.final_amt) as plus,(SELECT SUM(".PREFIX."debit_note_trasaction.final_amt) FROM ".PREFIX."debit_note_trasaction WHERE ".PREFIX."debit_note_trasaction.supplier_id='".$supplier_id."' AND (".PREFIX."debit_note_trasaction.debit_date BETWEEN '1990-01-01' and '$onedayMinusDate')) as minus,
				(SELECT ".PREFIX."supplier_master.opening_balance FROM ".PREFIX."supplier_master WHERE ".PREFIX."supplier_master.id='".$supplier_id."') as opening FROM ".PREFIX."purchase_bill_transaction WHERE ".PREFIX."purchase_bill_transaction.supplier_id='".$supplier_id."'  AND (".PREFIX."purchase_bill_transaction.supplier_bill_date BETWEEN '1990-01-01' and '$onedayMinusDate')";
				$sql=$this->query($query);
				return $this->fetch($sql);
				
				
			}
			function getBinCard($item_id,$from_date,$to_date){
				$query = 
				"select * from (SELECT ".PREFIX."opening_balance_master.quantity as opening_qty, 'Opening Quantity' as description, ".PREFIX."opening_balance_master.date as open_date ,'0' as received, ".PREFIX."opening_balance_master.rate as purchase_rate, '0' as sales_qty, ".PREFIX."opening_balance_master.id as id, 0 AS priority FROM ".PREFIX."opening_balance_master  WHERE ".PREFIX."opening_balance_master.item_id='".$item_id."'
				UNION
				SELECT '' as opening_qty,".PREFIX."purchase_bill_transaction.supplier_bill_no as description, ".PREFIX."purchase_bill_transaction.supplier_bill_date as open_date,'0' as received,'0' as purchase_rate, '0' as sales_qty, ".PREFIX."purchase_bill_transaction.id as id, 1 as priority FROM ".PREFIX."purchase_bill_transaction WHERE (".PREFIX."purchase_bill_transaction.supplier_bill_date BETWEEN '$from_date' and '$to_date')
				UNION
				SELECT '' as opening_qty,".PREFIX."tax_invoice_transaction.invoice_no as description, ".PREFIX."tax_invoice_transaction.invoice_date as open_date,'0' as received,'0' as purchase_rate, '0' as sales_qty, ".PREFIX."tax_invoice_transaction.id as id, 2 as priority FROM ".PREFIX."tax_invoice_transaction WHERE (".PREFIX."tax_invoice_transaction.invoice_date BETWEEN '$from_date' and '$to_date')) u ORDER BY open_date asc";
			return $this->query($query);
			}

			function getPurchaseRate($from_date,$to_date){ 
				$sql =($this->query("SELECT A.item_id, A.after_disc_amt,A.rate FROM ".PREFIX."purchase_bill_item_transaction A INNER JOIN ui_purchase_bill_transaction B ON A.tit_id=B.id  AND B.supplier_bill_date BETWEEN '$from_date' AND '$to_date' GROUP BY A.item_id ORDER BY  B.supplier_bill_date"));
				if($sql==''){
					$sql = $this->fetch($this->query("SELECT * FROM ".PREFIX."supplier_rate_master_history WHERE created_time BETWEEN '$from_date 00:00:00' AND '$to_date 23:59:59' GROUP BY item_id ORDER BY created_time"));
						if($sql==''){
							$sql = $this->fetch($this->query("SELECT * FROM ".PREFIX."supplier_rate_master WHERE created_time BETWEEN '$from_date 00:00:00' AND '$to_date 23:59:59' GROUP BY item_id ORDER BY created_time"));
							if($sql==''){
								$sql = $this->fetch($this->query("SELECT * FROM ".PREFIX."opening_balance_master WHERE date BETWEEN '$from_date' AND '$to_date' GROUP BY item_id ORDER BY date"));
							}
						}
				}
				return $sql;
			}

			function getPurchaseRateByItemIdAndDate($from_date,$to_date,$item_id){
				$sql = $this->fetch($this->query("SELECT A.item_id, A.after_disc_amt,A.rate FROM ".PREFIX."purchase_bill_item_transaction A INNER JOIN ui_purchase_bill_transaction B ON A.tit_id=B.id  AND B.supplier_bill_date BETWEEN '$from_date' AND '$to_date' AND A.item_id='".$item_id."' GROUP BY A.item_id ORDER BY  B.supplier_bill_date"));
				if($sql==''){
					$sql = $this->fetch($this->query("SELECT * FROM ".PREFIX."supplier_rate_master_history WHERE  item_id='".$item_id."' AND created_time BETWEEN '$from_date 00:00:00' AND '$to_date 23:59:59' GROUP BY item_id ORDER BY created_time"));
						if($sql==''){
							$sql = $this->fetch($this->query("SELECT * FROM ".PREFIX."supplier_rate_master WHERE  item_id='".$item_id."' AND created_time BETWEEN '$from_date 00:00:00' AND '$to_date 23:59:59' GROUP BY item_id ORDER BY created_time"));
							if($sql==''){
								$sql = $this->fetch($this->query("SELECT * FROM ".PREFIX."opening_balance_master WHERE  item_id='".$item_id."' AND date BETWEEN '$from_date' AND '$to_date' GROUP BY item_id ORDER BY date"));
							}
						}
				}
				return $sql;
			}

			function getPurchaseRateByItemId($item_id){
				$sql = $this->fetch($this->query("SELECT A.item_id, A.after_disc_amt,A.rate FROM ".PREFIX."purchase_bill_item_transaction A INNER JOIN ui_purchase_bill_transaction B ON A.tit_id=B.id AND A.item_id='".$item_id."' GROUP BY A.item_id"));
				if($sql==''){
					$sql = $this->fetch($this->query("SELECT * FROM ".PREFIX."supplier_rate_master_history WHERE  item_id='".$item_id."' GROUP BY item_id"));
						if($sql==''){
							$sql = $this->fetch($this->query("SELECT * FROM ".PREFIX."supplier_rate_master WHERE  item_id='".$item_id."' GROUP BY item_id"));
							if($sql==''){
								$sql = $this->fetch($this->query("SELECT * FROM ".PREFIX."opening_balance_master WHERE  item_id='".$item_id."' GROUP BY item_id"));
							}
						}
				}
				return $sql;
			}
			function getPurchaseRateSupplierFromToDate($from_date,$to_date){ 
				$sql =($this->query("SELECT A.customer_id, A.after_disc_amt,A.rate FROM ".PREFIX."purchase_bill_item_transaction A INNER JOIN ui_purchase_bill_transaction B ON A.tit_id=B.id  AND B.supplier_bill_date BETWEEN '$from_date' AND '$to_date' GROUP BY A.customer_id ORDER BY  B.supplier_bill_date"));
				if($sql==''){
					$sql = $this->fetch($this->query("SELECT * FROM ".PREFIX."supplier_rate_master_history WHERE created_time BETWEEN '$from_date 00:00:00' AND '$to_date 23:59:59' GROUP BY customer_id ORDER BY created_time"));
						if($sql==''){
							$sql = $this->fetch($this->query("SELECT * FROM ".PREFIX."supplier_rate_master WHERE created_time BETWEEN '$from_date 00:00:00' AND '$to_date 23:59:59' GROUP BY customer_id ORDER BY created_time"));
							if($sql==''){
								$sql = $this->fetch($this->query("SELECT * FROM ".PREFIX."opening_balance_master WHERE date BETWEEN '$from_date' AND '$to_date' GROUP BY customer_id ORDER BY date"));
							}
						}
				}
				return $sql;
			}

			function getPurchaseRateBySupplierIdAndDate($from_date,$to_date,$customer_id){
				$sql = $this->fetch($this->query("SELECT A.customer_id, A.after_disc_amt,A.rate FROM ".PREFIX."purchase_bill_item_transaction A INNER JOIN ui_purchase_bill_transaction B ON A.tit_id=B.id  AND B.supplier_bill_date BETWEEN '$from_date' AND '$to_date' AND A.item_id='".$item_id."' GROUP BY A.item_id ORDER BY  B.supplier_bill_date"));
				if($sql==''){
					$sql = $this->fetch($this->query("SELECT * FROM ".PREFIX."supplier_rate_master_history WHERE  item_id='".$item_id."' AND created_time BETWEEN '$from_date 00:00:00' AND '$to_date 23:59:59' GROUP BY item_id ORDER BY created_time"));
						if($sql==''){
							$sql = $this->fetch($this->query("SELECT * FROM ".PREFIX."supplier_rate_master WHERE  item_id='".$item_id."' AND created_time BETWEEN '$from_date 00:00:00' AND '$to_date 23:59:59' GROUP BY item_id ORDER BY created_time"));
							if($sql==''){
								$sql = $this->fetch($this->query("SELECT * FROM ".PREFIX."opening_balance_master WHERE  item_id='".$item_id."' AND date BETWEEN '$from_date' AND '$to_date' GROUP BY item_id ORDER BY date"));
							}
						}
				}
				return $sql;
			}

			function getPurchaseRateBySupplierId($date,$item_id){
				$thirtyDayMinusDate    = date('Y-m-d', strtotime('-30 day', strtotime($date)));
				$sql = $this->fetch($this->query("SELECT  A.after_disc_amt,A.rate FROM ".PREFIX."purchase_bill_item_transaction A INNER JOIN ".PREFIX."purchase_bill_transaction B ON A.tit_id=B.id  AND B.supplier_bill_date BETWEEN 'thirtyDayMinusDate' AND '$date' AND A.item_id='".$item_id."' "));
				if($sql==''){
					$sql = $this->fetch($this->query("SELECT * FROM ".PREFIX."supplier_rate_master_history WHERE  item_id='".$item_id."' GROUP BY item_id"));
						if($sql==''){
							$sql = $this->fetch($this->query("SELECT * FROM ".PREFIX."supplier_rate_master WHERE  item_id='".$item_id."' GROUP BY item_id"));
							if($sql==''){
								$sql = $this->fetch($this->query("SELECT * FROM ".PREFIX."opening_balance_master WHERE  item_id='".$item_id."' GROUP BY item_id"));
							}
						}
				}
				return $sql;
			}

			
			
}			

?>