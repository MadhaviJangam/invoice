<?php

$admin = new AdminFunctions();
	if(isset($admin)){
		$pdfObj = $admin;
   } 
   ob_start();

   $creditDetails=$admin->getUniqueCreditNoteDetailsById($_GET['id']);
   $customerDetails=$admin->getUniqueCustomerMasterById($creditDetails['customer_id']);
   $getStateDetails=$admin->getStateCodeMasterByName($customerDetails['statename']);
   $getItemDetailTOTAL= $admin->getUniqueCreditNoteDetailsById($_GET['id']);
   $getcreditItemDetails=$admin->getUniqueCreditNoteDetailsByTaxInvoiceId($_GET['id']);
   $rowcount=mysqli_num_rows($getcreditItemDetails);
   $getItemPerDetails=$admin->getUniqueTaxInvoiceGSTDetailsByTaxInvoiceId($_GET['id']);
   $companyInfo=$admin->getUniqueCompanyMasterById();

   $arrayHSNcode=array();

if ($creditDetails['credit_date'] == '0000-00-00') {
   $credit_date = '';
} else {
    $credit_date = date("d-m-Y", strtotime($creditDetails['credit_date']));
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   
   <body>
   <table border="0" style="width:100%">
        <tr> 
            <td style="text-align:center;">Credit Note</td>
        </tr>
        <tr>
            <td style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;color:#f26d00;font-size:20px;width:100%;text-align:center;"><?php echo $companyInfo['company_name'];?></td>
        </tr>
        <tr>
            <td colspan="6" style="text-align:center;border-left:1px solid #000;border-right:1px solid #000;"><?php echo $companyInfo['company_address'];?></td>
        </tr>
        <tr>
            <td colspan="6" style="text-align:center;border-left:1px solid #000;border-right:1px solid #000;border-bottom:1px solid #000;font-size:14px">CREDIT NOTE</td>
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;"  width="60%">GSTIN:&nbsp;&nbsp;<?php echo $companyInfo['gst_no'];?></td>  
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="40%">Credit Note No:&nbsp;&nbsp;<?php echo $creditDetails['credit_no']; ?></td>  
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;"  width="60%"></td>  
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="40%"></td>   
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="60%"></td>  
            <td style="text-align:left;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="40%">Date:&nbsp;&nbsp; <?php echo $credit_date; ?></td>   
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;"  width="60%">Party</td>  
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="40%">Credit Note Description:</td>   
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="60%"><?php echo substr($customerDetails['customer_name'] ,"0", 100); ?><br><?php echo $customerDetails['billing_address']; ?><br>State:&nbsp;<?php echo $customerDetails['statename'];?><br>State Code:<?php echo $getStateDetails['statecode']; ?><br>GSTIN / Unique ID:<?php echo $customerDetails['company_gst']; ?></td>  
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="40%"><?php echo nl2br($creditDetails['terms_of_payment']); ?></td>  
        </tr>
        <!-- <tr>
        <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;"  width="60%"><br></td>    
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="40%"></td>  
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;"  width="60%"></td>  
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="40%"></td>   
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;"  width="60%"></td>  
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="40%"></td>   
        </tr>
        <tr>
            <td style="text-align:left;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="60%"></td>  
            <td style="text-align:left;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="40%"></td>   
        </tr> -->
        <tr style="font-size:7px;">
            <td rowspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="4%">SR.NO</td>
            <td  rowspan="2" style="text-align:left;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="15%">DESCRIPTION OF GOODS AND SERVICE</td>
            <td   rowspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%">HSN CODE</td>
            <td  rowspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="5%">QTY</td>
            <td rowspan="2" style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="8%">RATE</td>
            <td rowspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="8%">UNIT</td>
            <td rowspan="2"style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="14%">TAXABLE VALUE </td>
            <td style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="12%">CGST</td>  
            <td style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="12%">SGST </td>  
            <td style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="12%">IGST </td>  
        </tr>
        <tr style="font-size:7px;">
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="5%">Rate</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="7%">Amount</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="5%">Rate</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="7%">Amount</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="5%">Rate</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="7%">Amount</td>
        </tr>
        <?php
             $x= 1;   
             
            while ($row = $admin->fetch($getcreditItemDetails)) {
                $getItemDetails= $admin->getUniqueItemMasterById($row['item_id']);
                array_push($arrayHSNcode,$getItemDetails['hsn_code']);
        ?>
        <tr style="font-size:9px;font-family: 'Times New Roman', Times, serif;">
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="4%"><?php echo $x; ?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="15%"><?php echo $getItemDetails['item_name']; ?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $getItemDetails['hsn_code']; ?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"><?php  echo $row['qty'];?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="8%"><?php  echo $row['rate'];?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="8%"><?php  echo $row['unit'];?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="14%"><?php  echo $row['amt'];?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"><?php echo $row['cgst_per']; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="7%"><?php echo $row['cgst_amt']; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"><?php echo $row['sgst_per']; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="7%"><?php echo $row['sgst_amt']; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"><?php echo $row['igst_per']; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="7%"><?php echo $row['igst_amt']; ?></td>
        </tr>
        <?php
            $x++;  
                }
        ?>
        <?php
         $rowprint= 30 - $rowcount;
        for($i=1;$i<=$rowprint;$i++){ ?>
        <tr style="font-size:10px;">
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="4%"></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="15%"></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="8%"></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="8%"></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="14%"></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="7%"></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="7%"></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="7%"></td>  
        </tr>
        <?php } ?>
        <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;border-left:1px solid #000;" width="64%"><?php echo $admin->formatAmount($getItemDetailTOTAL['total_net_amt']);?></td>
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="12%"><?php echo $admin->formatAmount($getItemDetailTOTAL['total_cgst_amt']);?></td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="12%"><?php echo $admin->formatAmount($getItemDetailTOTAL['total_sgst_amt']);?></td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="12%"><?php echo $admin->formatAmount($getItemDetailTOTAL['total_igst_amt']);?></td>
              
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:left;padding:0px;border-right:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;border-left:1px solid #000;" width="64%">Invoice Total ( In Words ): <br><?php echo $admin->getIndianCurrency($getItemDetailTOTAL['total_net_amt']+$getItemDetailTOTAL['total_cgst_amt']+$getItemDetailTOTAL['total_sgst_amt']+$getItemDetailTOTAL['total_igst_amt']);?> Only </td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;" width="36%">Total:<span style="text-align:right"><?php echo $admin->formatAmount($getItemDetailTOTAL['total_net_amt']+$getItemDetailTOTAL['total_cgst_amt']+$getItemDetailTOTAL['total_sgst_amt']+$getItemDetailTOTAL['total_igst_amt']);?></span></td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:left;padding:0px;border-right:1px solid #000;border-top:1px solid #000;border-left:1px solid #000;" width="64%">Company's PAN<br><?php echo $customerDetails['company_pan']; ?></td>
            <td style="text-align:left;padding:0px;border-right:1px solid #000;border-top:1px solid #000;border-left:1px solid #000;" width="36%">FOR <?php echo $companyInfo['company_name']; ?></td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="64%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="36%"><?php echo $companyInfo['designation']; ?></td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:left;padding:0px;border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;" width="64%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-bottom:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;" width="36%">Authorised Singnatory</td>
        </tr>
      </table>
   </body>
</html>
<?php 
	$invoiceMsg = ob_get_contents();
	ob_end_clean();
?>