<?php

$admin = new AdminFunctions();
	if(isset($admin)){
		$pdfObj = $admin;
   } 
   ob_start();
   if(isset($_GET['supplier_id'])){
       $supplier_id = $_GET['supplier_id'];
    }else{
        $supplier_id = '';
    }
    if(isset($_GET['item_id'])){
        $item_id = $_GET['item_id'];
     }else{
         $item_id = '';
     }
    $query='';
    if($supplier_id==''  && $item_id=='' ){
        $query="SELECT * FROM ".PREFIX."supplier_rate_master";
    }

    if($supplier_id!='' && $item_id!=''){
        $query="SELECT * FROM ".PREFIX."supplier_rate_master WHERE supplier_id='".$supplier_id."' AND item_id='".$item_id."' ";
    }
    if($supplier_id!='' && $item_id==''){
        $query="SELECT * FROM ".PREFIX."supplier_rate_master WHERE supplier_id='".$supplier_id."' ";
    }
    if($supplier_id=='' && $item_id!=''){
        $query="SELECT * FROM ".PREFIX."supplier_rate_master WHERE item_id='".$item_id."' ";
    }
   
    $result=$admin->query($query);
    $total_rate=0;
    //$num_rows1 = mysqli_num_rows($result);
  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   
   <body>
   <table border="0" style="width:100%">
        <!-- <tr style="">
            <td style="text-align:left;" width="50%">Customer Price List</td>
            <td style="text-align:right;"  width="50%">Printed On <?php echo date("d-m-Y"); ?></td>
        </tr>
        <tr style="">
            <td style="text-align:left;" width="50%"></td>
            <td style="text-align:right;"  width="50%"></td>
        </tr> -->
        <!--<tr style="">
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="40%">PARTY NAME</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="30%">ITEM NAME</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="10%">UNIT</td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"   width="20%">RATE</td>
        </tr> -->
        <?php 
         while($row = $admin->fetch($result)) {
           
            $total_rate = $total_rate + $row['rate'];
            ?>
        <tr>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="40%"><?php echo $admin->getUniqueSupplierMasterById($row['supplier_id'])['supplier_name'];?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="30%"><?php echo $admin->getUniqueItemMasterById($row['item_id'])['item_name'];?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $admin->getUniqueItemMasterById($row['item_id'])['stock_unit']; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="20%"><?php  echo $row['rate'];?></td>
      </tr>
      <?php }?>
      <tr>
      <td width="100%" style="border-bottom:1px solid #000;"></td>
      </tr>
      <tr>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="40%"></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="30%"></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="20%"><?php  echo $total_rate?></td>
      </tr>
      </table>
   </body>
</html>
<?php 
	$invoiceMsg = ob_get_contents();
	ob_end_clean();
?>