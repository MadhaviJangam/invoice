<?php

$admin = new AdminFunctions();
	if(isset($admin)){
		$pdfObj = $admin;
   } 
   ob_start();
   if(isset($_GET['supplier_id'])){
       $supplier_id = $_GET['supplier_id'];
    }else{
        $supplier_id = '';
    }

   if(isset($_GET['from_date'])){  
       $from_date = $_GET['from_date'];
    }else{
        $from_date = '';
    }

   if(isset($_GET['to_date'])){
       $to_date = $_GET['to_date'];
    }else{
        $to_date = '';
    }

   if(isset($_GET['supplier_id'])){
       $supplier_id = $_GET['supplier_id'];
    }else{
        $supplier_id = '';
    }
    $query='';
    if($supplier_id=='' && $from_date=='' && $to_date=='' ){
        $query="SELECT * FROM ".PREFIX."purchase_bill_transaction WHERE purchase_no=''";
    }

    if($supplier_id!='' && $from_date=='' && $to_date=='' ){
        $query="SELECT * FROM ".PREFIX."purchase_bill_transaction WHERE purchase_no=''AND supplier_id='".$supplier_id."' ";
    }
    
    if($supplier_id!=''  && $from_date!='' && $to_date!='' ){
        $query="SELECT * FROM ".PREFIX."purchase_bill_transaction WHERE purchase_no='' AND supplier_id='".$supplier_id."' AND supplier_bill_date BETWEEN '".$from_date."' AND '".$to_date."' ";
    }

    if($supplier_id==''  && $from_date!='' && $to_date!='' ){
        $query="SELECT * FROM ".PREFIX."purchase_bill_transaction WHERE purchase_no='' AND supplier_bill_date BETWEEN '".$from_date."' AND '".$to_date."' ";
    }

    $result=$admin->query($query);
    $num_rows1 = mysqli_num_rows($result);
    $total_inv_amt=0;
    $total_recd=0;
    $total_bal=0;
    $total_amount=0;

  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   
   <body>
   <table border="0" style="width:100%">
        <!-- <tr style="">
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="15%">PURCHASE BILL NO</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="10%">DATE</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="30%">PARTY NAME</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="15%">CHALLAN NO</td>
            <td style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"width="10%">DATE</td>
            <td style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"width="20%">AMOUNT</td>
        </tr> -->

        <?php  
            while($row = $admin->fetch($result)) {
        ?>
        <tr style="">
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="15%"><?php echo $row['supplier_bill_no'];?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo date("d-m-Y", strtotime($row['supplier_bill_date']));?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="30%"><?php echo $admin->getUniqueSupplierMasterById($row['supplier_id'])['supplier_name'];?></td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="15%"><?php echo $row['delivery_challen_no']; ?></td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $row['delivery_challen_date']; ?></td>
            <td  style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="20%"><?php echo $row['final_amt']; ?></td>
        </tr>
               <?php } ?>
      </table>
   </body>
</html>
<?php 
	$invoiceMsg = ob_get_contents();
	ob_end_clean();
?>