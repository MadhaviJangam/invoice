<?php

$admin = new AdminFunctions();
	if(isset($admin)){
		$pdfObj = $admin;
   } 
   ob_start();
   if(isset($_GET['customer_id'])){
       $customer_id = $_GET['customer_id'];
    }else{
        $customer_id = '';
    }

   if(isset($_GET['from_date'])){  
       $from_date = $_GET['from_date'];
    }else{
        $from_date = '';
    }

   if(isset($_GET['to_date'])){
       $to_date = $_GET['to_date'];
    }else{
        $to_date = '';
    }

   if(isset($_GET['item_id'])){
       $item_id = $_GET['item_id'];
    }else{
        $item_id = '';
    }
    $query='';
    if($customer_id=='' && $item_id=='' && $from_date=='' && $to_date=='' ){
        $query="SELECT * FROM ".PREFIX."delivery_challan_transaction";
    }

    if($customer_id!='' && $item_id=='' && $from_date=='' && $to_date=='' ){
        $query="SELECT * FROM ".PREFIX."delivery_challan_transaction WHERE customer_id='".$customer_id."' ";
    }

    if($customer_id!='' && $item_id!='' && $from_date=='' && $to_date=='' ){
        $query="SELECT * FROM ".PREFIX."delivery_challan_transaction WHERE customer_id='".$customer_id."' ";

    }

    if($customer_id!='' && $item_id!='' && $from_date!='' && $to_date!='' ){
        $query="SELECT * FROM ".PREFIX."delivery_challan_transaction WHERE customer_id='".$customer_id."' AND challan_date BETWEEN '".$from_date."' AND '".$to_date."' ";
    }

    if($customer_id=='' && $item_id=='' && $from_date!='' && $to_date!='' ){
        $query="SELECT * FROM ".PREFIX."delivery_challan_transaction WHERE challan_date BETWEEN '".$from_date."' AND '".$to_date."' ";
    }

    if($customer_id=='' && $item_id!='' && $from_date!='' && $to_date!='' ){
        $query="SELECT * FROM ".PREFIX."delivery_challan_transaction WHERE challan_date BETWEEN '".$from_date."' AND '".$to_date."' ";
    }

    if($customer_id!='' && $item_id=='' && $from_date!='' && $to_date!='' ){
        $query="SELECT * FROM ".PREFIX."delivery_challan_transaction WHERE customer_id='".$customer_id."' AND challan_date BETWEEN '".$from_date."' AND '".$to_date."' ";
    }

    if($customer_id=='' && $item_id!='' && $from_date=='' && $to_date=='' ){
        $query="SELECT * FROM ".PREFIX."delivery_challan_transaction WHERE customer_id='".$customer_id."' ";
    }
    $result=$admin->query($query);
    $num_rows1 = mysqli_num_rows($result);
    $finalqtyTotal=0;
    $total_rate=0;
    $total_final_amt=0;
  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   
   <body>
   <table border="0" style="width:100%">
        <!-- <tr style="">
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="10%">BILL NO</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="10%">DATE</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="30%">PARTY NAME</td>
            <td style="text-align:left;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"   width="10%">ITEM NAME</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="10%">QTY</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="10%">UNIT</td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"   width="10%">RATE</td>
            <td style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"width="10%">AMOUNT</td>
        </tr> -->
        <?php 
            $qtyTotal = 0;
            $totalamount = 0;
            $total_gst_amt = 0;
           
              if($num_rows1==0){ 
                $itemDetail = $admin -> getUniqueChallanItemDetailsById2($item_id);            
                $tit_id_count=0;
               $counter=0;
                while($row = $admin->fetch($itemDetail)) {
                $tit_id =  $row['tit_id'];
                $challanDetails =  $admin-> getUniqueChallanDetailsById($tit_id); 
                if ($challanDetails['challan_date'] == '0000-00-00') {
                    $challan_date = '';
                 } else {
                     $challan_date = date("d-m-Y", strtotime($challanDetails['challan_date']));
                 }
                if($tit_id_count!=$tit_id){ ?>
                      <tr style="">
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $challanDetails['delivery_challan_no'];?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $challan_date;?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="30%"><?php echo $admin->getUniqueCustomerMasterById($challanDetails['customer_id'])['customer_name'];?></td>
            <td  colspan="12" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="50%"></td>
            </tr>
                <?php    $tit_id_count = $row['tit_id'];
                }
                
               ?>
         <!-- START  -->
         <?php 
         $our_state_code =  $admin -> getUniqueCompanyMasterById()['state_code'];
         $statename= $admin ->getUniqueCustomerMasterById($challanDetails['customer_id'])['statename'];
         $customer_state_code=$admin ->getStateCodeMasterByName($statename);
         $cgst=0;
         $sgst=0;
         $igst=0;
         if($our_state_code==$customer_state_code){
            $cgst= $challanDetails['gst_per']/2;
            $sgst= $challanDetails['gst_per']/2;
            $igst=0;
         }else{
            $cgst=0;
            $sgst=0;
            $igst=$challanDetails['gst_per'];
         }
         $totalamount = $totalamount + $row['amt'];


         $total_rate = $total_rate+$row['rate'];
         $finalqtyTotal=$finalqtyTotal+$row['qty'];
         $total_final_amt=$total_final_amt+$row['amt'];

        
         ?>
        <tr>  
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%" ></td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%" ></td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="30%" ></td>

            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $admin->getUniqueItemMasterById($row['item_id'])['item_name']; ?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $row['qty'];?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $row['unit'];?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $row['rate'];?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $row['amt'];?></td>

        </tr>
                   <?php  
                     $itemDetails12 = $admin -> getUniqueChallanItemDetailsById1($row['tit_id'],$item_id);
                     $num_rows12 = mysqli_num_rows($itemDetails12);
                     $counter++;
                     if(($num_rows12)==($counter)){
                        $counter=0;
                          ?>
                <tr>  
                    <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%"></td>
                    <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%"></td>
                    <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="30%" ></td>

                    <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
                    <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"> </td>
                    <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
                    <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
                    <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $totalamount; ?></td>

                </tr>


                  <?php     $qtyTotal=0;  
                            $totalamount = 0;
                            $cgstTotal = 0;
                            $sgstTotal = 0;
                            $igstTotal = 0;  }
                   
                   ?>
        <!-- END  -->
        
               <?php  }
                  ?>

        
           <?php } ?>

            <?php 

            
            while($row = $admin->fetch($result)) {
                if ($row['challan_date'] == '0000-00-00') {
                    $challan_date = '';
                 } else {
                     $challan_date = date("d-m-Y", strtotime($row['challan_date']));
                 }
                 ?>
                 <?php 
                        if($item_id!=''){
                            $itemDetailsss = $admin -> getUniqueChallanItemDetailsById1($row['id'],$item_id);            
                            $num_rows = mysqli_num_rows($itemDetailsss);
                            if($num_rows!=0){
                        ?>
            <tr style="">
                <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php 
            


            ?><?php echo $row['delivery_challan_no'];?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $challan_date;?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="30%"><?php echo $admin->getUniqueCustomerMasterById($row['customer_id'])['customer_name'];?></td>
            <td  colspan="12" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="50%"></td>
            </tr>
            <?php 
            $qtyTotal = 0;
            $totalamount = 0;
            $cgstTotal = 0;
            $sgstTotal = 0;
            $igstTotal = 0;
           
            if($item_id!=''){
                $itemDetails = $admin -> getUniqueChallanItemDetailsById1($row['id'],$item_id);
            }else{
                $itemDetails = $admin -> getUniqueChallanItemDetailsById($row['id']);
            }

         
           while($rows = $admin->fetch($itemDetails)) { 
        
            $totalamount = $totalamount + $rows['amt'];

            
            $total_rate = $total_rate+$rows['rate'];
            $finalqtyTotal=$finalqtyTotal+$rows['qty'];
            $total_final_amt=$total_final_amt+$rows['amt'];
               ?>
            <tr>  
                <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%" ></td>
                <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%" ></td>
                <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="30%" ></td>

                <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $admin->getUniqueItemMasterById($rows['item_id'])['item_name']; ?></td>
                <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $rows['qty'];?></td>
                <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $rows['unit'];?></td>
                <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $rows['rate'];?></td>
                <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $rows['amt'];?></td>
            </tr>
           <?php } ?>
           <?php 

$our_state_code =  $admin -> getUniqueCompanyMasterById()['state_code'];
$statename= $admin ->getUniqueCustomerMasterById($row['customer_id'])['statename'];
$customer_state_code=$admin ->getStateCodeMasterByName($statename);
$cgst=0;
$sgst=0;
$igst=0;
if($our_state_code==$customer_state_code){
   $cgst= $row['gst_per']/2;
   $sgst= $row['gst_per']/2;
   $igst=0;
}else{
   $cgst=0;
   $sgst=0;
   $igst=$row['gst_per'];
}

?>
           
          <tr>  
                <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%"></td>
                <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%"></td>
                <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="30%" ></td>

                <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
                <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"> </td>
                <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
                <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
                <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $totalamount; ?></td>

            </tr>
         <?php } }else{ ?>
            <tr style="">
                <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php 
            
            if ($row['challan_date'] == '0000-00-00') {
                $challan_date = '';
             } else {
                 $challan_date = date("d-m-Y", strtotime($row['challan_date']));
             }

            ?><?php echo $row['delivery_challan_no'];?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $challan_date;?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="30%"><?php echo $admin->getUniqueCustomerMasterById($row['customer_id'])['customer_name'];?></td>
            <td  colspan="12" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="50%"></td>
            </tr>
            <?php 
            $qtyTotal = 0;
            $totalamount = 0;
            $cgstTotal = 0;
            $sgstTotal = 0;
            $igstTotal = 0;
           
            if($item_id!=''){
                $itemDetails = $admin -> getUniqueChallanItemDetailsById1($row['id'],$item_id);
            }else{
                $itemDetails = $admin -> getUniqueChallanItemDetailsById($row['id']);
            }

         
           while($rows = $admin->fetch($itemDetails)) { 
            $totalamount = $totalamount + $rows['amt'];

            
            $total_rate = $total_rate+$rows['rate'];
            $finalqtyTotal=$finalqtyTotal+$rows['qty'];
            $total_final_amt=$total_final_amt+$rows['amt'];
               ?>
            <tr>  
                <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%" ></td>
                <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%" ></td>
                <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="30%" ></td>

                <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $admin->getUniqueItemMasterById($rows['item_id'])['item_name']; ?></td>
                <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $rows['qty'];?></td>
                <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $rows['unit'];?></td>
                <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $rows['rate'];?></td>
                <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $rows['amt'];?></td>

            </tr>
           <?php } ?>
           <?php 

$our_state_code =  $admin -> getUniqueCompanyMasterById()['state_code'];
$statename= $admin ->getUniqueCustomerMasterById($row['customer_id'])['statename'];
$customer_state_code=$admin ->getStateCodeMasterByName($statename);
$cgst=0;
$sgst=0;
$igst=0;
if($our_state_code==$customer_state_code){
   $cgst= $row['gst_per']/2;
   $sgst= $row['gst_per']/2;
   $igst=0;
}else{
   $cgst=0;
   $sgst=0;
   $igst=$row['gst_per'];
}
?>
            
            <tr>  
                <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%"></td>
                <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%"></td>
                <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="30%" ></td>

                <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
                <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"> </td>
                <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
                <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
                <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $totalamount; ?></td>

            </tr>


         <?php } }  ?>
         <tr>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;" width="100%">&nbsp;&nbsp;</td>
         </tr>
         <tr>  
                <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%"></td>
                <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="10%"></td>
                <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="30%" ></td>

                <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
                <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $finalqtyTotal?></td>
                <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
                <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $total_rate?></td>
                <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $total_final_amt; ?></td>

            </tr>
      </table>
   </body>
</html>
<?php 
	$invoiceMsg = ob_get_contents();
	ob_end_clean();
?>