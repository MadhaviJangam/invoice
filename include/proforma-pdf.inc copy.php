<?php

$admin = new AdminFunctions();
	if(isset($admin)){
		$pdfObj = $admin;
   } 
   ob_start();

   $ProformaDetails=$admin->getUniqueProformaInvoiceDetailsById($_GET['id']);
   $customerDetails=$admin->getUniqueCustomerMasterById($ProformaDetails['customer_id']);
   $getStateDetails=$admin->getStateCodeMasterByName($customerDetails['statename']);
   $getItemDetailTOTAL= $admin->getUniqueProformaInvoiceDetailsById($_GET['id']);
   $getTaxInvoiceItemDetails=$admin->getUniqueProformaInvoiceItemDetailsByTaxInvoiceId($_GET['id']);
   $rowcount=mysqli_num_rows($getTaxInvoiceItemDetails);
   $getNumberToText=$admin->getIndianCurrency($getItemDetailTOTAL['final_amt']);
   $getItemPerDetails=$admin->getUniqueProformaInvoiceGSTDetailsByTaxInvoiceId($_GET['id']);
   $companyInfo=$admin->getUniqueCompanyMasterById();

   $arrayHSNcode=array();

if ($ProformaDetails['delivery_challen_date'] == '0000-00-00') {
   $delivery_challen_date = '';
} else {
    $delivery_challen_date = date("d-m-Y", strtotime($ProformaDetails['delivery_challen_date']));
}
if ($ProformaDetails['buyer_order_date'] == '0000-00-00') {
    $buyer_order_date = '';
 } else {
     $buyer_order_date = date("d-m-Y", strtotime($ProformaDetails['buyer_order_date']));
 }
 if ($ProformaDetails['lr_date'] == '0000-00-00') {
    $lr_date = '';
 } else {
     $lr_date = date("d-m-Y", strtotime($ProformaDetails['lr_date']));
 }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   
   <body>
   <table border="0" style="width:100%">
        <tr> 
            <td style="text-align:center;">Proforma Invoice</td>
        </tr>
        <tr>
            <td colspan="5" style="border-top:1px solid #000;border-left:1px solid #000;color:#f26d00;font-size:20px;width:75%;text-align:center;"><?php echo $companyInfo['company_name'];?></td>
            <td rowspan="2" style="width:25%;border-top:1px solid #000;border-right:1px solid #000;"><img style="border-top:10px solid #000;"></td>
        </tr>
        <tr>
            <td colspan="5" style="text-align:center;border-left:1px solid #000;">GSTN : <?php echo $companyInfo['gst_no'];?></td>
        </tr>
        <tr>
            <td colspan="6" style="text-align:center;border-left:1px solid #000;border-right:1px solid #000;"><?php echo $companyInfo['company_address'];?></td>
        </tr>
        <tr>
            <td colspan="3" style="text-align:left;border-left:1px solid #000;border-bottom:1px solid #000;">Mob No:<?php echo $companyInfo['mobile_no'];?></td>
            <td colspan="6" style="text-align:right;border-right:1px solid #000;border-bottom:1px solid #000;"><?php echo $companyInfo['email_id'];?></td>
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" colspan="3" width="50%">Buyer (Billed to)<br maxlength=50><?php echo substr($customerDetails['customer_name'] ,"0", 100); ?></td>  
            <td style="text-align:left;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="25%">Proforma No<br><?php echo $ProformaDetails['proforma_no']; ?></td>  
            <td style="text-left:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="25%">Proforma Date<br><?php echo date("d-m-Y", strtotime($ProformaDetails['proforma_date'])); ?></td>  
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" colspan="3" width="50%"><br><?php echo $customerDetails['billing_address']; ?></td>  
            <td style="text-align:left;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="25%">Delivery Challan<br><?php echo $ProformaDetails['delivery_challen_no']; ?></td> 
            <td style="text-align:left;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="25%">D.C Date<br><?php if($ProformaDetails['delivery_challen_no']!=''){ echo $delivery_challen_date; }?></td>   
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" colspan="3" width="50%">State: <?php echo $customerDetails['statename'];?>&nbsp;&nbsp;&nbsp;State code:<?php echo $getStateDetails['statecode']; ?><br>GST No:  <?php echo $customerDetails['company_gst']; ?></td>  
            <td style="text-align:left;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="25%">Buyres Order No<br><?php echo $ProformaDetails['buyer_order_on']; ?></td> 
            <td style="text-align:left;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="25%">Date<br><?php if($ProformaDetails['buyer_order_on']!='') {echo $buyer_order_date; }?></td>   
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" colspan="3" width="50%">Consignee (Shipped to)<br><?php echo $customerDetails['customer_name']; ?></td>  
            <td style="text-align:left;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="25%">Vehicle No<br><?php echo $ProformaDetails['vehicle_no']; ?></td> 
            <td style="text-align:left;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="25%">E.Way Bill No.<br><?php echo $ProformaDetails['e_way_bill_no']; ?></td>   
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" colspan="3" width="50%"><br><?php echo $customerDetails['shipping_address']; ?></td>  
            <td style="text-align:left;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="25%">LR No<br><?php echo $ProformaDetails['lr_no']; ?></td> 
            <td style="text-align:left;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="25%">LR Date<br><?php if($ProformaDetails['lr_no']!=''){ echo $lr_date; }?></td> 
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" colspan="3" width="50%">State:<?php echo $customerDetails['statename'];?>&nbsp;&nbsp;&nbsp;State code:<?php echo $getStateDetails['statecode'];?><br>GST No: <?php echo $customerDetails['company_gst']; ?><br>Contact No:<?php echo$customerDetails['contact_person_phone']; ?><br>Email-ID:<?php echo $customerDetails['contact_person_email']; ?></td>  
            <td style="text-align:left;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="50%">Terms Of Payment<br><?php echo nl2br($ProformaDetails['terms_of_payment']); ?></td>    
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="5%">SR.NO</td>
            <td  style="text-align:left;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="35%">DESCRIPTION OF GOODS AND SERVICE</td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="13%">HSN CODE</td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%">QTY</td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="12%">RATE</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%">UNIT</td>
            <td style="text-align:right;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="15%">TAXABLE VALUE </td>  
        </tr>
        <?php
             $x= 1;   
             
            while ($row = $admin->fetch($getTaxInvoiceItemDetails)) {
                $getItemDetails= $admin->getUniqueItemMasterById($row['item_id']);
                array_push($arrayHSNcode,$getItemDetails['hsn_code']);
        ?>
        <tr style="font-size:10px;">
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"><?php echo $x; ?></td>
            <td  style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="35%"><?php  echo $getItemDetails['item_name'];?></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="13%"><?php  echo $getItemDetails['hsn_code'];?></td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $row['qty'];?></td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"><?php  echo $row['rate'];?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $row['unit'];?></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;" width="15%"><?php  echo $row['net_amt'];?></td>  
        </tr>

            <?php
            $x++;  
                }
            ?>
            <?php 
            $rowprint= 10 - $rowcount;
            for($i=1;$i<=$rowprint;$i++){ ?>
            <tr style="font-size:10px;">
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="35%"></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="13%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="15%"></td>
        </tr>
            <?php } ?>
        <tr style="font-size:10px;">
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="35%"></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="13%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="15%"><?php echo$getItemDetailTOTAL['total_net_amt'];?></td>  
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"></td>
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="35%"></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="13%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:center;border-right:1px solid #000;border-left:1px solid #000;" width="15%"></td>  
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"></td>
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="35%"><b>Less Discount</b></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="13%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;" width="15%"><?php echo$getItemDetailTOTAL['total_after_disc_amt'];?></td>  
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"></td>
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="35%"><b>Transport Charges</b></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="13%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;" width="15%"><?php echo$getItemDetailTOTAL['service_charges'];?></td>  
        </tr>
        <?php  if($getItemPerDetails['cgst_per']!='0.00'){ ?>

        <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"></td>
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="35%"><b>CGST</b></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="13%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"><?php echo $getItemPerDetails['cgst_per'];?> </td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;" width="15%"><?php echo $admin->formatAmount($ProformaDetails['total_cgst_amt']+ $ProformaDetails['service_charges_cgst_amt']); ?></td>  
        </tr>
        <?php } ?>
        <?php  if($getItemPerDetails['sgst_per']!='0.00'){ ?>
        <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"></td>
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="35%"><b>SGST</b></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="13%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"><?php echo $getItemPerDetails['cgst_per'];?> </td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;" width="15%"><?php echo $admin->formatAmount($ProformaDetails['total_sgst_amt']+ $ProformaDetails['service_charges_sgst_amt']);?></td>  
        </tr>
        <?php } ?>
        <?php  if($getItemPerDetails['igst_per']!='0.00'){ ?>
        <tr>  
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"></td>
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="35%"><b>IGST</b></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="13%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"><?php echo $getItemPerDetails['igst_per'];?> </td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;" width="15%"><?php echo $admin->formatAmount($ProformaDetails['total_igst_amt']+ $ProformaDetails['service_charges_igst_amt']); ?></td>  
        </tr>
       <?php } ?>    
        <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;border-left:1px solid #000;" width="40%">Total</td>
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="13%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="10%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="12%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="10%"></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;" width="15%"><?php echo $admin->formatAmount($getItemDetailTOTAL['final_amt']);?></td>  
        </tr>
        <tr style="font-size:10px;">
            <td colspan="6" style="text-align:left;padding:0px;border-right:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;border-left:1px solid #000;" width="100%">Amount Chargable (In Words) : <br><?php echo ($getNumberToText); ?> Only </td>
        </tr>
        <tr style="font-size:10px;">
            <td rowspan="2" style="text-align:center;padding:0px;border-center:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;border-left:1px solid #000;" width="25%">HSN / SAC</td>
            <td rowspan="2" style="text-align:center;padding:0px;border-center:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="15%">TAXABLE VALUE</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="20%">CGST</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="20%">SGST</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="20%">IGST</td>
        </tr>
        <tr style="font-size:10px;">
        <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="10%">RATE</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="10%">AMOUNT</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="10%">RATE</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="10%">AMOUNT</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="10%">RATE</td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="10%">AMOUNT</td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;border-left:1px solid #000;" width="25%"><?php  $arrayHSNcode = array_unique($arrayHSNcode); 
        foreach ($arrayHSNcode as $key => $value) {
        if($key==0){
            echo $value;
        }else{
            echo', '.$value;
        }
        }                
            
            ?></td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="15%"><?php echo $ProformaDetails['total_net_amt'];?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="10%"><?php echo $getItemPerDetails['cgst_per'];?></td> 
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="10%"><?php echo $ProformaDetails['total_cgst_amt'];?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="10%"><?php echo $getItemPerDetails['sgst_per'];?></td> 
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="10%"><?php echo $ProformaDetails['total_sgst_amt'];?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="10%"><?php echo $getItemPerDetails['igst_per'];?> </td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="10%"><?php echo $ProformaDetails['total_igst_amt'];?></td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;border-left:1px solid #000;" width="25%">Transport Charges</td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="15%"><?php echo $ProformaDetails['service_charges'];?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="10%"><?php echo $getItemPerDetails['cgst_per'];?></td> 
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="10%"><?php echo $ProformaDetails['service_charges_cgst_amt'];?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="10%"><?php echo $getItemPerDetails['sgst_per'];?> </td> 
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="10%"><?php echo $ProformaDetails['service_charges_sgst_amt'];?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="10%"><?php echo $getItemPerDetails['igst_per'];?> </td> 
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="10%"><?php echo $ProformaDetails['service_charges_igst_amt'];?></td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:left;padding:0px;border-right:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;border-left:1px solid #000;" width="25%"></td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="15%"><?php echo $admin->formatAmount($ProformaDetails['total_net_amt']+$ProformaDetails['service_charges']);?></td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="20%"><?php echo $admin->formatAmount($ProformaDetails['total_cgst_amt']+ $ProformaDetails['service_charges_cgst_amt']);?>&nbsp;&nbsp;</td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="20%"><?php echo $admin->formatAmount($ProformaDetails['total_sgst_amt']+ $ProformaDetails['service_charges_sgst_amt']); ?>&nbsp;&nbsp;</td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="20%"><?php echo $admin->formatAmount($ProformaDetails['total_igst_amt']+ $ProformaDetails['service_charges_igst_amt']); ?>&nbsp;&nbsp;</td>
        </tr>
        <tr style="font-size:10px;">
            <td colspa="5" style="text-align:left;padding:0px;border-right:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;border-left:1px solid #000;" width="100%">Tax Amount In Words : <?php echo $admin->getIndianCurrency($ProformaDetails['total_cgst_amt']+ $ProformaDetails['service_charges_cgst_amt']+$ProformaDetails['total_sgst_amt']+ $ProformaDetails['service_charges_sgst_amt']+$ProformaDetails['total_igst_amt']+ $ProformaDetails['service_charges_igst_amt']); ?> Only</td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:left;padding:0px;border-right:1px solid #000;border-top:1px solid #000;border-left:1px solid #000;" width="60%">Bank Details</td>
            <td style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="40%"><br>FOR&nbsp;&nbsp;<?php echo $companyInfo['company_name'];?></td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="60%">NAME :&nbsp;<?php echo $companyInfo['bank_name'];?>.</td>
            <td style="text-align:left;padding:0px;border-right:1px solid #000;"  width="40%"></td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="60%">ADDRESS :&nbsp;<?php echo $companyInfo['bank_address'];?></td>
            <td style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="40%"></td>
        </tr>
        
        <tr style="font-size:10px;">
            <td  style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;" width="60%">A/c No. :&nbsp;<?php echo $companyInfo['account_no'];?></td>
            <td style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="40%"><?php echo $companyInfo['designation'];?></td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:left;padding:0px;border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;" width="60%">IFSC Code :&nbsp;<?php echo $companyInfo['ifsc_code'];?></td>
            <td style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="40%">Authorised Singnatory</td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center;padding:0px;border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;font-size:10px;" width="100%">Factory:<span style="font-size:8px;"><?php echo $companyInfo['factory_address'];?></span></td>
        </tr>
        <tr style="font-size:8px;">
            <td  style="text-align:left;padding:0px;border-left:1px solid #000;border-bottom:1px solid #000;" width="60%">This Is Computer Generated Invoice</td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;"  width="40%">Subject To <?php echo $companyInfo['subject_to'];?> Jurisdiction</td>
        </tr>
      </table>
   </body>
</html>
<?php 
	$invoiceMsg = ob_get_contents();
	ob_end_clean();
?>