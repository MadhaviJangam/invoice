<?php

$admin = new AdminFunctions();
	if(isset($admin)){
		$pdfObj = $admin;
   } 
   ob_start();

   $quotationDetails=$admin->getUniqueQuotationDetailsById($_GET['id']);
   $customerDetails=$admin->getUniqueCustomerMasterById($quotationDetails['customer_id']);
   $getStateDetails=$admin->getStateCodeMasterByName($customerDetails['statename']);
   $ItemDetails=$admin->getUniqueQuotationItemDetailsByTaxInvoiceId($_GET['id']);
   $rowcount=mysqli_num_rows($ItemDetails);
   $companyInfo=$admin->getUniqueCompanyMasterById();
   

   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   <body>
   <table border="0" style="width:100%">
        <tr> 
            <td style="text-align:center;">Quotation</td>
        </tr>
        <tr>
            <td colspan="5" style="border-top:1px solid #000;border-left:1px solid #000;font-size:20px;width:75%;text-align:center;"><b><?php echo $companyInfo['company_name'];?></b></td>
            <td rowspan="2" style="width:25%;border-top:1px solid #000;border-right:1px solid #000;"></td>
        </tr>
        <tr>
            <td colspan="5" style="text-align:center;border-left:1px solid #000;"><b>GSTN : <?php echo $companyInfo['gst_no'];?></b></td>
        </tr>
        <tr>
            <td colspan="6" style="text-align:center;border-left:1px solid #000;border-right:1px solid #000;">Office: <?php echo $companyInfo['company_address'];?></td>
        </tr>
        <tr>
            <td colspan="3" style="text-align:left;border-left:1px solid #000;border-bottom:1px solid #000;">Mob No:<?php echo $companyInfo['mobile_no'];?></td>
            <td colspan="6" style="text-align:right;border-right:1px solid #000;border-bottom:1px solid #000;"><?php echo $companyInfo['email_id'];?></td>
        </tr>
        <tr>
            <td style="text-align:right;border-left:1px solid #000;border-right:1px solid #000;" width="100%"></td>
        </tr>
        <tr>
            <td style="text-align:right;border-left:1px solid #000;border-right:1px solid #000;" width="100%">Quotation No:&nbsp;<b><?php echo $quotationDetails['quotation_no']; ?></b></td>
        </tr>
        <tr>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;" width="100%">Quotation Date:&nbsp;<b><?php echo date("d-m-Y", strtotime($quotationDetails['quotation_date'])); ?></b></td>
        </tr>
        <tr>
            <td style="text-align:right;border-left:1px solid #000;border-right:1px solid #000;" width="100%"></td>
        </tr>
        <tr>
            <td style="text-align:let;border-right:1px solid #000;border-left:1px solid #000;font-size:15px;" width="100%">To</td>  
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;font-size:15px;" width="100%"><?php echo $customerDetails['customer_name']; ?></td>  
        </tr>
        <tr>
            <td style="text-align:left;border-right:1px solid #000;border-left:1px solid #000;" width="%"><?php echo $customerDetails['billing_address']; ?></td>  
        </tr>
        <tr>
            <td style="text-align:right;border-left:1px solid #000;border-right:1px solid #000;" width="100%"></td>
        </tr>
        <tr>
            <td style="text-align:center;border-left:1px solid #000;border-right:1px solid #000;font-size:12px;" width="100%"><b>QUOTATION FOR THE FOLLOWING ITEMS AS ENQUIRE BY YOU</b></td>
        </tr>
        <tr>
            <td style="text-align:right;border-left:1px solid #000;border-right:1px solid #000;" width="100%"></td>
        </tr>
        <tr style="font-size:10px;" style="align:center">
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-top:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="9%"><b>SR.NO</b></td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-top:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="35%"><b>PARTICULARS</b></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-top:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"><b>QUANTITY</b></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-top:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"><b>RATE</b></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-top:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"><b>UNIT</b></td>
            <td style="text-align:right;border-bottom:1px solid #000;border-top:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="20%"><b>AMOUNT</b></td>  
        </tr>
        <?php
              $x= 1;   
            while ($row = $admin->fetch($ItemDetails)) {
                $QuotationItemDetails= $admin->getUniqueItemMasterById($row['item_id']);
                 
        ?> 
        <tr style="font-size:10px;">
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="9%"><?php echo $x; ?></td>
            <td  style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="35%"><?php  echo $QuotationItemDetails['item_name']; ?></td>
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="12%"><?php echo $row['qty'];?></td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="12%"><?php echo $row['rate']; ?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="12%"><?php echo $row['unit']; ?></td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="20%"><?php echo $row['amt']; ?></td>
        </tr>
            <?php
            $x++;  
               }

            ?> 
        <?php 
        $rowprint = 23 - $rowcount;
        for($i=1;$i<=$rowprint;$i++){?>
        <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="9%"></td>
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="35%"></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"></td>
            <td style="text-align:center;border-right:1px solid #000;border-left:1px solid #000;" width="20%"></td> 
        </tr>
        <?php } ?>
        <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="9%"></td>
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="35%"><b>Total</b></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="12%"></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;" width="20%"><?php echo $quotationDetails['total_amt']; ?></td> 
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="9%"></td>
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="35%"><b>Total GST</b></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="12%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="12%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="12%"></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="20%"><?php echo $quotationDetails['total_gst_amt']; ?></td> 
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="9%"></td>
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="35%"><b>Grand Total</b></td>
            <td  style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="12%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="12%"></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="12%"></td>
            <td style="text-align:right;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="20%"><?php echo $admin->formatAmount($quotationDetails['total_amt']+$quotationDetails['total_gst_amt']);?></td> 
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"  width="100%"></td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:left;padding:0px;border-right:1px solid #000;border-left:1px solid #000;font-size:12px;" width="100%"><b>Terms & Conditions : </b></td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:left;padding:0px;border-left:1px solid #000;" width="70%"><?php echo nl2br($quotationDetails['terms_and_condition']);?></td>
            <td style="text-align:center;padding:0px;border-right:1px solid #000;"  width="30%"><b>FOR&nbsp;&nbsp;<?php echo $companyInfo['company_name'];?></b></td>
        </tr>
        <!-- <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="100%"><?php echo $companyInfo['designation'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0x;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;" width="100%">Properitor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr> -->
        <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;"  width="100%"></td>
        </tr>
        <tr style="font-size:10px;">
            <td style="text-align:center;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;" width="100%">Factory: <span style="font-size:8px;"><?php echo $companyInfo['factory_address'];?></span></td>
        </tr>
      </table>
   </body>
</html>
<?php 
	$invoiceMsg = ob_get_contents();
	ob_end_clean();
?>