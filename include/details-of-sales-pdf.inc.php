<?php

$admin = new AdminFunctions();
	if(isset($admin)){
		$pdfObj = $admin;
   } 
   ob_start();

   $invoiceDetails=$admin->getUniqueTaxInvoiceDetailsById($_GET['customer_id']);
   $customerDetails=$admin->getUniqueCustomerMasterById($invoiceDetails['customer_id']);
   $getItemDetailTOTAL= $admin->getUniqueTaxInvoiceDetailsById($_GET['customer_id']);
   $getTaxInvoiceItemDetails=$admin->getUniqueTaxInvoiceItemDetailsByTaxInvoiceId($_GET['customer_id'],$_GET['item_id'],$_GET['from_date'],$_GET['to_date']);

    $customer_id= $_GET['customer_id'];
    $from_date = $_GET['from_date'];
    $to_date = $_GET['to_date'];
    $item_id = $_GET['item_id'];
    $customer = "select * from ".PREFIX."tax_invoice_transaction WHERE customer_id='".$customer_id."'";

    $item = "select * from ".PREFIX."tax_invoice_item_transaction WHERE item_id='".$item_id."'";


    if ($invoiceDetails['invoice_date'] == '0000-00-00') {
        $invoice_date = '';
    } else {
        $invoice_date = date("d-m-Y", strtotime($invoiceDetails['invoice_date']));
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   
   <body>
   <table border="0" style="width:100%">
        <tr style="font-size:10px;">
            <td rowspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="5%">BILL NO</td>
            <td rowspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="5%">DATE</td>
            <td rowspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="10%">PARTY NAME</td>
            <td  rowspan="2" style="text-align:left;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="10%">ITEM NAME</td>
            <td  rowspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="8%">QTY</td>
            <td rowspan="2" style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="8%">RATE</td>
            <td rowspan="2" style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="8%">UNIT</td>
            <td rowspan="2"style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="10%">AMOUNT</td>
            <td style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="12%">CGST</td>  
            <td style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="12%">SGST </td>  
            <td style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="12%">IGST </td>
            <!-- <td rowspan="2" style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="6%">BILL AMOUNT</td>     -->
        </tr>
        <tr style="font-size:10px;">
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="5%">Rate</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="7%">Amount</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="5%">Rate</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="7%">Amount</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="5%">Rate</td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="7%">Amount</td>
        </tr>
        <?php
            
             
            while ($row = $admin->fetch($getTaxInvoiceItemDetails)) {
                //while ($rows = "select * from ".PREFIX."tax_invoice_transaction WHERE customer_id='".$customer_id."'") {
                //echo $rows;
                $getItemDetails= $admin->getUniqueItemMasterById($row['item_id']);

                
        ?>
        <tr style="font-size:9px;font-family: 'Times New Roman', Times, serif;">
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"><?php echo $invoiceDetails['invoice_no'];?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"><?php echo $invoice_date;?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $customerDetails['customer_name'];?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php echo $getItemDetails['item_name']; ?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="8%"><?php  echo $row['qty'];?></td>
            <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="8%"><?php  echo $row['rate'];?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="8%"><?php  echo $row['unit'];?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"><?php  echo $row['net_amt'];?></td>

            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"><?php echo $row['cgst_per']; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="7%"><?php echo $row['cgst_amt']; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"><?php echo $row['sgst_per']; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="7%"><?php echo $row['cgst_amt']; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="5%"><?php echo $row['igst_per']; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="7%"><?php echo $row['igst_amt'];?> </td>
        </tr>
        <?php
            //} 
        }   
            
        ?>
      
         <tr style="font-size:10px;">
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;border-left:1px solid #000;" width="38%">Total</td>
            <td  style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="26%"></td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="12%"></td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="12%"></td>
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;"  width="12%"></td>
               
        </tr>
        <tr style="font-size:10px;">
            <td style="text-align:right;padding:0px;border-right:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;" width="100%">Total:<span style="text-align:right"></span></td>
        </tr> 
        
      </table>
   </body>
</html>
<?php 
	$invoiceMsg = ob_get_contents();
	ob_end_clean();
?>