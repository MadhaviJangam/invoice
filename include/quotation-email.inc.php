<?php

$admin = new AdminFunctions();
	if(isset($admin)){
		$pdfObj = $admin;
   } 
   ob_start();

   $quotationDetails=$admin->getUniqueQuotationDetailsById($_GET['id']);
   $customerDetails=$admin->getUniqueCustomerMasterById($quotationDetails['customer_id']);
   $getStateDetails=$admin->getStateCodeMasterByName($customerDetails['statename']);
   $ItemDetails=$admin->getUniqueQuotationItemDetailsByTaxInvoiceId($_GET['id']);
   

   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   <body>
   <table border="1" style="width:100%">
    <tr>
        <td colspan="5" width="100%">
            Unique Software<br>
            office no 213, ambica complex,<br>
            vasai East.
        </td>
    </tr>
    <tr>
        <td colspan="5" style="text-align:center" width="100%">
            <span>QUOTATION INVOICE</span>
    </td>
    </tr>
    <tr>
        <td  colspan="3" width="50%">
            Quotation No :  <?php echo $quotationDetails['quotation_no']; ?> <br>
            Quotation Date : <?php echo date("d-m-Y", strtotime($quotationDetails['quotation_date'])); ?><br>
            
        </td>
        <td colspan="3" width="50%"></td>
        
    </tr>
    <tr>
            <td colspan="5" width="100%">Bill To : </td>
    </tr>
    <tr>
            <td width="10%">Sr No.</td>
            <td width="40%">Particulars	</td>
            <td width="10%">Qty</td>
            <td width="20%">rate</td>
            <td width="20%">(Amount+GST)</td>
    </tr>
    <?php
    $total=0;
                $x= 1;   
                while ($row = $admin->fetch($ItemDetails)) {
                    $QuotationItemDetails= $admin->getUniqueItemMasterById($row['item_id']);
                    
            ?>
    <tr>
            <td width="10%"><?php echo $x; ?></td>
            <td width="40%"><?php  echo $QuotationItemDetails['item_name'];?></td>
            <td width="10%"><?php  echo $row['qty'];?></td>
            <td width="20%"><?php  echo $row['rate'];?></td>
            <td width="20%"><?php  echo $row['amt'];?></td>
    </tr>
    <?php
    $total+=$row['amt'];
                $x++;  
                    }

                ?>
    <tr>
        <td rowspan="2" colspan="3">
            Total In Words <br>
            Indian Rupee <?php echo $admin->getIndianCurrency($quotationDetails['total_amt']+ $quotationDetails['total_gst_amt']); ?> Only <br>
            Thanks for your business. 
        </td>
        <td colspan="2">
            Total : <?php echo $total; ?><br>
            Grand Total :	<?php echo $admin->formatAmount($quotationDetails['total_amt']+$quotationDetails['total_gst_amt']);?>
        </td>
        
    </tr>

<tr>    

        <td colspan="2"><br>   <br>   <br>   <br>   <br>   <br>Authorized Signature
</td>
 
</tr>

      </table>
   </body>
</html>
<?php 
	$invoiceMsg1 = ob_get_contents();
	ob_end_clean();
?>