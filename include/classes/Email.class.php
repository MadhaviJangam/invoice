<?php
	/*
	 * v1 - basic email class
	 * v1.1 - removed support for ADMIN email
	 *
	 * TEMPLATE CODE TO IMPLEMENT CLASS
		$newCustomerDetails = $user->addSomething($_POST);
		include_once("member-register-email.inc.php");
		include_once("Email.class.php");
		$emailObj = new Email();
		$emailObj->setAddress($email);
		$emailObj->setSubject("Welcome to example.com");
		$emailObj->setEmailBody($emailMsg);
		$emailObj->sendEmail();
	 *
	 */
	require("smtp/class.phpmailer.php");
	require 'aws/aws-autoloader.php';

	use Aws\Ses\SesClient;
	use Aws\Ses\Exception\SesException;
	
	class Email{
		private $to;
		// private $admin = ADMIN_EMAIL; // DEPRECATED
		private $admin = '';
		private $from = "support@cabiznet.com";
		private $subject;
		private $path;
		function setAddress($to){
			$this->to = $to;
		}
		function setAdminAddress($to){
			$this->admin = $to;
		}
		function setFromAddress($from){
			$this->from = $from;
		}
		function setSubject($subject){
			$this->subject = $subject;
		}
		function setEmailBody($msg){
			$this->msg = $msg;
		}
		function attachment($path /*,$filename,$encodedData,$type*/ ){
			$this->path = $path;
			/* $this->filename = $filename;
			$this->encodedData = $encodedData;
			$this->type = $type; */
		}
		function sendEmail(){
			$client = SesClient::factory(array(
				'version'=> 'latest',     
				'region' => 'ap-south-1',
				'credentials' => [
					'key'    => "AKIAIZZHBMWHKCBW6EAA",
					'secret' => "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",
				],
				'correctClockSkew' => true
			));
			try {
				$result = $client->sendEmail([
					'Destination' => [
						'ToAddresses' => [
							$this->to,
						],
					],
					'Message' => [
						'Body' => [
							'Html' => [
								'Charset' => 'UTF-8',
								'Data' => $this->msg,
							],
							'Text' => [
								'Charset' => 'UTF-8',
								'Data' => '',
							],
						],
						'Subject' => [
							'Charset' => 'UTF-8',
							'Data' => $this->subject,
						],
					],
					'Source' => $this->from,
					// If you are not using a configuration set, comment or delete the
					// following line
					// 'ConfigurationSetName' => CONFIGSET,
				]);
				$messageId = $result->get('MessageId');
				// echo("Email sent! Message ID: $messageId"."\n");
				// exit;
				return true;

			} catch (SesException $error) {
				// echo("The email was not sent. Error message: ".$error->getAwsErrorMessage()."\n");
				return false;
			}
		}
	}
?>