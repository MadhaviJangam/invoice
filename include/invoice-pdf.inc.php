<?php
	if(isset($admin)){
		$pdfObj = $admin;
   } 
   ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   <body>
   <table border="0" style="width:100%">
      <tr>
      <td rowspan="5" style="width:20%;border:1px solid #000;" ><img style="border-top:10px solid #000;" src="/image/logo.png"></td>
      <td colspan="2" style="border-top:1px solid #000;border-right:1px solid #000;color:#f26d00;font-size:24px;width:80%;text-align:center;">SHREE SAINATH ROADLINES</td>
   
      </tr>
      <tr>
      <td colspan="2" style="text-align:center;border-right:1px solid #000;">BUILDING NO. 53, GALA NO. 1, 2, 3 ARIHANT COMPLEX, KOPER VILLAGE, BHIWANDI</td>
     
      </tr>
      <tr>
      <td colspan="2" style="text-align:center;border-right:1px solid #000;">TEL. : +91 9923585366 / 9850085162</td>
     
      </tr>
      <tr>
      <td colspan="2" style="text-align:center;border-right:1px solid #000;">PAN NO. : ACGPL7571F</td>
     
      </tr>
      <tr>
      <td colspan="2" style="text-align:center;border-right:1px solid #000;border-bottom:1px solid #000;">GSTN : 27ACGPL7571F2ZD</td>
      </tr>

      <tr>
      <td colspan="3" style="border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;"></td>  
      </tr>

      <tr>
      <td colspan="3" style="border-right:1px solid #000;font-size:14px;text-align:center;background-color:#92a9d6;border-left:1px solid #000;border-bottom:1px solid #000;">Tax Invoice</td>  
      </tr>
  
      <tr>
      <td style="border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" colspan="2" width="50%"><div></div> Invoice No. : SSR/2020-21/0001<br></td>  
      <td style="border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="50%"><div></div>Invoice Date : 01-01-2021<br></td>  
      </tr>

      <tr>
      <td colspan="3" style="border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;"></td>  
      </tr>
      <tr>
      <td style="background-color:#92a9d6;text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" colspan="2" width="50%"> Details of Receiver (Bill to Party)</td>  
      <td style="background-color:#92a9d6;text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="50%">Our Bank Details</td>  
      </tr>
      
      <tr>
      <td style="border-right:1px solid #000;border-left:1px solid #000;" colspan="2" width="50%"> Billing Party Name</td>  
      <td style="border-right:1px solid #000;border-left:1px solid #000;" width="50%">Bank Name &nbsp;&nbsp;: HDFC BANK LTD.</td>  
      </tr>

      <tr>
      <td style="border-right:1px solid #000;border-left:1px solid #000;" colspan="2" rowspan="3" width="50%"> Address1 <br> Address2 <br> Address3</td>  
      <td style="border-right:1px solid #000;border-left:1px solid #000;" width="50%">Bank A/C &nbsp;&nbsp;&nbsp;: 04887630000277</td>  
      </tr>

      <tr>
      <td style="border-right:1px solid #000;border-left:1px solid #000;" width="50%">Bank IFSC &nbsp;&nbsp;: HDFC0000814</td>  
      </tr>
      <tr>
      <td style="border-right:1px solid #000;border-left:1px solid #000;" width="50%">Bank Branch : Ghodbunder Road Branch, Thane</td>  
      </tr>
     
      <tr>
      <td style="border-right:1px solid #000;border-left:1px solid #000;" colspan="2"  width="50%"> GSTN &nbsp;: </td>  
      <td style="border-right:1px solid #000;border-left:1px solid #000;"  width="50%"></td>  
      </tr>

      <tr>
      <td style="border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" colspan="2"  width="50%"> STATE : </td>  
      <td style="border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="50%"></td>  
      </tr>
      <tr>
      <td colspan="3" style="border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;"></td>  
      </tr>
      <tr style="font-size:9px;">
      <td rowspan="2" style="background-color:#92a9d6;text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%">SAC CODE</td>
      <td  rowspan="2" style="background-color:#92a9d6;text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="24%">PARTICULARS</td>
      <td  rowspan="2" style="background-color:#92a9d6;text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%">FERIGHT</td>
      <td style="background-color:#92a9d6;text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="12%">CGST</td>
      <td style="background-color:#92a9d6;text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="12%">SGST</td>
      <td style="background-color:#92a9d6;text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="12%">IGST</td>
      <td  rowspan="2" style="background-color:#92a9d6;text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;" width="20%">Total</td>  
      </tr>
      <tr style="font-size:7px;">

      <td style="background-color:#92a9d6;text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%">Rate</td>
      <td style="background-color:#92a9d6;text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%">Amount</td>
      <td style="background-color:#92a9d6;text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%">Rate</td>
      <td style="background-color:#92a9d6;text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%">Amount</td>
      <td style="background-color:#92a9d6;text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%">Rate</td>
      <td style="background-color:#92a9d6;text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%">Amount</td>
  
      </tr>
      <tr style="font-size:9px;font-family: 'Times New Roman', Times, serif;">
      <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%">996511</td>
      <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="24%">Transportation of Goods</td>
      <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%">6,00,00.00</td>
      <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%">2.5%</td>
      <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%">0</td>
      <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%">2.5%</td>
      <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%">0</td>
      <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%">5%</td>
      <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%"></td>
      <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="20%">6,00,00.00</td>
      </tr>
      <?php  for($i=1;$i<17;$i++){ ?>
      <tr style="font-size:9px;font-family: 'Times New Roman', Times, serif;">
      <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
      <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="24%"></td>
      <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="10%"></td>
      <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%"></td>
      <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%"></td>
      <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%"></td>
      <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%"></td>
      <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%"></td>
      <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%"></td>
      <td style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="20%"></td>
      </tr>

     <?php } ?>
      
      <tr style="font-size:9px;font-family: 'Times New Roman', Times, serif;">
      <td style="background-color:#92a9d6;text-align:right;font-size:12px;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="44%">TOTAL</td>
      <td style="background-color:#92a9d6;text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%"></td>
      <td style="background-color:#92a9d6;text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%"></td>
      <td style="background-color:#92a9d6;text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%"></td>
      <td style="background-color:#92a9d6;text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%"></td>
      <td style="background-color:#92a9d6;text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%"></td>
      <td style="background-color:#92a9d6;text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="6%"></td>
      <td style="background-color:#92a9d6;font-size:10px;text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"  width="20%">6,00,00.00</td>
      </tr>
      <tr>
      <td colspan="8" style="border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;">Amount in Words : Rupee Six Lakh Only</td>  
      </tr>
      <tr>
      <td colspan="8" style="border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;">Terms, Conditions and Remarks : </td>  
      </tr>
      <tr>
      <td colspan="8" style="border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;">1. refer annexure 1 for taxation details</td>  
      </tr>
      <tr>
      <td colspan="8" style="border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;">2. GST Payble under revers charges</td>  
      </tr>
      <tr>
      <td colspan="8" style="border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;"></td>  
      </tr>
      <tr>
      <td colspan="8" style="border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;"></td>  
      </tr>
      <tr>
      <td colspan="8" style="border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;"></td>  
      </tr>

      <tr>
      <td colspan="2" width="30%" style="background-color:#92a9d6;border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;"></td>  
      <td colspan="3" width="20%" style="background-color:#92a9d6;border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;"></td>  
      <td colspan="3" width="50%" style="background-color:#92a9d6;border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;"></td>  
      </tr>
     
      <tr>
      <td colspan="2" rowspan="3" width="30%" style="border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;"></td>  
      <td colspan="3" rowspan="3" width="20%" style="border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;"></td>  
      <td colspan="3" width="50%" style="border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;font-size:9px;font-family: 'Times New Roman', Times, serif;text-align:center;">certified that the particulars given above are true and currect</td>  
      </tr>
      <tr>
       <td colspan="3" width="50%" style="border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;text-align:center;">FOR SHREE SAINATH ROADLINES</td>  
      </tr>
      <tr>
       <td colspan="3" width="50%" style="border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;"><br><br><br><br><br><br><br><br><br><br></td>  
      </tr>
      <tr>
      <td colspan="2" width="30%" style="text-align:center;background-color:#92a9d6;border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;">Receiver Stap & Signature</td>  
      <td colspan="3"  width="20%" style="text-align:center;background-color:#92a9d6;border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;">Common Seal</td>  
      <td colspan="3" width="50%" style="text-align:center;background-color:#92a9d6;border-right:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;">Authorised signatory</td>  
      </tr>
   </table>
   </body>
</html>
<?php 
	$invoiceMsg = ob_get_contents();
	ob_end_clean();
?>