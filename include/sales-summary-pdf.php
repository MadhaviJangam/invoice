<?php

$admin = new AdminFunctions();
	if(isset($admin)){
		$pdfObj = $admin;
   } 
   ob_start();
   if(isset($_GET['customer_id'])){
       $customer_id = $_GET['customer_id'];
    }else{
        $customer_id = '';
    }

   if(isset($_GET['from_date'])){  
       $from_date = $_GET['from_date'];
    }else{
        $from_date = '';
    }

   if(isset($_GET['to_date'])){
       $to_date = $_GET['to_date'];
    }else{
        $to_date = '';
    }

   
    $query='';
    if($customer_id=='' && $from_date=='' && $to_date=='' ){
        $query="SELECT * FROM ".PREFIX."tax_invoice_transaction";
    }

    if($customer_id!='' && $from_date=='' && $to_date=='' ){
        $query="SELECT * FROM ".PREFIX."tax_invoice_transaction WHERE customer_id='".$customer_id."' ";
    }

    if($customer_id=='' && $from_date!=='' && $to_date!=='' ){
        $query="SELECT * FROM ".PREFIX."tax_invoice_transaction WHERE invoice_date BETWEEN '".$from_date."' AND '".$to_date."' ";

    }

    if($customer_id!='' && $from_date!='' && $to_date!='' ){
        $query="SELECT * FROM ".PREFIX."tax_invoice_transaction WHERE customer_id='".$customer_id."' AND invoice_date BETWEEN '".$from_date."' AND '".$to_date."' ";
    }
    

    $result=$admin->query($query);
    
    $num_rows1 = mysqli_num_rows($result);
   
    
  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
   </head>
   
   <body>
   <table border="0" style="width:100%">
        <!-- <tr>
            <td style="text-align:left;font-size:10px;" width="70%">Details Of Sales Report &nbsp;&nbsp;From Date: <?php if($from_date!=''){ echo date("d-m-Y",strtotime($from_date));}else { echo '';}?>&nbsp;&nbsp;To Date: <?php if($from_date!='') {echo date("d-m-Y",strtotime($to_date));} else{ echo '';}?></td>
            <td style="text-align:right;font-size:10px;" width="30%">Printed On:<?php echo date("d-m-Y");?></td>
        </tr>
        <tr style="">
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="15%">INVOICE NO</td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="10%">DATE</td>
            <td  style="text-align:LEFT;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="40%">PARTY NAME</td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"   width="10%">AMOUNT</td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"   width="10%">GST</td>
            <td style="text-align:right;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="15%">BILL AMOUNT</td>    
        </tr> -->
        <?php 
         $totalamount = 0;
         $gst=0;
         $total_gst_amt=0;
         $total_amt=0;
         $final_total=0;
      
           while($row = $admin->fetch($result)) {
            $totalamount = $row['total_after_disc_amt']+$row['service_charges'];
            $gst = $row['total_cgst_amt']+$row['total_sgst_amt'] + $row['total_igst_amt'] + $row['service_charges_cgst_amt']+ $row['service_charges_sgst_amt'] + $row['service_charges_igst_amt'];

            $final_total = $final_total + $row['final_amt'];
            $total_amt = $total_amt+($row['total_after_disc_amt']+$row['service_charges']);
            $total_gst_amt = $total_gst_amt+($row['total_cgst_amt']+$row['total_sgst_amt'] + $row['total_igst_amt'] + $row['service_charges_cgst_amt']+ $row['service_charges_sgst_amt'] + $row['service_charges_igst_amt']);
        ?>
        <tr style="">
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="15%"><?php echo $row['invoice_no']; ?></td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="10%"><?php echo date("d-m-Y", strtotime($row['invoice_date'])); ?></td>
            <td  style="text-align:LEFT;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="40%"><?php echo $admin->getUniqueCustomerMasterById($row['customer_id'])['customer_name'];?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"   width="10%"><?php echo $totalamount; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"   width="10%"><?php echo $gst;?></td>
            <td style="text-align:right;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="15%"><?php echo $row['final_amt']; ?></td>    
        </tr>
        <?php } ?>
        <tr>
            <td width="100%" style="border-bottom:1px solid #000;"></td>
        </tr>
        <tr style="">
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="15%"></td>
            <td  style="text-align:center;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="10%"></td>
            <td  style="text-align:LEFT;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"  width="40%"></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"   width="10%"><?php echo $total_amt; ?></td>
            <td style="text-align:right;padding:0px;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;"   width="10%"><?php echo $total_gst_amt;?></td>
            <td style="text-align:right;border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;border-top:1px solid #000;" width="15%"><?php echo $final_total; ?></td>    
        </tr>
      </table>
   </body>
</html>
<?php 
	$invoiceMsg = ob_get_contents();
	ob_end_clean();
?>