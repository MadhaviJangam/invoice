
<div class="demo">
  <h1>QuestionMark.js</h1>
  <p>A tiny (&lt;1kb), cross-browser, modal window that displays shortcut keys for your app, similar to what happens on Twitter, Gmail, GitHub, etc.</a></p>

  <p>This is an example page.</p>
  <p>Press the <code>?</code> key to see the modal.</p>
  <p>More info:<br><a href="https://github.com/impressivewebs/QuestionMark.js">https://github.com/impressivewebs/QuestionMark.js</a> and <a href="https://www.impressivewebs.com/questionmark-js-shortcut-keys-displayed/">the blog post</a>.</p>
  <p>Made by <a href="https://twitter.com/ImpressiveWebs">Louis Lazaris</a>. Use it for whatever you want.</p>
</div><!-- .demo -->


<style>
body {
  padding: 0 30px;
}

/* the first section below is just
for the demo page */

.demo {
  word-wrap: break-word;
  text-align: center;
  font-family: Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
}

.demo h1 {
  color: #aaa;
  font-size: 3em;
}

.demo p {
  font-family: Arial, sans-serif;
  max-width: 880px;
  color: #555;
  margin: 0 auto 30px auto;
  font-size: 30px;
}

.demo p:last-child {
  font-size: 20px;
}

.demo a {
  color: firebrick;
}

.demo a:hover {
  color: blue;
}

.demo code {
  background: #888;
  color: white;
  padding: 3px 8px;
}

/* question.mark.css starts here */

.help-underlay * {
  box-sizing: border-box;
}

.help-underlay {
  position: absolute;
  background: #555;
  background: rgba(0, 0, 0, 0.5);
  visibility: hidden;
  opacity: 0;
  transition: opacity 0.2s linear;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

.help-modal {
  position: absolute;
  z-index: 99999;
  left: 10%;
  right: 10%;
  top: 10%;
  bottom: 20%;
  margin: auto;
  background: #fff;
  color: #676767;
  max-width: 1178px;
  overflow: auto;
  font-family: Arial, sans-serif;
  border-radius: 3px;
  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.4),
    inset 0 1px 0 rgba(255, 255, 255, 0.5);
  transition: width 0.2s linear;
}

.help-modal-content {
  padding: 0 20px;
}

.help-close {
  position: absolute;
  top: 0.4em;
  right: 0.5em;
  font-size: 1.8em;
  cursor: pointer;
  transition: color 0.2s linear;
}

.help-close:hover {
  color: #000;
}

.help-modal h1 {
  text-align: center;
  margin: 0.5em;
  font-size: 1.5em;
  padding-bottom: 0.4em;
  border-bottom: solid 2px #ccc;
  font-weight: normal;
}

.help-modal h1 .help-key {
  line-height: 1.5;
  position: relative;
  bottom: 4px;
}

.help-isVisible {
  opacity: 1;
  visibility: visible;
  height: auto;
  z-index: 8888;
}

.help-list-wrap {
  transition: width 0.2s linear;
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
}

.help-list {
  list-style: none;
  margin: 0;
  padding: 0 0 10px 0;
}

.help-key-unit {
  line-height: 1.8;
  margin-right: 2em;
  padding: 5px 0;
}

.help-key {
  position: relative;
  bottom: 2px;
}

.help-key span {
  font-size: 14px;
  color: #555;
  display: inline-block;
  padding: 0 8px;
  text-align: center;
  background-color: #eee;
  background-repeat: repeat-x;
  background-image: linear-gradient(#f5f5f5 0%, #eee 100%);
  border: 1px solid #ccc;
  border-radius: 3px;
  box-shadow: inset 0 1px 0 #fff, 0 1px 0 #ccc;
}

.help-key-def {
  margin-left: 1em;
}

@media (max-width: 1460px) {
  .help-modal {
    left: 10%;
    right: 10%;
  }
}

@media (max-width: 1280px) {
  .help-modal {
    left: 25%;
    right: 25%;
  }

  .help-list {
    width: 50%;
  }
}

@media (max-width: 1230px) {
  .help-modal {
    left: 30%;
    right: 30%;
  }
  
  .help-list {
    width: 100%;
  }
}

@media (max-width: 870px) {
  .help-modal {
    left: 10%;
    right: 10%;
  }
}
</style>
<script>
/*
  QuestionMark.js 2.0 by Louis Lazaris
  http://impressivewebs.github.io/QuestionMark.js/
  Use it for whatever you want, no credit needed.
  This script should work everywhere, including IE9+.
*/

(function (d) {
  'use strict';

  function removeModal(helpUnderlay) {
    helpUnderlay.classList.remove('help-isVisible');
  }

  function doWhichKey(e) {
    let charCode = e.keyCode;
    // String.fromCharCode(charCode) gets the keycode if you want
    return charCode;
  }

  // Primary function, called in checkServerResponse()
  function doQuestionMark() {
    let helpUnderlay = d.getElementById('helpUnderlay'),
        helpModal = d.getElementById('helpModal'),
        helpClose = d.getElementById('helpClose'),
        classCol;

    d.addEventListener('keydown', function (e) {
      // 191 = '?' key
      // '?' key toggles the modal
      if (doWhichKey(e) === 191 && e.shiftKey === true) {
        helpUnderlay.classList.add('help-isVisible');
      }
    }, false);

    d.addEventListener('keyup', function (e) {
      // 27 = ESC key
      if (doWhichKey(e) === 27) {
        removeModal(helpUnderlay);
      }
    }, false);

    // Modal is removed if the background is clicked
    helpUnderlay.addEventListener('click', function () {
      removeModal(helpUnderlay);
    }, false);

    // this prevents click on modal from removing the modal
    helpModal.addEventListener('click', function (e) {
      e.stopPropagation();
    }, false);

    // the close button
    helpClose.addEventListener('click', function () {
      removeModal(helpUnderlay);
    }, false);
  }

  function doFetch() {
    fetch('https://codepen.io/impressivewebs/pen/HwDFu.html')
      .then((response) => {
        return response.text();
      })
      .then((data) => {
        d.body.innerHTML += data;
        doQuestionMark();
      });
  }

  // This fires the Fetch request and, in turn,
  // the primary function for the modal.
  doFetch();
}(document));

</script>