<?php

include_once 'include/config.php';
include_once 'include/admin-functions.php';
include_once 'include/classes/CSRF.class.php';

$admin = new AdminFunctions();

if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: index.php");
	exit();
}

$pageName = "Tax Transaction";
$pageURL = 'tax-master.php';
$deleteURL = 'tax-master.php';
$tableName = 'tax_master';

$getActiveCustomerDetails = $admin->getActiveCustomerDetails();
$getActiveItemDetails = $admin->getActiveItemDetails();
$getActiveTaxDetails = $admin->getActiveTaxDetails();
$item_details = $admin->getActiveItemTaxMaster();

$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);

$results = $admin->query("SELECT * FROM ".PREFIX.$tableName." WHERE deleted_time IS NULL GROUP BY id DESC");

if(isset($_GET['delId']) && !empty($_GET['delId'])){
	$id = $admin->escape_string($admin->strip_all($_GET['delId']));
	$delete = $admin->query("UPDATE ".PREFIX.$tableName." SET `deleted_by`=".$loggedInUserDetailsArr['id'].",`deleted_time`='".CURRENTMILIS."' WHERE id='".$id."'");
	header("location:".$pageURL."?deletesuccess");
	exit();
}

if(isset($_POST['register'])){
	if($csrf->check_valid('post')) {
		$result = $admin->addTaxMaster($_POST,$loggedInUserDetailsArr['id']);
		header("location:".$pageURL."?registersuccess");
	exit();
	}
}

if(isset($_GET['edit'])){
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
	$data = $admin->getUniqueTaxMasterById($id);
}

if(isset($_POST['id']) && !empty($_POST['id'])) {
	if($csrf->check_valid('post')) {
		$id = trim($admin->escape_string($admin->strip_all($_POST['id'])));
		$result = $admin->updateTaxMaster($_POST, $loggedInUserDetailsArr['id']);
		header("location:".$pageURL."?updatesuccess");
		exit();
	}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title> Unique Invoice | <?php echo $pageName; ?></title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">
  <script src="../../plugins/notiflix/notiflix-aio-1.5.0.min.js"></script>
</head>
<body class="hold-transition sidebar-mini layout-footer-fixed">
<!-- Site wrapper -->
<div class="wrapper">
 
  <?php 
  include('include/header.php');
  include('include/sidebar.php');
  ?>
<style>
    label{
        font-size:12px;
    }
    .dataTables_wrapper {
    font-size: 14px;
}
em{
  color:red;
}
    </style>
  <!-- Content Wrapper. Contains page content -->
  <?php if(isset($_GET['registersuccess'])){ ?>
  <script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Success('<?php echo $pageName; ?> successfully Added');
       </script>
<?php } ?>



<?php if(isset($_GET['updatesuccess'])){ ?>

<script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Warning('<?php echo $pageName; ?> successfully updated');
       </script>
<?php } ?>


<?php if(isset($_GET['deletesuccess'])){ ?>
<script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Failure('<?php echo $pageName; ?> successfully deleted');
       </script>

<?php } ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1> <?php echo $pageName; ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Transaction</a></li>
              <li class="breadcrumb-item active"> <?php echo $pageName; ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->

    <section class="content" >
    <?php  ?>
      <form action="" id="form" method="post" enctype="multipart/form-data" autocomplete="off">
      <!-- Default box -->
      
   <div id="cardeffect">
      <div class="card" >
        <div class="card-header">
          <h3 class="card-title" style = "text-align:center" ><b>Tax Invoice</b></h3>
          <a href="<?php echo $pageURL; ?>" align="right" style="font-size:14px;color:red;float:right;"> <i class="fas fa-hand-point-left"></i> Back</a>
        </div>
       
        <div class="card-body">
          <div class="form-group row">
            <div class="col-md-6 fromerrorcheck" >
                  <label>Invoice No<em>*</em> </label>
                  <span><input type="text" name="item_name"
                    value="<?php if(isset($_GET['edit'])) { echo $data['item_name']; } ?>"></span>
              </div>
              <div class="col-md-6 fromerrorcheck">
                  <label>Invoice Date<em>*</em> </label>
                  <span><input type="date" name="date" value="<?php if(isset($_GET['edit'])) { echo $data['date']; } ?>"></span>
              </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-6 fromerrorcheck">
                  <label>Customer Name</label>
                  <span><select class="select2" name="customer_id">
                  <option value="">Select Customer Name</option>
                  <?php while($row = $admin->fetch($getActiveCustomerDetails)){ ?>
                      <option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) and $data['customer_id']==$row['id']) { echo 'selected'; } ?>><?php echo $row['customer_name']; ?>
                      <?php } ?>
                  </select></span>
              </div>
              <div class="col-md-6 fromerrorcheck">
                  <label>Due Date<em>*</em> </label>
                  <span><input type="date" name="date"
                    value="<?php if(isset($_GET['edit'])) { echo $data['date']; } ?>" ></span>
              </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-6 fromerrorcheck">
                <label>GST Applicable</label>
                <span><select name="customer_id">
                <option value="none">None</option>
                <option value="CGST">CGST</option>
                <option value="SGST">SGST</option>
                <option value="SGST">IGST</option>
                </select></span>
            </div>
            <div class="col-sm-6 fromerrorcheck">
                <label>GST %</label>
                <span><select class="select2" name="description">
                <option value="">Select GST %</option>
                <?php while($row = $admin->fetch($getActiveTaxDetails)){ ?>
                    <option value="<?php echo $row['description']; ?>" <?php if(isset($_GET['edit']) and $data['description']==$row['description']) { echo 'selected'; } ?>><?php echo $row['description']; ?>
                    <?php } ?>
                </select></span>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-7 fromerrorcheck">
                <label>Scanner Focus<em>*</em> </label>
               <span> <input type="text" name="description"
                  value="<?php if(isset($_GET['edit'])) { echo $data['description']; } ?>"><span>
            </div>
          </div>
        </div>
          <div class="col-12">
            <div>
              <div>
                <h2 class="card-title"  align=right><b>Item Details</b></h2>
              </div>
              <!-- /.card-header -->
              <div>
              <table class="table table-bordered">
                  <thead>
                  <tr>
                    <th width=50%>Item Name</th>
                    <th width=5%>Rate</th>
                    <th width=5%>Qty</th>
                    <th width=5%>Unit</th>
                    <th width=10%>Amt</th>
                    <th width=10%>Disct %</th>
                    <th width=10%>Disc Amt</th>
                    <th width=10%>Tot Amt</th>
                    <th width=10%>CGST %</th>
                    <th width=10%>CGST Amt</th>
                    <th width=10%>SGST %</th>
                    <th width=10%>SGST Amt</th>
                    <th width=10%>IGST %</th>
                    <th width=10%>IGST Amt</th>
                    <th width=10%>Net Amt</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td width=30%>
                        <select class="form-control form-control-sm rounded-0 select2" name="item_name">
                        <?php while($row = $admin->fetch($item_details)){ ?>
                            <option value="<?php echo $row['item_name']; ?>" <?php if(isset($_GET['edit']) and $data['item_name']==$row['item_name']) { echo 'selected'; } ?>><?php echo $row['item_name']; ?>
                            <?php } ?>
                        </select>
                    </td>
                    <td><input type="text"  class=" form-control form-control-sm rounded-0"  name="rate"></td>
                    <td> <input type="text"  class=" form-control form-control-sm rounded-0"  name="item_quantity"></td>
                    <td> <input type="text"  class=" form-control form-control-sm rounded-0"  name="unit"></td>
                    <td> <input type="text"  class=" form-control form-control-sm rounded-0"  name="total"></td>
                    <td> <input type="text"  class=" form-control form-control-sm rounded-0"  name="dis_per"></td>
                    <td> <input type="text"  class=" form-control form-control-sm rounded-0"  name="dis_amt"></td>
                    <td> <input type="text"  class=" form-control form-control-sm rounded-0"  name="tot_after_dis"></td>
                    <td> <input type="text"  class=" form-control form-control-sm rounded-0"  name="cgst_per"></td>
                    <td> <input type="text"  class=" form-control form-control-sm rounded-0"  name="cgst_amt"></td>
                    <td> <input type="text"  class=" form-control form-control-sm rounded-0"  name="sgst_per"></td>
                    <td> <input type="text"  class=" form-control form-control-sm rounded-0"  name="sgst_amt"></td>
                    <td> <input type="text"  class=" form-control form-control-sm rounded-0"  name="igst_per"></td>
                    <td> <input type="text"  class=" form-control form-control-sm rounded-0"  name="igst_amt"></td>
                    <td> <input type="text"  class=" form-control form-control-sm rounded-0"  name="net_amt"></td>
                    <td><i class="fas fa-plus" onclick="test()"></i></td>
                  </tr>
                  
                  </tbody>
                  
                  </table>
                    <tr>Total
                      <td colsapn=5><input type="text" class="col-md-1 fromerrorcheck"></td>
                      <td colsapn=7><input type="text" class="col-md-1 fromerrorcheck"></td>
                      <td colsapn=7><input type="text" class="col-md-1 fromerrorcheck"></td>
                      <td colsapn=7><input type="text" class="col-md-1 fromerrorcheck"></td>
                      <td colsapn=7><input type="text" class="col-md-1 fromerrorcheck"></td>
                      <td colsapn=7><input type="text" class="col-md-1 fromerrorcheck"></td>
                      <td colsapn=7><input type="text" class="col-md-1 fromerrorcheck"></td>
                    </tr>
              </div>
              <!-- /.card-body -->
            </div>
            <div class="card-body">
          <div class="form-group row">
            <div class="col-md-4 fromerrorcheck" >
            <label>Service Charges<em>*</em> </label>
                  <span><input type="text" name="item_name"
                    value="<?php if(isset($_GET['edit'])) { echo $data['item_name']; } ?>"></span>
              </div>
              <div class="col-md-4 fromerrorcheck">
              <label>Final Paid Amount<em>*</em> </label>
                  <span><input type="text" name="text" value="<?php if(isset($_GET['edit'])) { echo $data['date']; } ?>"></span>
              </div>
              <div class="col-md-4 fromerrorcheck">
              <label>Description<em>*</em> </label>
                  <span><input type="text" name="text" value="<?php if(isset($_GET['edit'])) { echo $data['date']; } ?>"></span>
              </div>
          </div>
          </div>
        </div>
            <!-- /.card -->
      </div>
          <!-- /.col -->
          <!-- /.content -->
    </div>
  </div>
        <!-- /.card-body -->
</div>
      <!-- /.card -->
</form> 
</section>   
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="float-left d-none d-sm-block">
          <button type="submit" name="register" id="register" class="btn btn-success"><i class="fas fa-save"></i> Save as Draf</button>
            <a class="btn btn-success" name="register" id="register" href="compose-mail.php"><i class="fas fa-save"></i> Save and Send</a>
        <a class="btn btn-danger" href="javascript:void(0);" onclick="clearall()"><i class="fas fa-broom"></i>Cancel</a>
    </div>
        <strong class="float-right">Copyright &copy; 2020-2021 <a href="https://usssoft.com">Unique Software System</a>. All rights reserved.</strong> 
  </footer>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- jquery-validation -->
<script src="../../plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="../../plugins/jquery-validation/additional-methods.min.js"></script>
<script src="../../dist/js/demo.js"></script>
<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });

  <?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>
      $("#cardeffect").slideDown("slow");
      <?php } ?>
   
$(function () {
 

  


  $('#form').validate({
    rules: {
      ignore: [],
		  debug: false,
      customer_name : {
         required: true,
      },
      company_phone : {
         required: true,
      },
      company_pan : {
         required: true,
      },
      company_email : {
         required: true,
      },
      company_gst : {
         required: true,
      },
      billing_address : {
         required: true,
      },
      shipping_address : {
         required: true,
      },
      contact_person_name : {
         required: true,
      },
      contact_person_phone : {
         required: true,
      },
      contact_person_whatsapp : {
         required: true,
      },
      contact_person_email : {
         required: true,
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
    },
    errorElement: 'span',
     errorPlacement: function (error, element) {
       error.addClass('invalid-feedback');
       element.closest('.fromerrorcheck').append(error);
     },
     highlight: function (element, errorClass, validClass) {
       $(element).addClass('is-invalid');
     },
     unhighlight: function (element, errorClass, validClass) {
       $(element).removeClass('is-invalid');
     }
  });
});

function sameasbillingaddress(){
    $('#shipping_address').val($('#billing_address').val());
}

function clearall(){
  $('input').val('');
  $('textarea').val('');
}

$('.loadstarter').on('click',function(){
      Notiflix.Loading.Init({});
      Notiflix.Loading.Hourglass();
    });
    $(function () {
      $('.select2').select2()
    });
    
    function test(){
    $.ajax({url: "getAjaxItemDetails.php", success: function(result){
    $("table").append(result);
  }});
        }
</script>

</body>
</html>