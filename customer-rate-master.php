<?php

include_once 'include/config.php';
include_once 'include/admin-functions.php';
include_once 'include/classes/CSRF.class.php';
$admin = new AdminFunctions();

if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: index.php");
	exit();
}

$pageName = "Customer Rate Master";
$pageURL = 'customer-rate-master.php';
$deleteURL = 'customer-rate-master.php';
$tableName = 'customer_rate_master';

$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);

$results = $admin->query("SELECT * FROM ".PREFIX.$tableName." ORDER BY created_time DESC");

$getActiveItemDetails = $admin->getActiveItemDetails();
$getActiveCustomerDetails = $admin->getActiveCustomerDetails();
if(isset($_GET['delId']) && !empty($_GET['delId'])){
	$id = $admin->escape_string($admin->strip_all($_GET['delId']));
	$delete = $admin->query("UPDATE ".PREFIX.$tableName." SET deleted_by='".$loggedInUserDetailsArr['id']."' AND deleted_time='".CURRENTMILIS."' WHERE id = '".$id."'");
	header("location:".$pageURL."?deletesuccess");
	exit();
}

if(isset($_POST['register'])){
	if($csrf->check_valid('post')) {
		$result = $admin->addCustomerRateMaster($_POST,$loggedInUserDetailsArr['id']);
		header("location:".$pageURL."?registersuccess");
	exit();
	}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title> Unique Invoice | <?php echo $pageName; ?></title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <script src="../../plugins/notiflix/notiflix-aio-1.5.0.min.js"></script>

    <!-- Select2 -->
    <link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">

</head>
<body class="hold-transition sidebar-mini layout-footer-fixed">
<!-- Site wrapper -->
<div class="wrapper">
 
  <?php 
  include('include/header.php');
  include('include/sidebar.php');
  ?>
<style>
    label{
        font-size:12px;
    }
    .dataTables_wrapper {
    font-size: 14px;
}
em{
  color:red;
}

.select2-container--default .select2-selection--single {
  border-radius:0px;
}

.dataTables_wrapper {
    font-size: 12px; 
}

    </style>
  <!-- Content Wrapper. Contains page content -->
  <?php if(isset($_GET['registersuccess'])){ ?>
  <script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Success('<?php echo $pageName; ?> successfully Added');
       </script>
<?php } ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1> <?php echo $pageName; ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Master</a></li>
              <li class="breadcrumb-item active"> <?php echo $pageName; ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->

    <section class="content" >
    <?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>
      <form action="" id="form" method="post" enctype="multipart/form-data" autocomplete="off">
      <!-- Default box -->
   <div id="cardeffect" style="display:none;">
      <div class="card" >
        <div class="card-header">
          <h3 class="card-title"> <?php echo $pageName; ?></h3>
          <a href="<?php echo $pageURL; ?>" align="right" style="font-size:14px;color:red;float:right;"> <i class="fas fa-hand-point-left"></i> Back</a>
        </div>
       
        <div class="card-body">
          <div class="form-group row">

          <div class="col-sm-3 fromerrorcheck">
              <label>Customer Name</label>
              <select class="form-control form-control-sm rounded-0 select2" name="customer_id" id="customer_id">
              <option value="">Select Customer Name</option>
                <?php while($row = $admin->fetch($getActiveCustomerDetails)){ ?>
                  <option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) and $data['customer_id']==$row['id']) { echo 'selected'; } ?>><?php echo $row['customer_name']; ?>
                  <?php } ?>
              </select>
            </div>
          <div class="col-sm-3 fromerrorcheck">
              <label>Item Name</label>
              <select class="form-control form-control-sm rounded-0 select2" name="item_id"   onchange="fun_rate(this)">
              <option value="">Select Item Name</option>
                <?php while($row = $admin->fetch($getActiveItemDetails)){ ?>
                  <option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) and $data['item_id']==$row['id']) { echo 'selected'; } ?>><?php echo $row['item_name']; ?>
                  <?php } ?>
              </select>
            </div>

            <div class="col-md-3 fromerrorcheck">
              <label>Invoice Rate<em>*</em> </label>
              <input type="text" name="rate" id="rate"
                value="<?php if(isset($_GET['edit'])) { echo $data['rate']; } ?>"
                class="form-control form-control-sm rounded-0">
            </div>
            <div class="col-md-3 fromerrorcheck">
              <label>Delivery Challan Rate<em>*</em> </label>
              <input type="text" name="challan_rate" id="challan_rate"
                value="<?php if(isset($_GET['edit'])) { echo $data['challan_rate']; } ?>"
                class="form-control form-control-sm rounded-0">
            </div>
          </div>
       
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
        <input type="hidden" name="<?php echo $token_id; ?>" value="<?php echo $token_value; ?>" />

<?php if(isset($_GET['edit'])){ ?>

    <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id ?>"/>

    <button type="submit" name="update" value="update" id="update" class="btn btn-warning"><i class="fas fa-save"></i> Update <?php echo $pageName; ?></button>

<?php } else { ?>

    <button type="submit" name="register" id="register" class="btn btn-success"><i class="fas fa-save"></i> Add <?php echo $pageName; ?></button>

<?php } ?>
<a class="btn btn-danger" href="javascript:void(0);" onclick="clearall()"><i class="fas fa-broom"></i>Clear All</a>

        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->
      </form> 
    </section>
   
    <?php } ?>
    <!-- /.content -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><?php echo $pageName; ?> Details</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Sr. No.</th>
                    <th>Customer Name</th>
                    <th>Item Name</th>
                    <th>Tax Invoice Rate</th>
                    <th>Challan Rate</th>
                    <th>Rate Change By</th>
                    <th>Rate Change Date</th>
                   
                  </tr>
                  </thead>
                  <tbody>
                 <?php $x=1; while($row = $admin->fetch($results)){ 
                  
                  $customer_name = $admin -> getUniqueCustomerMasterById($row['customer_id'])['customer_name'];
                  $item_name = $admin -> getUniqueItemMasterById($row['item_id'])['item_name'];
                  $user_by = $admin -> getUniqueAdminById($row['created_by'])['username'];
                  
                   ?>
                  <tr>
                    <td><?php echo $x; ?></td>
                    <td><?php echo $customer_name; ?></td>
                    <td><?php echo $item_name; ?></td>
                    <td><?php echo $row['rate']; ?></td>
                    <td><?php echo $row['challan_rate']; ?></td>
                    <td><?php echo $user_by; ?></td>
                    <td> <?php echo date("d-m-Y", strtotime($row['created_time'])); ?></td>
              
                            
                  </tr>
                  <?php $x++; } ?>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="float-left d-none d-sm-block">
      <a href="<?php echo $pageURL; ?>?add" class="btn btn-primary"> <i class="fas fa-plus-circle"></i> Create New <?php echo $pageName; ?></a>
    </div>
    <strong class="float-right">Copyright &copy; 2020-2021 <a href="https://usssoft.com">Unique Software System</a>. All rights reserved.</strong> 
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- jquery-validation -->
<script src="../../plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="../../plugins/jquery-validation/additional-methods.min.js"></script>
<script src="../../dist/js/demo.js"></script>

<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>

<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });

  <?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>
      $("#cardeffect").slideDown("slow");
      <?php } ?>
   
$(function () {
 

  


  $('#form').validate({
    rules: {
    ignore: [],
		debug: false,
    customer_id:{
        required: true,
      },
      item_id:{
        required: true,
      },
      rate:{
        required: true,
        number:true,
      },
     
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
    },
    errorElement: 'span',
     errorPlacement: function (error, element) {
       error.addClass('invalid-feedback');
       element.closest('.fromerrorcheck').append(error);
     },
     highlight: function (element, errorClass, validClass) {
       $(element).addClass('is-invalid');
     },
     unhighlight: function (element, errorClass, validClass) {
       $(element).removeClass('is-invalid');
     }
  });
});

function fun_rate(e){
  $('#rate').val('');
  $('#challan_rate').val('');
  var customer_id=$('#customer_id').val();
  var item_id=$(e).val();
  if(customer_id!='' && item_id!=''){
    $.ajax({
              type: 'POST',
              data: 'customer_id='+customer_id+'&item_id='+item_id,
              url: 'getAjaxCustomerRate.php',
              success: function (respone) {
              if(respone!='null'){
                var res = JSON.parse(respone);
                $('#rate').val(res['rate']);
                $('#challan_rate').val(res['challan_rate']);
                }
              }
          });
  }
}
              
function clearall(){
  $('input').val('');
  $('textarea').val('');
}

$('.loadstarter').on('click',function(){
      Notiflix.Loading.Init({});
      Notiflix.Loading.Hourglass();
    });
    
    $(function () {
      $('.select2').select2()
    });
</script>

</body>
</html>