


<?php

include_once 'include/config.php';
include_once 'include/admin-functions.php';
include_once 'include/classes/CSRF.class.php';
$admin = new AdminFunctions();

if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: index.php");
	exit();
}
$pageName = "Bin Card";
// $pageURL = 'tax-invoice-add.php';
// $pageURL = 'credit-note-add.php';
// $pageURL = 'customer-master.php';
// $pageURL = 'delivery-chanllan-add.php';


$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);


$item_details = $admin->getActiveItemDetails(); 



if(isset($_GET['item_id'])){

  $BinCard =$admin->getBinCard($_GET['item_id'],$_GET['from_date'],$_GET['to_date']);
  

}else{
  $BinCard =$admin->getBinCard(0,'','');
 
}

 ?>
 
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title> Unique Invoice | <?php echo $pageName; ?></title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <script src="../../plugins/notiflix/notiflix-aio-1.5.0.min.js"></script>

    <!-- Select2 -->
    <link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">

</head>
<body class="hold-transition sidebar-mini layout-footer-fixed">
<!-- Site wrapper -->
<div class="wrapper">
 
  <?php 
  include('include/header.php');
  include('include/sidebar.php');
  ?>
<style>
    label{
        font-size:12px;
    }
    .dataTables_wrapper {
    font-size: 14px;
}
em{
  color:red;
}

.select2-container--default .select2-selection--single {
  border-radius:0px;
}

.dataTables_wrapper {
    font-size: 12px; 
}

    </style>
  <!-- Content Wrapper. Contains page content -->
  <?php if(isset($_GET['registersuccess'])){ ?>
  <script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Success('<?php echo $pageName; ?> successfully Added');
       </script>
<?php } ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1> <?php echo $pageName; ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Transaction</a></li>
              <li class="breadcrumb-item active"> <?php echo $pageName; ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->

    <section class="content" >
    
    <!-- /.content -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><b><?php echo $pageName; ?> Details</b></h3><br>
              </div>
              <div class="card-body">
              <form action = "" method = "GET" name="form" id="form">
                <div class="form-group row">
                    <div class="col-md-3 fromerrorcheck">
                        <label>From Date</label>
                        <input type="date" class="form-control form-control-sm rounded-0 " name="from_date" id="from_date" value="<?php if(isset($_GET['edit'])) { echo $_GET['from_date']; }else{ echo date("Y-m-d"); } ?>">
                    </div>
                    <div class="col-md-3 fromerrorcheck">
                        <label>To Date</label><input type="date" class="form-control form-control-sm rounded-0 " name="to_date" id="to_date" value="<?php if(isset($_GET['edit'])) { echo $_GET['to_date']; }else{ echo date("Y-m-d"); } ?>">
                    </div>
                    <div class="col-md-3 fromerrorcheck">
                    <label>Item Name</label>
                        <select class="form-control form-control-sm rounded-0 item_id" name="item_id">
                        <option value="">Select Item</option>
                        <?php while($row = $admin->fetch($item_details)){
                        ?>
                        <option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) and $rows['item_id']==$row['id']) { echo 'selected'; } ?>><?php echo $row['item_name']; ?>
                        <?php } ?>
                    </select>
                    </div>
                    <div class="col-md-3 fromerrorcheck"> 
                      <label>Search <em>*</em> </label>
                    <button class="btn btn-sm btn-primary btn-block">Search</button>
                </div> 
                </form>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Date</th>
                    <th>Ref No</th>
                    <th style="text-align:right">Opening QTY</th>
                    <th style="text-align:right">Received QTY</th>
                    <th style="text-align:right">Purchase Rate</th>
                    <th style="text-align:right">Sales QTY</th>
                    <th style="text-align:right">Closing Stock</th>
                                     
                  </tr>
                  </thead>
                  <tbody>
                   <?php
                    $opening_balance=0;
                    $debitTotal=0;
                    $cradit_total=0;
                    $balance = 0;  
                    $closingStock =0; 
                    while($rows = $admin->fetch($BinCard)){
                     $purchaseDetails = $admin ->itemReceivedPurchaseRate($rows['description'],$rows['priority']);
                     $salesDetails = $admin ->itemReceivedPurchaseRateBYTaxInvoice($rows['description'],$rows['priority']);

                    if($rows['description']=='Opening Quantity'){
                      $received='0';
                      $purchase_rate= $rows['purchase_rate'];
                      $sales_qty='0';
                    } else {
                      $received=$purchaseDetails['qty'];
                      $purchase_rate=$purchaseDetails['rate'];
                      $sales_qty = $salesDetails['qty'];
                    }
                    $closingStock = $closingStock + $salesDetails['qty'];
                   
                   ?>   
                  <tr>
                        <td><?php if($rows['open_date']!=''){ echo date("d-m-Y",strtotime($rows['open_date']));}?></td>
                        <td><?php echo $rows['description']; ?></td>
                        <td style="text-align:right"><?php echo $rows['opening_qty']; ?></td>
                        <td style="text-align:right"><?php echo $received;?></td>
                        <td style="text-align:right"><?php echo $purchase_rate;?></td>
                        <td style="text-align:right"><?php echo $sales_qty; ?></td>
                        <td style="text-align:right"><?php echo $closingStock; ?></td>
                    
                    
                    </tr>
                    
                    <?php  }
                   
                    //$closing = ($opening_balance+$debitTotal)-$cradit_total;
                    ?>
                    <tr>
                        <td><?php ?></td>
                        <td></td>
                        <td>Total</td>
                        <td></td>
                       
                        <td></td> 
                        <td></td> 
                        <td style="text-align:right"></td>
                          
                    </tr>
                    
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <!-- <div class="float-left d-none d-sm-block">
      <a href="<?php echo $subpageURL; ?>" class="btn btn-primary"> <i class="fas fa-plus-circle"></i> Create New <?php echo $pageName; ?></a>
    </div> -->
    <strong class="float-right">Copyright &copy; 2020-2021 <a href="https://usssoft.com">Unique Software System</a>. All rights reserved.</strong> 
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- jquery-validation -->
<script src="../../plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="../../plugins/jquery-validation/additional-methods.min.js"></script>
<script src="../../dist/js/demo.js"></script>

<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>

<!-- Page specific script -->
<script>
  // $(function () {
  //   $("#example1").DataTable({
  //     "paging": false,
  //     "responsive": true, "lengthChange": false, "autoWidth": false,
  //     "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
  //   }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    
  //   $('#example2').DataTable({
  //     "paging": true,
  //     "lengthChange": false,
  //     "searching": false,
  //     "ordering": true,
  //     "info": true,
  //     "autoWidth": false,
  //     "responsive": true,
  //   });
  // });

  <?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>
      $("#cardeffect").slideDown("slow");
      <?php } ?>
   
$(function () {
 

  


  $('#form').validate({
    rules: {
    ignore: [],
		debug: false,
    from_date:{
        required: true,
      },
      to_date:{
        required: true,
      },
      item_id:{
        required: true,
        number:true,
      },
     
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
    },
    errorElement: 'span',
     errorPlacement: function (error, element) {
       error.addClass('invalid-feedback');
       element.closest('.fromerrorcheck').append(error);
     },
     highlight: function (element, errorClass, validClass) {
       $(element).addClass('is-invalid');
     },
     unhighlight: function (element, errorClass, validClass) {
       $(element).removeClass('is-invalid');
     }
  });
});

// function CustomerLedger(){

//   var from_date=$('#from_date').val();
//   var to_date=$('#to_date').val();
//   var item_id=$('#item_id').val();
 
// if(from_date!='' && to_date!='' && item_id!=''){
//       alert('Select From And To Date And Customer Name');
//     }
  
// }
function clearall(){
  $('input').val('');
  $('textarea').val('');
}

$('.loadstarter').on('click',function(){
      Notiflix.Loading.Init({});
      Notiflix.Loading.Hourglass();
    });
    
    $(function () {
      $('.select2').select2()
    });

</script>



</body>
</html>