<?php 
include_once 'include/admin-functions.php';
$admin = new AdminFunctions();
$item_details = $admin->getActiveItemDetails(); 
$count=$_POST['count'];
?>
<tr>
                <td>
              <select class="form-control form-control-sm rounded-0 chosen " onchange="fun_item_details(this)" name="item_id[<?php echo $count; ?>]"required>
                  <option value="">Select Item</option>
                  <?php while($row = $admin->fetch($item_details)){
                   ?>
                  <option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) and $data['item_name']==$row['item_name']) { echo 'selected'; } ?>><?php echo $row['item_name']; ?>
                  <?php } ?>
              </select>
               
                </td>
                <td><input type="text" onkeyup="calc()" name="rate[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['rate']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 rate" required></td>
                <td><input type="text" onkeyup="calc()" name="qty[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['qty']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 qty" required></td>
                <td><input type="text"  onkeyup="calc()" name="unit[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['unit']; }else{ echo ''; } ?>" class="form-control form-control-sm rounded-0 unit" readonly></td>
                <td><input type="text"  onkeyup="calc()" name="amt[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 amt" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="dicountcalc(this)" name="disc_per[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['disc_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 disc_per"  ></td>
                <td><input type="text"  onkeyup="dicountcalc1(this)" name="disc_amt[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['disc_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 disc_amt" ></td>
                <td><input type="text"  onkeyup="calc()" name="after_disc_amt[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['after_disc_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 after_disc_amt" style="text-align:right"  readonly ></td>
                <td><input type="text"  onkeyup="calc()" name="cgst_per[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['cgst_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 cgst_per" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="cgst_amt[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['cgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 cgst_amt" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="sgst_per[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['sgst_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 sgst_per" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="sgst_amt[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['sgst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 sgst_amt" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="igst_per[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['igst_per']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 igst_per" style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="igst_amt[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['igst_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 igst_amt"  style="text-align:right"  readonly></td>
                <td><input type="text"  onkeyup="calc()" name="net_amt[<?php echo $count; ?>]" value="<?php if(isset($_GET['edit'])) { echo $data['net_amt']; }else{ echo '0.0'; } ?>" class="form-control form-control-sm rounded-0 net_amt" style="text-align:right"  readonly></td>
                          <td><a href="javascript:void(0);" onclick="removeitem(this)" class="btn btn-sm btn-danger">-</a></td>
                </tr>

<script>
jQuery(document).ready(function(){
  
	jQuery(".chosen").chosen();
  
});
</script>